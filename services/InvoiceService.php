<?php

namespace App\Services;

use App\Models\Invoice;
use Illuminate\Support\Facades\Request;

class InvoiceService
{
  public function createInvoice(Request $request)
  {
    $data = $request->all();
    $data['status'] = 'pending';
    $invoice = Invoice::create($data);
    return $invoice;
  }
}
