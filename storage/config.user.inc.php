<?php
// Default server configuration

$i = 0;

// Server 1
$i++;
$cfg['Servers'][$i]['verbose'] = 'Server URILLL-NG';
$cfg['Servers'][$i]['host'] = 'db';
$cfg['Servers'][$i]['port'] = '3306';
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['user'] = '';
$cfg['Servers'][$i]['password'] = '';

// Server 2
$i++;
$cfg['Servers'][$i]['verbose'] = 'Server MTEMPUR';
$cfg['Servers'][$i]['host'] = 'another_db_server';
$cfg['Servers'][$i]['port'] = '3306';
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['user'] = '';
$cfg['Servers'][$i]['password'] = '';