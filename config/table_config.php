<?php

return array(
  'pengguna' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Nama',
      'data' => 'name',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'Email',
      'data' => 'email',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Avatar',
      'data' => 'avatar',
      'class' => 'text-center',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Aksi',
      'data' => 'delete',
      'class' => 'text-center',
      'fungsi' => '',
    ),
  ),
  'tipe' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Nama',
      'data' => 'nama',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'Aksi',
      'data' => 'delete',
      'class' => 'text-center',
      'fungsi' => '',
    ),
  ),
  'bahan' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Kode Bahan',
      'data' => 'kode_bahan',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'Tipe',
      'data' => 'tipe.nama',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Nama',
      'data' => 'nama',
      'class' => '',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Aksi',
      'data' => 'delete',
      'class' => 'text-center',
      'fungsi' => '',
    ),
  ),
  'lebar' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
    1 =>
    array(
      'label' => 'Bahan',
      'data' => 'bahan.nama',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'Meter',
      'data' => 'meter',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Harga Lebar',
      'data' => 'harga_lebar',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
  ),
  'menu' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Category Menu',
      'data' => 'categorymenu.nama',
      'class' => 'capitalize',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'Menu',
      'data' => 'nama',
      'class' => 'capitalize',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Slug',
      'data' => 'slug',
      'class' => '',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Namespace',
      'data' => 'namespace',
      'class' => '',
      'fungsi' => '',
    ),
    5 =>
    array(
      'label' => 'Controller',
      'data' => 'controller',
      'class' => '',
      'fungsi' => '',
    ),
    6 =>
    array(
      'label' => 'Route Name',
      'data' => 'route_name',
      'class' => '',
      'fungsi' => '',
    ),
    7 =>
    array(
      'label' => 'Aksi',
      'data' => 'delete',
      'class' => 'text-center',
      'fungsi' => '',
    ),
  ),
  'finishing' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'deskripsi',
      'data' => 'deskripsi',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'harga',
      'data' => 'harga',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
  ),
  'menucategory' =>
  array(
    0 =>
    array(
      'label' => 'id',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'nama',
      'data' => 'nama',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'check',
      'data' => 'check',
      'class' => '',
      'fungsi' => '',
    ),
  ),
  'pesanan' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Customer',
      'data' => 'customer.nama',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'No Pesanan',
      'data' => 'no_pesanan',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Nama Cetakan',
      'data' => 'nama_cetakan',
      'class' => '',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Tipe',
      'data' => 'tipe.nama',
      'class' => '',
      'fungsi' => '',
    ),
    5 =>
    array(
      'label' => 'bahan',
      'data' => 'bahan.nama',
      'class' => '',
      'fungsi' => '',
    ),
    6 =>
    array(
      'label' => 'finishing',
      'data' => 'finishing.deskripsi',
      'class' => '',
      'fungsi' => '',
    ),
    7 =>
    array(
      'label' => 'panjang',
      'data' => 'panjang',
      'class' => '',
      'fungsi' => '',
    ),
    8 =>
    array(
      'label' => 'harga',
      'data' => 'harga',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
  ),
  'jurnal_umum' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Tanggal',
      'data' => 'tanggal',
      'class' => '',
      'fungsi' => 'formatDate',
    ),
    2 =>
    array(
      'label' => 'No Ref',
      'data' => 'no_ref',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Keterangan',
      'data' => 'keterangan',
      'class' => '',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Nominal',
      'data' => 'nominal',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
  ),
  'pesanan_cetakan' =>
  array(
    0 =>
    array(
      'label' => 'ID',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'Tanggal',
      'data' => 'tanggal',
      'class' => '',
      'fungsi' => 'formatDate',
    ),
    2 =>
    array(
      'label' => 'No Pesanan',
      'data' => 'no_pesanan',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Customer',
      'data' => 'customer.nama',
      'class' => '',
      'fungsi' => '',
    ),
    4 =>
    array(
      'label' => 'Nama Cetakan',
      'data' => 'nama_cetakan',
      'class' => '',
      'fungsi' => '',
    ),
    5 =>
    array(
      'label' => 'Jumlah Cetakan',
      'data' => 'jumlah_cetakan',
      'class' => '',
      'fungsi' => '',
    ),
    6 =>
    array(
      'label' => 'Jumlah Drag',
      'data' => 'jumlah_drag',
      'class' => '',
      'fungsi' => '',
    ),
    7 =>
    array(
      'label' => 'Jenis Cetak',
      'data' => 'jenis_cetak',
      'class' => '',
      'fungsi' => '',
    ),
    8 =>
    array(
      'label' => 'Jumlah Plate',
      'data' => 'jumlah_plate',
      'class' => '',
      'fungsi' => '',
    ),
    9 =>
    array(
      'label' => 'Harga Total',
      'data' => 'total_harga',
      'class' => '',
      'fungsi' => 'rupiah',
    ),
  ),
  'customer' =>
  array(
    0 =>
    array(
      'label' => 'id',
      'data' => 'id',
      'class' => '',
      'fungsi' => '',
    ),
    1 =>
    array(
      'label' => 'nama',
      'data' => 'nama',
      'class' => '',
      'fungsi' => '',
    ),
    2 =>
    array(
      'label' => 'notelp/hp',
      'data' => 'no_hp',
      'class' => '',
      'fungsi' => '',
    ),
    3 =>
    array(
      'label' => 'Membership',
      'data' => 'member',
      'class' => '',
      'fungsi' => '',
    ),
  ),
);
