<?php

namespace App\Constants;

class  Constant
{
  const STATUS_PEMBAYARAN_LUNAS = 'Lunas';
  const STATUS_PEMBAYARAN_PARTIAL = 'Partial';
  const STATUS_PEMBAYARAN_BELUM_LUNAS = 'Belum Lunas';

  const STATUS_PESANAN_PENDING = 'Pending';
  const STATUS_PESANAN_PROSES = 'Proses';
  const STATUS_PESANAN_SELESAI = 'Selesai';
  const JENIS_TRANSAKSI = [
    'Jurnal Umum',
    'Modal',
    'Penerimaan Kas',
    'Pengeluaran Kas',
    'Piutang',
    'Hutang'
  ];
}
