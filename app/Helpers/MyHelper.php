<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('formatDate')) {
  function formatDate($date)
  {
    return \Carbon\Carbon::parse($date)->format('d-m-Y');
  }
}


if (!function_exists('logError')) {
  /**
   * This function used to get config datatable.
   *
   * @param Class $e
   * @author Tsani Nashrullah
   **/
  function logError($e)
  {
    Log::error([
      'File' => $e->getFile(),
      'Message' => maskingEncryptDecrypt($e->getMessage()),
      'Line' => $e->getLine(),
    ]);
  }

  function maskingEncryptDecrypt($message)
  {
    $tes = config('app.aes_key');
    $key = [$tes, 'AES_DECRYPT', 'FROM_BASE64', 'TO_BASE64', 'AES_ENCRYPT'];
    foreach ($key as $value) {
      $message = str_replace($value, '', $message);
    }

    return $message;
  }
}

if (!function_exists('rupiah')) {
  function rupiah($angka, $decimal = 0)
  {
    $rupiah = number_format($angka, $decimal, ',', '.');
    return $rupiah;
  }
}

if (!function_exists('logDebug')) {
  function logDebug($message)
  {
    Log::debug($message);
  }
}

if (!function_exists('startDate')) {
  function startDate($format = 'Y-m-d')
  {
    return (new DateTime())->modify('first day of this month')->format($format);
  }
}

if (!function_exists('endDate')) {
  function endDate($format = 'Y-m-d')
  {
    return (new DateTime())->modify('last day of this month')->format($format);
  }
}

if (!function_exists('periode')) {
  function periode($format = 'Y-m-d')
  {
    $start = (new DateTime())->modify('first day of this month')->format($format);
    $end = (new DateTime())->modify('last day of this month')->format($format);
    return $start . ' - ' . $end;
  }
}
