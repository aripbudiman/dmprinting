<?php

namespace App\Helpers;

class Helpers
{
  public static function rupiah($angka, $decimal = 0)
  {
    $rupiah = number_format($angka, $decimal, ',', '.');
    return $rupiah;
  }
}
