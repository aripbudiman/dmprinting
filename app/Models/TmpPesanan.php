<?php

namespace App\Models\dmv;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class TmpPesanan extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 't_tmp_pesanan2';

    public function pesanan()
    {
        return $this->belongsTo('App\Models\dmv\Pesanan', 'no_pesanan', 'no_pesanan');
    }
}
