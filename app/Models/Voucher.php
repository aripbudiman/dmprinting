<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Voucher extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voucher';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['kode_voucher', 'bounty', 'expired_at', 'is_active'];


    public static function cekVoucher($value)
    {
        $voucher = self::where('kode_voucher', $value)->first();

        if ($voucher) {
            if ($voucher->is_active != 1) {
                return [
                    'success' => false,
                    'message' => 'Voucher sudah digunakan',
                ];
            }

            if (Carbon::now()->gt(Carbon::parse($voucher->expired_at))) {
                return [
                    'success' => false,
                    'message' => 'Voucher sudah kadaluarsa'
                ];
            }

            if ($voucher->is_active == 1 && Carbon::now()->lt(Carbon::parse($voucher->expired_at))) {
                return [
                    'success' => true,
                    'message' => 'Voucher digunakan',
                    'data' => $voucher->bounty
                ];
            }
        }

        return [
            'success' => false,
            'message' => 'Voucher tidak ditemukan'
        ];
    }
}
