<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_invoice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tanggal',
        'customer_id',
        'no_invoice',
        'subtotal',
        'disc_percentage',
        'disc',
        'grand_total',
        'sisa',
        'status',
        'no_spk',
        'kode_voucher'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at', 'deleted_at', 'updated_by', 'deleted_by'];

    public function setSubtotalAttribute($value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        $this->attributes['subtotal'] = $value;
    }

    public function setDiscAttribute($value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        $this->attributes['disc'] = $value;
    }

    public function setGrandTotalAttribute($value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        $this->attributes['grand_total'] = $value;
    }

    // Contoh getter (optional)
    public function getGrandTotalAttribute($value)
    {
        return $this->attributes['grand_total'];
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->no_invoice = self::generateNoInvoice();
            $model->no_spk = self::generateNoSpk();
        });
    }

    private static function generateNoInvoice()
    {
        $prefix = 'INV';
        $date = date('ymd');
        $latestInvoice = self::whereDate('created_at', now()->toDateString())
            ->orderBy('created_at', 'desc')
            ->first();
        if (!$latestInvoice) {
            $number = 1;
        } else {
            $latestNumber = (int)substr($latestInvoice->no_invoice, -4);
            $number = $latestNumber + 1;
        }

        return $prefix . $date . str_pad($number, 4, '0', STR_PAD_LEFT);
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id', 'id');
    }

    public function invoicedetail()
    {
        return $this->hasMany(InvoiceDetail::class, 'invoice_id', 'id');
    }

    public function pembayaran()
    {
        return $this->hasMany(Pembayaran::class, 'invoice_id', 'id');
    }

    public static function generateNoSpk()
    {
        $prefix = 'SPK';
        $latestInvoice = self::whereDate('created_at', now()->toDateString())->orderBy('created_at', 'desc')
            ->first();
        if (!$latestInvoice) {
            $number = 1;
        } else {
            $latestNumber = (int)substr($latestInvoice->no_spk, -8);
            $number = $latestNumber + 1;
        }

        return $prefix . str_pad($number, 8, '0', STR_PAD_LEFT);
    }
}
