<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Klasifikasi extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'klasifikasi';

    public function subklasifikasi()
    {
        return $this->hasMany(SubKlasifikasi::class, 'klasifikasi_id', 'id');
    }
}
