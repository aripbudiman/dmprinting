<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jurnal extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jurnal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['transaksi_id', 'tanggal', 'kode_akun', 'debet', 'kredit'];

    public function setDebetAttribute($value)
    {
        $this->attributes['debet'] = str_replace('.', '', $value);
    }

    public function setKreditAttribute($value)
    {
        $this->attributes['kredit'] = str_replace('.', '', $value);
    }

    public function setTanggalAttribute($value)
    {
        $this->attributes['tanggal'] = date('Y-m-d', strtotime($value));
    }

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'transaksi_id', 'id');
    }

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'kode_akun', 'id');
    }
}
