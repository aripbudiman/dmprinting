<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_customers';
    protected $guarded = [];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function pesanan()
    {
        return $this->hasMany(Pesanan::class, 'customer_id', 'id');
    }

    public function pesananNotLunas()
    {
        return $this->hasMany(Pesanan::class)->whereNot('status_pembayaran', 'lunas');
    }

    public static function getCustomer($payload)
    {
        return self::where('nama', 'like', '%' . $payload['keyword'] . '%')->paginate($payload['show']);
    }

    public function getMemberAttribute($value)
    {
        return $value == 1 ? 'Member' : 'Non Member';
    }
}
