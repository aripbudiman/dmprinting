<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Customers;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PesananCetakan extends Model
{
    use HasFactory, SoftDeletes, UpdateOutstanding;

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 't_pesanan_cetakan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tanggal',
        'customer_id',
        'nama_cetakan',
        'jumlah_cetakan',
        'jumlah_drag',
        'jenis_cetak',
        'jumlah_plate',
        'harga_per_plate',
        'harga_per_drag',
        'total_harga_perdrag',
        'total_harga',
        'created_by',
        'updated_by',
        'deleted_by',
        'status_pembayaran',
        'status_pesanan'
    ];

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id', 'id');
    }

    public static function getAllCetakan($payload)
    {
        $query = self::with('customer')
            ->where('nama_cetakan', 'like', '%' . ($payload['keyword'] ?? '') . '%');

        if (isset($payload['start_date']) && isset($payload['end_date'])) {
            $startDate = $payload['start_date'] ?? date('Y-m-01');
            $endDate = $payload['end_date'] ?? date('Y-m-t');
            $query->whereBetween('tanggal', [$startDate, $endDate]);
        }

        $query->where('status_pembayaran', null);

        $query->orderBy('created_at', 'desc');

        $perPage = $payload['show'] ?? 10;
        return $query->paginate($perPage)->withQueryString();
    }

    public function keranjang()
    {
        return $this->morphMany(Keranjang::class, 'pesanan');
    }

    public function setNoPesananAttribute($value)
    {
        if (empty($value)) {
            $today = Carbon::now()->format('Y-m-d');
            $todayStart = Carbon::now()->startOfDay();
            $todayEnd = Carbon::now()->endOfDay();

            $countToday = self::whereBetween('created_at', [$todayStart, $todayEnd])->count() + 1;
            $formattedCount = str_pad($countToday, 3, '0', STR_PAD_LEFT);

            $this->attributes['no_pesanan'] = 'CTK' . Carbon::now()->format('ymd') . $formattedCount;
        } else {
            $this->attributes['no_pesanan'] = $value;
        }
    }

    // Mengatasi pembuatan otomatis no_pesanan jika tidak ada
    public static function booted()
    {
        static::creating(function ($model) {
            if (empty($model->no_pesanan)) {
                $model->no_pesanan = $model->setNoPesananAttribute(null);
            }

            if (Auth::check()) {
                $model->created_by = Auth::user()->name;
                $model->updated_by = Auth::user()->name;
            }
        });

        static::updating(function ($model) {

            if (Auth::check()) {
                $model->updated_by = Auth::user()->name;
            }
        });

        static::deleting(function ($model) {
            if (Auth::check()) {
                $model->deleted_by = Auth::user()->name;
            }
        });

        static::created(function ($model) {
            $model->updateOutstanding($model->customer_id, $model->total_harga);
            $model->updateOutstandingPending($model->customer_id, 1);
        });
    }

    public static function updateCetakan($request, $status = null, $status_pembayaran = null)
    {
        return self::whereIn('no_pesanan', $request)->update([
            'status_pesanan' => $status,
            'status_pembayaran' => $status_pembayaran,
        ]);
    }

    public function spk()
    {
        return $this->morphMany(Spk::class, 'pesanan');
    }
}
