<?php

namespace App\Models;

use App\Models\ModelConnector;
use App\Models\SecondaryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bahan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_bahan';
    protected $guarded = [];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function tipe()
    {
        return $this->belongsTo(Tipe::class, 'tipe_id', 'id')->withTrashed();
    }

    public function lebar()
    {
        return $this->hasMany(Lebar::class, 'bahan_id', 'id');
    }
}
