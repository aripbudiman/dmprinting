<?php

namespace App\Models;

use App\Helpers\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Finishing extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_finishing';

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['deskripsi', 'harga'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function pesanan()
    {
        return $this->hasMany(Pesanan::class, 'finishing_id', 'id');
    }

    public function getFinishing()
    {
        return $this->attributes['deskripsi'] . ' + ' . Helpers::rupiah($this->attributes['harga']);
    }
}
