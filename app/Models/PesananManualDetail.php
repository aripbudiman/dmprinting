<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesananManualDetail extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_pesanan_manual_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pesanan_manual_id',
        'nama_cetakan',
        'qty',
        'harga_satuan',
        'total_harga',
    ];


    public function pesananmanual()
    {
        return $this->belongsTo(PesananManual::class, 'pesanan_manual_id', 'id');
    }
}
