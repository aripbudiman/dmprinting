<?php

namespace App\Models;

use App\Constants\Constant;
use App\Traits\SoftVerifikasi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaksi extends Model
{
    use HasFactory, SoftDeletes, SoftVerifikasi;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaksi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jenis_transaksi',
        'tanggal',
        'no_pesanan',
        'no_ref',
        'keterangan',
        'nominal',
        'voucher',
        'created_by',
        'updated_by',
        'deleted_by',
        'verified_at',
        'verified_by'
    ];

    public function jurnal()
    {
        return $this->hasMany(Jurnal::class, 'transaksi_id', 'id');
    }

    public static function store()
    {
        self::create();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (Auth::check()) {
                $model->created_by = Auth::user()->name;
                $model->updated_by = Auth::user()->name;
            }

            $model->voucher = self::generateVoucherNumber();
        });

        static::updating(function ($model) {
            if (Auth::check()) {
                $model->updated_by = Auth::user()->name;
            }
        });

        static::deleting(function ($model) {
            if (Auth::check()) {
                $model->deleted_by = Auth::user()->name;
            }
        });
    }

    public static function generateVoucherNumber()
    {
        $prefix = 'V';
        $lastVoucher = self::orderBy('id', 'desc')->first();

        if ($lastVoucher) {
            $lastNumber = (int) substr($lastVoucher->voucher, 1);
            $newNumber = str_pad($lastNumber + 1, 8, '0', STR_PAD_LEFT);
        } else {
            $newNumber = str_pad(1, 8, '0', STR_PAD_LEFT);
        }

        return $prefix . $newNumber;
    }

    public static function getAllJurnalUmum($payload)
    {
        $model = Transaksi::with('jurnal', 'jurnal.akun:id,nama')
            ->select('id', 'jenis_transaksi', 'tanggal', 'no_ref', 'keterangan', 'nominal')
            ->where('jenis_transaksi', $payload['jenis'])
            ->when($payload['jenis'] != Constant::JENIS_TRANSAKSI[0], function ($query) use ($payload) {
                $query->whereNotNull('verified_at');
            }, function ($query) use ($payload) {
                $query->whereNull('verified_at');
            })
            ->whereBetween('tanggal', [$payload['start_date'], $payload['end_date']])
            ->when(isset($payload['keyword']), function ($query) use ($payload) {
                $query->where('keterangan', 'like', '%' . $payload['keyword'] . '%');
            })
            ->paginate($payload['show'] ?? 10);
        return $model->withQueryString();
    }

    public function setTanggalAttribute($value)
    {
        $this->attributes['tanggal'] = date('Y-m-d', strtotime($value));
    }

    public function setNominalAttribute($value)
    {
        $this->attributes['nominal'] = str_replace('.', '', $value);
    }

    public static function getPenerimaanKas($payload)
    {
        $kategoriAkun = Akun::where('subklasifikasi_id', 111)->pluck('id')->toArray();

        $model = Transaksi::whereHas('jurnal', function ($query) use ($kategoriAkun) {
            $query->whereIn('kode_akun', $kategoriAkun);
        })
            ->with(['jurnal' => function ($query) use ($kategoriAkun) {
                $query->whereIn('kode_akun', $kategoriAkun)
                    ->with('akun:id,nama');
            }])
            ->select('id', 'jenis_transaksi', 'tanggal', 'no_ref', 'keterangan', 'nominal', 'verified_at', 'voucher')
            ->whereBetween('tanggal', [$payload['start_date'], $payload['end_date']])
            ->when(isset($payload['keyword']), function ($query) use ($payload) {
                $query->where('keterangan', 'like', '%' . $payload['keyword'] . '%');
            });

        $model = $model->paginate($payload['show'] ?? 10)->withQueryString();

        $totalDebet = 0;
        $totalKredit = 0;

        foreach ($model as $transaksi) {
            foreach ($transaksi->jurnal as $jurnal) {
                $totalDebet += $jurnal->debet;
                $totalKredit += $jurnal->kredit;
            }
        }

        $models = self::response($model, $totalDebet, $totalKredit);
        return $models;
    }

    public static function getPiutang($payload)
    {
        $kategoriPiutang = Akun::where('subklasifikasi_id', 112)->pluck('id')->toArray();
        $model = Transaksi::whereHas('jurnal', function ($query) use ($kategoriPiutang) {
            $query->whereIn('kode_akun', $kategoriPiutang)
                ->where('debet', '>', 0);
        })
            ->with(['jurnal' => function ($query) use ($kategoriPiutang) {
                $query->whereIn('kode_akun', $kategoriPiutang)
                    ->where('debet', '>', 0);
            }, 'jurnal.akun:id,nama'])
            ->select('id', 'jenis_transaksi', 'tanggal', 'no_ref', 'keterangan', 'nominal', 'verified_at')
            ->whereBetween('tanggal', [$payload['start_date'], $payload['end_date']])
            ->when(isset($payload['keyword']), function ($query) use ($payload) {
                $query->where('keterangan', 'like', '%' . $payload['keyword'] . '%');
            });

        $model = $model->paginate($payload['show'] ?? 50);
        $totalDebet = 0;
        $totalKredit = 0;

        foreach ($model as $transaksi) {
            foreach ($transaksi->jurnal as $jurnal) {
                $totalDebet += $jurnal->debet;
                $totalKredit += $jurnal->kredit;
            }
        }

        $models = self::response($model, $totalDebet, $totalKredit);
        return $models;
    }

    public static function response($models, $totalDebet, $totalKredit)
    {
        $response = [
            'current_page' => $models->currentPage(),
            'data' => $models->items(),
            'first_page_url' => $models->url(1),
            'from' => $models->firstItem(),
            'last_page' => $models->lastPage(),
            'last_page_url' => $models->url($models->lastPage()),
            'links' => $models->links(),
            'next_page_url' => $models->nextPageUrl(),
            'path' => $models->path(),
            'per_page' => $models->perPage(),
            'prev_page_url' => $models->previousPageUrl(),
            'to' => $models->lastItem(),
            'total' => $models->total(),
            'total_debet' => $totalDebet,
            'total_kredit' => $totalKredit,
        ];
        return (object)$response;
    }

    public static function getPengeluaran($payload)
    {
        $model = Transaksi::with('jurnal', 'jurnal.akun:id,nama')
            ->select('id', 'jenis_transaksi', 'tanggal', 'no_ref', 'keterangan', 'nominal')
            ->where('jenis_transaksi', $payload['jenis'])
            ->whereBetween('tanggal', [$payload['start_date'], $payload['end_date']])
            ->when(isset($payload['keyword']), function ($query) use ($payload) {
                $query->where('keterangan', 'like', '%' . $payload['keyword'] . '%');
            })
            ->paginate($payload['show'] ?? 10);
        return $model->withQueryString();
    }
}
