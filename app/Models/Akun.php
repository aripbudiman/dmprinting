<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Akun extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'akun';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'subklasifikasi_id', 'nama', 'saldo_normal'];

    public function subklasifikasi()
    {
        return $this->belongsTo(SubKlasifikasi::class, 'subklasifikasi_id', 'id');
    }

    public function jurnal()
    {
        return $this->hasMany(Jurnal::class, 'kode_akun', 'id');
    }

    public function metodepembayaran()
    {
        return $this->hasMany(MetodePembayaran::class, 'kode_akun', 'id');
    }
}
