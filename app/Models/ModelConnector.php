<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelConnector extends Model
{
    use HasFactory;

    protected $connection;

    /**
     * Create a new model instance.
     *  @author arip budiman
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = 'old';
    }
}
