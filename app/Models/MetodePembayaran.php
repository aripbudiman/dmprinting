<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetodePembayaran extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'metode_pembayaran';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'icon',
        'value',
        'is_active',
        'deskripsi',
        'kode_akun'
    ];

    public function akun()
    {
        return $this->belongsTo(Akun::class, 'kode_akun', 'id');
    }
}
