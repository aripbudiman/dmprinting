<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolePermissions extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'role_permissions';
    protected $guarded = [];

    public $timestamps = true;

    public function role()
    {
        return $this->belongsTo(Roles::class);
    }

    public function permission()
    {
        return $this->belongsTo(Permissions::class);
    }
}
