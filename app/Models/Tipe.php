<?php

namespace App\Models;

use App\Models\ModelConnector;
use App\Models\SecondaryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tipe extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_tipe';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function bahan()
    {
        return $this->hasMany(Bahan::class, 'tipe_id', 'id');
    }
}
