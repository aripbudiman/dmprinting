<?php

namespace App\Models;

use App\Helpers\Helpers;
use App\Models\ModelConnector;
use App\Models\SecondaryModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Lebar extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_lebar';
    protected $guarded = [];

    public function bahan()
    {
        return $this->belongsTo(Bahan::class, 'bahan_id', 'id');
    }

    public function getMeter()
    {
        return $this->attributes['meter'] . ' Meter + ' . Helpers::rupiah($this->attributes['harga_lebar']);
    }

    public function getLebar()
    {
        return $this->attributes['meter'] . ' Meter';
    }
}
