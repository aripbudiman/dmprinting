<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\Auth;
use App\Traits\HasJournalTransactions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PesananManual extends Model
{
    use HasFactory, UpdateOutstanding, HasJournalTransactions;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_pesanan_manual';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_pesanan',
        'customer_id',
        'tanggal',
        'total_harga',
        'status_pembayaran',
        'status_pesanan',
        'created_by',
        'updated_by',
        'deleted_by',
    ];


    public function pesananmanualdetail()
    {
        return $this->hasMany(PesananManualDetail::class, 'pesanan_manual_id', 'id');
    }

    public function setNoPesananAttribute($value)
    {
        if (empty($value)) {
            $today = Carbon::now()->format('Y-m-d');
            $todayStart = Carbon::now()->startOfDay();
            $todayEnd = Carbon::now()->endOfDay();

            $countToday = self::whereBetween('created_at', [$todayStart, $todayEnd])->count() + 1;
            $formattedCount = str_pad($countToday, 3, '0', STR_PAD_LEFT);

            $this->attributes['no_pesanan'] = 'PM' . Carbon::now()->format('ymd') . $formattedCount;
        } else {
            $this->attributes['no_pesanan'] = $value;
        }
    }

    // Mengatasi pembuatan otomatis no_pesanan jika tidak ada
    public static function booted()
    {
        static::creating(function ($model) {
            if (empty($model->no_pesanan)) {
                $model->no_pesanan = $model->setNoPesananAttribute(null);
            }

            if (Auth::check()) {
                $model->created_by = Auth::user()->name;
                $model->updated_by = Auth::user()->name;
            }
        });

        static::updating(function ($model) {
            if (Auth::check()) {
                $model->updated_by = Auth::user()->name;
            }
        });

        static::deleting(function ($model) {
            if (Auth::check()) {
                $model->deleted_by = Auth::user()->name;
            }
        });

        static::created(function ($model) {
            $model->updateOutstanding($model->customer_id, $model->total_harga);
            $model->updateOutstandingPending($model->customer_id, 1);
            $model->createJurnalPesanan($model);
        });
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id', 'id');
    }

    public function keranjang()
    {
        return $this->morphMany(Keranjang::class, 'pesanan');
    }

    public static function updateManual($request, $status = null, $status_pembayaran = null)
    {
        return self::whereIn('no_pesanan', $request)->update([
            'status_pesanan' => $status,
            'status_pembayaran' => $status_pembayaran,
        ]);
    }

    public function spk()
    {
        return $this->morphMany(Spk::class, 'pesanan');
    }
}
