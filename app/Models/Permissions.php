<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissions extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'permissions';
    protected $guarded = [];

    public function roles()
    {
        return $this->belongsToMany(Roles::class);
    }
}
