<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Spk extends Model
{
    use HasFactory;

    protected $table = 'spk';

    protected $fillable = [
        'customer_id',
        'pesanan_id',
        'pesanan_type',
        'no_spk',
        'status'
    ];

    public function pesanan()
    {
        return $this->morphTo();
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class);
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'no_spk', 'no_spk');
    }
}
