<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pesanan extends Model
{
    use HasFactory, SoftDeletes, UpdateOutstanding;

    protected $table = 't_pesanan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipe_id',
        'bahan_id',
        'lebar_id',
        'finishing_id',
        'panjang',
        'customer_id',
        'qty',
        'harga',
        'nama_cetakan',
        'tanggal',
        'no_pesanan',
        'status',
        'status_pesanan',
        'status_pembayaran'
    ];

    protected $hidden = ['updated_at', 'deleted_at', 'created_at', 'created_by', 'updated_by', 'deleted_by'];

    public function setHargaAttribute($value)
    {
        $cleanedValue = preg_replace('/[^0-9]/', '', $value);
        $this->attributes['harga'] = intval($cleanedValue);
    }

    public function setStatus($value)
    {
        $this->attributes['status'] = 'unpaid';
    }

    public function setNoPesananAttribute($value)
    {
        if (empty($value)) {
            $today = Carbon::now()->format('Y-m-d');
            $todayStart = Carbon::now()->startOfDay();
            $todayEnd = Carbon::now()->endOfDay();

            $countToday = self::whereBetween('created_at', [$todayStart, $todayEnd])->count() + 1;
            $formattedCount = str_pad($countToday, 3, '0', STR_PAD_LEFT);

            $this->attributes['no_pesanan'] = 'DMP' . Carbon::now()->format('ymd') . $formattedCount;
        } else {
            $this->attributes['no_pesanan'] = $value;
        }
    }

    // Mengatasi pembuatan otomatis no_pesanan jika tidak ada
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->no_pesanan)) {
                $model->no_pesanan = $model->setNoPesananAttribute(null);
            }

            if (Auth::check()) {
                $model->created_by = Auth::user()->name;
                $model->updated_by = Auth::user()->name;
            }
        });

        static::updating(function ($model) {
            if (Auth::check()) {
                $model->updated_by = Auth::user()->name;
            }
        });

        static::deleting(function ($model) {
            if (Auth::check()) {
                $model->deleted_by = Auth::user()->name;
            }
        });

        static::created(function ($model) {
            $model->updateOutstanding($model->customer_id, $model->harga);
            $model->updateOutstandingPending($model->customer_id, 1);
        });
    }

    public function getTanggal()
    {
        return Carbon::parse($this->attributes['tanggal'])->format('Y-m-d');
    }

    public static function getAllPesanan($payload)
    {
        $query = Pesanan::with('customer', 'bahan', 'tipe', 'lebar', 'finishing')
            ->where('nama_cetakan', 'like', '%' . ($payload['keyword'] ?? '') . '%');

        $query->where('status_pembayaran', null);

        if (isset($payload['start_date']) && isset($payload['end_date'])) {
            $startDate = $payload['start_date'] ?? date('Y-m-01');
            $endDate = $payload['end_date'] ?? date('Y-m-t');
            $query->whereBetween('created_at', [$startDate, $endDate]);
        }

        if (isset($payload['start_date']) && !isset($payload['end_date'])) {
            $startDate = $payload['start_date'] ?? date('Y-m-01');
            $query->where('created_at', '>=', $startDate);
        }

        if (!isset($payload['start_date']) && isset($payload['end_date'])) {
            $endDate = $payload['end_date'] ?? date('Y-m-t');
            $query->where('created_at', '<=', $endDate);
        }

        $query->orderBy('created_at', 'desc');

        $perPage = $payload['show'] ?? 10;
        return $query->paginate($perPage)->withQueryString();
    }

    public function getPanjangAttribute($value)
    {
        return $value . ' Meter';
    }

    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customer_id', 'id');
    }

    public function tipe()
    {
        return $this->belongsTo(Tipe::class, 'tipe_id', 'id')->withTrashed();
    }

    public function bahan()
    {
        return $this->belongsTo(Bahan::class, 'bahan_id', 'id')->withTrashed();
    }

    public function lebar()
    {
        return $this->belongsTo(Lebar::class, 'lebar_id', 'id');
    }

    public function finishing()
    {
        return $this->belongsTo(Finishing::class, 'finishing_id', 'id');
    }

    public static function updatePesanan($request, $status = null, $status_pembayaran = null)
    {
        return self::whereIn('no_pesanan', $request)->update([
            'status_pesanan' => $status,
            'status_pembayaran' => $status_pembayaran,
        ]);
    }

    public function keranjang()
    {
        return $this->morphMany(Keranjang::class, 'pesanan');
    }

    public function spk()
    {
        return $this->morphMany(Spk::class, 'pesanan');
    }
}
