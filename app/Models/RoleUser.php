<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
    use HasFactory, SoftDeletes;


    public function role()
    {
        return $this->belongsTo(Roles::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected $timeStamp = true;
}
