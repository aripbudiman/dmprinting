<?php

namespace App\Models;

use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pembayaran extends Model
{
    use HasFactory, UpdateOutstanding;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_pembayaran';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_pembayaran',
        'tanggal',
        'invoice_id',
        'metode_pembayaran',
        'total_uang',
        'bayar',
        'kembalian',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
        'kode_akun',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (Auth::check()) {
                $model->created_by = Auth::user()->name;
                $model->updated_by = Auth::user()->name;
            }
            $model->no_pembayaran = 'PAY' . time();
        });

        static::created(function ($model) {
            $bayar = $model->bayar + $model->invoice->disc;
            $model->updateOutstanding($model->invoice->customer_id, $bayar, 'subtract');
        });
    }

    public function setTotalUangAttribute($value)
    {
        $this->attributes['total_uang'] = (float) str_replace('.', '', $value);
    }

    public function setBayarAttribute($value)
    {
        $this->attributes['bayar'] = (float) str_replace('.', '', $value);
    }

    public function setKembalianAttribute($value)
    {
        $this->attributes['kembalian'] = (float) str_replace('.', '', $value);
    }

    public function setTanggal($value)
    {
        $this->attributes['tanggal'] = date('Y-m-d');
    }

    public function setStatus($value)
    {
        $this->attributes['status'] = 'pending';
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id', 'id');
    }
}
