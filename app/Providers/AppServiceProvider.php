<?php

namespace App\Providers;

use App\Models\CPesanan;
use App\Models\MenuCategory;
use App\Models\MetodePembayaran;
use App\Models\MNavbar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        View::composer('layouts.main', function ($view) {
            $navbar = MNavbar::where('is_active', 1)->orderBy('order')->get();
            $view->with('navbar', $navbar);
        });
        
            View::composer('*', function ($view) {
                $metodePembayaran = new MetodePembayaran();
                $view->with('metodePembayaran', $metodePembayaran->where('is_active', 1)->get());
            });
            View::composer('dmv.pesanan.create', function ($view) {
                $hargaplate = CPesanan::select('value')->where('id', 'harga_plate')->first();
                $hargadrag = CPesanan::select('value')->where('id', 'harga_drag')->first();
                $view->with('plate', $hargaplate->value);
                $view->with('drag',  json_decode(trim($hargadrag->value)));
            });
    }
}
