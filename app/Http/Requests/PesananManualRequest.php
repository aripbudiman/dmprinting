<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesananManualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'customer_id' => 'required',
            'tanggal' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'total_harga' => array_map(function ($value) {
                return floatval(str_replace('.', '', $value));
            }, $this->total_harga),
            'grand_total' => floatval(str_replace('.', '', $this->grand_total)),
        ]);
    }

    public function messages()
    {
        return [
            'customer_id.required' => 'Customer wajib diisi.',
            'tanggal.required' => 'Tanggal wajib diisi.',
        ];
    }
}
