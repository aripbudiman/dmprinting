<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class PesananCetakanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'tanggal' => 'required',
            'customer_id' => 'required',
            'nama_cetakan' => 'required',
            'jumlah_cetakan' => 'required',
            'jumlah_drag' => 'required',
            'jenis_cetak' => 'required',
            'jumlah_plate' => 'required',
            'harga_per_plate' => 'required',
            'harga_per_drag' => 'required',
            'total_harga_perdrag' => 'required',
            'total_harga' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'harga_per_plate' => str_replace('.', '', $this->harga_per_plate),
            'harga_per_drag' => str_replace('.', '', $this->harga_per_drag),
            'total_harga_perdrag' => str_replace('.', '', $this->total_harga_perdrag),
            'total_harga' => str_replace('.', '', $this->total_harga),
        ]);
    }

    public function messages()
    {
        return [
            'required' => ':attribute harus diisi',
        ];
    }
}
