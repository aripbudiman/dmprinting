<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PesananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'tipe_id' => 'required|numeric',
            'bahan_id' => 'required|numeric',
            'lebar_id' => 'required|numeric',
            'finishing_id' => 'required|numeric',
            'qty' => 'required|numeric',
            'harga' => 'required',
            'customer_id' => 'required',
            'tanggal' => 'required|date_format:Y-m-d',
            'panjang' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'tipe_id.required' => 'Tipe wajib diisi.',
            'bahan_id.required' => 'Bahan wajib diisi.',
            'lebar_id.required' => 'Lebar wajib diisi.',
            'finishing_id.required' => 'Finishing wajib diisi.',
            'qty.required' => 'Jumlah (Qty) wajib diisi.',
            'qty.numeric' => 'Jumlah (Qty) harus berupa angka.',
            'harga.required' => 'Harga wajib diisi.',
            'customer_id.required' => 'Customer wajib diisi.',
            'tanggal.required' => 'Tanggal wajib diisi.',
            'tanggal.date_format' => 'Tanggal harus dalam format Y-m-d.',
            'panjang.required' => 'Panjang wajib diisi.',
        ];
    }
}
