<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use App\Models\Transaksi;
use App\Constants\Constant;
use Illuminate\Http\Request;

class PiutangController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $payload['jenis'] = Constant::JENIS_TRANSAKSI[4];
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $data = Transaksi::getPiutang($payload);
        $akunDebet = Akun::select('id', 'nama')->where('subklasifikasi_id', 111)->get();
        $akunKredit = Akun::select('id', 'nama')->whereIn('subklasifikasi_id', [212, 411, 421, 412])->get();
        $route = route('penerimaan_kas.store');
        $method = 'store';
        $title = 'Penerimaan Kas';
        return view('piutang.index', compact('data', 'payload', 'akunDebet', 'akunKredit', 'route', 'method', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
