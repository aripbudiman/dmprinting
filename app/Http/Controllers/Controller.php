<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use App\Traits\HasJournalTransactions;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests, HasJournalTransactions;

    public static function getPermissions(): array
    {
        $data = [];
        $routeNames = Route::getRoutes()->getRoutesByName();
        foreach ($routeNames as $route) {
            $action = $route->getActionName();
            $routeName = $route->getName();
            $namespace = Str::beforeLast($action, '@');
            $controller = Str::afterLast($action, '\\');
            $data[] = ['controller' => $controller, 'namespace' => $namespace, 'route_name' => $routeName];
        }
        $collection = collect($data);
        $filtered = $collection->filter(function ($item) {
            return strpos($item['namespace'], 'App\\Http\\Controllers\\') !== false;
        });
        $filteredArray = $filtered->values()->all();
        return $filteredArray;
    }
}
