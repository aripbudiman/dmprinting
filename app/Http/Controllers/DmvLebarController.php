<?php

namespace App\Http\Controllers;

use App\Models\dmv\Lebar;
use Illuminate\Http\Request;

class DmvLebarController extends Controller
{
    protected $namespace='App\Http\Controllers\DmvLebarController';

    public function index(Request  $request){
        $show=$request->input('show', 10);
        $lebar = Lebar::whereHas('bahan', function ($query) {
            $query->whereNull('deleted_at');
        })
        ->with('bahan')
        ->paginate($show)
        ->withQueryString();
        return view('dmv.lebar.index',compact('lebar','show'));
    }

}
