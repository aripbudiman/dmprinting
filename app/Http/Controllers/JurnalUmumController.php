<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use App\Models\Transaksi;
use App\Constants\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JurnalUmumController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $payload['jenis'] = Constant::JENIS_TRANSAKSI[0];
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $data = Transaksi::getAllJurnalUmum($payload);
        return view('jurnalumum.index', compact('data', 'payload'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $url = route('jurnal_umum.store');
        $akun = Akun::select('id', 'nama')->get();
        return view('jurnalumum.create', compact('akun', 'url'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $response = '';
        DB::beginTransaction($response);
        try {
            $this->createOrUpdateJurnal(Constant::JENIS_TRANSAKSI[0], $request->all());
            $response = [
                'status' => 'success',
                'message' => 'Jurnal Umum Berhasil Ditambah',
            ];
            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(Transaksi $jurnalUmum)
    {
        $jurnalUmum->load('jurnal', 'jurnal.akun:id,nama');
        $akun = Akun::select('id', 'nama')->get();
        return view('jurnalumum.show', compact('jurnalUmum', 'akun'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Transaksi $jurnalUmum)
    {
        $url = route('jurnal_umum.update', $jurnalUmum->id);
        $jurnalUmum->load('jurnal', 'jurnal.akun:id,nama');
        $akun = Akun::select('id', 'nama')->get();
        return view('jurnalumum.edit', compact('jurnalUmum', 'akun', 'url'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Transaksi $jurnalUmum)
    {
        $response = '';
        DB::beginTransaction($response);
        try {
            $this->createOrUpdateJurnal(Constant::JENIS_TRANSAKSI[0], $request->merge(['id' => $jurnalUmum->id])->all());
            $response = [
                'status' => 'success',
                'message' => 'Jurnal Umum Berhasil Ditambah',
            ];
            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Transaksi $jurnalUmum)
    {
        //
    }
}
