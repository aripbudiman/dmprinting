<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Voucher;

class VoucherController extends Controller
{

    public static function applyVoucher($kode, Voucher $voucher)
    {
        return $voucher::cekVoucher($kode);
    }
}
