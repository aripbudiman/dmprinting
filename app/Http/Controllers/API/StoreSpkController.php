<?php

namespace App\Http\Controllers\API;

use App\Models\Spk;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\InvoiceDetail;
use App\Http\Controllers\Controller;

class StoreSpkController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $listNoPesanan = $request->data;
        $data = Spk::whereIn('id', $listNoPesanan)->with('pesanan')->get();
        $subtotal = 0;
        $invoiceDetail = [];

        foreach ($data as $key => $value) {
            if ($value->pesanan_type == 'App\Models\Pesanan') {
                $subtotal += $value->pesanan->harga;
            } else {
                $subtotal += $value->pesanan->total_harga;
            }

            $invoiceDetail[] = [
                'pesanan_id' => $value->pesanan->id,
                'pesanan_type' => $value->pesanan_type
            ];

            $value->pesanan->update([
                'status_pembayaran' => 'pending',
                'status_pesanan' => 'pending'
            ]);
        }

        $invoice = Invoice::create([
            'tanggal' => date('Y-m-d'),
            'customer_id' => $request->customer_id,
            'subtotal' => $subtotal,
            'grand_total' => $subtotal,
            'sisa' => $subtotal,
            'status' => 'pending',
        ]);

        foreach ($invoiceDetail as &$detail) {
            $detail['invoice_id'] = $invoice->id;
        }

        InvoiceDetail::insert($invoiceDetail);
        foreach ($data as $key => $value) {
            $value->update([
                'no_spk' => $invoice->no_spk,
                'status' => 1
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Invoice created successfully',
            'spk' => $invoice->no_spk,
            'status' => $invoice->status
        ]);
    }
}
