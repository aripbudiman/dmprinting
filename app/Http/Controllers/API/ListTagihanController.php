<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Spk;
use Illuminate\Http\Request;

class ListTagihanController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $data = Invoice::select('id', 'customer_id', 'no_invoice', 'subtotal', 'sisa', 'status', 'no_spk')
            ->with('invoicedetail:id,invoice_id,pesanan_id,pesanan_type', 'customer:id,nama')
            ->withCount(['invoicedetail as totalpesanan'])
            ->where('status', 'pending')
            ->orWhere('status', 'partial')
            ->paginate(10);
        return response()->json($data);
    }
}
