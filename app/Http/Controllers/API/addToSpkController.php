<?php

namespace App\Http\Controllers\API;

use App\Constants\Constant;
use App\Models\Spk;
use App\Models\Pesanan;
use Illuminate\Http\Request;
use App\Models\PesananManual;
use App\Models\PesananCetakan;
use App\Http\Controllers\Controller;

class addToSpkController extends Controller
{
    public function store(Request $request)
    {

        switch ($request->jenis) {
            case 'digital':
                $pesanan = Pesanan::where('no_pesanan', $request->no_pesanan)->first(['no_pesanan', 'customer_id', 'id']);
                break;
            case 'ongkos_cetak':
                $pesanan = PesananCetakan::where('no_pesanan', $request->no_pesanan)->first(['no_pesanan', 'customer_id', 'id']);
                break;
            case 'cetakan':
                $pesanan = PesananManual::where('no_pesanan', $request->no_pesanan)->first(['no_pesanan', 'customer_id', 'id']);
                break;
            default:
                break;
        }

        if ($pesanan) {
            $result = Spk::where('pesanan_id', $pesanan->id)
                ->where('customer_id', $request->customer_id)
                ->where('pesanan_type', get_class($pesanan))
                ->exists();

            if (!$result) {
                Spk::create([
                    'pesanan_id' => $pesanan->id,
                    'customer_id' => $request->customer_id,
                    'pesanan_type' => get_class($pesanan),
                    'status' => 0,
                ]);
            }

            $pesanan->status_pesanan = Constant::STATUS_PESANAN_PENDING;
            $pesanan->status_pembayaran = Constant::STATUS_PESANAN_PENDING;
            $pesanan->save();
            return response()->json([
                'success' => true,
                'message' => 'success',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => $request->all,
            ], 404);
        }
    }
}
