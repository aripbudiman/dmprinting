<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use Illuminate\Http\Request;

class ApplyVoucherController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $voucher = new Voucher();
        $data = $voucher::cekVoucher($request->kode_voucher);
        return response()->json($data);
    }
}
