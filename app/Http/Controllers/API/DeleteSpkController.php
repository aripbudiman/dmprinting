<?php

namespace App\Http\Controllers\API;

use App\Models\Spk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeleteSpkController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke($id)
    {
        $spk = Spk::find($id);
        $spk->pesanan->status_pesanan = null;
        $spk->pesanan->status_pembayaran = null;
        $spk->pesanan->save();
        $spk->delete();
        return response()->json([
            'message' => 'success',
            'data' => $spk
        ]);
    }
}
