<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PiutangController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $data = DB::table('m_customers as customer')
            ->select(
                'customer.id',
                'customer.nama',
                DB::raw('COUNT(CASE WHEN pesanan.status_pembayaran IS NULL OR pesanan.status_pembayaran != "lunas" THEN 1 END) as pesanan_belum_lunas'),
                DB::raw('SUM(CASE WHEN pesanan.status_pembayaran IS NULL OR pesanan.status_pembayaran != "lunas" THEN pesanan.harga ELSE 0 END) as piutang_pesanan')
            )
            ->leftJoin('t_pesanan as pesanan', 'customer.id', '=', 'pesanan.customer_id')
            ->groupBy('customer.id', 'customer.nama')
            ->get();

        return $data;
    }
}
