<?php

namespace App\Http\Controllers\API;

use CURLFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SendWAController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        // info($request->all());
        // return false;
        // $tanggal = '21-09-2024';
        // $no_invoice = 'INV2409210002';
        // $no_spk = 'SPK2409210002';
        // $customer = 'ASEP';
        // $total_tagihan = '1.250.000';
        // $status_pembayaran = 'Partial';

        $message = "
        DMPRINTING
        INVOICE ELEKTRONIK
        Tanggal: 
        $request->tanggal
        No Invoice: 
        $request->no_invoice
        No SPK: 
        $request->no_spk
        Customer: 
        $request->customer
        Total Tagihan: 
        $request->sisa
        Status Pembayaran: 
        $request->status
        Link Invoice:
        $request->url_tagihan
        SEGERA MELAKUKAN PEMBAYARAN
        ";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.fonnte.com/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('target' => $request->nohp, 'message' => $message),
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . env('TOKEN_FONNTE')
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return response()->json($response);
    }
}
