<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;

class ScreenshootController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $url = 'https://spatie.be/docs/browsershot/v4/usage/introduction';

        // Simpan screenshot ke dalam folder public
        Browsershot::url($url)
            ->setScreenshotType('jpeg', 100) // Mengatur tipe file dan kualitas (opsional)
            ->save(public_path('screenshots/example.jpg'));

        return response()->json(['message' => 'Screenshot berhasil diambil!']);
    }
}
