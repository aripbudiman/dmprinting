<?php

namespace App\Http\Controllers\API;

use App\Models\Invoice;
use App\Models\Pembayaran;
use App\Constants\Constant;
use Illuminate\Http\Request;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use PhpParser\Node\Stmt\Const_;

class PaynowController extends Controller
{
    /**
     * Handle the incoming request.
     */
    use UpdateOutstanding;
    public function __invoke(Request $request, Invoice $invoice)
    {
        try {
            DB::beginTransaction();
            $bayar = Pembayaran::create([
                'tanggal' => date('Y-m-d'),
                'invoice_id' => $request->invoice_id,
                'metode_pembayaran' => $request->metode_pembayaran,
                'total_uang' => $request->total_uang,
                'bayar' => $request->bayar,
                'kembalian' => $request->kembalian,
                'kode_akun' => $request->kode_akun,
            ]);
            $invoice = $invoice->find($request->invoice_id);
            $invoice->load('invoicedetail', 'invoicedetail.pesanan');
            $disc = $request->status == "partial" ? 0 : $request->disc;
            $sisa = $request->status == "partial" ? $request->sisa : $request->grand_total;
            $invoice->update([
                'disc_percentage' => $request->disc_percentage,
                'disc' => $request->disc,
                'grand_total' => $request->grand_total,
                'sisa' => ($sisa - $request->bayar - $disc),
            ]);
            if ($invoice->sisa == 0) {
                $this->jurnalBayar($bayar);
                $invoice->update([
                    'status' => Constant::STATUS_PEMBAYARAN_LUNAS
                ]);
                $invoice->invoicedetail->each(function ($detail) {
                    $detail->pesanan->update([
                        'status_pesanan' => Constant::STATUS_PESANAN_SELESAI,
                        'status_pembayaran' => Constant::STATUS_PEMBAYARAN_LUNAS
                    ]);
                });
                $bayar->updateOutstandingPending($invoice->customer_id, count($invoice->invoicedetail), 'subtract');
            } elseif ($invoice->sisa > 0 && $invoice->sisa < $invoice->grand_total) {
                $this->jurnalBayar($bayar);
                $invoice->update([
                    'status' => Constant::STATUS_PEMBAYARAN_PARTIAL
                ]);
                $invoice->invoicedetail->each(function ($detail) {
                    info($detail);
                    $detail->pesanan->update([
                        'status_pesanan' => Constant::STATUS_PESANAN_PENDING,
                        'status_pembayaran' => Constant::STATUS_PEMBAYARAN_PARTIAL
                    ]);
                });
                $bayar->updateOutstandingPending($invoice->customer_id, count($invoice->invoicedetail), 'subtract');
                $bayar->updateOutstandingPartial($invoice->customer_id, count($invoice->invoicedetail));
            }
            $data = $bayar->load('invoice:id,customer_id,subtotal,disc,sisa', 'invoice.invoicedetail', 'invoice.invoicedetail.pesanan', 'invoice.customer:id,nama');
            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' => 'Pembayaran Berhasil',
                'data' => $data,
                'url' => url('/dmv/pembayaran/struk/' . $bayar->id)
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage(),
            ]);
        }
    }
}
