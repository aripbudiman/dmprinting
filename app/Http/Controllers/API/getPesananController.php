<?php

namespace App\Http\Controllers\API;

use App\Models\Pesanan;
use Illuminate\Http\Request;
use App\Models\PesananManual;
use App\Models\PesananCetakan;
use App\Http\Controllers\Controller;

class getPesananController extends Controller
{
    public function digitalPrinting(Request $request)
    {
        if ($request->customer_id == null) {
            return response()->json(['message' => 'Silahkan pilih customer terlebih dahulu'], 404);
        }
        $digitalPrinting = Pesanan::select('tanggal', 'no_pesanan', 'nama_cetakan', 'qty', 'harga', 'customer_id')
            ->where('customer_id', $request->input('customer_id', ''))
            ->whereNull('status_pesanan')
            ->with('customer')
            ->get();
        $PesananCetakan = PesananCetakan::select('tanggal', 'no_pesanan', 'nama_cetakan', 'total_harga', 'customer_id')
            ->where('customer_id', $request->input('customer_id', ''))
            ->whereNull('status_pesanan')
            ->with('customer')
            ->get();
        $PesananManual = PesananManual::select('tanggal', 'no_pesanan', 'total_harga', 'customer_id')
            ->where('customer_id', $request->input('customer_id', ''))
            ->whereNull('status_pesanan')
            ->with('customer')
            ->get();

        if ($digitalPrinting->count() > 0 || $PesananCetakan->count() > 0 || $PesananManual->count() > 0) {
            return response()->json([
                'digitalPrinting' => $digitalPrinting,
                'PesananCetakan' => $PesananCetakan,
                'PesananManual' => $PesananManual
            ]);
        }
        return response()->json(['message' => 'Sudah tidak ada Pesanan'], 404);
    }
}
