<?php

namespace App\Http\Controllers\API;

use App\Models\Spk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoadSpkController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke($customer_id, $status, Request $request)
    {
        $spk = Spk::where('customer_id', $customer_id)
            ->with('pesanan')
            ->where('status', $status ?? 0)
            ->get();

        return response()->json($spk);
    }
}
