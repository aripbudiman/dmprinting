<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Finishing;

class DmvFinishingController extends Controller
{
    public function index(Request $request)
    {
        $show = $request->input('show', 10);
        $data = Finishing::paginate($show);
        return view('dmv.finishing.index', compact('data', 'show'));
    }

    public function create()
    {
        $url = route('dmv.finishing.store');
        return view('dmv.finishing.create', compact('url'));
    }

    public function store(Request $request)
    {
        Finishing::create($request->all());
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Ditambah';
        return $response;
    }
}
