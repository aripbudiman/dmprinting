<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Roles;
use App\Models\Permissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class RolePermissionController extends Controller
{
    public function index(Request $request)
    {
        $roles = Roles::all();
        return view('rolepermission.roles', compact('roles'));
    }

    public function settingrolepermission(Request $request, $id)
    {
        $roles = Roles::findOrFail($id);
        $permissions = Permissions::all();
        return view('rolepermission.settingrolepermission', compact('roles', 'permissions'));
    }

    public function store(Request $request, $id)
    {
        $role = Roles::find($id);
        $role->permissions()->sync($request->permission_id);
        return response('ok', 200);
    }

    public function listpengguna()
    {
        $users = User::all();
        return view('rolepermission.list-pengguna', compact('users'));
    }

    public function create()
    {
        return view('rolepermission.create');
    }

    public function storeRoles(Request $request)
    {
        Roles::create($request->all());
        return response(['success' => true, 'message' => 'Data Berhasil Ditambah'], 200);
    }

    public function delete(Roles $roles)
    {
        $roles->delete();
        return response(['success' => true, 'message' => 'Data Berhasil Dihapus'], 200);
    }
}
