<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $data = Customers::getCustomer($payload);
        $title = 'Customer';
        return view('customer.index', compact('data', 'title', 'payload'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Customers $customer)
    {
        try {
            DB::beginTransaction();
            $customer->create($request->all());
            $response = [
                'status' => 'success',
                'message' => 'Customer Berhasil Ditambah',
            ];
            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Customers $customer)
    {
        return $customer;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Customers $customer)
    {
        try {
            DB::beginTransaction();
            $customer->update($request->all());
            $response = [
                'status' => 'success',
                'message' => 'Customer Berhasil Diupdate',
            ];
            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage(),
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
