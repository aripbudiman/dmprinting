<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $valueSync = $request->input('valueSync');
            $route = Route::getRoutes()->getByName($valueSync)->getAction()['controller'];
            $routeController = class_basename($route);
            self::getControllerMenu($valueSync, $routeController);
            return response()->json('Berhasil', 200);
        }
        $show = $request->input('show', 10);
        $menus = Menu::with('categorymenu')
            ->paginate($show)
            ->withQueryString();
        return view('menu.index', compact('menus', 'show'));
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            try {
                $routeName = $request->input('route_name');
                $route = Route::getRoutes()->getByName($routeName);
                if (!$route) {
                    return response()->json(['message' => 'Route not found'], 404);
                }
                $controller = $route->getAction()['controller'];
                $routeController = class_basename($controller);
                $namespace = Str::beforeLast($controller, '@');
                return response()->json([$routeController, $namespace], 200);
            } catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        $categories = MenuCategory::all();
        return view('menu.create', compact('categories'));
    }

    public static function getControllerMenu($valueSync, $controller)
    {
        $menu = Menu::where('route_name', $valueSync)->first();
        $menu->controller = $controller;
        $menu->save();
        return $menu;
    }

    public function listMenuCategory(Request $request)
    {
        $route = self::getPermissions();
        $show = $request->input('show', 10);
        $data = MenuCategory::paginate($show);
        return view('menu.list-menu-category', compact('data', 'show'));
    }

    public function show(Menu $menu)
    {
        $menu->load('categorymenu');
        return view('menu.show', compact('menu'));
    }

    public function edit(Menu $menu, Request $request)
    {
        if ($request->ajax()) {
            try {
                $routeName = $request->input('route_name');
                $route = Route::getRoutes()->getByName($routeName);
                if (!$route) {
                    return response()->json(['message' => 'Route not found'], 404);
                }
                $controller = $route->getAction()['controller'];
                $routeController = class_basename($controller);
                $namespace = Str::beforeLast($controller, '@');
                return response()->json([$routeController, $namespace], 200);
            } catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        $categories = MenuCategory::all();
        $menu->load('categorymenu');
        return view('menu.edit', compact('menu', 'categories'));
    }

    public function update(Request $request, Menu $menu)
    {
        $menu->update($request->all());
        $menu->categorymenu()->associate($request->input('category_id'));
        $menu->save();
        return response()->json('Data Berhasil Diperbaharui', 200);
    }

    public function store(Request $request)
    {
        $menu = Menu::create($request->all());
        $menu->categorymenu()->associate($request->input('category_id'));
        $menu->save();
        return response()->json('Data Berhasil Ditambah', 200);
    }

    public function createCategoryMenu()
    {
        return view('menu.create-menu-category');
    }

    public function editCategoryMenu(MenuCategory $menucategory)
    {
        return view('menu.edit-menu-category', compact('menucategory'));
    }

    public function storeCategoryMenu(Request $request)
    {
        MenuCategory::create($request->all());
        return response()->json('Data Berhasil Ditambah', 200);
    }

    public function destroyCategoryMenu(MenuCategory $menucategory)
    {
        DB::beginTransaction();
        try {
            $menucategory->delete();
            Log::info('Data MenuCategory Dihapus', $menucategory->toArray());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }

    public function destroy(Menu $menu)
    {
        DB::beginTransaction();
        try {
            $menu->delete();
            Log::info('Data Menu Dihapus', $menu->toArray());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }
}
