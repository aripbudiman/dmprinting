<?php

namespace App\Http\Controllers;

use App\Models\CPesanan;
use Illuminate\Http\Request;

class CPesananController extends Controller
{
    public function index()
    {
        $drag = CPesanan::getDrag();
        $plate = CPesanan::where('id', 'harga_plate')->get()[0];
        return view('cpesanan.index', compact('drag', 'plate'));
    }

    public function drag(Request $request)
    {
        $cpesanan = CPesanan::find($request->id);
        $cpesanan->value = $request->value;
        $cpesanan->save();

        return response()->json([
            'success' => true,
            'message' => 'success',
        ]);
    }

    public function plate(Request $request)
    {
        $cpesanan = CPesanan::find($request->id);
        $cpesanan->value = $request->harga;
        $cpesanan->save();
        return response()->json([
            'success' => true,
            'message' => 'success'
        ]);
    }
}
