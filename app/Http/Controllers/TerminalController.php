<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TerminalController extends Controller
{
    public function index()
    {
        return view('terminal.index');
    }

    public function configCache()
    {
        Artisan::call('config:cache');
        return 'Config cached successfully!';
    }

    public function routeCache()
    {
        Artisan::call('route:cache');
        return 'Route cached successfully!';
    }

    public function routeList()
    {
        Artisan::call('route:list');
        return Artisan::output();
    }

    public function routeClear()
    {
        Artisan::call('route:clear');
        return 'Route cleared successfully!';
    }

    public function clearCache()
    {
        Artisan::call('cache:clear');
        return 'Cache cleared successfully!';
    }

    public function clearView()
    {
        Artisan::call('view:clear');
        return 'View cleared successfully!';
    }

    public function clearConfig()
    {
        Artisan::call('config:clear');
        return 'Config cleared successfully!';
    }
}
