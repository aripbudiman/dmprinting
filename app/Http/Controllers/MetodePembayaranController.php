<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use Illuminate\Http\Request;
use App\Models\MetodePembayaran;

class MetodePembayaranController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() && $request->toggle) {
            MetodePembayaran::findOrFail($request->id)->update($request->all());
            return response([
                'message' => 'Data Berhasil Diperbaharui',
                'status' => 'success',
            ]);
        }
        if ($request->ajax()  && $request->mapping) {
            MetodePembayaran::findOrFail($request->id)->update($request->all());
            return response([
                'message' => 'Data Berhasil Diperbaharui',
                'status' => 'success',
            ]);
        }
        if ($request->ajax() && $request->autocomplete) {
            $query = $request->input('search');
            $results = Akun::select('id', 'nama')->where('nama', 'like', '%' . $query . '%')->where('subklasifikasi_id', 111)->limit(10)->get();
            $response = array();
            foreach ($results as $value) {
                $response[] = [
                    'value' => $value->id,
                    'label' => strtoupper($value->nama),
                ];
            }
            return response()->json(['data' => $response]);
        }

        $data = MetodePembayaran::with('akun')->get();
        return view('metodepembayaran.index', compact('data'));
    }
}
