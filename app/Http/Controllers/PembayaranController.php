<?php

namespace App\Http\Controllers;

use stdClass;
use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\Voucher;
use App\Helpers\Helpers;
use App\Models\Keranjang;
use App\Models\Pembayaran;
use App\Constants\Constant;
use App\Models\Pesanan;
use App\Models\Outstanding;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Customers;
use App\Models\InvoiceDetail;
use App\Traits\TableRenderer;
use App\Models\PesananCetakan;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\InvoiceRequest;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\VoucherController;
use App\Models\PesananManual;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PembayaranController extends Controller
{
    use UpdateOutstanding, TableRenderer;

    public function index(Request  $request)
    {
        if ($request->ajax()) {
            $customer = $request->input('customer', '');
            $data = $this->getCustomers($customer);
            return response($data);
        }
        return view('dmv.pembayaran.index');
    }

    public function pembayaran(Request $request, Customers $customer)
    {
        if ($request->ajax() && $request->itempesanan) {
            $itempesanan = $request->input('itempesanan', '');
            $jenis = $request->input('jenis', '');
            $response['itempesanan'] = self::addToCart($itempesanan, $jenis) ?? '';
            return response($response);
        }
        if ($request->ajax() && $request->voucher) {
            return VoucherController::applyVoucher($request->voucher, new Voucher());
        }
        if ($request->ajax()) {
            $keranjang = $request->input('keranjang', '');
            $response['pesanan'] = $this->getPesananByCustomer($customer->id);
            $response['cetakan'] = $this->getPesananCetakanByCustomer($customer->id);
            $response['manual'] = $this->getPesananManualByCustomer($customer->id);
            $response['keranjang'] = $this->getKeranjang()->keranjang;
            $response['subtotal'] = $this->getKeranjang()->subtotal;
            $response['customer_id'] = $customer->id;
            $response['customer'] = $customer->nama;
            $response['no_pesanan'] = $this->getKeranjang()->no_pesanan;
            $invoice = Invoice::with('customer', 'invoicedetail:id,invoice_id,pesanan_id,pesanan_type', 'invoicedetail.pesanan', 'pembayaran:id,invoice_id,no_pembayaran,bayar')->where('customer_id', $customer->id)->where('status', Constant::STATUS_PEMBAYARAN_PARTIAL)->get();
            $response['table_invoice'] = $this->renderInvoiceTable($invoice);
            return response($response);
        }
        return view('dmv.pembayaran.pembayaran');
    }

    public function bayar(InvoiceRequest $request, Invoice $invoices, Pembayaran $bayar)
    {
        DB::beginTransaction();
        try {
            $invoiceData = $request->except('no_pesanan');
            $invoice = $invoices->fill($invoiceData);
            $invoice->save();

            $payload = [];
            $no_pesanan_list = isset($request['no_pesanan']) ? explode(',', $request['no_pesanan']) : [];

            foreach ($no_pesanan_list as $value) {
                $pesanan = Pesanan::where('no_pesanan', $value)->first();

                if (!$pesanan) {
                    $pesanan = PesananCetakan::where('no_pesanan', $value)->first();
                }

                if (!$pesanan) {
                    $pesanan = PesananManual::where('no_pesanan', $value)->first();
                }

                if ($pesanan) {
                    $payload[] = [
                        'invoice_id' => $invoice->id,
                        'pesanan_id' => $pesanan->id,
                        'pesanan_type' => get_class($pesanan),
                    ];
                }
            }
            InvoiceDetail::insert($payload);

            $pembayaran = $bayar->fill($request->all());
            $pembayaran->invoice_id = $invoice->id;
            $pembayaran->save();
            $invoice->sisa = $invoice->grand_total - $pembayaran->bayar;
            $status_pembayaran = '';
            $status_pesanan = '';
            $count = count(array_column($payload, 'pesanan_id'));
            if ($invoice->sisa == 0) {
                $this->jurnalBayar($pembayaran);
                $invoice->status = Constant::STATUS_PEMBAYARAN_LUNAS;
                $status_pesanan = Constant::STATUS_PESANAN_SELESAI;
                $status_pembayaran = Constant::STATUS_PEMBAYARAN_LUNAS;
                $pembayaran->updateOutstandingPending($invoice->customer_id, $count, 'subtract');
            } elseif ($invoice->sisa > 0 && $invoice->sisa < $invoice->grand_total) {
                $this->jurnalBayar($pembayaran);
                $invoice->status = Constant::STATUS_PEMBAYARAN_PARTIAL;
                $status_pesanan = Constant::STATUS_PESANAN_PENDING;
                $status_pembayaran = Constant::STATUS_PEMBAYARAN_PARTIAL;
                $pembayaran->updateOutstandingPending($invoice->customer_id, $count, 'subtract');
                $pembayaran->updateOutstandingPartial($invoice->customer_id, $count);
            }

            $invoice->save();

            if (!empty($status_pesanan) && !empty($status_pembayaran)) {
                foreach ($payload as $value) {
                    $pesanan = (new $value['pesanan_type'])->find($value['pesanan_id']);
                    $pesanan->update([
                        'status_pesanan' => $status_pesanan,
                        'status_pembayaran' => $status_pembayaran
                    ]);
                }
            }
            $data = $this->responsePembayaran(array_merge($pembayaran->toArray(), $invoice->toArray()));
            $response = [
                'status' => 'success',
                'message' => 'Pembayaran berhasil dilakukan',
                'data' => $data,
                'pembayaran_id' => url('/dmv/pembayaran/struk/' . $pembayaran->id),
                'spk' => url('/dmv/spk/' . $pembayaran->id),
            ];
            DB::commit();
            $this->clearKeranjang(['status_pesanan' => $status_pesanan, 'status_pembayaran' => $status_pembayaran]);
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            $response = [
                'status' => 'error',
                'message' => 'Pembayaran gagal dilakukan'
            ];
        }
        return response($response);
    }

    public function bayarAngsuranInvoice(Request $request, Invoice $invoice, Pembayaran $pembayaran)
    {
        $response = [];
        DB::beginTransaction();
        try {
            $bayar = $pembayaran->fill($request->all());
            $bayar->invoice->sisa = $bayar->invoice->sisa - $bayar->bayar;
            if ($bayar->invoice->sisa == 0) {
                $bayar->invoice->status = Constant::STATUS_PEMBAYARAN_LUNAS;
                $nopesanan = $bayar->invoice->invoicedetail->pluck('pesanan_id')->toArray();
                $bayar->updateOutstandingPartial($bayar->invoice->customer_id, count($nopesanan), 'subtract');
                $listpesanan = $bayar->invoice->invoicedetail->toArray();
                foreach ($listpesanan as $value) {
                    $pesanan = (new $value['pesanan_type'])->find($value['pesanan_id']);
                    $pesanan->update([
                        'status_pesanan' => Constant::STATUS_PESANAN_SELESAI,
                        'status_pembayaran' => Constant::STATUS_PEMBAYARAN_LUNAS
                    ]);
                }
            }

            $bayar->invoice->save();
            $bayar->save();
            $this->jurnalPelunasan($bayar);
            $response = [
                'status' => 'success',
                'message' => 'Pembayaran berhasil dilakukan',
                'data' => $this->responsePembayaran(array_merge($bayar->toArray(), $bayar->invoice->toArray())),
                'pembayaran_id' => url('/dmv/pembayaran/struk/' . $bayar->id)
            ];
            DB::commit();
        } catch (\Exception $error) {
            DB::rollBack();
            Log::error($error->getMessage());
            $response = [
                'status' => 'error',
                'message' => 'Pembayaran gagal dilakukan'
            ];
        }
        return response($response);
    }

    public function responsePembayaran($data)
    {
        return [
            'total_bayar' => Helpers::rupiah($data['bayar']),
            'tanggal' => Carbon::parse($data['tanggal'])->translatedFormat('d F Y'),
            'metode_pembayaran' => $data['metode_pembayaran'],
            'total_tagihan' => Helpers::rupiah($data['grand_total']),
            'sisa' => Helpers::rupiah($data['sisa']),
            'diskon' => Helpers::rupiah($data['disc']),
            'no_invoice' => $data['no_invoice'],
        ];
    }

    public function getKeranjang()
    {
        $keranjang = Keranjang::with('pesanan', 'customer')->orderBy('pesanan_type')->get();
        $data = new stdClass();
        $data->keranjang = self::loadKeranjang($keranjang);
        $data->subtotal = $keranjang->sum(function ($item) {
            if ($item->pesanan_type == 'App\Models\Pesanan') {
                return $item->pesanan->harga ?? 0;
            } else {
                return $item->pesanan->total_harga ?? 0;
            }
        });
        $data->customer_id = $keranjang->first()->customer_id ?? '';
        $data->customer = $keranjang->first()->customer->nama ?? '';
        $data->no_pesanan = $keranjang->map(fn($item) => $item->pesanan->no_pesanan)->toArray();
        return $data;
    }

    public static function loadKeranjang($keranjang)
    {
        // dd($keranjang->pesanan->pesananmanualdetail);
        $html = "";
        $html2 = "";
        $html3 = "";
        foreach ($keranjang as $key => $value) {
            if ($value->pesanan_type == 'App\Models\Pesanan') {
                $html .= '<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <td class="px-3 py-2">
                ' . \Carbon\Carbon::parse($value->pesanan->tanggal)->translatedFormat('d-m-Y') . '
                </td>
                <td class="px-3 py-2">
                ' . $value->pesanan->no_pesanan . '
                </td>
                <td class="px-3 py-2">
                    ' . Str::limit($value->pesanan->nama_cetakan, 50, '...') . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->tipe->nama . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->bahan->nama . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->lebar->getLebar() . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->finishing->deskripsi . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->panjang . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->qty . '
                </td>
                <td class="px-3 py-2">
                    ' . Helpers::rupiah($value->pesanan->harga) . '
                </td>
                <td class="px-3 py-2">
                    <a onclick="deleteKeranjang(' . $value->id . ')"
                        class="font-medium text-red-600 dark:text-red-500 hover:underline cursor-pointer">Hapus</a>
                </td>
            </tr>';
            } elseif ($value->pesanan_type == 'App\Models\PesananCetakan') {
                $html2 .= '<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <td class="px-3 py-2">
                ' . \Carbon\Carbon::parse($value->pesanan->tanggal)->translatedFormat('d-m-Y') . '
                </td>
                <td class="px-3 py-2">
                ' . $value->pesanan->no_pesanan . '
                </td>
                <td class="px-3 py-2" colspan="3">
                    ' . Str::limit($value->pesanan->nama_cetakan, 50, '...') . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->jumlah_cetakan . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->jumlah_drag . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->jenis_cetak . '
                </td>
                <td class="px-3 py-2">
                    ' . $value->pesanan->jumlah_plate . '
                </td>
                <td class="px-3 py-2">
                    ' . rupiah($value->pesanan->total_harga) . '
                </td>
                <td class="px-3 py-2">
                    <a onclick="deleteKeranjang(' . $value->id . ')"
                        class="font-medium text-red-600 dark:text-red-500 hover:underline cursor-pointer">Hapus</a>
                </td>
            </tr>';
            } else {
                $html3 .= '<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td class="px-3 py-2">
                    ' . \Carbon\Carbon::parse($value->pesanan->tanggal)->translatedFormat('d-m-Y') . '
                    </td>
                    <td class="px-3 py-2">
                    ' . $value->customer->nama . '
                    </td>
                    <td class="px-3 py-2" colspan="3">
                        ' . $value->pesanan->no_pesanan . '
                    </td>
                    <td class="px-3 py-2" colspan="4">
                        <ul class="space-y-2 grid grid-cols-2">';
                foreach ($value->pesanan->pesananmanualdetail as $key => $detail) {
                    $html3 .= '<li>' . ($key + 1) . '.' . $detail->nama_cetakan . '<b>@</b>' . $detail->qty . '<b>x</b>' . rupiah($detail->harga_satuan) . '=' . rupiah($detail->total_harga) . '</li>';
                }
                $html3 .= '</ul>
                    </td>
                    <td class="px-3 py-2">
                        ' . rupiah($value->pesanan->total_harga) . '
                    </td>
                    <td class="px-3 py-2">
                        <a onclick="deleteKeranjang(' . $value->id . ')"
                            class="font-medium text-red-600 dark:text-red-500 hover:underline cursor-pointer">Hapus</a>
                    </td>
                </tr>
                ';
            }
        }
        $banner = self::theadBanner($html);
        $cetakan = self::theadCetakan($html2);
        $manual = self::theadManual($html3);
        return $banner . $cetakan . $manual;
    }

    public static function theadBanner($konten)
    {
        return '<thead
        class="text-xs text-gray-800 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400 sticky top-0 border-b">
        <tr>
            <th scope="col" class="px-3 py-3">
                Tanggal
            </th>
            <th scope="col" class="px-3 py-3">
                No Pesanan
            </th>
            <th scope="col" class="px-3 py-3">
                Cetakan
            </th>
            <th scope="col" class="px-3 py-3">
                Tipe
            </th>
            <th scope="col" class="px-3 py-3">
                Bahan
            </th>
            <th scope="col" class="px-3 py-3">
                Lebar
            </th>
            <th scope="col" class="px-3 py-3">
                Finishing
            </th>
            <th scope="col" class="px-3 py-3">
                Panjang
            </th>
            <th scope="col" class="px-3 py-3">
                Qty
            </th>
            <th scope="col" class="px-3 py-3">
                Harga
            </th>
            <th scope="col" class="px-3 py-3">
                Action
            </th>
        </tr>
    </thead>
    <tbody class="overflow-x-auto">
        ' . $konten . '
    </tbody>';
    }

    public static function theadCetakan($konten)
    {
        return '
        <thead
        class="text-xs text-gray-800 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400 sticky top-0 border-b">
        <tr>
        <th scope="col" class="px-3 py-3">
            Tanggal
        </th>
        <th scope="col" class="px-3 py-3">
            No Pesanan
        </th>
        <th scope="col" class="px-3 py-3" colspan="3">
            Cetakan
        </th>
        <th scope="col" class="px-3 py-3">
            Jumlah Cetakan
        </th>
        <th scope="col" class="px-3 py-3">
            Jumlah Drag
        </th>
        <th scope="col" class="px-3 py-3">
            Jenis Cetak
        </th>
        <th scope="col" class="px-3 py-3">
            Jumlah Plate
        </th>
        <th scope="col" class="px-3 py-3">
            Harga Total
        </th>
        <th scope="col" class="px-3 py-3">
            Action
        </th>
    </tr>
    </thead>
    <tbody class="overflow-x-auto">
        ' . $konten . '
    </tbody>';
    }

    public static function theadManual($konten)
    {
        return '
        <thead
        class="text-xs text-gray-800 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400 sticky top-0 border-b">
        <tr>
        <th scope="col" class="px-3 py-3">
            Tanggal
        </th>
        <th scope="col" class="px-3 py-3">
            Customer
        </th>
        <th scope="col" class="px-3 py-3" colspan="3">
            No  Pesanan
        </th>
        <th scope="col" class="px-3 py-3" colspan="4">
            Detail Pesanan
        </th>
        <th scope="col" class="px-3 py-3">
            Harga Total
        </th>
        <th scope="col" class="px-3 py-3">
            Action
        </th>
    </tr>
    </thead>
    <tbody class="overflow-x-auto">
        ' . $konten . '
    </tbody>';
    }

    public function getCustomers($customer)
    {
        $customers = Outstanding::select('customer_id', 'nominal', 'pesanan_pending', 'pesanan_partial')
            ->with('customer:id,nama,no_hp,member')
            ->orderBy('pesanan_pending', 'desc')
            ->get();
        $total = $customers->sum('nominal');
        return $this->tableCustomer($customers, $total);
    }

    public static function getTotalHargaByCustomer($customer_id)
    {
        return Keranjang::where('customer_id', $customer_id)
            ->join('t_pesanan', 't_keranjang.no_pesanan', '=', 't_pesanan.no_pesanan')
            ->sum('t_pesanan.harga');
    }

    /*
    * menambahkan pesanan ke keranjang
    */
    public static function addToCart($listpesanan, $jenis)
    {
        switch ($jenis) {
            case 'manual':
                $pesanan = PesananManual::whereIn('no_pesanan', $listpesanan)->get(['no_pesanan', 'customer_id', 'id']);
                break;
            case 'cetakan':
                $pesanan = PesananCetakan::whereIn('no_pesanan', $listpesanan)->get(['no_pesanan', 'customer_id', 'id']);
                break;
            default:
                $pesanan = Pesanan::whereIn('no_pesanan', $listpesanan)->get(['no_pesanan', 'customer_id', 'id']);
                break;
        }

        $customer_id = $pesanan->first()->customer_id;
        $keranjangCustomer = Keranjang::where('customer_id', '!=', $customer_id)->first();

        if ($keranjangCustomer) {
            Keranjang::where('customer_id', $keranjangCustomer->customer_id)->delete();
        }

        $dataToInsert = [];

        foreach ($pesanan as $item) {
            $exists = Keranjang::where('pesanan_id', $item->id)
                ->where('customer_id', $item->customer_id)
                ->where('pesanan_type', get_class($item))
                ->exists();

            if (!$exists) {
                $dataToInsert[] = [
                    'pesanan_id' => $item->id,
                    'customer_id' => $item->customer_id,
                    'pesanan_type' => get_class($item),
                ];
            }
        }
        if (!empty($dataToInsert)) {
            Keranjang::insert($dataToInsert);
        }

        $pesananid = array_filter($pesanan->pluck('no_pesanan')->toArray());
        switch ($jenis) {
            case 'cetakan':
                PesananCetakan::updateCetakan($pesananid, 'keranjang');
                break;
            case 'manual':
                PesananManual::updateManual($pesananid, 'keranjang');
                break;
            default:
                Pesanan::updatePesanan($pesananid, 'keranjang');
                break;
        }
        return 'sukses';
    }

    // query pesanan by customer id
    public function getPesananByCustomer($customer)
    {
        $customers = Pesanan::where('customer_id', $customer)
            ->whereNull('status_pesanan')
            ->with('customer', 'tipe', 'bahan', 'lebar', 'finishing')
            ->get();
        return $this->tablePesananByCustomerId($customers);
    }

    public function getPesananCetakanByCustomer($customer)
    {
        $customers = PesananCetakan::where('customer_id', $customer)
            ->whereNull('status_pesanan')
            ->with('customer')
            ->get();
        return $this->tablePesananByCustomerId($customers);
    }

    public function getPesananManualByCustomer($customer)
    {
        $customers = PesananManual::where('customer_id', $customer)
            ->whereNull('status_pesanan')
            ->with('customer')
            ->get();
        return $this->tablePesananByCustomerId($customers);
    }

    // data table customer
    public function tableCustomer($customers, $total)
    {
        $html = "";
        foreach ($customers as $customer) {
            $html .= '<tr class="bg-gray-50 border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600" ondblclick="pembayaran_detail(' . $customer->customer_id . ')">
            <th scope="row" class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
                <img class="w-10 h-10 rounded-full" src="' . asset('image/user.png') . '" alt="Jese image">
                <div class="ps-3">
                    <div class="text-base font-semibold">' . $customer->customer->nama . '</div>
                    <div class="font-normal text-gray-500">0890923427385</div>
                </div>
            </th>
            <td class="px-4 py-4">
                <div class="flex items-center">
                    <div class="rounded-full bg-green-600 me-2">' . $customer->customer->member . '</div>
                </div>
            </td>
            <td class="px-4 py-4">
                ' . Helpers::rupiah($customer->nominal) . '
            </td>
            <td class="px-4 py-4" align="center">
            ' . $customer->pesanan_pending . '
            </td>
            <td class="px-4 py-4" align="center">
            ' . $customer->pesanan_partial . '
            </td>
        </tr>';
        }
        $html .= '<tr class="bg-gray-50 border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600" ondblclick="pembayaran_detail(' . $customer->customer_id . ')">
        <th scope="row" class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
           Grand Total
        </th>
        <td class="px-4 py-4">
            <div class="flex items-center">
            </div>
        </td>
        <td class="px-4 py-4">
            ' . Helpers::rupiah($total) . '
        </td>
        <td class="px-4 py-4" align="center">
        </td>
        <td class="px-4 py-4" align="center">
        </td>
    </tr>';
        return $html;
    }

    // data table pesanan by customer id
    public function tablePesananByCustomerId($pesanan)
    {
        $html = "";
        foreach ($pesanan as $pesan) {
            $html .= '<li class="dmv-list-item px-6 py-2 flex items-center gap-x-5">
            <input type="checkbox" id="' . $pesan->id . '" value="' . $pesan->no_pesanan . '" class="peer" required="">
            <label for="' . $pesan->id . '">                           
                <div class="block">
                    <div class="w-full text-sm font-semibold text-gray-700 cursor-pointer">' . $pesan->no_pesanan . ' - ' . $pesan->nama_cetakan  . '</div>
                </div>
            </label>
        </li>';
        }
        return $html;
    }

    // megnhapus semua data yang ada di keranjang
    public function clearKeranjang(array $payload = [])
    {
        DB::beginTransaction();
        try {
            $nopesanan = Keranjang::select('pesanan_id', 'pesanan_type')->get()->toArray();
            foreach ($nopesanan as $value) {
                $pesanan = (new $value['pesanan_type'])->find($value['pesanan_id']);
                $pesanan->status_pesanan = $payload['status_pesanan'];
                $pesanan->status_pembayaran = $payload['status_pembayaran'];
                $pesanan->save();
            }
            Keranjang::truncate();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            return response([
                'status' => 'fail',
                'message' => $e->getMessage()
            ]);
        }

        return response([
            'status' => 'success',
            'message' => 'Keranjang sudah dikosongkan'
        ]);
    }

    // menghapus satu pesanan yang ada di keranjang
    public function hapusKeranjang(Request $request, Keranjang $keranjang)
    {
        DB::beginTransaction();
        try {
            $pesanan = $keranjang->pesanan;
            $pesanan->status_pesanan = null;
            $pesanan->save();
            $keranjang->delete();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            return response([
                'status' => 'fail',
                'message' => $e->getMessage()
            ]);
        }
        return response([
            'status' => 'success',
            'message' => 'Pesanan sudah dihapus'
        ]);
    }

    public function struk($idpembayaran)
    {
        $data = Pembayaran::with('invoice', 'invoice.invoicedetail', 'invoice.invoicedetail.pesanan')->findOrFail($idpembayaran);
        $encryptedId = Crypt::encryptString($idpembayaran);
        $url = url('/dmv/pembayaran/struk/' . $idpembayaran);
        $url2 = 'hello world';
        $logoPath = public_path('image/QR.png');
        $qrCode = QrCode::format('png')
            ->size(130)
            ->errorCorrection('L')
            // ->merge($logoPath, 0.3, true)
            ->generate($url);
        $qrCodeDataUri = 'data:image/png;base64,' . base64_encode($qrCode);
        return view('dmv.pembayaran.struk-pembayaran', compact('data'), ['qrCode' => $qrCodeDataUri]);
    }

    // data table pesanan by customer id
    public function tablePesananCetakanByCustomerId($pesanan)
    {
        $html = "";
        foreach ($pesanan as $pesan) {
            $html .= '<li class="dmv-list-item px-6 py-2 flex items-center gap-x-5">
            <input type="checkbox" id="' . $pesan->id . '" value="' . $pesan->no_pesanan . '" class="peer" required="">
            <label for="' . $pesan->id . '">                           
                <div class="block">
                    <div class="w-full text-sm font-semibold text-gray-700 cursor-pointer">' . $pesan->no_pesanan . ' - ' . $pesan->nama_cetakan . '</div>
                </div>
            </label>
        </li>';
        }
        return $html;
    }

    public function spk($idpembayaran)
    {
        $data = Pembayaran::with('invoice', 'invoice.invoicedetail', 'invoice.invoicedetail.pesanan')->findOrFail($idpembayaran);
        $encryptedId = Crypt::encryptString($idpembayaran);
        $url = url('/dmv/spk/' . $idpembayaran);
        $url2 = 'hello world';
        $logoPath = public_path('image/QR.png');
        $qrCode = QrCode::format('png')
            ->size(130)
            ->errorCorrection('L')
            // ->merge($logoPath, 0.3, true)
            ->generate($url);
        $qrCodeDataUri = 'data:image/png;base64,' . base64_encode($qrCode);
        return view('dmv.pembayaran.spk', compact('data'), ['qrCode' => $qrCodeDataUri]);
    }
}
