<?php

namespace App\Http\Controllers;

use App\Models\SubKlasifikasi;
use App\Reports\BukuBesar;
use App\Reports\JurnalUmum;
use App\Reports\NeracaSaldo;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class LaporanKeuanganController extends Controller
{
    public function jurnalumum(Request $request)
    {
        $data = JurnalUmum::getJurnalUmum();
        return view('laporan.jurnalumum', compact('data'));
    }

    public function export(Request $request)
    {
        $jenis = $request->query('jenis');

        switch ($jenis) {
            case 'jurnalumum':
                $data = JurnalUmum::getJurnalUmum();
                $export = new JurnalUmum($data);
                break;
            case 'neracasaldo':
                $data = NeracaSaldo::getNeracaSaldo();
                $export = new NeracaSaldo($data);
                break;
            default:
                break;
        }
        $filePath = $export->export();
        return response()->download($filePath);
    }

    public function bukubesar(Request $request)
    {
        $data = BukuBesar::getBukuBesar();
        return view('laporan.bukubesar', compact('data'));
    }

    public function neracasaldo(Request $request)
    {
        $data = NeracaSaldo::getNeracaSaldo();
        return view('laporan.neracasaldo', compact('data'));
    }

    public function labarugi()
    {
        $pendapatan = SubKlasifikasi::where('jenis', 'pendapatan')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])
            ->get();
        $hpp = SubKlasifikasi::where('jenis', 'hpp')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])->get();
        $bebanUsaha = SubKlasifikasi::where('jenis', 'beban')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])->get();
        $bebanNonUsaha = SubKlasifikasi::where('jenis', 'beban_non_usaha')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])->get();
        return view('laporan.labarugi', compact('pendapatan', 'hpp', 'bebanUsaha', 'bebanNonUsaha'));
    }

    public function neraca()
    {
        $asset = SubKlasifikasi::where('jenis', 'asset')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])
            ->get();
        $kewajiban = SubKlasifikasi::where('jenis', 'kewajiban')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])->get();
        $modal = SubKlasifikasi::where('jenis', 'modal')
            ->with(['akun' => function ($query) {
                $query->withSum('jurnal as total_debet', 'debet')
                    ->withSum('jurnal as total_kredit', 'kredit');
            }])->get();
        return view('laporan.neraca', compact('asset', 'kewajiban', 'modal'));
    }
}
