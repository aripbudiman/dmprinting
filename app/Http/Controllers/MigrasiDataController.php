<?php

namespace App\Http\Controllers;

use App\Models\Migrasi;
use App\Models\Pesanan;
use Illuminate\Http\Request;
use App\Traits\UpdateOutstanding;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MigrasiDataController extends Controller
{
    use UpdateOutstanding;

    protected static $pesanan;
    protected static $tmp_pesanan;
    protected static $newPesanan;

    public function __construct()
    {
        self::$pesanan = DB::connection('old')->table('pesanan');
        self::$tmp_pesanan = DB::connection('old')->table('tmp_pesanan');
        self::$newPesanan = new Pesanan();
    }

    public function index(Request $request)
    {
        $pesanan = self::$pesanan->where('status_migrasi', null)->paginate(20);
        // dd($pesanan);
        if ($request->ajax()) {
            $payload = explode(',', $request->no_pesanan);
            $existingPesanan = Pesanan::whereIn('no_pesanan', $payload)->pluck('no_pesanan')->toArray();
            $result = array_diff($payload, $existingPesanan);
            self::createArray($result);
            return response('Migrasi data success');
        }
        return view('migrasidata.index', compact('pesanan'));
    }

    public static function getPesananById($id)
    {
        return self::$pesanan->where('no_pesanan', $id)->first();
    }

    public static function createArray($array)
    {
        $data = [];
        $result = self::$pesanan->whereIn('no_pesanan', $array)->get();

        foreach ($result as $key => $value) {
            $data[] = [
                'customer_id' => $value->id_customer,
                'no_pesanan' => $value->no_pesanan,
                'nama_cetakan' => $value->nama_cetakan,
                'tipe_id' => $value->id_tipe,
                'bahan_id' => $value->id_bahan,
                'lebar_id' => $value->id_lebar,
                'finishing_id' => $value->id_finishing,
                'panjang' => $value->panjang,
                'qty' => $value->qty,
                'harga' => $value->harga,
                'tanggal' => $value->created_at,
            ];
            self::jurnalPesananMigrasi($data[0]);
            self::$newPesanan->updateOutstanding($value->id_customer, $value->harga);
            self::$newPesanan->updateOutstandingPending($value->id_customer, 1);
        }
        Pesanan::insert($data);
    }
}
