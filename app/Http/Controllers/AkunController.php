<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use Illuminate\Http\Request;
use App\Models\SubKlasifikasi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AkunController extends Controller
{
    public function index()
    {
        $subKlasifikasi = SubKlasifikasi::withCount('akun')->get();
        $akun = Akun::with('subklasifikasi')->get();
        return view('akuntansi.akun.index', compact('subKlasifikasi', 'akun'));
    }

    public function store(Request $request, Akun $akun)
    {
        DB::beginTransaction();
        try {
            $akun = $akun->fill($request->all());
            $akun->save();
            Log::info('Berhasil Buat Akun');
            DB::commit();
            return response([
                'success' => true,
                'message' => 'Akun Berhasil',
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            return response([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function mappingSaldoNormal(Request $request)
    {
        $subKlasifikasi = SubKlasifikasi::with('akun')->get();
        foreach ($subKlasifikasi as $key => $value) {
            $value->akun()->update(['saldo_normal' => $value->saldo_normal]);
        }
        return response()->json(['success' => true]);
    }

    public function subklasifikasi()
    {
        $subKlasifikasi = SubKlasifikasi::with('akun')->get();
        return view('akuntansi.subklasifikasi.index', compact('subKlasifikasi'));
    }

    public function mappingJenis(Request $request)
    {
        try {
            DB::beginTransaction();
            foreach ($request->id as $key => $value) {
                $subKlasifikasi = SubKlasifikasi::find($value);
                $subKlasifikasi->update(['jenis' => $request->jenis[$key]]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
        }

        return response()->json(['success' => true]);
    }
}
