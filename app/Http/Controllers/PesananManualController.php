<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Transaksi;
use App\Constants\Constant;
use App\Models\Outstanding;
use Illuminate\Http\Request;
use App\Models\PesananManual;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PesananManualRequest;

class PesananManualController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $data = PesananManual::with('customer', 'pesananmanualdetail')->where('status_pembayaran', null)->paginate();
        return view('pesananmanual.index', compact('payload', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            if ($request->autocomplete) {
                $query = $request->input('search');
                $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
                $response = array();
                foreach ($results as $value) {
                    $response[] = [
                        'value' => $value->id,
                        'label' => strtoupper($value->nama),
                    ];
                }
                return response()->json(['data' => $response]);
            }
        }
        $url = route('pesanan_manual.store');
        return view('pesananmanual.create', compact('url'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PesananManualRequest $request, PesananManual $pesananmanual)
    {
        try {
            DB::beginTransaction();
            $pesanan = $pesananmanual->fill($request->except('total_harga'));
            $details = [];
            foreach ($request->total_harga as $key => $value) {
                $details[] = [
                    'nama_cetakan' => $request->nama_cetakan[$key],
                    'qty' => $request->qty[$key],
                    'harga_satuan' => floatval(str_replace('.', '', $request->harga_satuan[$key])),
                    'total_harga' => $request->total_harga[$key],
                ];
            }
            $pesanan->total_harga = $request->grand_total;
            $pesanan->save();
            $pesanan->pesananmanualdetail()->createMany($details);
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Pesanan Manual Berhasilimpan',
                'redirect' => route('pesanan_manual.edit', $pesanan->id)
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PesananManual $pesanan_manual)
    {
        $pesanan = $pesanan_manual->load('pesananmanualdetail');
        $url = route('pesanan_manual.update', $pesanan->id);
        return view('pesananmanual.create', compact('pesanan', 'url'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PesananManualRequest $request, PesananManual $pesanan_manual)
    {
        try {
            DB::beginTransaction();
            $total_harga = $request->getOriginal('total_harga');
            $outstanding = Outstanding::where('customer_id', $request->customer_id)->first();
            $outstanding->nominal -= $total_harga;
            $outstanding->save();
            $pesanan = $pesanan_manual->fill($request->except('total_harga'));
            $pesanan->pesananmanualdetail()->delete();
            $details = [];
            foreach ($request->total_harga as $key => $value) {
                $details[] = [
                    'nama_cetakan' => $request->nama_cetakan[$key],
                    'qty' => $request->qty[$key],
                    'harga_satuan' => floatval(str_replace('.', '', $request->harga_satuan[$key])),
                    'total_harga' => $request->total_harga[$key],
                ];
            }
            $pesanan->total_harga = $request->grand_total;
            $pesanan->save();
            $pesanan->pesananmanualdetail()->createMany($details);
            $outstanding = Outstanding::where('customer_id', $request->customer_id)->first();
            $outstanding->nominal += $request->total_harga;
            $outstanding->save();

            $jurnal = Transaksi::where('no_pesanan', $request->no_pesanan)->with('jurnal')->first();
            $jurnal->nominal = $request->total_harga;
            $jurnal->save();
            $jurnal->jurnal[0]->update(['debet' => $jurnal->nominal]);
            $jurnal->jurnal[1]->update(['kredit' => $jurnal->nominal]);
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Pesanan Manual Berhasilimpan',
                'redirect' => 'edit'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            $response = [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
