<?php

namespace App\Http\Controllers;

use App\Models\Tipe;
use App\Models\Bahan;
use App\Models\Lebar;
use App\Models\Jurnal;
use App\Models\Pesanan;
use App\Models\Customers;
use App\Models\Finishing;
use App\Models\Transaksi;
use App\Constants\Constant;
use App\Models\Outstanding;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PesananRequest;

class PesananController extends Controller
{
    public function index(Request $request)
    {
        $payload['start_date'] = $request->input('start_date');
        $payload['end_date'] = $request->input('end_date');
        $payload['show'] = $request->input('show', 20);
        $payload['keyword'] = $request->input('search', '');
        $data = Pesanan::getAllPesanan($payload);
        return view('dmv.pesanan.index', compact('data', 'payload'));
    }

    public function create(Request $request)
    {
        if ($request->ajax()) {
            if ($request->tipe_id) {
                $bahan = Bahan::where('tipe_id', $request->input('tipe_id', ''))->withTrashed()->get();
                $htmlBahan = "";
                foreach ($bahan as $key => $value) {
                    $htmlBahan .= "<option value='$value->id'>$value->nama</option>";
                }
                return response([
                    'bahan' => $htmlBahan,
                    'bahan_id' => $bahan->first()->id
                ]);
            }
            if ($request->bahan_id) {
                $lebar = Lebar::where('bahan_id', $request->input('bahan_id', ''))->withTrashed()->get();
                $html = "";
                foreach ($lebar as $key => $value) {
                    $html .= "<option value='$value->id' data-lebar='$value->harga_lebar'>" . $value->getMeter() . "</option>";
                }
                return response([
                    'lebar' => $html
                ]);
            }
            if ($request->autocomplete) {
                $query = $request->input('search');
                $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
                $response = array();
                foreach ($results as $value) {
                    $response[] = [
                        'value' => $value->id,
                        'label' => strtoupper($value->nama),
                    ];
                }
                return response()->json(['data' => $response]);
            }
        }
        $tipe = Tipe::withCount('bahan')->withTrashed()->has('bahan', '>', 0)->get();
        $finishing = Finishing::withTrashed()->get();
        $route = route('pesanan.store');
        return view('dmv.pesanan.create', compact('tipe', 'finishing', 'route'));
    }

    public function store(PesananRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = Pesanan::create($request->all());
            $this->createJurnalPesanan($data);
            DB::commit();
            return response(['success' => true, 'message' => 'Data berhasil disimpan dengan no pesanan ' . $data->no_pesanan]);
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            return response(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function show($id, Request $request)
    {
        if ($request->ajax()) {
            if ($request->tipe_id) {
                $bahan = Bahan::where('tipe_id', $request->input('tipe_id', ''))->withTrashed()->get();
                $htmlBahan = "";
                foreach ($bahan as $key => $value) {
                    $htmlBahan .= "<option value='$value->id'>$value->nama</option>";
                }
                return response([
                    'bahan' => $htmlBahan,
                    'bahan_id' => $bahan->first()->id
                ]);
            }
            if ($request->bahan_id) {
                $lebar = Lebar::where('bahan_id', $request->input('bahan_id', ''))->withTrashed()->get();
                $html = "";
                foreach ($lebar as $key => $value) {
                    $html .= "<option value='$value->id' data-lebar='$value->harga_lebar'>" . $value->getMeter() . "</option>";
                }
                return response([
                    'lebar' => $html
                ]);
            }
            if ($request->autocomplete) {
                $query = $request->input('search');
                $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
                $response = array();
                foreach ($results as $value) {
                    $response[] = [
                        'value' => $value->id,
                        'label' => strtoupper($value->nama),
                    ];
                }
                return response()->json(['data' => $response]);
            }
        }
        $pesanan = Pesanan::with('customer:id,nama', 'bahan', 'tipe', 'lebar', 'finishing')->findOrFail($id);
        $tipe = Tipe::withCount('bahan')->withTrashed()->has('bahan', '>', 0)->get();
        $finishing = Finishing::withTrashed()->get();
        return view('dmv.pesanan.show', compact('tipe', 'finishing', 'pesanan'));
    }

    public function edit($id, Request $request)
    {
        if ($request->ajax()) {
            if ($request->tipe_id) {
                $bahan = Bahan::where('tipe_id', $request->input('tipe_id', ''))->withTrashed()->get();
                $htmlBahan = "";
                foreach ($bahan as $key => $value) {
                    $htmlBahan .= "<option value='$value->id'>$value->nama</option>";
                }
                return response([
                    'bahan' => $htmlBahan,
                    'bahan_id' => $bahan->first()->id
                ]);
            }
            if ($request->bahan_id) {
                $lebar = Lebar::where('bahan_id', $request->input('bahan_id', ''))->withTrashed()->get();
                $html = "";
                foreach ($lebar as $key => $value) {
                    $html .= "<option value='$value->id' data-lebar='$value->harga_lebar'>" . $value->getMeter() . "</option>";
                }
                return response([
                    'lebar' => $html
                ]);
            }
            if ($request->autocomplete) {
                $query = $request->input('search');
                $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
                $response = array();
                foreach ($results as $value) {
                    $response[] = [
                        'value' => $value->id,
                        'label' => strtoupper($value->nama),
                    ];
                }
                return response()->json(['data' => $response]);
            }
        }
        $pesanan = Pesanan::with('customer:id,nama', 'bahan', 'tipe', 'lebar', 'finishing')->findOrFail($id);
        $tipe = Tipe::withCount('bahan')->withTrashed()->has('bahan', '>', 0)->get();
        $finishing = Finishing::withTrashed()->get();
        $route = route('pesanan.update', $id);
        return view('dmv.pesanan.create', compact('tipe', 'finishing', 'pesanan', 'route'));
    }

    public function update(PesananRequest $request, Pesanan $pesanan)
    {
        try {
            DB::beginTransaction();
            $outstanding = Outstanding::where('customer_id', $pesanan->customer_id)->first();
            $outstanding->nominal -= $pesanan->harga;
            $outstanding->save();
            $pesanan->update($request->all());
            $outstanding = Outstanding::where('customer_id', $pesanan->customer_id)->first();
            $outstanding->nominal += $pesanan->harga;
            $outstanding->save();
            $jurnal = Transaksi::where('no_pesanan', $pesanan->no_pesanan)->with('jurnal')->first();
            $jurnal->nominal = $pesanan->harga;
            $jurnal->save();
            $jurnal->jurnal[0]->update(['debet' => $jurnal->nominal]);
            $jurnal->jurnal[1]->update(['kredit' => $jurnal->nominal]);
            DB::commit();
            return response(['success' => true, 'message' => $pesanan->no_pesanan . ' Berhasil diubah']);
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            return response(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function createCetakan()
    {
        return view('dmv.pesanan.create');
    }

    public function storeCetakan(Request $request)
    {
        dd($request);
    }
}
