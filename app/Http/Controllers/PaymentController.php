<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\MetodePembayaran;

class PaymentController extends Controller
{
    public function payment($noinvoice)
    {
        $data = Invoice::select('id', 'tanggal', 'customer_id', 'no_invoice', 'subtotal', 'disc_percentage', 'disc', 'grand_total', 'sisa', 'status', 'kode_voucher', 'no_spk', 'created_at')
            ->where('no_invoice', $noinvoice)
            ->with('invoicedetail:id,invoice_id,pesanan_id,pesanan_type', 'invoicedetail.pesanan')
            ->withCount(['invoicedetail as totalpesanan'])
            ->first();
        return view('payment.index', compact('data'));
    }

    public function paymentMethod()
    {
        $metodePembayaran = new MetodePembayaran();
        return response()->json($metodePembayaran->where('is_active', 1)->get());
    }
}
