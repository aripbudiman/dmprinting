<?php

namespace App\Http\Controllers;

use App\Models\Tipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class DmvTipeController extends Controller
{
    public function index(Request $request)
    {
        Log::info('Halaman Tipe');
        $show = $request->input('show', 10);
        $keyword = $request->input('search');
        $tipe = Tipe::paginate($show);
        return view('dmv.tipe.index', compact('tipe', 'show', 'keyword'));
    }

    public function delete(Tipe $tipe)
    {
        $tipe->delete();
        Log::info('Data Tipe Dihapus', $tipe->toArray());
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }

    public function create()
    {
        Log::info('Halaman Tambah Tipe');
        return view('dmv.tipe.create');
    }

    public function store(Request $request)
    {
        Tipe::create($request->all());
        Log::info('Data Tipe Ditambah', $request->all());
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Ditambah';
        return $response;
    }

    public function edit(Tipe $tipe)
    {
        Log::info('Halaman Edit Tipe', $tipe->toArray());
        return view('dmv.tipe.edit', compact('tipe'));
    }

    public function update(Request $request, Tipe $tipe)
    {
        $tipe->update($request->all());
        Log::info('Data Tipe Diubah', $tipe->toArray());
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Diubah';
        return $response;
    }
}
