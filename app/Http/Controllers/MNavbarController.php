<?php

namespace App\Http\Controllers;

use App\Models\MNavbar;
use App\Models\MenuCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MNavbarController extends Controller
{

    public function index(Request $request)
    {
        $navbar = MNavbar::all();
        return view('navbar.index', compact('navbar'));
    }

    public function create()
    {
        $menu = MenuCategory::all();
        return view('navbar.create', compact('menu'));
    }

    public function edit(MNavbar $navbar)
    {
        $menu = MenuCategory::all();
        return view('navbar.edit', compact('menu', 'navbar'));
    }

    public function store(Request $request, MNavbar $navbar)
    {
        DB::beginTransaction();
        try {
            $navbar->fill($request->all());
            $navbar->save();
            DB::commit();
            Log::info('data menu tersimpan');
            return response([
                'status' => true,
                'message' => 'data tersimpan'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, MNavbar $navbar)
    {
        DB::beginTransaction();
        try {
            $navbar->fill($request->all());
            $navbar->is_active = !empty($request->is_active) ? 1 : 0;
            $navbar->save();
            DB::commit();
            Log::info('data menu diperbarui');
            return response([
                'status' => true,
                'message' => 'data diperbarui'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(MNavbar $navbar)
    {
        $navbar->delete();
        return redirect()->back();
    }

    public function isActive(Request $request)
    {
        $navbar = MNavbar::find($request->id);
        $navbar->find($request->id);
        $navbar->update(['is_active' => $request->is_active]);
        return response([
            'status' => true,
            'message' => 'data diperbarui'
        ]);
    }
}
