<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Outstanding;
use Illuminate\Http\Request;
use App\Models\PesananCetakan;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PesananCetakanRequest;
use App\Models\Transaksi;

class PesananCetakanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $payload['show'] = $request->input('show', 20);
        $payload['keyword'] = $request->input('search', '');
        $data = PesananCetakan::getAllCetakan($payload);
        return view('pesanancetakan.index', compact('data', 'payload'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        if ($request->ajax() && $request->autocomplete) {
            $query = $request->input('search');
            $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
            $response = array();
            foreach ($results as $value) {
                $response[] = [
                    'value' => $value->id,
                    'label' => strtoupper($value->nama),
                ];
            }
            return response()->json(['data' => $response]);
        }
        $url = route('pesanan-cetakan.store');
        return view('dmv.pesanan.create', compact('url'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PesananCetakanRequest $request)
    {
        $response = '';
        try {
            DB::beginTransaction();
            $pesanan = PesananCetakan::create($request->all());
            $this->createJurnalPesanan($pesanan);
            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Pesanan Berhasil Disimpan'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(PesananCetakan $pesanan_cetakan)
    {
        $pesanan_cetakan->load('customer');
        return view('pesanancetakan.show', compact('pesanan_cetakan'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PesananCetakan $pesanan_cetakan)
    {
        $pesanan_cetakan->load('customer');
        $pesanan = $pesanan_cetakan;
        $url = route('pesanan-cetakan.update', $pesanan->id);
        $update = true;
        return view('dmv.pesanan.create', compact('pesanan', 'url', 'update'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PesananCetakanRequest $request, PesananCetakan $pesanan_cetakan)
    {
        try {
            DB::beginTransaction();
            $outstanding = Outstanding::where('customer_id', $request->customer_id)->first();
            $outstanding->nominal -= $pesanan_cetakan->total_harga;
            $outstanding->save();
            $pesanan_cetakan->update($request->all());
            $outstanding->nominal += $request->total_harga;
            $outstanding->save();

            $jurnal = Transaksi::where('no_pesanan', $pesanan_cetakan->no_pesanan)->with('jurnal')->first();
            $jurnal->nominal = $request->total_harga;
            $jurnal->save();
            $jurnal->jurnal[0]->update(['debet' => $jurnal->nominal]);
            $jurnal->jurnal[1]->update(['kredit' => $jurnal->nominal]);
            DB::commit();
            $response = [
                'success' => true,
                'message' => 'Data Berhasil Diubah'
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            logError($e);
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
