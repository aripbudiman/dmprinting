<?php

namespace App\Http\Controllers;

use App\Models\Tipe;
use App\Models\Bahan;
use Illuminate\Http\Request;

class DmvBahanController extends Controller
{
    public function index(Request $request)
    {
        $show = $request->input('show', 10);
        $keyword = $request->input('search');
        $data = Bahan::with('tipe')
            ->where('nama', 'like', '%' . $keyword . '%')
            ->paginate($show)
            ->withQueryString();
        return view('dmv.bahan.index', compact('data', 'show', 'keyword'));
    }

    public function create()
    {
        $tipe = Tipe::all();
        return view('dmv.bahan.create', compact('tipe'));
    }

    public function store(Request $request)
    {
        Bahan::create($request->all());
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Ditambah';
        return $response;
    }

    public function show(Bahan $bahan)
    {
        $bahan->load('tipe');
        return view('dmv.bahan.show', compact('bahan'));
    }

    public function edit(Bahan $bahan)
    {
        $tipe = Tipe::all();
        return view('dmv.bahan.edit', compact('bahan', 'tipe'));
    }

    public function update(Request $request, Bahan $bahan)
    {
        $bahan->update($request->all());
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Diubah';
        return $response;
    }

    public function destroy(Bahan $bahan)
    {
        $bahan->delete();
        $response['success'] = true;
        $response['message'] = 'Data Berhasil Dihapus';
        return $response;
    }
}
