<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use App\Models\Transaksi;
use App\Constants\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengeluaranKasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $payload['jenis'] = Constant::JENIS_TRANSAKSI[3];
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $data = Transaksi::getPengeluaran($payload);
        $akunDebet = Akun::select('id', 'nama')->whereIn('subklasifikasi_id', [113, 114, 115, 121, 412, 611, 612, 621, 521])->get();
        $akunKredit = Akun::select('id', 'nama')->whereIn('subklasifikasi_id', [111])->get();
        $route = route('pengeluaran_kas.store');
        $method = 'store';
        $title = 'Pengeluaran Kas';
        return view('pengeluarankas.index', compact('data', 'payload', 'akunDebet', 'akunKredit', 'route', 'method', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $response = [
            'status' => 'error',
            'message' => 'Transaksi Gagal',
        ];
        try {
            DB::beginTransaction();
            $this->createJurnalKhusus(Constant::JENIS_TRANSAKSI[3], $request->all());
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Transaksi Berhasil',
            ];
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Transaksi $pengeluaran_ka)
    {
        return response()->json([
            'data' => $pengeluaran_ka->load('jurnal'),
            'route' => route('pengeluaran_kas.update', $pengeluaran_ka->id),
            'method' => 'PUT'
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Transaksi $pengeluaran_ka)
    {
        $response = [
            'status' => 'error',
            'message' => 'Transaksi Gagal',
        ];
        try {
            DB::beginTransaction();
            $this->createJurnalKhusus(Constant::JENIS_TRANSAKSI[3], $request->merge(['id' => $pengeluaran_ka->id])->all());
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Transaksi Berhasil',
            ];
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
