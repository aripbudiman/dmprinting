<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Customers;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Models\InvoiceDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\InvoiceRequest;
use App\Models\Pesanan;
use App\Models\Spk;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InvoiceController extends Controller
{
    // create invoice
    public static function store($request, Invoice $invoice)
    {
        $data = $request->all();
        $data['status'] = 'pending';
        $result = $invoice::create($data);

        $payload = [];
        $no_pesanan_list = isset($data['no_pesanan']) ? explode(',', $data['no_pesanan']) : [];
        foreach ($no_pesanan_list as $value) {
            $payload[] = [
                'invoice_id' => $result->id,
                'no_pesanan' => $value
            ];
        }
        InvoiceDetail::insert($payload);
        $result->no_pesanan = array_column($payload, 'no_pesanan');
        Log::info('Invoice created');
        return $result;
    }

    public function create(Request $request)
    {
        if ($request->autocomplete) {
            $query = $request->input('search');
            $results = Customers::select('id', 'nama', 'member')->where('nama', 'like', '%' . $query . '%')->limit(10)->get();
            $response = array();
            foreach ($results as $value) {
                $response[] = [
                    'value' => $value->id,
                    'label' => strtoupper($value->nama),
                ];
            }
            return response()->json(['data' => $response]);
        }
        return view('dmv.pembayaran.invoicecreate');
    }

    public function indexInvoice()
    {
        return view('dmv.pembayaran.invoice');
    }

    public function show($id)
    {
        return view('dmv.pembayaran.invoicedetail');
    }

    public function showSpk($nospk)
    {
        $data = Spk::where('no_spk', $nospk)->get();
        $data->load('pesanan', 'invoice:id,no_spk,no_invoice,status');
        $url_spk = url('/spk/' . $nospk . '/print');
        $url_invoice = url('/payment/' . $data[0]->invoice->no_invoice . '?status=' . $data[0]->invoice->status . '&page=show');
        $url_payment = url('/payment/' . $data[0]->invoice->no_invoice . '?status=' . $data[0]->invoice->status . '&page=pay');
        return view('payment.showspk', compact('data', 'url_spk', 'url_invoice', 'url_payment'));
    }

    public function printSpk($nospk)
    {
        $data = Spk::where('no_spk', $nospk)->orderBy('id')->get();
        $data->load('pesanan', 'invoice:no_spk,created_by');
        return view('payment.printspk', compact('data'));
    }

    public function globalInvoice($noinvoice)
    {
        $data = Invoice::where('no_invoice', $noinvoice)->first();
        $data->load('invoicedetail:id,invoice_id,pesanan_id,pesanan_type', 'invoicedetail.pesanan', 'customer', 'pembayaran');
        // return $data;
        return view('payment.global-invoice', compact('data'));
    }
}
