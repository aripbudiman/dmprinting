<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use App\Models\Transaksi;
use App\Constants\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PenerimaanKasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $payload['show'] = $request->input('show', 10);
        $payload['keyword'] = $request->input('search', '');
        $payload['jenis'] = Constant::JENIS_TRANSAKSI[2];
        $payload['start_date'] = $request->input('start_date', startDate());
        $payload['end_date'] = $request->input('end_date', endDate());
        $data = Transaksi::getPenerimaanKas($payload);
        $akunDebet = Akun::select('id', 'nama')->where('subklasifikasi_id', 111)->get();
        $akunKredit = Akun::select('id', 'nama')->whereIn('subklasifikasi_id', [212, 411, 421, 522])->get();
        $route = route('penerimaan_kas.store');
        $method = 'store';
        $title = 'Penerimaan Kas';
        return view('jurnalkhusus.index', compact('data', 'payload', 'akunDebet', 'akunKredit', 'route', 'method', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $response = [
            'status' => 'error',
            'message' => 'Transaksi Gagal',
        ];
        try {
            DB::beginTransaction();
            $this->createJurnalKhusus(Constant::JENIS_TRANSAKSI[2], $request->all());
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Transaksi Berhasil',
            ];
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Transaksi $penerimaan_ka)
    {
        return response()->json([
            'data' => $penerimaan_ka->load('jurnal'),
            'route' => route('penerimaan_kas.update', $penerimaan_ka->id),
            'method' => 'PUT'
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Transaksi $penerimaan_ka)
    {
        $response = [
            'status' => 'error',
            'message' => 'Transaksi Gagal',
        ];
        try {
            DB::beginTransaction();
            $this->createJurnalKhusus(Constant::JENIS_TRANSAKSI[2], $request->merge(['id' => $penerimaan_ka->id])->all());
            DB::commit();
            $response = [
                'status' => 'success',
                'message' => 'Transaksi Berhasil',
            ];
        } catch (\Exception $error) {
            DB::rollBack();
            logError($error);
            $response = [
                'status' => 'error',
                'message' => $error->getMessage()
            ];
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
