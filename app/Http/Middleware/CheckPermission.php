<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!auth()->check() || !session()->has('permission') || !in_array(Str::afterLast($request->route()->getAction('controller'), '\\'), session('permission'))) {
            return response()->view('403', [], Response::HTTP_FORBIDDEN);
        }
        
        return $next($request);
    }
}
