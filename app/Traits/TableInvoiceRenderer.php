<?php

namespace App\Traits;

use App\Helpers\Helpers;

trait TableInvoiceRenderer
{
  public function renderInvoiceTable($model)
  {
    $html = "";
    foreach ($model as $item) {
      $html .= '<tr class="bg-white dark:bg-gray-800">
                  <th scope="row"
                      class="px-4 py-2 font-medium text-blue-600 whitespace-nowrap cursor-pointer hover:underline">
                      ' . $item->no_invoice . '
                  </th>
                  <td class="px-4 py-2">
                      ' . Helpers::rupiah($item->sisa) . '
                  </td>
                  <td class="px-4 py-2">
                      <button type="button" class="btn btn-sm btn-emerald" onclick="_showInvoice(' . htmlspecialchars(json_encode($item)) . ')">Lunasi
                          Sekarang</button>
                  </td>
              </tr>';
    }
    return $html;
  }
}
