<?php

namespace App\Traits;

use App\Models\Jurnal;
use App\Models\Transaksi;
use App\Constants\Constant;
use App\Models\Customers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait HasJournalTransactions
{
  public function createJurnal(string $jenis, $data)
  {
    $transaksi = new Transaksi();
    $transaksi->fill(array_merge($data, ['jenis_transaksi' => $jenis]));
    $transaksi->save();
    $nominal = 0;
    foreach ($data['akun'] as $key => $value) {
      $nominal += (int)str_replace('.', '', $data['debet'][$key]);
      Jurnal::create([
        'transaksi_id' => $transaksi->id,
        'tanggal' => $data['tanggal'],
        'kode_akun' => $value,
        'debet' => $data['debet'][$key] ?? 0,
        'kredit' => $data['kredit'][$key] ?? 0,
      ]);
    }
    $transaksi->nominal = $nominal;
    $transaksi->save();
    return $transaksi;
  }

  public function createOrUpdateJurnal(string $jenis, $data)
  {
    $transaksi = Transaksi::find($data['id'] ?? '') ?? new Transaksi();
    $transaksi->fill(array_merge($data, ['jenis_transaksi' => $jenis]));
    $transaksi->save();

    $nominal = 0;

    if (isset($data['id'])) {
      Jurnal::where('transaksi_id', $transaksi->id)->delete();
    }

    foreach ($data['akun'] as $key => $value) {
      $nominal += (float)str_replace('.', '', $data['debet'][$key]);
      Jurnal::create([
        'transaksi_id' => $transaksi->id,
        'tanggal' => $data['tanggal'],
        'kode_akun' => $value,
        'debet' => $data['debet'][$key] ?? 0,
        'kredit' => $data['kredit'][$key] ?? 0,
      ]);
    }

    $transaksi->nominal = $nominal;
    $transaksi->save();

    return $transaksi;
  }

  public function createJurnalKhusus(string $jenis, $data)
  {
    $transaksi = Transaksi::find($data['id'] ?? '') ?? new Transaksi();
    $transaksi->fill(array_merge($data, ['jenis_transaksi' => $jenis]));
    $transaksi->save();

    if (isset($data['id'])) {
      Jurnal::where('transaksi_id', $transaksi->id)->delete();
    }
    $nominal = (float)str_replace('.', '', $data['nominal']);
    $jurnals = [
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
        'kode_akun' => $data['debet'],
        'debet' => $nominal,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
        'kode_akun' => $data['kredit'],
        'debet' => 0,
        'kredit' => $nominal,
      ],
    ];

    Jurnal::insert($jurnals);

    $transaksi->nominal = $nominal;
    $transaksi->save();
    return $transaksi;
  }

  public function createJurnalPesanan($data)
  {
    $transaksi = Transaksi::create([
      'jenis_transaksi' => Constant::JENIS_TRANSAKSI[4],
      'tanggal' => $data->tanggal,
      'no_pesanan' => $data->no_pesanan,
      'no_ref' => '-',
      'keterangan' => 'Pesanan A/N ' . $data->customer->nama . ' - No. Pesanan ' . $data->no_pesanan,
      'nominal' => $data->harga ?? $data->total_harga,
      'verified_at' => date('Y-m-d H:i:s'),
      'verified_by' => Auth::user()->name
    ]);
    $jurnals = [
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
        'kode_akun' => '1121',
        'debet' => $transaksi->nominal,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
        'kode_akun' => '4111',
        'debet' => 0,
        'kredit' => $transaksi->nominal,
      ],
    ];

    Jurnal::insert($jurnals);
  }

  public function jurnalBayar($data)
  {
    $transaksi = Transaksi::create([
      'jenis_transaksi' => '',
      'tanggal' => date('Y-m-d'),
      'no_ref' => '-',
      'keterangan' => 'Pembayaran A/N ' . $data->invoice->customer->nama . ' - No. Pembayaran ' . $data->no_pembayaran . ' - No. Invoice ' . $data->invoice->no_invoice,
      'nominal' => $data->bayar,
      'verified_at' => date('Y-m-d H:i:s'),
      'verified_by' => Auth::user()->name
    ]);
    $jurnals = [
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($transaksi->tanggal)),
        'kode_akun' => $data->kode_akun,
        'debet' => $data->bayar,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($transaksi->tanggal)),
        'kode_akun' => '4122',
        'debet' => $data->invoice->disc,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($transaksi->tanggal)),
        'kode_akun' => '1121',
        'debet' => 0,
        'kredit' => $data->bayar + $data->invoice->disc,
      ],
    ];
    Jurnal::insert($jurnals);
  }

  public function jurnalPelunasan($data)
  {
    $transaksi = Transaksi::create([
      'jenis_transaksi' => '',
      'tanggal' => date('Y-m-d'),
      'no_ref' => '-',
      'keterangan' => 'Pembayaran Pelunasan A/N ' . $data->invoice->customer->nama . ' - No. Pembayaran ' . $data->no_pembayaran . ' - No. Invoice ' . $data->invoice->no_invoice,
      'nominal' => $data->bayar,
      'verified_at' => date('Y-m-d H:i:s'),
      'verified_by' => Auth::user()->name
    ]);
    $jurnals = [
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($transaksi->tanggal)),
        'kode_akun' => $data->kode_akun,
        'debet' => $data->bayar,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => date('Y-m-d', strtotime($transaksi->tanggal)),
        'kode_akun' => '1121',
        'debet' => 0,
        'kredit' => $data->bayar,
      ],
    ];
    Jurnal::insert($jurnals);
  }

  public static function jurnalPesananMigrasi($data)
  {
    $data = (object) $data;
    $transaksi = Transaksi::create([
      'jenis_transaksi' => Constant::JENIS_TRANSAKSI[4],
      'tanggal' => Carbon::parse($data->tanggal)->format('Y-m-d'),
      'no_ref' => '-',
      'keterangan' => 'Migrasi Pesanan ' . $data->customer_id . ' - No. Pesanan ' . $data->no_pesanan,
      'nominal' => $data->harga ?? $data->total_harga,
      'verified_at' => date('Y-m-d H:i:s'),
      'verified_by' => Auth::user()->name
    ]);
    $jurnals = [
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => Carbon::parse($data->tanggal)->format('Y-m-d'),
        'kode_akun' => '1121',
        'debet' => $transaksi->nominal,
        'kredit' => 0,
      ],
      [
        'transaksi_id' => $transaksi->id,
        'tanggal' => Carbon::parse($data->tanggal)->format('Y-m-d'),
        'kode_akun' => '4111',
        'debet' => 0,
        'kredit' => $transaksi->nominal,
      ],
    ];

    Jurnal::insert($jurnals);
  }
}
