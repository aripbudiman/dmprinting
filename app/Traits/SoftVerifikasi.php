<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait SoftVerifikasi
{
  public function scopeWithVerifiedAt(Builder $query)
  {
    return $query->whereNotNull('verified_at');
  }
}
