<?php

namespace App\Traits;

use App\Models\Outstanding;

trait UpdateOutstanding
{
  public function updateOutstanding($customer_id, $nominal, $operation = 'add')
  {
    $outstanding = Outstanding::where('customer_id', $customer_id)->first();
    if ($outstanding) {
      if ($operation == 'add') {
        $outstanding->nominal += $nominal;
      } else {
        $outstanding->nominal -= $nominal;
      }
    } else {
      $outstanding = new Outstanding();
      $outstanding->customer_id = $customer_id;
      $outstanding->nominal += $nominal;
    }
    return $outstanding->save();
  }

  public function updateOutstandingPending($customer_id, $pendingCount, $operation = 'add')
  {
    $outstanding = Outstanding::where('customer_id', $customer_id)->first();
    if ($outstanding && $operation == 'add') {
      $outstanding->pesanan_pending += $pendingCount;
    } else {
      $outstanding->pesanan_pending -= $pendingCount;
    }

    return $outstanding->save();
  }

  public function updateOutstandingPartial($customer_id, $partialCount, $operation = 'add')
  {
    $outstanding = Outstanding::where('customer_id', $customer_id)->first();
    if ($outstanding && $operation == 'add') {
      $outstanding->pesanan_partial += $partialCount;
    } else {
      $outstanding->pesanan_partial -= $partialCount;
    }

    return $outstanding->save();
  }
}
