<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class MakeColumn extends Command
{
    protected $signature = 'make:column {nama} {count}';
    protected $description = 'Create Column in table_config.php';


    public function handle()
    {
        $nama = $this->argument('nama');
        $count = $this->argument('count');

        $filePath = base_path('config/table_config.php');
        $config = require $filePath;

        if (!isset($config[$nama])) {
            $config[$nama] = [];
        }
        
        $newColumns = [];
        for ($i = 0; $i < $count; $i++) {
            $newColumns[] = [
                'label' => '',
                'data' => '',
                'class' => '',
                'fungsi' => ''
            ];
        }

        // $config[$nama] = array_merge($config[$nama], $newColumns);
        $config[$nama] = $newColumns;
        $newConfig = "<?php\n\nreturn " . var_export($config, true) . ";\n";

        File::put($filePath, $newConfig);

        $this->info('New columns added to table_config.php successfully.');
    }
}
