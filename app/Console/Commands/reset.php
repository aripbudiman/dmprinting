<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class reset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:reset {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset table';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table($this->argument('table'))->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->info('Table ' . $this->argument('table') . ' sudah direset!');
    }
}
