<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset tables';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tables = [
            'jurnal',
            'm_outstandings',
            'transaksi',
            't_invoice',
            't_invoice_detail',
            't_keranjang',
            't_pembayaran',
            't_pesanan',
            't_pesanan_cetakan',
            't_pesanan_manual',
            't_pesanan_manual_detail',
            'spk',
            'saldo_akun'
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tables as $table) {
            DB::table($table)->truncate();
            $this->info('Table ' . $table . ' has been reset.');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->info('Table sudah direset!');
    }
}
