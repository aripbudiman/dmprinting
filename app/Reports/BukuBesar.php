<?php

namespace App\Reports;

use App\Models\Akun;
use App\Models\Transaksi;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BukuBesar
{

  protected $data;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public static function getBukuBesar()
  {
    return Akun::with('jurnal')
      ->whereHas('jurnal', function ($q) {
        $q->whereBetween('tanggal', [startDate(), endDate()]);
      })
      ->get();
  }
}
