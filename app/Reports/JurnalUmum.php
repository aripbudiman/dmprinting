<?php

namespace App\Reports;

use App\Models\Transaksi;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class JurnalUmum
{
  protected $data;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public static function getJurnalUmum()
  {
    return Transaksi::with('jurnal')
      ->whereBetween('tanggal', [startDate(), endDate()])
      ->get();
  }

  public function export()
  {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $sheet->setCellValue('A1', 'LAPORAN JURNAL UMUM');
    $sheet->mergeCells('A1:E1');
    $sheet->getStyle('A1')->getFont()->setSize(14);
    $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

    $sheet->setCellValue('A2', 'PERIODE : ' . startDate('d F Y') . ' - ' . endDate('d F Y'));
    $sheet->mergeCells('A2:E2');
    $sheet->getStyle('A2')->getFont()->setSize(12);
    $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');

    $sheet->setCellValue('A4', 'Tanggal');
    $sheet->setCellValue('B4', 'Keterangan');
    $sheet->setCellValue('C4', 'Akun');
    $sheet->setCellValue('D4', 'Debet');
    $sheet->setCellValue('E4', 'Kredit');

    $sheet->getColumnDimension('A')->setWidth(20);
    $sheet->getColumnDimension('B')->setWidth(60);
    $sheet->getColumnDimension('C')->setWidth(25);
    $sheet->getColumnDimension('D')->setWidth(17);
    $sheet->getColumnDimension('E')->setWidth(17);

    $row = 5;
    $totalDebet = 0;
    $totalKredit = 0;

    foreach ($this->data as $item) {
      $sheet->setCellValue("A$row", formatDate($item->tanggal));
      $sheet->mergeCells("A$row:A" . ($row + 1));
      $sheet->getStyle("A$row:A" . ($row + 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
      $sheet->setCellValue("B$row", $item->keterangan);
      $sheet->mergeCells("B$row:B" . ($row + 1));
      $sheet->getStyle("B$row:B" . ($row + 1))->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

      foreach ($item->jurnal as $jurnal) {
        $sheet->setCellValue("C$row", $jurnal->akun->nama);
        $sheet->setCellValue("D$row", $jurnal->debet);
        $sheet->setCellValue("E$row", $jurnal->kredit);

        $totalDebet += $jurnal->debet;
        $totalKredit += $jurnal->kredit;
        $row++;
      }
    }
    $sheet->mergeCells("A$row:C$row");
    $sheet->setCellValue("A$row", 'TOTAL');
    $sheet->setCellValue("D$row", $totalDebet);
    $sheet->setCellValue("E$row", $totalKredit);

    $writer = new Xlsx($spreadsheet);
    $fileName = 'transactions.xlsx';
    $filePath = storage_path('app/' . $fileName);
    $writer->save($filePath);

    return $filePath;
  }
}
