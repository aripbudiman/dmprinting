<?php

namespace App\Reports;

use DB;
use stdClass;
use App\Models\Akun;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class NeracaSaldo
{
  protected $data;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public static function getNeracaSaldo()
  {
    $akuns = Akun::whereHas('jurnal', function ($query) {
      $query->whereBetween('tanggal', [startDate(), endDate()]);
    })
      ->withSum(['jurnal as debet' => function ($query) {
        $query->whereBetween('tanggal', [startDate(), endDate()]);
      }], 'debet')
      ->withSum(['jurnal as kredit' => function ($query) {
        $query->whereBetween('tanggal', [startDate(), endDate()]);
      }], 'kredit')
      ->get();

    return $akuns;
  }

  public function export()
  {
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $sheet->setCellValue('A1', 'LAPORAN NERACA SALDO');
    $sheet->mergeCells('A1:C1');
    $sheet->getStyle('A1')->getFont()->setSize(14);
    $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');

    $sheet->setCellValue('A2', 'PERIODE : ' . startDate('d F Y') . ' - ' . endDate('d F Y'));
    $sheet->mergeCells('A2:C2');
    $sheet->getStyle('A2')->getFont()->setSize(12);
    $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');

    $sheet->setCellValue('A4', 'Akun');
    $sheet->setCellValue('B4', 'Debet');
    $sheet->setCellValue('C4', 'Kredit');

    $sheet->getColumnDimension('A')->setWidth(60);
    $sheet->getColumnDimension('B')->setWidth(30);
    $sheet->getColumnDimension('C')->setWidth(30);

    $row = 5;
    $totalDebet = 0;
    $totalKredit = 0;

    foreach ($this->data as $item) {
      $sheet->setCellValue("A$row", $item->nama);
      $sheet->setCellValue("B$row", $item->saldo_normal == 'debet' ? ($item->debet - $item->kredit) : 0);
      $sheet->setCellValue("C$row", $item->saldo_normal == 'kredit' ? ($item->kredit - $item->debet) : 0);

      $totalDebet += $item->debet;
      $totalKredit += $item->kredit;
      $row++;
    }
    $sheet->setCellValue("A$row", 'TOTAL');
    $sheet->setCellValue("B$row", $totalDebet);
    $sheet->setCellValue("C$row", $totalKredit);

    $writer = new Xlsx($spreadsheet);
    $fileName = 'transactions.xlsx';
    $filePath = storage_path('app/' . $fileName);
    $writer->save($filePath);

    return $filePath;
  }
}
