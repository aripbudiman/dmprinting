
FROM php:8.2-fpm

COPY composer.* /var/www/laraveldocker

RUN rm -rf /var/www/laraveldocker

WORKDIR /var/www/laraveldocker

# Install required packages and Imagick dependencies
RUN apt-get update && apt-get install -y \
  build-essential \
  libmcrypt-dev \
  mariadb-client \
  libpng-dev \
  libjpeg62-turbo-dev \
  libfreetype6-dev \
  locales \
  jpegoptim optipng pngquant gifsicle \
  vim \
  nano \
  unzip \
  git \
  curl \
  libzip-dev \
  zip \
  libmagickwand-dev \
  imagemagick \
  gnupg2

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && \
  apt-get install -y nodejs

RUN docker-php-ext-install pdo pdo_mysql gd zip mysqli && \
  pecl install imagick && \
  docker-php-ext-enable imagick

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

COPY . .
COPY --chown=www:www . .

RUN composer install --no-scripts --no-interaction --prefer-dist --optimize-autoloader

RUN chown -R www:www /var/www/laraveldocker/storage \
  && chown -R www:www /var/www/laraveldocker/bootstrap/cache \
  && chmod -R 775 /var/www/laraveldocker/storage \
  && chmod -R 775 /var/www/laraveldocker/bootstrap/cache

RUN npm install
RUN npm run build

RUN php artisan config:clear \
  && php artisan route:clear \
  && php artisan view:clear \
  && php artisan cache:clear

USER www

EXPOSE 9000

CMD ["php-fpm"]
