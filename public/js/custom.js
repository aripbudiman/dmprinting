// custom alert
function alert(message = '', icon = 'info', limit = 3000) {
  let heading = ''
  if (icon == 'success') {
    heading = 'Success'
  } else if (icon == 'info') {
    heading = 'Information'
  } else if (icon == 'warning') {
    heading = 'Warning'
  } else if (icon == 'error') {
    heading = 'Error'
  }
  $.toast({
    heading: heading,
    text: message,
    icon: icon,
    position: 'top-center',
    loaderBg: '#fff',
    hideAfter: limit,
  })
}

// format rupiah
Number.prototype.rupiah = function (digit = 2) {
  return this.toLocaleString('id-ID', {
    minimumFractionDigits: digit,
    maximumFractionDigits: digit
  });
}

String.prototype.parseRupiah = function () {
  return this.replace(/\./g, '').replace(',00', '');
}

String.prototype.formatRupiah = function () {
  var number_string = this.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? "." : "";
    rupiah += separator + ribuan.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
  return rupiah;
}

/**
* membuat limit teks for javascript
* @author arip budiman
* @return string
*/
String.prototype.limit = function (limit = 10) {
  if (this.length > limit) {
    return this.substring(0, limit) + '...';
  }
  return this.substring(0, limit)
}

/**
* membuat type html khusus number tidak boleh angka
* @author arip budiman
* @return number input
*/
const inputNumeric = document.querySelectorAll('input[type="numeric"]');
inputNumeric.forEach(element => {
  element.addEventListener('keypress', function (event) {
    if (event.keyCode < 48 || event.keyCode > 57) {
      event.preventDefault();
    }
  });
  element.addEventListener('input', function (event) {
    if (event.target.value.length > 0 && isNaN(event.target.value)) {
      event.target.value = event.target.value.slice(0, -1);
    }
  });
});
$('.rupiah').on('input', function (event) {
  let rupiah = $(this).val().formatRupiah();
  $(this).val(rupiah);
})
