<?php

namespace Database\Seeders;

use App\Models\MNavbar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class NavbarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MNavbar::insert([
            [
                'title' => 'Transaksi',
                'order' => 1,
                'value' => json_encode([
                    "6", "8"
                ]),
                'is_active' => 1
            ],
            [
                'title' => 'Pengelolaan',
                'order' => 2,
                'value' => json_encode([
                    "1", "4"
                ]),
                'is_active' => 1
            ],
            [
                'title' => 'Pengaturan',
                'order' => 3,
                'value' => json_encode([
                    "5"
                ]),
                'is_active' => 1
            ]
        ]);
    }
}
