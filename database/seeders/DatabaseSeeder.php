<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            MenuCategorySeeder::class,
            MenuSeeder::class,
            MetodePembayaranSeeder::class,
            VoucherSeeder::class,
            CPesananSeeder::class,
            JenisTransaksiSeeder::class,
            NavbarSeeder::class,
            PermissionSeeder::class,
        ]);
    }
}
