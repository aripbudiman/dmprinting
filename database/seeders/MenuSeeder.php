<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Menu::insert([
            [
                'nama' => 'menu',
                'slug' => 'menu',
                'route_name' => 'menu.index',
                'controller' => 'MenuController@index',
                'namespace' => 'App\Http\Controllers\MenuController',
                'category_id' => 1
            ],
            [
                'nama' => 'list menu category',
                'slug' => 'list-menu-category',
                'route_name' => 'menu.list-menu-category.index',
                'controller' => 'MenuController@listMenuCategory',
                'namespace' => 'App\Http\Controllers\MenuController',
                'category_id' => 1
            ],
            [
                'nama' => 'Role Permission',
                'slug' => 'role-permission',
                'route_name' => 'rolepermission.index',
                'controller' => 'RolePermissionController@index',
                'namespace' => 'App\Http\Controllers\RolePermissionController',
                'category_id' => 5
            ],
            [
                'nama' => 'Pembayaran',
                'slug' => 'pembayaran',
                'route_name' => 'pembayaran.index',
                'controller' => 'PembayaranController@index',
                'namespace' => 'App\Http\Controllers\PembayaranController',
                'category_id' => 1
            ],
            [
                'nama' => 'Migrasi Data',
                'slug' => 'migrasi-data',
                'route_name' => 'migrasi.index',
                'controller' => 'MigrasiDataController@index',
                'namespace' => 'App\Http\Controllers\MigrasiDataController',
                'category_id' => 1
            ],
            [
                'nama' => 'Penerimaan Kas',
                'slug' => 'penerimaan-kas',
                'route_name' => 'penerimaan_kas.index',
                'controller' => 'PenerimaanKasController@index',
                'namespace' => 'App\Http\Controllers\PenerimaanKasController',
                'category_id' => 8
            ],
            [
                'nama' => 'Pengeluaran Kas',
                'slug' => 'pengeluaran-kas',
                'route_name' => 'pengeluaran_kas.index',
                'controller' => 'PengeluaranKasController@index',
                'namespace' => 'App\Http\Controllers\PengeluaranKasController',
                'category_id' => 8
            ],
            [
                'nama' => 'Pesanan Banner',
                'slug' => 'pesanan-banner',
                'route_name' => 'pesanan.index',
                'controller' => 'PesananController@index',
                'namespace' => 'App\Http\Controllers\PesananController',
                'category_id' => 6
            ],
            [
                'nama' => 'Pesanan Cetakan',
                'slug' => 'pesanan-cetakan',
                'route_name' => 'pesanan-cetakan.index',
                'controller' => 'PesananCetakanController@index',
                'namespace' => 'App\Http\Controllers\PesananCetakanController',
                'category_id' => 6
            ],
            [
                'nama' => 'Pembayaran',
                'slug' => 'pembayaran',
                'route_name' => 'pembayaran.index',
                'controller' => 'PembayaranController@index',
                'namespace' => 'App\Http\Controllers\PembayaranController',
                'category_id' => 6
            ],
            [
                'nama' => 'Navbar',
                'slug' => 'navbar',
                'route_name' => 'navbar.index',
                'controller' => 'MNavbarController@index',
                'namespace' => 'App\Http\Controllers\MNavbarController',
                'category_id' => 5
            ]
        ]);
    }
}
