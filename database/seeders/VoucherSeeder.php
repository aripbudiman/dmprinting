<?php

namespace Database\Seeders;

use App\Models\Voucher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 10; $i++) {
            Voucher::create([
                'kode_voucher' => 'DISKON' . $i + 1,
                'bounty' => $i + 1,
                'is_active' => 1,
                'expired_at' => '2029-12-01 23:59:59',
            ]);
        }
    }
}
