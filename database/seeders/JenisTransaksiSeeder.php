<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('jenis_transaksi')->insert([
            [
                'nama' => 'Jurnal Umum',
                'slug' => 'jurnal_umum',
            ],
            [
                'nama' => 'Modal',
                'slug' => 'modal',
            ],
            [
                'nama' => 'Penerimaan Kas',
                'slug' => 'penerimaan_kas',
            ],
            [
                'nama' => 'Pengeluaran Kas',
                'slug' => 'pengeluaran_kas',
            ],
            [
                'nama' => 'Piutang',
                'slug' => 'piutang',
            ],
            [
                'nama' => 'Hutang',
                'slug' => 'hutang',
            ],
        ]);
    }
}
