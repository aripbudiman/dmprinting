<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use App\Models\Permissions as Permission;
use App\Models\RolePermissions;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [];
        $routeNames = Route::getRoutes()->getRoutesByName();
        foreach ($routeNames as $route) {
            $action = $route->getActionName();
            $routeName = $route->getName();
            $namespace = Str::beforeLast($action, '@');
            $controller = Str::afterLast($action, '\\');
            $data[] = ['controller' => $controller, 'namespace' => $namespace, 'route_name' => $routeName];
        }
        $collection = collect($data);
        $filtered = $collection->filter(function ($item) {
            return strpos($item['namespace'], 'App\\Http\\Controllers\\') !== false;
        });
        $permissions = $filtered->values()->all();
        foreach ($permissions as $item) {
            $result = Permission::updateOrCreate(
                ['name' => $item['controller'], 'display' => $item['route_name'], 'namespace' => $item['namespace']],
                ['name' => $item['controller'], 'display' => $item['route_name'], 'namespace' => $item['namespace']]
            );
            RolePermissions::updateOrCreate(
                ['role_id' => 1, 'permission_id' => $result->id],
                ['role_id' => 1, 'permission_id' => $result->id]
            );
        }
        $filtered2 = $filtered->filter(function ($item) {
            return strpos($item['route_name'], '.index') !== false;
        });
        $permissions = $filtered2->values()->all();
        foreach ($permissions as $item) {
            $existingMenu = Menu::where('route_name', $item['route_name'])->first();
            if (!$existingMenu && $item['route_name'] != 'menu.index' && $item['route_name'] != 'menu.list-menu-category.index') {
                Menu::create([
                    'controller' => $item['controller'],
                    'route_name' => $item['route_name'],
                    'namespace' => $item['namespace']
                ]);
            }
        }
    }
}
