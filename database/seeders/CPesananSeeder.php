<?php

namespace Database\Seeders;

use App\Models\CPesanan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CPesananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CPesanan::insert([
            [
                'id' => 'harga_plate',
                'label' => 'Harga Per Plate',
                'value' => 70000,
            ],
            [
                'id' => 'harga_drag',
                'label' => 'Harga Per Drag',
                'value' => json_encode([
                    [
                        'id' => '<=3',
                        'harga' => 0,
                    ], [
                        'id' => '>3',
                        'harga' => 0,
                    ]
                ])
            ]
        ]);
    }
}
