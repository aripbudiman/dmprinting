<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("UPDATE sub_klasifikasi
        SET saldo_normal = CASE 
            WHEN id IN (111, 112, 113, 114, 115, 121, 131, 141, 511, 521, 522, 611, 612, 613, 621) THEN 'debet'
            WHEN id IN (122, 211, 212, 221, 311, 321, 411, 412, 421) THEN 'kredit'
        END
        WHERE id IN (111, 112, 113, 114, 115, 121, 122, 131, 141, 211, 212, 221, 311, 321, 411, 412, 421, 511, 521, 522, 611, 612, 613, 621)
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {}
};
