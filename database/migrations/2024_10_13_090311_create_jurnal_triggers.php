<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::unprepared("
        CREATE TRIGGER after_insert_jurnal
        AFTER INSERT ON jurnal
        FOR EACH ROW
        BEGIN
            DECLARE normal CHAR(16);
            DECLARE current_saldo_akhir DECIMAL(10,2);
            DECLARE last_saldo_akhir DECIMAL(10,2);

            SELECT saldo_normal INTO normal
            FROM akun
            WHERE id = NEW.kode_akun;

            SELECT IFNULL(saldo_akhir,0) INTO last_saldo_akhir FROM saldo_akun WHERE kode_akun = NEW.kode_akun ORDER BY id DESC LIMIT 1;

            IF last_saldo_akhir IS NULL THEN
                SET last_saldo_akhir = 0;
            END IF;

            IF normal='debet' THEN
                SET current_saldo_akhir=last_saldo_akhir+NEW.debet-NEW.kredit;
            ELSE
                SET current_saldo_akhir=last_saldo_akhir+NEW.kredit-NEW.debet;
            END IF;

            INSERT INTO saldo_akun (id,tanggal, kode_akun, debet, kredit, saldo_normal,saldo_akhir)
            VALUES (NEW.id,NEW.tanggal, NEW.kode_akun, NEW.debet, NEW.kredit, normal,current_saldo_akhir);

        END");

        DB::unprepared("
        CREATE TRIGGER after_update_jurnal
        AFTER UPDATE ON jurnal
        FOR EACH ROW
        BEGIN
            -- Deklarasi variabel
            DECLARE normal CHAR(16);
            DECLARE current_saldo_akhir DECIMAL(10,2);
            DECLARE last_saldo_akhir DECIMAL(10,2);
            DECLARE last_debet DECIMAL(10,2);
            DECLARE last_kredit DECIMAL(10,2);
            DECLARE last_saldo_normal CHAR(16);

            -- Ambil saldo_akhir terakhir dari saldo_akun berdasarkan kode_akun
            SELECT IFNULL(saldo_akhir, 0) INTO last_saldo_akhir 
            FROM saldo_akun 
            WHERE kode_akun = NEW.kode_akun 
            ORDER BY id DESC 
            LIMIT 1;

            -- Ambil nilai debet dan kredit dari saldo_akun terkait
            SELECT IFNULL(debet, 0) INTO last_debet 
            FROM saldo_akun 
            WHERE kode_akun = NEW.kode_akun 
            ORDER BY id DESC 
            LIMIT 1;

            SELECT IFNULL(kredit, 0) INTO last_kredit 
            FROM saldo_akun 
            WHERE kode_akun = NEW.kode_akun 
            ORDER BY id DESC 
            LIMIT 1;

            -- Ambil saldo_normal dari saldo_akun berdasarkan kode_akun
            SELECT saldo_normal INTO last_saldo_normal 
            FROM saldo_akun 
            WHERE kode_akun = NEW.kode_akun 
            ORDER BY id DESC 
            LIMIT 1;

            -- Hitung saldo_akhir berdasarkan saldo_normal
            IF last_saldo_normal = 'debet' THEN
                SET last_saldo_akhir = last_saldo_akhir - last_debet + last_kredit;
            ELSE
                SET last_saldo_akhir = last_saldo_akhir - last_kredit + last_debet;
            END IF;

            -- Jika tidak ada saldo_akhir, set ke 0
            IF last_saldo_akhir IS NULL THEN
                SET last_saldo_akhir = 0; 
            END IF;

            -- Hitung saldo_akhir terbaru berdasarkan debet/kredit dan saldo_normal
            IF last_saldo_normal = 'debet' THEN
                SET current_saldo_akhir = last_saldo_akhir + NEW.debet - NEW.kredit;
            ELSE
                SET current_saldo_akhir = last_saldo_akhir + NEW.kredit - NEW.debet;
            END IF;

            -- Ambil saldo_normal terbaru dari akun
            SELECT saldo_normal INTO normal
            FROM akun
            WHERE id = NEW.kode_akun;

            -- Update tabel saldo_akun
            UPDATE saldo_akun
            SET 
                tanggal = NEW.tanggal,
                kode_akun = NEW.kode_akun,
                debet = NEW.debet,
                kredit = NEW.kredit,
                saldo_normal = normal,
                saldo_akhir = current_saldo_akhir
            WHERE kode_akun = OLD.kode_akun
            ORDER BY id DESC LIMIT 1;  -- Pastikan hanya memperbarui data terakhir
        END
        ");

        DB::unprepared("
            CREATE TRIGGER `after_delete_jurnal` AFTER DELETE ON `jurnal`
            FOR EACH ROW BEGIN
                DELETE FROM saldo_akun WHERE id = OLD.id;
            END
        ");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::unprepared("DROP TRIGGER IF EXISTS after_insert_jurnal");
        DB::unprepared("DROP TRIGGER IF EXISTS after_update_jurnal");
        DB::unprepared("DROP TRIGGER IF EXISTS after_delete_jurnal");
    }
};
