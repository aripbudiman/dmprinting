<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_keranjang')) {
            Schema::create('t_keranjang', function (Blueprint $table) {
                $table->id();
                $table->foreignId('customer_id')->constrained('m_customers')->onDelete('restrict')->onUpdate('cascade');
                $table->unsignedBigInteger('pesanan_id');
                $table->string('pesanan_type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('t_keranjang')) {
            Schema::dropIfExists('t_keranjang');
        }
    }
};
