<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("INSERT INTO `m_finishing` (`id`, `deskripsi`, `harga`) VALUES
        (1, 'LIPAT PAS MATA AYAM', 0),
        (2, 'Mata Ayam per 1/2 meter', 1000),
        (3, 'Mata ayam per meter', 0),
        (4, 'Mata Ayam Pojok2', 0),
        (5, 'LEM KOREA', 1000),
        (6, 'selongsong', 1000),
        (7, 'Tanpa Finishing', 0),
        (8, 'CUTTING ', 30000),
        (9, 'TAMBAL SALAH', 5000),
        (10, 'SETTING', 25000),
        (11, 'SAMBUNG KORCIN', 5000),
        (12, 'SAMBUNG FLEXY', 3500),
        (14, 'MATA AYAM PEL 30CM', 1500),
        (15, '', 0)");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('m_finishing')->delete();
    }
};
