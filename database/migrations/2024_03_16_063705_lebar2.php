<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('m_lebar')) {
            Schema::create('m_lebar', function (Blueprint $table) {
                $table->id();
                $table->foreignId('bahan_id')->constrained('m_bahan')->onDelete('cascade')->onUpdate('cascade');
                $table->float('meter', 10, 0)->nullable(true);
                $table->decimal('harga_lebar', 10, 0)->nullable(true);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('m_lebar')) {
            Schema::dropIfExists('m_lebar');
        }
    }
};
