<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_pesanan_manual')) {
            Schema::create('t_pesanan_manual', function (Blueprint $table) {
                $table->id();
                $table->foreignId('customer_id')->constrained('m_customers')->onDelete('cascade')->onUpdate('cascade');
                $table->date('tanggal')->nullable()->default(null);
                $table->string('no_pesanan', 32)->nullable()->default(null);
                $table->decimal('total_harga', 10, 2)->nullable()->default(null);
                $table->string('status_pembayaran', 20)->nullable();
                $table->string('status_pesanan', 20)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->string('created_by', 100)->nullable();
                $table->string('updated_by', 100)->nullable();
                $table->string('deleted_by', 100)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('t_pesanan_manual')) {
            Schema::dropIfExists('t_pesanan_manual');
        }
    }
};
