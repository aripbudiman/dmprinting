<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_invoice')) {
            Schema::create('t_invoice', function (Blueprint $table) {
                $table->id();
                $table->date('tanggal')->nullable()->default(null);
                $table->foreignId('customer_id')->constrained('m_customers')->onDelete('cascade')->onUpdate('cascade');
                $table->string('no_invoice', 64)->nullable()->default(null)->unique();
                $table->decimal('subtotal', 10, 2)->nullable()->default(null);
                $table->integer('disc_percentage')->nullable()->default(null);
                $table->decimal('disc', 10, 2)->nullable()->default(null);
                $table->decimal('grand_total', 10, 2)->nullable()->default(null);
                $table->decimal('sisa', 10, 2)->nullable()->default(null);
                $table->enum('status', ['pending', 'lunas', 'partial', 'batal'])->nullable()->default(null);
                $table->string('kode_voucher', 32)->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes();
                $table->string('created_by', 100)->nullable();
                $table->string('updated_by', 100)->nullable();
                $table->string('deleted_by', 100)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('t_invoice')) {
            Schema::dropIfExists('t_invoice');
        }
    }
};
