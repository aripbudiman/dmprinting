<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('m_bahan')) {
            Schema::create('m_bahan', function (Blueprint $table) {
                $table->id();
                $table->string('kode_bahan', 10)->nullable(true);
                $table->foreignId('tipe_id')->constrained('m_tipe')->onDelete('cascade')->onUpdate('cascade');
                $table->string('nama', 150)->nullable(true);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('m_bahan')) {
            Schema::dropIfExists('m_bahan');
        }
    }
};
