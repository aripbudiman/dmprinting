<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('t_pesanan_cetakan', 'status_pembayaran')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->string('status_pembayaran')->nullable()->after('total_harga');
            });
        }
        if (!Schema::hasColumn('t_pesanan_cetakan', 'status_pesanan')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->string('status_pesanan')->nullable()->after('status_pembayaran');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('t_pesanan_cetakan', 'status_pembayaran')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->dropColumn('status_pembayaran');
            });
        }
        if (Schema::hasColumn('t_pesanan_cetakan', 'status_pesanan')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->dropColumn('status_pesanan');
            });
        }
    }
};
