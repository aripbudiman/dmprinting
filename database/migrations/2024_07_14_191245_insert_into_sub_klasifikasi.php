<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("INSERT INTO sub_klasifikasi (id, nama, klasifikasi_id) VALUES
            ('111', 'KAS & BANK', '1'),
            ('112', 'PIUTANG', '1'),
            ('113', 'PERSEDIAAN', '1'),
            ('114', 'PERLENGKAPAN', '1'),
            ('115', 'PEMBAYARAN DIMUKA', '1'),
            ('121', 'ASET TETAP', '2'),
            ('122', 'AKUMULASI PENYUSUTAN', '2'),
            ('131', 'ASET TAK BERWUJUD', '3'),
            ('141', 'INVESTASI', '4'),
            ('211', 'HUTANG', '5'),
            ('212', 'PENDAPATAN DITERIMA DIMUKA', '5'),
            ('221', 'HUTANG JK PANJANG', '6'),
            ('311', 'MODAL', '7'),
            ('321', 'PRIVE', '8'),
            ('411', 'PENDAPATAN USAHA', '9'),
            ('412', 'RETUR DAN POT.PENJUALAN', '9'),
            ('421', 'PENDAPATAN NON USAHA', '10'),
            ('511', 'HPP', '11'),
            ('521', 'PEMBELIAN', '12'),
            ('522', 'RETUR DAN POT.PEMBELIAN', '12'),
            ('611', 'BIAYA ADMINISTRASI DAN UMUM', '13'),
            ('612', 'BIAYA UMUM LAINNYA', '13'),
            ('613', 'BIAYA PENYUSUTAN', '13'),
            ('621', 'BEBAN NON USAHA', '14')");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
