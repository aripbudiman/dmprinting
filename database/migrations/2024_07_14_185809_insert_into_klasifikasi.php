<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("INSERT INTO klasifikasi (id, nama) VALUES
            ('1', 'ASET LANCAR'),
            ('2', 'ASET TETAP'),
            ('3', 'ASET TAK BERWUJUD'),
            ('4', 'INVESTASI'),
            ('5', 'HUTANG LANCAR'),
            ('6', 'HUTANG JK PANJANG'),
            ('7', 'MODAL'),
            ('8', 'PRIVE'),
            ('9', 'PENDAPATAN'),
            ('10', 'PENDAPATAN NON USAHA'),
            ('11', 'HPP'),
            ('12', 'PEMBELIAN'),
            ('13', 'BEBAN USAHA'),
            ('14', 'BEBAN NON USAHA')");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
