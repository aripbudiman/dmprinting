<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('saldo_akun')) {
            Schema::create('saldo_akun', function (Blueprint $table) {
                $table->id();
                $table->date('tanggal')->nullable()->default(null);
                $table->unsignedBigInteger('kode_akun');
                $table->foreign('kode_akun')->references('id')->on('akun')->onDelete('cascade')->onUpdate('cascade');
                $table->decimal('debet', 10, 2)->nullable()->default(null);
                $table->decimal('kredit', 10, 2)->nullable()->default(null);
                $table->decimal('saldo_akhir', 10, 2)->nullable()->default(null);
                $table->char('saldo_normal', 16)->nullable()->default(null);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('saldo_akun')) {
            Schema::dropIfExists('saldo_akun');
        }
    }
};
