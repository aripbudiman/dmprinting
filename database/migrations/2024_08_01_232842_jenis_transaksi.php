<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('jenis_transaksi')) {
            Schema::create('jenis_transaksi', function (Blueprint $table) {
                $table->id();
                $table->string('nama', 64)->default(null)->nullable(true);
                $table->string('slug', 64)->default(null)->nullable(true);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('jenis_transaksi')) {
            Schema::dropIfExists('jenis_transaksi');
        }
    }
};
