<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('t_pesanan_cetakan', 'no_pesanan')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->string('no_pesanan')->nullable()->unique()->after('tanggal');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('t_pesanan_cetakan', 'no_pesanan')) {
            Schema::table('t_pesanan_cetakan', function (Blueprint $table) {
                $table->dropColumn('no_pesanan');
            });
        }
    }
};
