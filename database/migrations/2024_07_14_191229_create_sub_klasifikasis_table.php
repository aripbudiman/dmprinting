<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sub_klasifikasi', function (Blueprint $table) {
            $table->char('id', 8)->primary();
            $table->string('nama', 128)->nullable()->default(null);
            $table->foreignId('klasifikasi_id')->constrained('klasifikasi')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sub_klasifikasi');
    }
};
