<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('menu')) {
            Schema::create('menu', function (Blueprint $table) {
                $table->id();
                $table->foreignId('category_id')->nullable(true)->default(null)->references('id')->on('menu_categories')->onDelete('set null')->onUpdate('cascade');
                $table->string('nama', 100)->nullable(true)->default(null);
                $table->string('slug', 100)->nullable(true)->default(null);
                $table->string('controller', 100)->nullable(true)->default(null);
                $table->string('route_name', 100)->nullable(true)->default(null);
                $table->string('namespace', 100)->nullable(true)->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('menu')) {
            Schema::dropIfExists('menu');
        }
    }
};
