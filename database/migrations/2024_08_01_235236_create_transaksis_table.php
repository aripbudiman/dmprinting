<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('transaksi')) {
            Schema::create('transaksi', function (Blueprint $table) {
                $table->id();
                $table->string('jenis_transaksi', 128)->default(null)->nullable(true);
                $table->date('tanggal')->nullable()->default(null);
                $table->char('no_ref', 16)->nullable()->default(null);
                $table->text('keterangan')->nullable()->default(null);
                $table->decimal('nominal', 10, 2)->nullable()->default(null);
                $table->char('voucher', 16)->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes();
                $table->char('created_by', 64)->nullable();
                $table->char('updated_by', 64)->nullable();
                $table->char('deleted_by', 64)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('transaksi')) {
            Schema::dropIfExists('transaksi');
        }
    }
};
