<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('t_pembayaran', function (Blueprint $table) {
            $table->id();
            $table->string('no_pembayaran', 32)->nullable()->default(null);
            $table->date('tanggal')->nullable()->default(null);
            $table->foreignId('invoice_id')->constrained('t_invoice')->onDelete('cascade')->onUpdate('cascade');
            $table->string('metode_pembayaran', 128)->nullable()->default(null);
            $table->decimal('total_uang', 10, 2)->nullable()->default(null);
            $table->decimal('bayar', 10, 2)->nullable()->default(null);
            $table->decimal('kembalian', 10, 2)->nullable()->default(null);
            $table->string('status', 64)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by', 100)->nullable();
            $table->string('updated_by', 100)->nullable();
            $table->string('deleted_by', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('t_pembayaran');
    }
};
