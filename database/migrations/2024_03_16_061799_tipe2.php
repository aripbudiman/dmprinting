<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('m_tipe')) {
            Schema::create('m_tipe', function (Blueprint $table) {
                $table->id();
                $table->string('nama', 50)->nullable(true);
                $table->double('harga_tipe', 10, 0)->nullable(true);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('m_tipe')) {
            Schema::dropIfExists('m_tipe');
        }
    }
};
