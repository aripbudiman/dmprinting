<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("INSERT INTO `m_tipe` (`id`, `nama`, `harga_tipe`) VALUES
        (1, 'OUTDOOR SOLVENT', 0),
        (2, 'INDOOR UV GEL', 0),
        (4, 'INDOOR UV GEL WHITE', 0),
        (5, 'CUTTING + MASKING', 0),
        (6, 'A3+', 0),
        (7, 'POLOSAN', 0),
        (8, 'uv gel', 0),
        (9, 'brosur', 0),
        (10, 'INDOOR', 0)");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('m_tipe')->delete();
    }
};
