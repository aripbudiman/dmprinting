<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (
            !Schema::hasColumn('transaksi', 'verifikasi') &&
            !Schema::hasColumn('transaksi', 'verified_at') &&
            !Schema::hasColumn('transaksi', 'verified_by')
        ) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->char('verifikasi', 32)->after('voucher')->nullable()->default(null);
                $table->timestamp('verified_at')->after('deleted_at')->nullable()->default(null);
                $table->char('verified_by', 32)->after('deleted_by')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (
            Schema::hasColumn('transaksi', 'verifikasi') &&
            Schema::hasColumn('transaksi', 'verified_at') &&
            Schema::hasColumn('transaksi', 'verified_by')
        ) {
            Schema::table('transaksi', function (Blueprint $table) {
                $table->dropColumn('verifikasi');
                $table->dropColumn('verified_at');
                $table->dropColumn('verified_by');
            });
        }
    }
};
