<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('akun')) {
            Schema::create('akun', function (Blueprint $table) {
                $table->char('id', 8)->primary();
                $table->char('subklasifikasi_id', 8)->nullable();
                $table->string('nama', 128)->nullable()->default(null);
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('subklasifikasi_id')->references('id')->on('sub_klasifikasi')->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('akun')) {
            Schema::dropIfExists('akun');
        }
    }
};
