]<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration
    {
        /**
         * Run the migrations.
         */
        public function up(): void
        {
            if (!Schema::hasColumn('t_pesanan', 'tanggal')) {
                Schema::table('t_pesanan', function (Blueprint $table) {
                    $table->date('tanggal')->nullable()->default(null)->after('id');
                });
            }
        }

        /**
         * Reverse the migrations.
         */
        public function down(): void
        {
            if (Schema::hasColumn('t_pesanan', 'tanggal')) {
                Schema::table('t_pesanan', function (Blueprint $table) {
                    $table->dropColumn('tanggal');
                });
            }
        }
    };
