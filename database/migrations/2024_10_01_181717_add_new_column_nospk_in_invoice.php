<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('t_invoice', 'no_spk')) {
            Schema::table('t_invoice', function (Blueprint $table) {
                $table->char('no_spk', 16)->nullable()->default(null)->after('no_invoice');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('t_invoice', 'no_spk')) {
            Schema::table('t_invoice', function (Blueprint $table) {
                $table->dropColumn('no_spk');
            });
        }
    }
};
