<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('spk')) {
            Schema::create('spk', function (Blueprint $table) {
                $table->id();
                $table->foreignId('customer_id')->constrained('m_customers')->onDelete('cascade')->onUpdate('cascade');
                $table->unsignedBigInteger('pesanan_id');
                $table->string('pesanan_type');
                $table->string('no_spk')->nullable()->default(null);
                $table->char('status', 4)->nullable()->default(null);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('spk')) {
            Schema::dropIfExists('spk');
        }
    }
};
