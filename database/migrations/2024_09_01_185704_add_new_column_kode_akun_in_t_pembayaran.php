<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('t_pembayaran', 'kode_akun')) {
            Schema::table('t_pembayaran', function (Blueprint $table) {
                $table->string('kode_akun')->after('status')->nullable()->default(null);
                $table->foreign('kode_akun')->references('id')->on('akun')->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('t_pembayaran', 'kode_akun')) {
            Schema::table('t_pembayaran', function (Blueprint $table) {
                $table->dropForeign(['kode_akun']);
                $table->dropColumn('kode_akun');
            });
        }
    }
};
