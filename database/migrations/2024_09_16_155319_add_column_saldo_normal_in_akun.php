<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasColumn('sub_klasifikasi', 'saldo_normal')) {
            Schema::table('sub_klasifikasi', function (Blueprint $table) {
                $table->char('saldo_normal')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasColumn('sub_klasifikasi', 'saldo_normal')) {
            Schema::table('sub_klasifikasi', function (Blueprint $table) {
                $table->dropColumn('saldo_normal');
            });
        }
    }
};
