<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    // public function up(): void
    // {
    //     if (!Schema::hasTable('t_tmp_pesanan2')) {
    //         Schema::create('t_tmp_pesanan2', function (Blueprint $table) {
    //             $table->bigIncrements('id');
    //             $table->string('no_pesanan', 20)->nullable(true);
    //             $table->string('status', 20)->nullable(true);
    //             $table->timestamps();
    //             $table->softDeletes();
    //         });
    //     }
    // }

    /**
     * Reverse the migrations.
     */
    // public function down(): void
    // {
    //     if (Schema::hasTable('t_tmp_pesanan2')) {
    //         Schema::dropIfExists('t_tmp_pesanan2');
    //     }
    // }
};
