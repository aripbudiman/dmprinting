<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_pesanan_manual_detail')) {
            Schema::create('t_pesanan_manual_detail', function (Blueprint $table) {
                $table->id();
                $table->foreignId('pesanan_manual_id')->references('id')->on('t_pesanan_manual')->onDelete('cascade')->onUpdate('cascade');
                $table->text('nama_cetakan')->nullable()->default(null);
                $table->integer('qty')->nullable()->default(null);
                $table->decimal('harga_satuan', 10, 2)->nullable()->default(null);
                $table->decimal('total_harga', 10, 2)->nullable()->default(null);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('PesananManualDetail', function (Blueprint $table) {
            //
        });
    }
};
