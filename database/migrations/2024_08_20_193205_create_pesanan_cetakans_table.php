<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_pesanan_cetakan')) {
            Schema::create('t_pesanan_cetakan', function (Blueprint $table) {
                $table->id();
                $table->date('tanggal')->nullable();
                $table->foreignId('customer_id')->constrained('m_customers')->onDelete('restrict');
                $table->text('nama_cetakan')->nullable()->default(null);
                $table->integer('jumlah_cetakan')->default(0);
                $table->integer('jumlah_drag')->default(0);
                $table->string('jenis_cetak', 28);
                $table->integer('jumlah_plate');
                $table->decimal('harga_per_plate', 10, 0);
                $table->decimal('harga_per_drag', 10, 0);
                $table->decimal('total_harga_perdrag', 10, 0);
                $table->decimal('total_harga', 10, 0);
                $table->timestamps();
                $table->softDeletes();
                $table->char('created_by', 36)->nullable();
                $table->char('updated_by', 36)->nullable();
                $table->char('deleted_by', 36)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('t_pesanan_cetakan')) {
            Schema::dropIfExists('t_pesanan_cetakan');
        }
    }
};
