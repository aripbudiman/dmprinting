<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('t_invoice_detail')) {
            Schema::create('t_invoice_detail', function (Blueprint $table) {
                $table->id();
                $table->foreignId('invoice_id')->constrained('t_invoice')->onDelete('cascade')->onUpdate('cascade');
                $table->unsignedBigInteger('pesanan_id');
                $table->string('pesanan_type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('t_invoice_detail')) {
            Schema::dropIfExists('t_invoice_detail');
        }
    }
};
