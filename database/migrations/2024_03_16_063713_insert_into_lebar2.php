<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::insert("INSERT INTO `m_lebar` (`id`, `bahan_id`, `meter`, `harga_lebar`) VALUES
        (1, 1, 1, 15000),
        (2, 1, 1.5, 22500),
        (3, 1, 2, 30000),
        (4, 1, 3, 45000),
        (5, 2, 1, 17000),
        (6, 2, 1.5, 25500),
        (7, 2, 2, 34000),
        (8, 2, 3, 51000),
        (9, 3, 1, 21000),
        (19, 3, 1.5, 31500),
        (20, 3, 2, 42000),
        (21, 3, 3, 63000),
        (22, 4, 1, 40000),
        (23, 4, 1.5, 60000),
        (24, 4, 2, 40000),
        (25, 4, 3, 120000),
        (26, 6, 1, 50000),
        (27, 6, 1.5, 75000),
        (28, 6, 2, 100000),
        (29, 6, 3, 150000),
        (30, 7, 1.2, 70000),
        (31, 8, 1.2, 60000),
        (32, 8, 1.5, 75000),
        (33, 9, 1, 55000),
        (34, 10, 1, 60000),
        (35, 21, 1, 55000),
        (36, 21, 1.5, 82500),
        (37, 22, 1, 60000),
        (38, 22, 1.5, 90000),
        (39, 23, 1.2, 40000),
        (40, 24, 1.2, 84000),
        (41, 25, 1.2, 110000),
        (42, 26, 1.6, 65000),
        (43, 27, 1.6, 80000),
        (44, 28, 1.6, 75000),
        (45, 29, 1.6, 90000),
        (46, 30, 1.6, 170000),
        (47, 30, 2, 185000),
        (48, 31, 1.6, 190000),
        (49, 31, 2, 210000),
        (50, 32, 1.6, 210000),
        (51, 32, 2, 230000),
        (52, 33, 60, 3000),
        (53, 34, 1.6, 60000),
        (54, 35, 1, 45000),
        (55, 35, 1.5, 67500),
        (56, 35, 2, 90000),
        (57, 35, 3, 135000),
        (58, 37, 1, 25000),
        (59, 38, 1.2, 70000),
        (60, 38, 1.5, 82500),
        (61, 8, 1, 50000),
        (62, 42, 1, 70000),
        (63, 42, 2, 110000),
        (64, 44, 1, 3000),
        (65, 45, 1, 10000),
        (66, 48, 1, 400000),
        (67, 50, 1, 15000),
        (68, 51, 1, 450000),
        (69, 52, 1, 200000),
        (70, 36, 1, 23500),
        (71, 36, 1.5, 35250),
        (72, 36, 2, 47000),
        (73, 36, 3, 70500),
        (74, 53, 1, 30000),
        (75, 54, 1, 18000),
        (76, 54, 1.5, 27000),
        (77, 54, 2, 36000),
        (78, 54, 3, 54000),
        (79, 55, 1, 2500),
        (80, 56, 1, 28000),
        (81, 56, 2, 38000),
        (82, 57, 1, 45000),
        (83, 57, 2, 90000),
        (84, 57, 3, 135000),
        (85, 58, 1, 60000),
        (86, 58, 1.5, 90000),
        (87, 58, 2, 120000),
        (88, 58, 3, 180000),
        (89, 59, 1, 70000),
        (90, 59, 1.2, 88000),
        (91, 59, 1.5, 115000),
        (92, 60, 1, 85000),
        (93, 60, 1.2, 102000),
        (94, 61, 1.5, 145000),
        (95, 61, 1.7, 167000),
        (96, 61, 2, 200000),
        (97, 61, 3, 310000),
        (98, 61, 1, 90000),
        (99, 61, 5, 530000),
        (100, 9, 1.2, 66000),
        (102, 57, 1.5, 67500),
        (103, 62, 1, 100000),
        (104, 62, 1.5, 160000),
        (105, 62, 2, 220000),
        (106, 62, 3, 340000),
        (107, 62, 5, 580000),
        (108, 63, 1, 60000),
        (109, 63, 1, 40000),
        (110, 63, 1, 40000),
        (111, 65, 1, 15000),
        (112, 65, 1.5, 22500),
        (113, 65, 2, 30000),
        (114, 65, 3, 45000),
        (115, 66, 1, 17000),
        (116, 66, 1.5, 25500),
        (117, 66, 2, 34000),
        (118, 66, 3, 51000),
        (119, 67, 1, 21000),
        (120, 67, 1.5, 31500),
        (121, 67, 2, 42000),
        (122, 67, 3, 63000),
        (123, 68, 1, 40000),
        (125, 68, 1.5, 60000),
        (126, 68, 2, 80000),
        (129, 68, 3, 120000),
        (130, 69, 1, 50000),
        (131, 69, 1.5, 75000),
        (132, 69, 2, 100000),
        (133, 69, 3, 150000),
        (138, 70, 1, 90000),
        (139, 70, 1.5, 135000),
        (140, 70, 2, 180000),
        (141, 70, 3, 270000),
        (142, 71, 1, 100000),
        (143, 71, 1.5, 150000),
        (144, 71, 2, 200000),
        (145, 71, 3, 300000),
        (146, 72, 1.2, 42000),
        (147, 73, 1, 50000),
        (148, 73, 1.2, 60000),
        (149, 73, 1.5, 75000),
        (150, 74, 1, 55000),
        (151, 74, 1.2, 66000),
        (152, 74, 1.5, 82500),
        (159, 77, 1, 65000),
        (160, 77, 1.2, 78000),
        (161, 77, 1.5, 97500),
        (162, 80, 1.6, 60000),
        (163, 81, 1.6, 70000),
        (165, 83, 1.6, 165000),
        (166, 82, 1.6, 80000),
        (167, 83, 2, 210000),
        (168, 84, 1.6, 175000),
        (169, 84, 2, 220000),
        (170, 85, 1.6, 180000),
        (171, 85, 2, 230000),
        (172, 75, 1, 65000),
        (173, 75, 1.2, 78000),
        (174, 75, 1.5, 97500),
        (175, 76, 1, 70000),
        (176, 76, 1.2, 84000),
        (177, 76, 1.5, 105000),
        (178, 86, 1, 50000),
        (179, 86, 1.5, 75000),
        (180, 86, 2, 100000),
        (181, 86, 3, 150000),
        (182, 87, 1, 55000),
        (183, 87, 1.5, 82500),
        (184, 87, 2, 110000),
        (185, 87, 3, 165000),
        (186, 88, 1, 75000),
        (187, 88, 1.5, 112500),
        (188, 88, 2, 150000),
        (189, 88, 3, 225000),
        (190, 89, 1, 85000),
        (191, 89, 1.5, 127500),
        (192, 89, 2, 170000),
        (193, 89, 3, 255000),
        (194, 90, 1, 125000),
        (195, 90, 1.5, 187500),
        (196, 90, 2, 250000),
        (197, 90, 3, 375000),
        (198, 91, 1, 135000),
        (199, 91, 1.5, 202500),
        (200, 91, 2, 270000),
        (201, 91, 3, 405000),
        (202, 92, 1.2, 84000),
        (203, 114, 1, 45000),
        (204, 114, 1.5, 67500),
        (205, 114, 2, 90000),
        (206, 114, 3, 135000),
        (207, 115, 1, 55000),
        (208, 115, 1.5, 82500),
        (209, 115, 2, 110000),
        (210, 115, 3, 165000),
        (211, 93, 1, 90000),
        (212, 93, 1.2, 108000),
        (213, 93, 1.5, 135000),
        (214, 94, 1, 95000),
        (215, 94, 1.2, 114000),
        (216, 94, 1.5, 142500),
        (217, 95, 1, 110000),
        (218, 95, 1.2, 132000),
        (219, 95, 1.5, 165000),
        (220, 96, 1, 115000),
        (221, 96, 1.2, 138000),
        (222, 96, 1.5, 172500),
        (223, 97, 1, 110000),
        (224, 97, 1.2, 132000),
        (225, 97, 1.5, 165000),
        (226, 98, 1.6, 120000),
        (227, 99, 1.6, 125000),
        (228, 100, 1.6, 135000),
        (229, 101, 1.6, 150000),
        (230, 102, 1.6, 165000),
        (231, 103, 1.6, 225000),
        (232, 103, 2, 280000),
        (233, 104, 1.6, 230000),
        (234, 104, 2, 285000),
        (235, 105, 1.6, 240000),
        (236, 105, 2, 300000),
        (237, 106, 1.6, 250000),
        (238, 106, 2, 325000),
        (239, 107, 1.6, 265000),
        (240, 107, 2, 340000),
        (241, 108, 1, 138000),
        (242, 108, 1.2, 165600),
        (243, 110, 1, 90000),
        (244, 110, 1.2, 108000),
        (245, 110, 1.5, 135000),
        (246, 116, 1, 225000),
        (247, 116, 1.6, 360000),
        (248, 109, 1, 60000),
        (249, 109, 1.2, 72000),
        (250, 109, 1.5, 90000),
        (251, 111, 1, 95000),
        (252, 111, 2, 175000),
        (253, 117, 1, 235000),
        (254, 117, 2, 315000),
        (255, 118, 1, 140000),
        (256, 119, 1, 25000),
        (257, 120, 1, 90000),
        (258, 119, 2, 15000),
        (259, 121, 1.5, 180000),
        (260, 122, 1.2, 144000),
        (261, 123, 1.5, 375000),
        (262, 124, 1.2, 300000),
        (263, 125, 1, 30000),
        (264, 126, 1, 50000),
        (265, 127, 1.5, 67500),
        (267, 128, 1, 500000),
        (268, 129, 1, 65000),
        (269, 129, 1.5, 97500),
        (270, 130, 1, 10000),
        (271, 131, 1, 75000)");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('m_lebar')->delete();
    }
};
