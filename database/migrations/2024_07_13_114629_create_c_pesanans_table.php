<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('c_pesanan', function (Blueprint $table) {
            $table->char('id', 32)->primary();
            $table->text('label')->nullable()->default(null);
            $table->text('value')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by', 32)->nullable();
            $table->string('updated_by', 32)->nullable();
            $table->string('deleted_by', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('c_pesanan');
    }
};
