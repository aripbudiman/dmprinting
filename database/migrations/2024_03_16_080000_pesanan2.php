<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('t_pesanan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->string('no_pesanan', 100)->unique()->nullable();
            $table->text('nama_cetakan')->nullable();
            $table->unsignedBigInteger('tipe_id');
            $table->unsignedBigInteger('bahan_id');
            $table->unsignedBigInteger('lebar_id');
            $table->unsignedBigInteger('finishing_id');
            $table->double('panjang', 20, 2)->nullable();
            $table->integer('qty')->nullable()->autoIncrement(false); // Menghapus auto_increment sebagai kunci utama
            $table->decimal('harga', 10, 0)->nullable();
            $table->string('status_pembayaran', 20)->nullable();
            $table->string('status_pesanan', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('created_by', 100)->nullable();
            $table->string('updated_by', 100)->nullable();
            $table->string('deleted_by', 100)->nullable();
            $table->foreign('customer_id')->references('id')->on('m_customers');
            $table->foreign('tipe_id')->references('id')->on('m_tipe');
            $table->foreign('bahan_id')->references('id')->on('m_bahan');
            $table->foreign('lebar_id')->references('id')->on('m_lebar');
            $table->foreign('finishing_id')->references('id')->on('m_finishing');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('t_pesanan');
    }
};
