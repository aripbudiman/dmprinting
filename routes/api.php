<?php

use App\Http\Controllers\API\addToSpkController;
use App\Http\Controllers\API\ApplyVoucherController;
use App\Http\Controllers\API\DeleteSpkController;
use App\Http\Controllers\API\getPesananController;
use App\Http\Controllers\API\ListTagihanController;
use App\Http\Controllers\API\LoadSpkController;
use App\Http\Controllers\API\PaynowController;
use App\Http\Controllers\API\PiutangController;
use App\Http\Controllers\API\ScreenshootController;
use App\Http\Controllers\API\SendWAController;
use App\Http\Controllers\API\StoreSpkController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware(['web', 'auth'])->group(function () {
    Route::post('pesanan/digitalprinting', [getPesananController::class, 'digitalPrinting'])->name('pesanan.digitalprinting');
    Route::post('add_to_spk', [addToSpkController::class, 'store'])->name('add_to_spk');
    Route::get('load_spk/{customer_id}/{status}', LoadSpkController::class)->name('load_spk');
    Route::delete('delete_spk/{id}', DeleteSpkController::class)->name('delete_spk');
    Route::post('store_spk', StoreSpkController::class)->name('store_spk');
    Route::get('invoice', ListTagihanController::class)->name('invoice');
    Route::post('apply_voucher', ApplyVoucherController::class)->name('apply_voucher');
    Route::post('paynow', PaynowController::class)->name('paynow');
    Route::get('piutang', PiutangController::class)->name('piutang');
    Route::post('send_wa', SendWAController::class)->name('send_wa');
    Route::post('screenshoot', ScreenshootController::class)->name('screenshoot');
});
