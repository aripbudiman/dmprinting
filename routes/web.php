<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AkunController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\DmvTipeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\MNavbarController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\PiutangController;
use App\Http\Controllers\CPesananController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DmvBahanController;
use App\Http\Controllers\DmvLebarController;
use App\Http\Controllers\TerminalController;
use App\Http\Controllers\JurnalUmumController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\MigrasiDataController;
use App\Http\Controllers\DmvFinishingController;
use App\Http\Controllers\PenerimaanKasController;
use App\Http\Controllers\PesananManualController;
use App\Http\Controllers\PengeluaranKasController;
use App\Http\Controllers\PesananCetakanController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\LaporanKeuanganController;
use App\Http\Controllers\MetodePembayaranController;
use App\Http\Controllers\PembayaranCetakanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

// terminal
Route::group(['middleware' => ['auth', 'permission']], function () {
    Route::get('terminal', [TerminalController::class, 'index'])->name('terminal.index');
    Route::get('config-cache', [TerminalController::class, 'configCache'])->name('terminal.config-cache');
    Route::get('route-cache', [TerminalController::class, 'routeCache'])->name('terminal.route-cache');
    Route::get('route-list', [TerminalController::class, 'routeList'])->name('terminal.route-list');
    Route::get('route-clear', [TerminalController::class, 'routeClear'])->name('terminal.route-clear');
    Route::get('clear-cache', [TerminalController::class, 'clearCache'])->name('terminal.clear-cache');
    Route::get('clear-view', [TerminalController::class, 'clearView'])->name('terminal.clear-view');
    Route::get('clear-config', [TerminalController::class, 'clearConfig'])->name('terminal.clear-config');
});

Route::group(['middleware' => ['auth', 'permission']], function () {
    Route::get('rolepermission', [RolePermissionController::class, 'index'])->name('rolepermission.index');
    Route::get('rolepermission/create', [RolePermissionController::class, 'create'])->name('rolepermission.create');
    Route::post('roles/store', [RolePermissionController::class, 'storeRoles'])->name('roles.store');
    Route::post('roles/{roles?}', [RolePermissionController::class, 'delete'])->name('roles.delete');
    Route::get('rolepermission/settingrolepermission/{id?}', [RolePermissionController::class, 'settingrolepermission'])->name('rolepermission.settingrolepermission');
    Route::post('rolepermission/store/{id?}', [RolePermissionController::class, 'store'])->name('rolepermission.store');
    Route::get('rolepermission/listpengguna', [RolePermissionController::class, 'listpengguna'])->name('rolepermission.listpengguna');
    Route::get('menu', [MenuController::class, 'index'])->name('menu.index');
    Route::get('menu/create', [MenuController::class, 'create'])->name('menu.create');
    Route::get('menu/{menu}/edit', [MenuController::class, 'edit'])->name('menu.edit');
    Route::get('menu/{menu}', [MenuController::class, 'show'])->name('menu.show');
    Route::post('menu/{menu}/delete', [MenuController::class, 'destroy'])->name('menu.delete');
    Route::post('menu', [MenuController::class, 'store'])->name('menu.store');
    Route::post('menu/{menu}/update', [MenuController::class, 'update'])->name('menu.update');
    Route::get('list-menu-category', [MenuController::class, 'listMenuCategory'])->name('menu.list-menu-category.index');
    Route::get('menucategory/create', [MenuController::class, 'createCategoryMenu'])->name('menu.list-menu-category.create');
    Route::get('menucategory/{menucategory}/edit', [MenuController::class, 'editCategoryMenu'])->name('menu.list-menu-category.edit');
    Route::post('menu-category', [MenuController::class, 'storeCategoryMenu'])->name('menu.list-menu-category.store');
    Route::post('menu-category/{menucategory}/delete', [MenuController::class, 'destroyCategoryMenu'])->name('menu.list-menu-category.delete');
    Route::post('pembayaran/bayar', [PembayaranController::class, 'bayar'])->name('pembayaran.bayar');
});

Route::group(['middleware' => ['auth', 'permission'], 'prefix' => 'dmv'], function () {
    // dmv tipe
    Route::get('tipe', [DmvTipeController::class, 'index'])->name('dmv.tipe.index');
    Route::get('tipe/create', [DmvTipeController::class, 'create'])->name('dmv.tipe.create');
    Route::post('tipe', [DmvTipeController::class, 'store'])->name('dmv.tipe.store');
    Route::get('tipe/{tipe}/edit', [DmvTipeController::class, 'edit'])->name('dmv.tipe.edit');
    Route::post('tipe/{tipe}/update', [DmvTipeController::class, 'update'])->name('dmv.tipe.update');
    Route::post('tipe/{tipe}', [DmvTipeController::class, 'delete'])->name('dmv.tipe.delete');

    // dmv bahan
    Route::get('bahan', [DmvBahanController::class, 'index'])->name('dmv.bahan.index');
    Route::get('bahan/create', [DmvBahanController::class, 'create'])->name('dmv.bahan.create');
    Route::post('bahan', [DmvBahanController::class, 'store'])->name('dmv.bahan.store');
    Route::get('bahan/{bahan}/edit', [DmvBahanController::class, 'edit'])->name('dmv.bahan.edit');
    Route::get('bahan/{bahan}/show', [DmvBahanController::class, 'show'])->name('dmv.bahan.show');
    Route::post('bahan/{bahan}/update', [DmvBahanController::class, 'update'])->name('dmv.bahan.update');
    Route::post('bahan/{bahan}', [DmvBahanController::class, 'destroy'])->name('dmv.bahan.delete');

    // dmv lebar
    Route::get('lebar', [DmvLebarController::class, 'index'])->name('dmv.lebar');

    // dmv finishing
    Route::get('finishing', [DmvFinishingController::class, 'index'])->name('dmv.finishing');
    Route::get('finishing/create', [DmvFinishingController::class, 'create'])->name('dmv.finishing.create');
    Route::post('finishing', [DmvFinishingController::class, 'store'])->name('dmv.finishing.store');
    Route::get('finishing/{finishing}/edit', [DmvFinishingController::class, 'edit'])->name('dmv.finishing.edit');

    // dmv  pesanan
    Route::get('pesanan', [PesananController::class, 'index'])->name('pesanan.index');
    Route::get('pesanan/create', [PesananController::class, 'create'])->name('pesanan.create');
    Route::post('pesanan/store', [PesananController::class, 'store'])->name('pesanan.store');
    Route::post('pesanan/{pesanan}/update', [PesananController::class, 'update'])->name('pesanan.update');
    Route::get('pesanan/{pesanan}/show', [PesananController::class, 'show'])->name('pesanan.show');
    Route::get('pesanan/{pesanan}/edit', [PesananController::class, 'edit'])->name('pesanan.edit');
    // Route::get('/pesanan/create-cetakan', [PesananController::class, 'createCetakan'])->name('pesanan.create-cetakan');

    // dmv cetakan
    Route::resource('pesanan-cetakan', PesananCetakanController::class);

    // dmv pembayaran
    Route::get('pembayaran', [PembayaranController::class, 'index'])->name('pembayaran.index');
    Route::get('pembayaran/{customer}', [PembayaranController::class, 'pembayaran'])->name('pembayaran.detail');
    Route::get('load-table-customer', [PembayaranController::class, 'tableCustomer'])->name('pembayaran.load-table-customer');
    Route::get('pembayaran/get-customer-html', [PembayaranController::class, 'getCustomersHtml'])->name('pembayaran.get-customer-html');
    Route::post('clear-keranjang', [PembayaranController::class, 'clearKeranjang'])->name('pembayaran.clear-keranjang');
    Route::post('invoice', [InvoiceController::class, 'store'])->name('invoice.store');
    Route::delete('pembayaran/{keranjang}', [PembayaranController::class, 'hapusKeranjang'])->name('keranjang.destroy');
    Route::get('pembayaran/struk/{invoice}', [PembayaranController::class, 'struk'])->name('pembayaran.struk');
    Route::get('spk/{invoice}', [PembayaranController::class, 'spk'])->name('spk');
    Route::post('pembayaran/angsuran', [PembayaranController::class, 'bayarAngsuranInvoice'])->name('pembayaran.angsuran');

    // migrasi data
    Route::get('migrasi', [MigrasiDataController::class, 'index'])->name('migrasi.index');

    // Navbar
    Route::resource('navbar', MNavbarController::class);
    Route::post('isactive', [MNavbarController::class, 'isActive'])->name('isactive');
});

Route::group(['middleware' => ['auth', 'permission']], function () {
    // konfigurasi pesanan
    Route::get('/cpesanan', [CPesananController::class, 'index'])->name('cpesanan.index');
    Route::post('/cpesanan/drag', [CPesananController::class, 'drag'])->name('cpesanan.drag');
    Route::post('/cpesanan/plate', [CPesananController::class, 'plate'])->name('cpesanan.plate');

    // akun
    Route::get('/akun', [AkunController::class, 'index'])->name('akun.index');
    Route::post('/akun', [AkunController::class, 'store'])->name('akun.store');
    Route::post('/mapping_saldo_normal', [AkunController::class, 'mappingSaldoNormal'])->name('akun.mapping_saldo_normal');
    Route::get('subklasifikasi', [AkunController::class, 'subklasifikasi'])->name('akun.subklasifikasi');
    Route::post('mappingjenis', [AkunController::class, 'mappingJenis'])->name('akun.mapping_jenis');
});
// jurnal umum
Route::resource('jurnal_umum', JurnalUmumController::class)->middleware(['auth', 'permission']);

// penerimaan kas
Route::resource('penerimaan_kas', PenerimaanKasController::class)->middleware(['auth', 'permission']);
Route::resource('pengeluaran_kas', PengeluaranKasController::class)->middleware(['auth', 'permission']);

// piutang
Route::resource('piutang', PiutangController::class);

// metode pembayaran
Route::get('metode_pembayaran', [MetodePembayaranController::class, 'index'])->name('metode-pembayaran.index')->middleware(['auth', 'permission']);

// Pesanan Manual
Route::resource('pesanan_manual', PesananManualController::class)->middleware(['auth', 'permission']);

// customers
Route::resource('customer', CustomerController::class)->middleware(['auth', 'permission']);

// Payment
Route::get('payment/{payment}', [PaymentController::class, 'payment'])->name('payment');
Route::get('payment_method', [PaymentController::class, 'paymentMethod'])->name('payment_method');

// Laporan
Route::group(['middleware' => ['auth', 'permission'], 'prefix' => 'laporan'], function () {
    Route::get('jurnalumum', [LaporanKeuanganController::class, 'jurnalumum'])->name('laporan.jurnalumum');
    Route::get('jurnalumum/export', [LaporanKeuanganController::class, 'export'])->name('laporan.jurnalumum.export');
    Route::get('bukubesar', [LaporanKeuanganController::class, 'bukubesar'])->name('laporan.bukubesar');
    Route::get('neracasaldo', [LaporanKeuanganController::class, 'neracasaldo'])->name('laporan.neracasaldo');
    Route::get('labarugi', [LaporanKeuanganController::class, 'labarugi'])->name('laporan.labarugi');
    Route::get('neraca', [LaporanKeuanganController::class, 'neraca'])->name('laporan.neraca');
});
Route::group(['middleware' => ['auth', 'permission']], function () {
    Route::get('invoice/create', [InvoiceController::class, 'create'])->name('invoice.create');
    Route::get('invoice', [InvoiceController::class, 'indexInvoice'])->name('invoice.index');
    Route::get('invoice/{id?}', [InvoiceController::class, 'show'])->name('invoice.show');
    Route::get('spk/{id?}/show', [InvoiceController::class, 'showSpk'])->name('spk.show');
    Route::get('spk/{id?}/print', [InvoiceController::class, 'printSpk'])->name('cetak.spk');
});

Route::get('invoice/n/{invoice}/customer', [InvoiceController::class, 'globalInvoice'])->name('global.invoice');

require __DIR__ . '/auth.php';
