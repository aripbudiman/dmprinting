import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
const { addDynamicIconSelectors } = require('@iconify/tailwind');

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
        "./node_modules/flowbite/**/*.js",
        'node_modules/preline/dist/*.js',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
                robot: ['Roboto', ...defaultTheme.fontFamily.sans],
            },

            colors: {
                'primary': '#00a8ff',
                'secondary': '#ff00ff',
            },
        }
    },

    plugins: [
        require('flowbite/plugin'),
        // require('preline/plugin'),
        addDynamicIconSelectors(),
        forms
    ],

    darkMode: 'class',
};
