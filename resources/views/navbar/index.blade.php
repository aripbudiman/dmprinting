@extends('layouts.main')
@section('konten')
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-7xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">List Navbar</h1>
            <button class="btn btn-md btn-indigo" onclick="window.location.href='{{ route('navbar.create') }}'">Buat
                Navlist</button>
        </div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg px-5">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-3">
                            No
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Title
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Order
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Value
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($navbar as $key => $item)
                        <tr
                            class="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-gray-800 border-b dark:border-gray-700">
                            <th scope="row"
                                class="px-6 py-2 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $key + 1 }}
                            </th>
                            <td class="px-6 py-2">
                                {{ $item->title }}
                            </td>
                            <td class="px-6 py-2">
                                {{ $item->order }}
                            </td>
                            <td class="px-6 py-2">
                                <ul class="space-y-2">
                                    @foreach ($item->menuCategories() as $key => $value)
                                        <li><span
                                                class="bg-blue-100 text-blue-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">{{ $value->nama }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </td>
                            <td class="px-6 py-2 flex items-start space-x-2">
                                <a href="{{ route('navbar.edit', $item->id) }}" class="btn btn-sm btn-emerald">Edit</a>
                                <form action="{{ route('navbar.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-rose">Delete</button>
                                </form>
                                <label class="inline-flex items-center mb-5 cursor-pointer">
                                    <input type="checkbox" value="{{ $item->is_active ? 0 : 1 }}" class="sr-only peer"
                                        {{ $item->is_active ? 'checked' : '' }}
                                        onclick="isActive(this.value,{{ $item->id }})">
                                    <div
                                        class="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-green-600">
                                    </div>
                                </label>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        function isActive(value, item_id) {
            $.ajax({
                type: "POST",
                url: "{{ route('isactive') }}",
                data: {
                    'is_active': value,
                    'id': item_id,
                    _token: "{{ csrf_token() }}"
                },
                dataType: "JSON",
                success: function(response) {
                    if (response.status) {
                        alert(response.message)
                        setTimeout(() => {
                            window.location.reload()
                        }, 3000);
                    }
                }
            });
        }
    </script>
@endpush
