<div class="contianer flex space-x-5">
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">Buat Menu Baru</h1>
        </div>
        <form id="form-menu" class="p-5 flex flex-col gap-3">
            @csrf
            @if (isset($navbar))
                @method('PUT')
            @endif
            <div class="mb-1 relative">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white  required">Title</label>
                <input type="text" id="title" name="title" class="form-input" placeholder="Title"
                    value="{{ isset($navbar) ? $navbar->title : '' }}">
            </div>
            <div class="mb-1 relative">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white  required">Order</label>
                <input type="text" id="order" name="order" class="form-input" placeholder="Order"
                    value="{{ isset($navbar) ? $navbar->order : '' }}">
            </div>
            <h3 class="text-lg font-medium text-gray-900 dark:text-white">Menu:</h3>
            <ul class="grid w-full gap-3 md:grid-cols-2">
                @forelse ($menu as $key => $item)
                    <li>
                        <input type="checkbox" id="{{ $item->id }}" value="{{ $item->id }}" name="value[]"
                            class="hidden peer"
                            {{ isset($navbar) && in_array($item->id, $navbar->value) ? 'checked' : '' }}>
                        <label for="{{ $item->id }}"
                            class="inline-flex items-center justify-between w-full px-5 py-3 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">
                            <div class="flex items-center space-x-3">
                                <x-icon-laravel />
                                <div class="w-full text-lg font-semibold">{{ $item->nama }}</div>
                            </div>
                        </label>
                    </li>
                @empty
                @endforelse
            </ul>
            <div class="flex justify-end border-t pt-3 space-x-3">
                <button type="submit" class="btn btn-indigo">{{ isset($navbar) ? 'Update' : 'Simpan' }}</button>
                <button type="button" onclick="window.location.href='{{ route('navbar.index') }}'"
                    class="btn btn-rose">Kembali</button>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $('#form-menu').submit(function(e) {
            e.preventDefault();
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ $url }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status) {
                        alert(response.message, 'success')
                        setTimeout(() => {
                            window.location.reload()
                        }, 2000);
                    }
                },
                error: function(xhr, status, error) {
                    console.log(error)
                }
            });
        });
    </script>
@endpush
