@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.jurnal_umum')" :tr="'show'" :button="'edit'">
        <x-slot name="header">
            <x-header-table :title="$title">
                <div class="space-x-2">
                    <button onclick="_tambah()" class="btn btn-md rn btn-indigo">
                        Tambah Pengeluaran Kas
                    </button>
                </div>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$payload['show']" />
            <x-search :keyword="$payload['keyword']" />
        </x-slot>
    </x-table>
    @include('jurnalkhusus._form', [
        'label_debet' => 'Untuk Keperluan',
        'label_kredit' => 'Dikeluarkan dari',
    ])
@endsection
@push('scripts')
    <script>
        let modal;
        $(document).ready(function() {
            sessionStorage.setItem('route', "{{ $route }}")
            sessionStorage.setItem('method', "{{ $method }}")
            const targetModal = document.getElementById('modal-tambah-penerimaan-kas');
            const options = {
                placement: 'center',
                backdrop: 'dynamic',
                backdropClasses: 'bg-gray-800/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                closable: true,
                onHide: () => {
                    defaultValue()
                },
                onShow: () => {
                    defaultValue()
                    targetModal.classList.add('open');
                    if (editData) {
                        const debetAkun = editData.jurnal[0].kode_akun;
                        const kreditAkun = editData.jurnal[1].kode_akun;
                        $('.simpan-ke[name="debet"]').val(debetAkun).trigger('change');
                        $('.simpan-ke[name="kredit"]').val(kreditAkun).trigger('change');
                        $('[name="nominal"]').val(parseInt(editData.nominal).rupiah(0));
                        $('[name="keterangan"]').val(editData.keterangan);
                        $('[name="tanggal"]').val(moment(editData.tanggal).format('DD-MM-YYYY'));
                        $('[name="no_ref"]').val(editData.no_ref);
                    } else {
                        sessionStorage.setItem('route', "{{ $route }}")
                        sessionStorage.setItem('method', "{{ $method }}")
                        defaultValue()
                    }
                },
                onToggle: () => {
                    console.log('modal has been toggled');
                },
            };

            modal = new Modal(targetModal, options);
        });

        function _edit(id, el) {
            fetch(`{{ route('pengeluaran_kas.edit', ':id') }}`.replace(':id', id))
                .then(response => response.json())
                .then(data => {
                    editData = data.data;
                    sessionStorage.setItem('route', data.route);
                    sessionStorage.setItem('method', data.method);
                    modal.show();
                })
                .catch(error => {
                    console.error('Error fetching data:', error);
                });
        }


        function _tambah() {
            editData = null
            modal.show()
        }

        function _close() {
            modal.hide()
        }

        function defaultValue() {
            $('.simpan-ke[name="debet"]').val('').trigger('change');
            $('.simpan-ke[name="kredit"]').val('').trigger('change');
            $('[name="nominal"]').val('');
            $('[name="keterangan"]').val('');
            $('[name="tanggal"]').val('');
            $('[name="no_ref"]').val('-');
        }
    </script>
@endpush
