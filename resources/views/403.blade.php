<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Forbidden</title>
  @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
  <div class="flex justify-center flex-col items-center w-full bg-blue-700" style="height: 100vh;">
    <img src="{{ asset('image/403.png') }}" width="400px">
    <a href="{{ url('/') }}"  class="p-5 btn btn-sm -mt-28">Kembali</a>
  </div>
</body>
</html>