@extends('layouts.main')
@section('konten')
    <div class="relative overflow-x-auto shadow-md rounded-md max-w-5xl">
        <table id="table" class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-3 py-3">
                        Nama
                    </th>
                    <th scope="col" class="px-3 py-3">
                        Icon
                    </th>
                    <th scope="col" class="px-3 py-3">
                        Value
                    </th>
                    <th scope="col" class="px-3 py-3">
                        Is Active
                    </th>
                    <th scope="col" class="px-3 py-3">
                        Mapping Akun
                    </th>
                    <th scope="col" class="px-3 py-3">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ($data as $item)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row" class="px-3 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $item->nama }}
                        </th>
                        <td class="px-3 py-4">
                            {!! $item->icon !!}
                        </td>
                        <td class="px-3 py-4">
                            {{ $item->value }}
                        </td>
                        <td class="px-3 py-4">
                            <label class="inline-flex items-center cursor-pointer">
                                <input type="checkbox" value="{{ $item->is_active }}" class="sr-only peer"
                                    id="toggleSwitch{{ $item->id }}" {{ $item->is_active == 1 ? 'checked' : '' }}>
                                <div
                                    class="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-700">
                                </div>
                            </label>
                        </td>
                        <td class="px-3 py-4">
                            <input type="text" id="nama_akun" class="nama_akun form-input max-w-[200px] py-2"
                                value="{{ $item->akun->nama ?? '' }}">
                            <input type="text" name="kode_akun" id="kode_akun"
                                class="kode_akun form-input max-w-[200px] py-2 hidden" value="{{ $item->kode_akun ?? '' }}">
                        </td>
                        <td class="px-3 py-4">
                            <button class="btn btn-md btn-indigo" onclick="simpan({{ $item }},this)">Simpan</button>
                        </td>
                    </tr>
                @empty
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            document.querySelectorAll('.peer').forEach(toggle => {
                toggle.addEventListener('change', function() {
                    if (this.checked) {
                        isActive({
                            'id': this.id.replace('toggleSwitch', ''),
                            'is_active': 1
                        });
                    } else {
                        isActive({
                            'id': this.id.replace('toggleSwitch', ''),
                            'is_active': 0
                        });
                    }
                });
            });
        });

        function isActive(value) {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'id': value.id,
                    'is_active': value.is_active,
                    'toggle': 1
                },
                success: function(response) {
                    if (response.status == 'success') {
                        alert(response.message, 'success', 1500);
                        setTimeout(() => {
                            window.location.reload()
                        }, 1500);
                    }
                }
            });
        }

        $('.nama_akun').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ Request::url() }}",
                    dataType: "json",
                    data: {
                        'autocomplete': 1,
                        'search': request.term
                    },
                    success: function(data) {
                        response(data.data);
                    }
                });
            },
            select: function(event, ui) {
                $(this).val(ui.item.label);
                $(this).next('input').val(ui.item.value);
                return false;
            }
        })

        function simpan(value, el) {
            const kode_akun = $(el).closest('.bg-white').find('.kode_akun').val();
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'id': value.id,
                    'mapping': true,
                    'kode_akun': kode_akun
                },
                success: function(response) {
                    if (response.status == 'success') {
                        alert(response.message, 'success', 1500);
                        setTimeout(() => {
                            window.location.reload()
                        }, 1500);
                    }
                }
            });
        }
    </script>
@endpush
