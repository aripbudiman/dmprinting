<html lang="en" class="bg-gray-100 text-gray-800">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SPK</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        @media print {

            @page {
                size: 75mm auto;
                margin: 0;
            }

            body {
                padding: 14px;
            }
        }
    </style>
    <script type="text/javascript">
        function print_win() {
            window.print();
            setTimeout(() => {
                window.close();
            }, 500);
        }
    </script>
</head>
@php
    $hasPesanan = $data->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\Pesanan';
    });
    $hasPesananCetakan = $data->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\PesananCetakan';
    });
    $hasPesananManual = $data->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\PesananManual';
    });
@endphp

<body onload="print_win()" class="w-[75mm] bg-white text-xs pl-[5mm] pr-4 relative overflow-hidden">
    <header class="flex justify-center flex-col items-center space-y-3 pt-3">
        <img src="{{ asset('image/LOGO BARU.png') }}" alt="logo" class="w-12">
        <h1 class="font-bold text-xl">Surat Perintah Kerja</h1>
        <p class="text-sm text-center">Jl. Veteran No.68, RT.005/RW.003, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa
            Barat 17141</p>
    </header>
    <section id="informasi" class="mt-5 space-y-1 overflow-hidden">
        <p>====================================================</p>
        <ul class="flex justify-between">
            <li>No SPK</li>
            <li>{{ $data[0]->no_spk }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Tanggal</li>
            <li>{{ formatDate($data[0]->created_at) }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Customer</li>
            <li>{{ $data[0]->customer->nama }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Kasir</li>
            <li>{{ Auth::check() ? Auth::user()->name : $data[0]->invoice->created_by }}</li>
        </ul>
        <p>============================================================</p>
    </section>
    <section id="detail-pesanan" class="text-xs mt-3 space-y-3 border-b-2 pb-5 border-dashed border-gray-400">
        @foreach ($data as $item)
            @if ($item->pesanan_type == 'App\Models\Pesanan' && $hasPesanan)
                <div class="space-y-0.5">
                    <p class="text-justify text-[11px] font-medium">
                        {{ $item->pesanan->nama_cetakan }} @ {{ $item->pesanan->bahan->nama }}</p>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">{{ $item->pesanan->panjang }} X {{ $item->pesanan->lebar->meter }}
                            Meter
                        </li>
                    </ul>
                </div>
            @endif
        @endforeach
        @foreach ($data as $item)
            @if ($item->pesanan_type == 'App\Models\PesananCetakan' && $hasPesananCetakan)
                <div class="space-y-0.5">
                    <p class="text-justify text-[11px] font-medium">
                        {{ $item->pesanan->nama_cetakan }} @ {{ $item->pesanan->jumlah_cetakan }}ctk @
                        {{ $item->pesanan->jumlah_drag }}drag @ {{ $item->pesanan->jenis_cetak }}
                    </p>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">
                            {{ $item->pesanan->jumlah_plate }} Plate
                        </li>
                    </ul>
                </div>
            @endif
        @endforeach
        @foreach ($data as $item)
            @if ($item->pesanan_type == 'App\Models\PesananManual' && $hasPesananManual)
                @foreach ($item->pesanan->pesananmanualdetail as $value)
                    <div class="space-y-0.5">
                        <p class="text-justify text-[11px] font-medium">
                            {{ $value->nama_cetakan }} @ {{ $value->qty }}qty
                        </p>
                    </div>
                @endforeach
            @endif
        @endforeach
    </section>
    <section class="hidden">
        <p class="text-[10px] text-center pt-1">Terimakasih sudah menggunakan jasa kami!</p>
        <p class="text-xs text-center pt-1">Surat Perintah Kerja, silahkan scan QR-Code di bawah ini:</p>
        <div class="flex justify-center mt-2">
            {{-- <img src="{{ $qrCode }}" alt="QR Code"> --}}
        </div>
    </section>
</body>

</html>
