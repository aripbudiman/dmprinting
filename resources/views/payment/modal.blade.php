<!-- Main modal -->
<div id="modal-invoice" tabindex="-1" aria-hidden="true"
    class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 w-full max-w-4xl max-h-full">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <div class="space-y-4">
                <div class="max-w-[85rem]">
                    <div class=" mx-auto">
                        <div class="flex flex-col p-4 sm:p-10 bg-white shadow-md rounded-xl dark:bg-neutral-800">
                            <div class="flex justify-between">
                                <div>
                                    <img src="{{ asset('image/LOGO BARU.png') }}" alt="logo" class="w-32">

                                    <h1
                                        class="mt-2 text-lg md:text-xl font-semibold text-blue-600 dark:text-white hidden">
                                        Dmprinting.</h1>
                                </div>

                                <div class="text-end">
                                    <h2 class="text-2xl md:text-3xl font-semibold text-gray-800 dark:text-neutral-200">
                                        Struk #</h2>
                                    <span class="mt-1 block text-gray-500 dark:text-neutral-500"
                                        v-html="struk?.no_pembayaran"></span>

                                    <address class="mt-4 not-italic text-gray-800 dark:text-neutral-200 w-52">
                                        Jl. Veteran No.68, RT.005/RW.003, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa
                                        Barat 17141
                                    </address>
                                </div>
                            </div>

                            <div class="mt-8 grid sm:grid-cols-2 gap-3">
                                <div>
                                    <h3 class="text-lg font-semibold text-gray-800 dark:text-neutral-200">Customer:</h3>
                                    <h3 class="text-lg font-semibold text-gray-800 dark:text-neutral-200"
                                        v-html="struk?.invoice?.customer?.nama">
                                    </h3>
                                </div>

                                <div class="sm:text-end space-y-2">
                                    <div class="grid grid-cols-2 sm:grid-cols-1 gap-3 sm:gap-2">
                                        <dl class="grid sm:grid-cols-5 gap-x-3">
                                            <dt class="col-span-3 font-semibold text-gray-800 dark:text-neutral-200">
                                                Tanggal:</dt>
                                            <dd class="col-span-2 text-gray-500 dark:text-neutral-500"
                                                v-html="struk?.tanggal"></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-6">
                                <div
                                    class="border border-gray-200 px-4 pb-2 rounded-lg space-y-4 dark:border-neutral-700 max-h-72 overflow-auto no-scrollbar">
                                    <div
                                        class="hidden sm:grid sm:grid-cols-5 bg-white sticky top-0 py-4 border-b border-gray-200">
                                        <div
                                            class="sm:col-span-2 text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                            Item</div>
                                        <div
                                            class="text-start text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                            Qty</div>
                                        <div
                                            class="text-start text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                            Rate</div>
                                        <div
                                            class="text-end text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                            Amount</div>
                                    </div>

                                    <div class="grid grid-cols-3 sm:grid-cols-5 gap-2"
                                        v-for="item in struk?.invoice?.invoicedetail">
                                        <div class="col-span-full sm:col-span-2">
                                            <h5
                                                class="sm:hidden text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                                Item</h5>
                                            <p class="font-medium text-gray-800 dark:text-neutral-200"
                                                v-html="item?.pesanan?.no_pesanan">
                                            </p>
                                        </div>
                                        <div>
                                            <h5
                                                class="sm:hidden text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                                Qty</h5>
                                            <p class="text-gray-800 dark:text-neutral-200">1</p>
                                        </div>
                                        <div>
                                            <h5
                                                class="sm:hidden text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                                Rate</h5>
                                            <p class="text-gray-800 dark:text-neutral-200">5</p>
                                        </div>
                                        <div>
                                            <h5
                                                class="sm:hidden text-xs font-medium text-gray-500 uppercase dark:text-neutral-500">
                                                Amount</h5>
                                            <p class="sm:text-end text-gray-800 dark:text-neutral-200">
                                                @{{ new Intl.NumberFormat('id-ID').format(item?.pesanan?.harga ?? item?.pesanan?.total_harga) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-8 flex sm:justify-end">
                                <div class="w-full max-w-2xl sm:text-end space-y-2">
                                    <div class="grid grid-cols-2 sm:grid-cols-1 gap-3 sm:gap-2">
                                        <dl class="grid sm:grid-cols-5 gap-x-3">
                                            <dt class="col-span-3 font-semibold text-gray-800 dark:text-neutral-200">
                                                Subtotal:</dt>
                                            <dd class="col-span-2 text-gray-500 dark:text-neutral-500">
                                                @{{ new Intl.NumberFormat('id-ID').format(struk?.invoice?.subtotal) }}</dd>
                                        </dl>

                                        <dl class="grid sm:grid-cols-5 gap-x-3">
                                            <dt class="col-span-3 font-semibold text-gray-800 dark:text-neutral-200">
                                                Diskon:</dt>
                                            <dd class="col-span-2 text-gray-500 dark:text-neutral-500">
                                                @{{ new Intl.NumberFormat('id-ID').format(struk?.invoice?.disc) }}</dd>
                                        </dl>

                                        <dl class="grid sm:grid-cols-5 gap-x-3">
                                            <dt class="col-span-3 font-semibold text-gray-800 dark:text-neutral-200">
                                                Nominal Bayar:</dt>
                                            <dd class="col-span-2 text-gray-500 dark:text-neutral-500">
                                                @{{ new Intl.NumberFormat('id-ID').format(struk?.bayar) }}</dd>
                                        </dl>

                                        <dl class="grid sm:grid-cols-5 gap-x-3">
                                            <dt class="col-span-3 font-semibold text-gray-800 dark:text-neutral-200">
                                                Sisa Tagihan:</dt>
                                            <dd class="col-span-2 text-gray-500 dark:text-neutral-500">
                                                @{{ new Intl.NumberFormat('id-ID').format(struk?.invoice?.sisa) }}</dd>
                                        </dl>
                                    </div>
                                    <button class="btn btn-indigo mt-3" @click="printStruk">Print Struk</button>
                                </div>
                            </div>

                            <div class="mt-8 sm:mt-12">
                                <h4 class="text-lg font-semibold text-gray-800 dark:text-neutral-200">Terimakasih!</h4>
                                <p class="text-gray-500 dark:text-neutral-500">Jika ada pertanyaan silahkan hubungi
                                    informasi kontak kami:</p>
                                <div class="mt-2">
                                    <p class="block text-sm font-medium text-gray-800 dark:text-neutral-200">
                                        example@site.com</p>
                                    <p class="block text-sm font-medium text-gray-800 dark:text-neutral-200">+1 (062)
                                        109-9222</p>
                                </div>
                            </div>

                            <p class="mt-5 text-sm text-gray-500 dark:text-neutral-500">© 2024 Dmprinting.</p>
                            <div class="flex justify-end relative">
                                <button class="btn rounded-full absolute -top-5 -right-5"
                                    @click="modal.hide()">X</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
