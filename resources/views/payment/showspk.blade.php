@extends('layouts.main')
@section('konten')
    @php
        $hasPesanan = $data->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\Pesanan';
        });
        $hasPesananCetakan = $data->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\PesananCetakan';
        });
        $hasPesananManual = $data->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\PesananManual';
        });
    @endphp
    <div id="app">
        <div class="container">
            <div class="bg-gray-300 border border-gray-400/60 rounded-t-md flex justify-between p-3 items-center">
                <h1 class="text-xl font-medium text-gray-800">Detail SPK</h1>
                <a href="{{ url('/invoice') }}" class="btn btn-lg btn-rose"><x-icon-back />Kembali</a>
            </div>
            <div class="bg-white p-5 border-x border-gray-400/60">
                <div class="flex justify-between items-center">
                    <div class="grid grid-cols-4 gap-5 w-1/2">
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">No
                                SPK</label>
                            <input type="text"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="otomatis" value="{{ $data[0]->no_spk }}" readonly>
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama
                                Customer</label>
                            <input type="text" id="customer" value="{{ $data[0]->customer->nama }}" readonly
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <input type="hidden" id="customer_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kasir</label>
                            <input type="text" value="{{ Auth::user()->name }}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                readonly>
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Tanggal</label>
                            <input type="text" value="{{ formatDate($data[0]->created_at) }}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                readonly>
                        </div>
                    </div>
                </div>
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        @if ($hasPesanan)
                            <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        No Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Qty
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Harga
                                    </th>
                                </tr>
                            </thead>
                        @endif
                        <tbody>
                            @foreach ($data as $key => $item)
                                @if ($item->pesanan_type == 'App\Models\Pesanan')
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->no_pesanan }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->nama_cetakan }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->qty }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ rupiah($item->pesanan->harga) }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                        @if ($hasPesananCetakan)
                            <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        No Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Jumlah Cetak
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Harga
                                    </th>
                                </tr>
                            </thead>
                        @endif
                        <tbody>
                            @foreach ($data as $item)
                                @if ($item->pesanan_type == 'App\Models\PesananCetakan')
                                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->no_pesanan }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->nama_cetakan }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ $item->pesanan->jumlah_cetakan }}
                                        </td>
                                        <td class="px-6 py-2.5">
                                            {{ rupiah($item->pesanan->total_harga) }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                        @if ($hasPesananManual)
                            <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        No Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Pesanan
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Qty
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Harga
                                    </th>
                                </tr>
                            </thead>
                        @endif
                        <tbody>
                            @foreach ($data as $key => $item)
                                @if ($item->pesanan_type == 'App\Models\PesananManual')
                                    @foreach ($item->pesanan->pesananmanualdetail as $value)
                                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                            <td class="px-6 py-2.5">
                                                {{ $item->pesanan->no_pesanan }}
                                            </td>
                                            <td class="px-6 py-2.5">
                                                {{ $value->nama_cetakan }}
                                            </td>
                                            <td class="px-6 py-2.5">
                                                {{ $value->qty }}
                                            </td>
                                            <td class="px-6 py-2.5">
                                                {{ rupiah($value->total_harga) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="border border-gray-400/60 bg-white p-3 flex justify-center space-x-3 rounded-b-md">
                <button class="btn btn-lg btn-emerald" @click="printSpk"><x-icon-spk />Print SPK</button>
                <button class="btn btn-lg btn-rose" @click="invoice"> <x-icon-invoice />Invoice</button>
                <button class="btn btn-lg btn-indigo" @click="payment"><x-icon-card-credit /> Lanjut Ke Pembayaran</button>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        import {
            createApp,
            ref,
            reactive
        } from '/js/vue3.5.11.js'

        createApp({
            data() {
                return {
                    url_spk: @json($url_spk),
                    url_invoice: @json($url_invoice),
                    url_payment: @json($url_payment)
                }
            },
            methods: {
                printSpk() {
                    var width = 1000;
                    var height = 600;
                    var top = 100;
                    var left = 300;

                    window.open(this.url_spk, '_blank',
                        `top=${top},left=${left},height=${height},width=${width},scrollbars=yes,status=yes`);
                },
                invoice() {
                    window.location.href = this.url_invoice
                },
                payment() {
                    window.location.href = this.url_payment
                }
            },
            mounted() {

            },
            computed: {

            },
            setup() {

            }

        }).mount('#app')
    </script>
@endpush
