@php
    $hasPesanan = $data->invoiceDetail->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\Pesanan';
    });
    $hasCetakan = $data->invoiceDetail->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\PesananCetakan';
    });
    $hasManual = $data->invoiceDetail->contains(function ($item) {
        return $item->pesanan_type === 'App\Models\PesananManual';
    });
@endphp
<html lang="en" class="bg-gray-100 text-gray-800">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Customer</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <style>
        .h-200 {
            height: 200px
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    <div id="app" class="max-w-full mx-auto xl:max-w-lg">
        <div class="flex justify-center items-center h-200">
            <img src="{{ asset('image/LOGO BARU.svg') }}" class="w-[100px]">
        </div>
        <div class="flex flex-col justify-center items-center space-y-1">
            <h1 class="text-4xl font-bold text-gray-600">DMPRINTING</h1>
            <p class="text-center text-gray-500 font-robot">Jl. Veteran No.68, RT.005/RW.003, Marga Jaya, Kec. Bekasi
                Sel., Kota
                Bks, Jawa
                Barat 17141</p>
            <div class="flex justify-center space-x-16 pt-2 font-robot">
                <p class="flex items-center font-medium"><svg xmlns="http://www.w3.org/2000/svg" width="1em"
                        height="1em" viewBox="0 0 16 16">
                        <path fill="currentColor" fill-rule="evenodd"
                            d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.68.68 0 0 0 .178.643l2.457 2.457a.68.68 0 0 0 .644.178l2.189-.547a1.75 1.75 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.6 18.6 0 0 1-7.01-4.42a18.6 18.6 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877z" />
                    </svg> Telp</p>
                <p class="flex items-center font-medium"><svg xmlns="http://www.w3.org/2000/svg" width="1em"
                        height="1em" viewBox="0 0 20 20">
                        <path fill="currentColor"
                            d="M16.8 5.7C14.4 2 9.5.9 5.7 3.2C2 5.5.8 10.5 3.2 14.2l.2.3l-.8 3l3-.8l.3.2c1.3.7 2.7 1.1 4.1 1.1c1.5 0 3-.4 4.3-1.2c3.7-2.4 4.8-7.3 2.5-11.1m-2.1 7.7c-.4.6-.9 1-1.6 1.1c-.4 0-.9.2-2.9-.6c-1.7-.8-3.1-2.1-4.1-3.6c-.6-.7-.9-1.6-1-2.5c0-.8.3-1.5.8-2q.3-.3.6-.3H7c.2 0 .4 0 .5.4c.2.5.7 1.7.7 1.8c.1.1.1.3 0 .4c.1.2 0 .4-.1.5s-.2.3-.3.4c-.2.1-.3.3-.2.5c.4.6.9 1.2 1.4 1.7c.6.5 1.2.9 1.9 1.2c.2.1.4.1.5-.1s.6-.7.8-.9s.3-.2.5-.1l1.6.8c.2.1.4.2.5.3c.1.3.1.7-.1 1" />
                    </svg> WhatsApp</p>
                <p class="flex items-center  font-medium"><svg xmlns="http://www.w3.org/2000/svg" width="1em"
                        height="1em" viewBox="0 0 512 512">
                        <path fill="currentColor"
                            d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4c32.7 12.3 69 19.4 107.4 19.4c141.4 0 256-93.1 256-208S397.4 32 256 32M128.2 304H116c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h12.3c6 0 10.4-3.5 10.4-6.6c0-1.3-.8-2.7-2.1-3.8l-21.9-18.8c-8.5-7.2-13.3-17.5-13.3-28.1c0-21.3 19-38.6 42.4-38.6H156c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8h-12.3c-6 0-10.4 3.5-10.4 6.6c0 1.3.8 2.7 2.1 3.8l21.9 18.8c8.5 7.2 13.3 17.5 13.3 28.1c.1 21.3-19 38.6-42.4 38.6m191.8-8c0 4.4-3.6 8-8 8h-16c-4.4 0-8-3.6-8-8v-68.2l-24.8 55.8c-2.9 5.9-11.4 5.9-14.3 0L224 227.8V296c0 4.4-3.6 8-8 8h-16c-4.4 0-8-3.6-8-8V192c0-8.8 7.2-16 16-16h16c6.1 0 11.6 3.4 14.3 8.8l17.7 35.4l17.7-35.4c2.7-5.4 8.3-8.8 14.3-8.8h16c8.8 0 16 7.2 16 16zm48.3 8H356c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h12.3c6 0 10.4-3.5 10.4-6.6c0-1.3-.8-2.7-2.1-3.8l-21.9-18.8c-8.5-7.2-13.3-17.5-13.3-28.1c0-21.3 19-38.6 42.4-38.6H396c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8h-12.3c-6 0-10.4 3.5-10.4 6.6c0 1.3.8 2.7 2.1 3.8l21.9 18.8c8.5 7.2 13.3 17.5 13.3 28.1c.1 21.3-18.9 38.6-42.3 38.6" />
                    </svg> SMS</p>
            </div>
        </div>
        <div id="qrcode" class=" py-5 bg-white mx-3 rounded-lg mt-5">
            <div class="flex justify-center items-center flex-col pb-3">
                <h2 class="text-lg font-medium font-robot mb-2">{{ $data->customer->nama }}</h2>
                {!! QrCode::size(200)->generate(Request::fullUrl()) !!}
                <p class="text-xl font-robot mt-2">{{ $data->no_invoice }}</p>
                <span
                    class="bg-gray-100 text-gray-800 text-lg me-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">{{ $data->customer->member }}</span>
            </div>
            <hr class="mx-5">
            <div class="px-5 pt-5">
                <p class="text-sm font-medium font-robot">No HP:</p>
                <p class="text-sm  font-robot">{{ $data->customer->no_hp ?? '-' }}</p>
            </div>
        </div>

        <div id="accordion-collapse" data-accordion="collapse" class="mx-3 mt-4 bg-white rounded-lg focus:rounded-lg">
            <h2 id="accordion-collapse-heading-2" @click="toggleSentuh">
                <button type="button"
                    class="flex flex-col justify-between bg-white w-full p-5 font-semibold rtl:text-right rounded-lg text-[#111827] focus:bg-white focus:rounded-lg"
                    data-accordion-target="#detail-pesanan" aria-expanded="false" aria-controls="detail-pesanan">
                    <div class="flex justify-between items-center w-full">
                        <span class="font-robot text-2xl">Detail Pesanan</span>
                        <span
                            class="inline-flex items-center justify-center gap-1 rounded-full bg-red-500 px-1.5 text-sm text-white">
                            {{ $data->invoicedetail->count() }}
                        </span>
                    </div>
                    <span v-if="sentuh" class="font-robot text-sm italic text-gray-400 pt-1">Sentuh untuk melihat
                        rincian</span>
                </button>
            </h2>
            <div id="detail-pesanan" class="hidden transition-all duration-300 ease-in-out pb-3"
                aria-labelledby="accordion-collapse-heading-2">
                <hr class="border-y border-dashed mx-5">
                <div class="space-y-3 py-5">
                    @foreach ($data->invoicedetail as $item)
                        @if ($item->pesanan_type == 'App\Models\Pesanan' && $hasPesanan)
                            <ul class="font-robot space-y-1 mx-5">
                                <li class="font-medium text-sm">
                                    {{ $item->pesanan->nama_cetakan }}
                                </li>
                                <li class="flex justify-between">
                                    <p class="text-sm">{{ $item->pesanan->bahan->nama }}</p>
                                    <p class="text-sm">{{ $item->pesanan->qty }} Pcs</p>
                                </li>
                                <li class="flex justify-between">
                                    <p class="text-sm">{{ $item->pesanan->panjang }} X
                                        {{ $item->pesanan->lebar->meter }}
                                        Meter</p>
                                    <p class="text-sm">Rp{{ rupiah($item->pesanan->harga) }}</p>
                                </li>
                            </ul>
                        @endif
                    @endforeach
                    @foreach ($data->invoicedetail as $item)
                        @if ($item->pesanan_type == 'App\Models\PesananCetakan' && $hasCetakan)
                            <ul class="font-robot space-y-1 mx-5">
                                <li class="font-medium text-sm">
                                    {{ $item->pesanan->nama_cetakan }}
                                </li>
                                <li class="flex">
                                    <p class="text-sm">{{ $item->pesanan->jumlah_cetakan }}Ctk</p>&nbsp; - &nbsp;
                                    <p class="text-sm">{{ $item->pesanan->jenis_cetak }}</p>&nbsp; - &nbsp;
                                    <p class="text-sm">{{ $item->pesanan->jumlah_drag }} Drag</p>
                                </li>
                                <li class="flex justify-between">
                                    <p class="text-sm">
                                        {{ $item->pesanan->jumlah_plate }} Plate</p>
                                    <p class="text-sm">Rp{{ rupiah($item->pesanan->total_harga) }}</p>
                                </li>
                            </ul>
                        @endif
                    @endforeach
                    @foreach ($data->invoicedetail as $item)
                        @if ($item->pesanan_type == 'App\Models\PesananManual' && $hasManual)
                            @foreach ($item->pesanan->pesananmanualdetail as $value)
                                <ul class="font-robot space-y-1 mx-5">
                                    <li class="font-medium text-sm">
                                        {{ $value->nama_cetakan }}
                                    </li>
                                    <li class="flex justify-between">
                                        <p class="text-sm">
                                            {{ $value->qty }} Pcs</p>
                                        <p class="text-sm">Rp{{ rupiah($item->pesanan->total_harga) }}</p>
                                    </li>
                                </ul>
                            @endforeach
                        @endif
                    @endforeach
                </div>
                <hr class="border-y border-dashed mx-5">
                <div class="px-5 font-robot my-4">
                    <ul class="font-medium space-y-1">
                        <li class="flex justify-between">
                            <p>Subtotal</p>
                            <p>Rp{{ rupiah($data->subtotal) }}</p>
                        </li>
                        <li class="flex justify-between">
                            <p>Diskon <span class="text-gray-300">{{ $data->disc_percentage ?? 0 }}%</span></p>
                            <p>Rp{{ rupiah($data->disc) }}</p>
                        </li>
                        <li class="flex justify-between">
                            <p>Voucher</p>
                            <p>{{ $data->kode_voucher ?? '-' }}</p>
                        </li>
                        <li class="flex justify-between">
                            <p>Grand Total</p>
                            <p>Rp{{ rupiah($data->grand_total) }}</p>
                        </li>
                    </ul>
                </div>
                <hr class="border-y border-dashed mx-5">
                <div class="px-5 font-robot my-4">
                    <ul class="font-medium space-y-1">
                        <li class="flex justify-between">
                            <p>Terbayar</p>
                            <p>Rp{{ rupiah($data->grand_total - $data->sisa) }}</p>
                        </li>
                        <li class="flex justify-between">
                            <p>Sisa Tagihan</p>
                            <p>Rp{{ rupiah($data->sisa) }}</p>
                        </li>
                        <li class="flex justify-between">
                            <p>Status</p>
                            <p><span
                                    class="{{ $data->status == 'pending' ? 'bg-red-400' : 'bg-green-400' }} font-semibold py-1 px-3 text-white rounded-md">{{ $data->status }}</span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div
            class="border-t-2 mt-5 border-dashed border-gray-300 flex justify-center flex-col items-center text-gray-400 pt-2 pb-4 bg-white">
            <p class="text-lg">DMPRINTING - 2024</p>
            <p>Powered by printhub</p>
        </div>
    </div>
    <script type="module">
        import {
            createApp
        } from '/js/vue3.5.11.js'
        createApp({
            data() {
                return {
                    sentuh: true
                }
            },
            methods: {
                toggleSentuh() {
                    this.sentuh = !this.sentuh
                }
            }
        }).mount('#app')
    </script>
</body>

</html>
