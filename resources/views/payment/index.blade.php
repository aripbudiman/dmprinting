@extends('layouts.main')
@section('konten')
    @php
        $hasPesanan = $data->invoiceDetail->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\Pesanan';
        });
        $hasPesananCetakan = $data->invoiceDetail->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\PesananCetakan';
        });
        $hasPesananManual = $data->invoiceDetail->contains(function ($item) {
            return $item->pesanan_type === 'App\Models\PesananManual';
        });
    @endphp
    <div id="app">
        <div id="invoice" class="bg-white shadow-md rounded-md px-5 pb-5 max-w-[21cm] mx-auto" v-show="page=='show'">
            <div class="py-3">
                <h1 class="text-3xl font-semibold text-slate-800">Invoice</h1>
            </div>
            <div class="grid grid-cols-2">
                <div class="max-w-xs space-y-2 text-slate-800 text-md bg-white p-3 rounded-lg border">
                    <div class="flex justify-between border-b pb-1">
                        <p>Tanggal</p>
                        <p>{{ formatDate($data->tanggal) }}</p>
                    </div>
                    <div class="flex justify-between border-b pb-1">
                        <p>No Invoice</p>
                        <p>{{ $data->no_invoice }}</p>
                    </div>
                    <div class="flex justify-between border-b pb-1">
                        <p>No SPK</p>
                        <p>{{ $data->no_spk }}</p>
                    </div>
                    <div class="flex justify-between border-b pb-1">
                        <p>Customer</p>
                        <p>{{ $data->customer->nama }}</p>
                    </div>
                    <div class="flex justify-between border-b pb-1">
                        <p>Total Tagihan</p>
                        <p>{{ rupiah($data->sisa) }}</p>
                    </div>
                    <div class="flex justify-between">
                        <p>Status Pembayaran</p>
                        <p class="capitalize">{{ $data->status }}</p>
                    </div>
                </div>
                <div class="flex justify-start flex-col items-end">
                    <img src="{{ asset('image/LOGO BARU.svg') }}" alt="img" class="w-36">
                    <p class="text-right w-64 text-slate-700"> Jl. Veteran No.68, RT.005/RW.003, Marga Jaya, Kec. Bekasi
                        Sel., Kota Bks,
                        Jawa
                        Barat 17141</p>
                </div>
            </div>
            <div class="relative overflow-x-auto mt-5">
                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    @if ($hasPesanan)
                        <thead class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-2 py-3 rounded-s-lg">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-3 py-3" colspan="2">
                                    Pesanan Nama
                                <th scope="col" class="px-1 py-3">
                                    Bahan
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Panjang
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Lebar
                                </th>
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Qty
                                </th>
                                <th scope="col" class="px-1 py-3 rounded-e-lg">
                                    Harga
                                </th>
                            </tr>
                        </thead>
                    @endif
                    @foreach ($data->invoicedetail as $key => $value)
                        @if ($value->pesanan_type == 'App\Models\Pesanan')
                            <tbody>
                                <tr class="bg-white dark:bg-gray-800">
                                    <th scope="row"
                                        class="px-1 py-1.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $value->pesanan->no_pesanan }}
                                    </th>
                                    <td class="px-3 py-1.5" colspan="2">
                                        {{ $value->pesanan->nama_cetakan }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->bahan->nama }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->panjang }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->lebar->getLebar() }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->qty }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ rupiah($value->pesanan->harga) }}
                                    </td>
                                </tr>
                            </tbody>
                        @endif
                    @endforeach
                    @if ($hasPesananCetakan)
                        <thead class="text-xs text-gray-700  bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-1 py-3 rounded-s-lg">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-1 py-3" colspan="3">
                                    Pesanan Nama
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Jmlh Cetakan
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Jenis Cetak
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Jumlah Plate
                                </th>
                                <th scope="col" class="px-1 py-3 rounded-e-lg">
                                    Total Harga
                                </th>
                            </tr>
                        </thead>
                    @endif
                    @foreach ($data->invoicedetail as $key => $value)
                        @if ($value->pesanan_type == 'App\Models\PesananCetakan')
                            <tbody>
                                <tr class="bg-white dark:bg-gray-800">
                                    <th scope="row"
                                        class="px-1 py-1.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $value->pesanan->no_pesanan }}
                                    </th>
                                    <td class="px-1 py-1.5" colspan="3">
                                        {{ $value->pesanan->nama_cetakan }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->jumlah_cetakan }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->jenis_cetak }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ $value->pesanan->jumlah_plate }}
                                    </td>
                                    <td class="px-1 py-1.5">
                                        {{ rupiah($value->pesanan->total_harga) }}
                                    </td>
                                </tr>
                            </tbody>
                        @endif
                    @endforeach
                    @if ($hasPesananManual)
                        <thead class="text-xs text-gray-700  bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-1 py-3 rounded-s-lg">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-1 py-3" colspan="3">
                                    Pesanan Nama
                                </th>
                                <th scope="col" class="px-1 py-3">
                                    Qty
                                </th>
                                <th scope="col" class="px-1 py-3" colspan="2">
                                    Harga Satuan
                                </th>
                                <th scope="col" class="px-1 py-3 rounded-e-lg">
                                    Total Harga
                                </th>
                            </tr>
                        </thead>
                    @endif
                    @foreach ($data->invoicedetail as $key => $value)
                        @if ($value->pesanan_type == 'App\Models\PesananManual')
                            <tbody>
                                @foreach ($value->pesanan->pesananmanualdetail as $item)
                                    <tr class="bg-white dark:bg-gray-800">
                                        <th scope="row"
                                            class="px-1 py-1.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {{ $value->pesanan->no_pesanan }}
                                        </th>
                                        <td class="px-1 py-1.5" colspan="3">
                                            {{ $item->nama_cetakan }}
                                        </td>
                                        <td class="px-1 py-1.5">
                                            {{ $item->qty }}
                                        </td>
                                        <td class="px-1 py-1.5" colspan="2">
                                            {{ rupiah($item->harga_satuan) }}
                                        </td>
                                        <td class="px-1 py-1.5">
                                            {{ rupiah($item->total_harga) }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                    @endforeach
                    <tfoot>
                        <tr class="font-semibold text-gray-900 dark:text-white">
                            <th scope="row" class="px-3 py-3 text-base" colspan="5">Total Tagihan</th>
                            <td class="px-3 py-3">{{ $data->totalpesanan }}</td>
                            <td class="px-3 py-3">{{ rupiah($data->sisa) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div v-show="page=='show'" id="section-btn-invoice"
            class="flex items-center justify-between space-x-2 border-t p-3 max-w-[21cm] my-2 shadow mx-auto bg-white rounded-md">
            <div>
                <a href="{{ route('invoice.index') }}" class="btn btn-lg btn-rose"><svg
                        xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" viewBox="0 0 48 48">
                        <defs>
                            <mask id="ipTBack0">
                                <path fill="#555" fill-rule="evenodd" stroke="#fff" stroke-linejoin="round"
                                    stroke-width="4"
                                    d="M44 40.836q-7.34-8.96-13.036-10.168t-10.846-.365V41L4 23.545L20.118 7v10.167q9.523.075 16.192 6.833q6.668 6.758 7.69 16.836Z"
                                    clip-rule="evenodd" />
                            </mask>
                        </defs>
                        <path fill="currentColor" d="M0 0h48v48H0z" mask="url(#ipTBack0)" />
                    </svg>Kembali</a>
            </div>
            <div class="flex items-center space-x-2">
                <button @click="sendWA" class="btn btn-lg btn-emerald"><svg xmlns="http://www.w3.org/2000/svg"
                        width="1.5em" height="1.5em" viewBox="0 0 256 258">
                        <defs>
                            <linearGradient id="logosWhatsappIcon0" x1="50%" x2="50%" y1="100%"
                                y2="0%">
                                <stop offset="0%" stop-color="#1faf38" />
                                <stop offset="100%" stop-color="#60d669" />
                            </linearGradient>
                            <linearGradient id="logosWhatsappIcon1" x1="50%" x2="50%" y1="100%"
                                y2="0%">
                                <stop offset="0%" stop-color="#f9f9f9" />
                                <stop offset="100%" stop-color="#fff" />
                            </linearGradient>
                        </defs>
                        <path fill="url(#logosWhatsappIcon0)"
                            d="M5.463 127.456c-.006 21.677 5.658 42.843 16.428 61.499L4.433 252.697l65.232-17.104a123 123 0 0 0 58.8 14.97h.054c67.815 0 123.018-55.183 123.047-123.01c.013-32.867-12.775-63.773-36.009-87.025c-23.23-23.25-54.125-36.061-87.043-36.076c-67.823 0-123.022 55.18-123.05 123.004" />
                        <path fill="url(#logosWhatsappIcon1)"
                            d="M1.07 127.416c-.007 22.457 5.86 44.38 17.014 63.704L0 257.147l67.571-17.717c18.618 10.151 39.58 15.503 60.91 15.511h.055c70.248 0 127.434-57.168 127.464-127.423c.012-34.048-13.236-66.065-37.3-90.15C194.633 13.286 162.633.014 128.536 0C58.276 0 1.099 57.16 1.071 127.416m40.24 60.376l-2.523-4.005c-10.606-16.864-16.204-36.352-16.196-56.363C22.614 69.029 70.138 21.52 128.576 21.52c28.3.012 54.896 11.044 74.9 31.06c20.003 20.018 31.01 46.628 31.003 74.93c-.026 58.395-47.551 105.91-105.943 105.91h-.042c-19.013-.01-37.66-5.116-53.922-14.765l-3.87-2.295l-40.098 10.513z" />
                        <path fill="#fff"
                            d="M96.678 74.148c-2.386-5.303-4.897-5.41-7.166-5.503c-1.858-.08-3.982-.074-6.104-.074c-2.124 0-5.575.799-8.492 3.984c-2.92 3.188-11.148 10.892-11.148 26.561s11.413 30.813 13.004 32.94c1.593 2.123 22.033 35.307 54.405 48.073c26.904 10.609 32.379 8.499 38.218 7.967c5.84-.53 18.844-7.702 21.497-15.139c2.655-7.436 2.655-13.81 1.859-15.142c-.796-1.327-2.92-2.124-6.105-3.716s-18.844-9.298-21.763-10.361c-2.92-1.062-5.043-1.592-7.167 1.597c-2.124 3.184-8.223 10.356-10.082 12.48c-1.857 2.129-3.716 2.394-6.9.801c-3.187-1.598-13.444-4.957-25.613-15.806c-9.468-8.442-15.86-18.867-17.718-22.056c-1.858-3.184-.199-4.91 1.398-6.497c1.431-1.427 3.186-3.719 4.78-5.578c1.588-1.86 2.118-3.187 3.18-5.311c1.063-2.126.531-3.986-.264-5.579c-.798-1.593-6.987-17.343-9.819-23.64" />
                    </svg> Kirim Via WA</button>
                <button @click="page='pay'" class="btn btn-lg btn-indigo" v-show="data.status!='lunas'"><svg
                        xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M4 6h16v2H4zm0 6h16v6H4z" opacity="0.3" />
                        <path fill="currentColor"
                            d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2m0 14H4v-6h16zm0-10H4V6h16z" />
                    </svg> Lanjutkan Ke Pembayaran</button>
                {{-- <button class="btn btn-indigo" @click="modal.show()">tes modal</button> --}}
            </div>
            <div>

            </div>
        </div>
        <div class="w-full max-w-[21cm] mx-auto bg-white p-5 rounded shadow" v-show="page=='show'">
            <div class="mb-2 flex justify-between items-center">
                <label for="website-url" class="text-sm font-medium text-gray-900 dark:text-white">Link Invoice
                    Elektronik:</label>
            </div>
            <div class="flex items-center">
                <span
                    class="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-s-lg dark:bg-gray-600 dark:text-white dark:border-gray-600">URL</span>
                <div class="relative w-full">
                    <input id="website-url" type="text" aria-describedby="helper-text-explanation"
                        class="bg-gray-50 border border-e-0 border-gray-300 text-gray-500 dark:text-gray-400 text-sm border-s-0 focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        v-model="url_tagihan"
                        value="{{ route('global.invoice', $data->no_invoice) }}?customer={{ $data->customer->nama }}"
                        readonly disabled />
                </div>
                <button data-tooltip-target="tooltip-website-url" data-copy-to-clipboard-target="website-url"
                    class="flex-shrink-0 z-10 inline-flex items-center py-3 px-4 text-sm font-medium text-center text-white bg-blue-700 rounded-e-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 border border-blue-700 dark:border-blue-600 hover:border-blue-800 dark:hover:border-blue-700"
                    type="button">
                    <span id="default-icon">
                        <svg class="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                            viewBox="0 0 18 20">
                            <path
                                d="M16 1h-3.278A1.992 1.992 0 0 0 11 0H7a1.993 1.993 0 0 0-1.722 1H2a2 2 0 0 0-2 2v15a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2Zm-3 14H5a1 1 0 0 1 0-2h8a1 1 0 0 1 0 2Zm0-4H5a1 1 0 0 1 0-2h8a1 1 0 1 1 0 2Zm0-5H5a1 1 0 0 1 0-2h2V2h4v2h2a1 1 0 1 1 0 2Z" />
                        </svg>
                    </span>
                    <span id="success-icon" class="hidden inline-flex items-center">
                        <svg class="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 16 12">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M1 5.917 5.724 10.5 15 1.5" />
                        </svg>
                    </span>
                </button>
                <div id="tooltip-website-url" role="tooltip"
                    class="absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white transition-opacity duration-300 bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip dark:bg-gray-700">
                    <span id="default-tooltip-message">Copy link</span>
                    <span id="success-tooltip-message" class="hidden">Copied!</span>
                    <div class="tooltip-arrow" data-popper-arrow></div>
                </div>
            </div>
        </div>

        <div class="flex justify-center space-x-3 bg-white -mx-10 -mt-5 pb-5" v-show="page=='pay'">
            <div class="w-[550px] bg-white p-5 border mt-10 rounded-lg">
                <div>
                    <div class="flex justify-between items-center">
                        <h3 class="mb-7 text-3xl font-semibold text-gray-900 dark:text-white">Payment Details</h3>
                        <button @click="page='show'"
                            class="inline-flex text-lg items-center font-medium text-blue-600 dark:text-white mb-3 cursor-pointer">&larr;
                            Back To Invoice</button>
                    </div>
                    <ul class="w-full space-y-4">
                        <li v-for="payment in listPaymentMethod" class="flex items-center"
                            @click="changePaymentMethod(payment.value, payment.kode_akun)">
                            <input type="radio" :id="payment.id" name="metode_pembayaran" :value="payment.value"
                                class="peer hidden" required />
                            <label :for="payment.id"
                                class="inline-flex w-full p-5 text-gray-500 bg-white border border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 dark:peer-checked:text-indigo-500 peer-checked:border-indigo-600 peer-checked:text-indigo-600 hover:text-gray-600 hover:bg-gray-100 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">
                                <div class="flex justify-between items-center w-full">
                                    <div class="flex space-x-2">
                                        <div class="text-lg font-semibold" v-html="payment.icon"></div>
                                        <div class="text-xl" v-html="payment.nama"></div>
                                    </div>
                                    <div v-if="payment.value === 'BRI'">
                                        <x-bri />
                                    </div>
                                    <div v-if="payment.value === 'BCA'">
                                        <x-bca />
                                    </div>
                                    <div v-if="payment.value === 'Mandiri'">
                                        <x-mandiri />
                                    </div>
                                    <div v-if="payment.value === 'gopay'">
                                        <x-gopay />
                                    </div>
                                    <div v-if="payment.value === 'BNI'">
                                        <x-bni />
                                    </div>
                                </div>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="relative" v-if="data.status == 'pending'">
                    <hr class="my-10">
                    <span
                        class="absolute translate-x-1/2 -translate-y-1/2 top-1/2 left-1/3 bg-white px-4 text-sm text-gray-500">OR</span>
                </div>
                <div class="border p-5 mt-5 rounded-md" v-if="data.status == 'pending'">
                    <label for="voucher">Voucher Diskon</label>
                    <input type="text" class="w-full mt-5 border border-gray-300 rounded-lg p-2" name="kode_voucher"
                        v-model="kode_voucher">
                    <button class="btn btn-indigo w-full mt-3" @click="applyVoucher()">Gunakan Voucher</button>
                </div>
                <div class="relative" v-if="data.status != 'lunas'">
                    <hr class="my-10">
                    <span
                        class="absolute translate-x-1/2 -translate-y-1/2 top-1/2 left-1/3 bg-white px-4 text-sm text-gray-500">OR</span>
                </div>
                <div v-if="data.status != 'lunas'">
                    <div class="space-y-3 mb-3">
                        <div class="flex items-center first-line total-uang"
                            v-if="payloadBayar.metode_pembayaran === 'cash'">
                            <label for="first_name"
                                class="block mb-2 text-md font-medium text-gray-900 dark:text-white w-[400px]">Total
                                Uang Customer</label>
                            <input type="text" v-model="payloadBayar.total_uang"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0" />
                        </div>
                        <div class="flex items-center">
                            <label for="last_name"
                                class="block mb-2 text-md font-medium text-gray-900 dark:text-white w-[400px]">Total
                                Bayar</label>
                            <input type="text" v-model="payloadBayar.bayar" @input="formatRupiahInput"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0" />
                        </div>
                        <div class="flex items-center kembalian" v-if="payloadBayar.metode_pembayaran === 'cash'">
                            <label for="phone"
                                class="block mb-2 text-md font-medium text-gray-900 dark:text-white w-[400px]">Kembalian</label>
                            <input type="tel" v-model="payloadBayar.kembalian" :value="hitungKembalian"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="0" />
                        </div>
                    </div>
                    <button class="btn border border-indigo-600 text-indigo-600 mb-2" @click="bayarSemua()">Bayar
                        Semua</button>
                    <button class="btn btn-indigo w-full" @click="paynow()">Pay Now</button>
                </div>
            </div>
            <div class="text-slate-800 text-md bg-white p-2 rounded-lg w-[450px] mt-8">
                <div class="border border-gray-200 rounded-lg w-full py-5 px-8 space-y-2">
                    <h3 class="text-xl font-semibold mb-3">Order Summary</h3>
                    <div class="max-h-52 overflow-auto">
                        <ul class="space-y-2">
                            <li v-for="pesan in data.invoicedetail" class="flex justify-between items-center">
                                <span>@{{ pesan.pesanan.no_pesanan }}</span>
                                <span>@{{ new Intl.NumberFormat('id-ID').format(pesan.pesanan?.harga ?? pesan.pesanan?.total_harga) }}</span>
                            </li>
                        </ul>
                    </div>
                    <hr>
                    <div class="flex justify-between">
                        <p>Subtotal</p>
                        <p id="summary-subtotal">@{{ formattedSubtotal }}</p>
                    </div>
                    <div class="flex justify-between">
                        <p>Diskon</p>
                        <p id="summary-diskon">@{{ formattedDiskon }}</p>
                    </div>
                    <div class="flex justify-between">
                        <p>Terbayar</p>
                        <p id="summary-terbayar">@{{ new Intl.NumberFormat('id-ID').format(terbayar) }}</p>
                    </div>
                    <hr>
                    <div class="flex justify-between font-semibold">
                        <p>Sisa Tagihan / Total Bayar</p>
                        <p id="summary-sisa">@{{ new Intl.NumberFormat('id-ID').format(payloadBayar.sisa) }}</p>
                    </div>
                </div>
                <h3 class="pl-2 font-semibold mt-5">History Payment</h3>
            </div>
            @include('payment.modal')
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        import {
            createApp,
            ref
        } from '/js/vue3.5.11.js'
        createApp({
            data() {
                return {
                    listPaymentMethod: [],
                    data: @json($data),
                    struk: {},
                    url_struk: '',
                    url_tagihan: "{{ route('global.invoice', $data->no_invoice) }}?customer={{ $data->customer->nama }}",
                    payloadBayar: {
                        no_invoice: @json($data->no_invoice),
                        invoice_id: @json($data->id),
                        metode_pembayaran: '',
                        total_uang: '',
                        bayar: '',
                        kembalian: '',
                        kode_akun: '',
                        kode_voucher: '',
                        disc_percentage: @json($data->disc_percentage ?? 0),
                        disc: @json($data->disc ?? 0),
                        sisa: @json($data->sisa ?? 0),
                        subtotal: @json($data->subtotal ?? 0),
                        grand_total: @json($data->grand_total ?? 0),
                        status: @json($data->status)
                    },
                    kode_voucher: '',
                    page: @json(request()->query('page')),
                    options: {
                        placement: 'center',
                        backdrop: 'dynamic',
                        backdropClasses: 'bg-blue-400/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                        closable: true,
                        onHide: () => {
                            console.log('modal is hidden');
                        },
                        onShow: () => {
                            console.log('modal is shown');
                        },
                        onToggle: () => {
                            console.log('modal has been toggled');
                        },
                    },
                    terbayar: @json($data->grand_total - $data->sisa - $data->disc),
                }
            },
            methods: {
                loadPaymentMethod() {
                    const url = `{{ route('payment_method') }}`;
                    axios.get(url)
                        .then(response => {
                            this.listPaymentMethod = response.data
                        })
                        .catch(error => {
                            console.log(error);
                        })
                },
                changePaymentMethod(value, kode_akun) {
                    this.payloadBayar.metode_pembayaran = value
                    this.payloadBayar.kode_akun = kode_akun
                    if (this.payloadBayar.metode_pembayaran != 'cash') {
                        this.payloadBayar.kembalian = 0
                        this.payloadBayar.total_uang = 0
                    }
                },
                applyVoucher() {
                    const url = `{{ route('apply_voucher') }}`
                    axios.post(url, {
                        kode_voucher: this.kode_voucher
                    }).then(response => {
                        if (response.data.success) {
                            this.payloadBayar.disc_percentage = response.data.data
                            this.payloadBayar.kode_voucher = this.kode_voucher
                            this.hitungDiskon()
                        } else {
                            this.kode_voucher = ''
                        }
                        alert(response.data.message)
                    }).catch(error => {
                        console.log(error)
                    })
                },
                hitungDiskon() {
                    const newDiskon = this.data.subtotal * this.payloadBayar.disc_percentage / 100
                    this.payloadBayar.disc = newDiskon
                    this.payloadBayar.sisa -= newDiskon
                },
                paynow() {
                    if (this.payloadBayar.metode_pembayaran == '') {
                        alert('Pilih Metode Pembayaran', 'warning')
                        return false
                    }
                    if (this.payloadBayar.bayar == '') {
                        alert('Masukkan Total Bayar', 'warning')
                        return false
                    }
                    if (this.payloadBayar.kembalian < 0 && this.payloadBayar.metode_pembayaran == 'cash') {
                        alert('Total Uang kurang atau belum dimasukan', 'warning')
                        return false
                    }
                    console.log(this.data)
                    const url = `{{ route('paynow') }}`
                    axios.post(url, this.payloadBayar)
                        .then(response => {
                            if (response.data.status == 'success') {
                                this.struk = response.data.data
                                this.url_struk = response.data.url
                                this.modal.show()
                            }
                        })
                        .catch(error => {
                            console.log(error)
                        })
                },
                formatRupiah(angka) {
                    var number_string = angka.replace(/[^,\d]/g, "").toString(),
                        split = number_string.split(","),
                        sisa = split[0].length % 3,
                        rupiah = split[0].substr(0, sisa),
                        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
                    var separator = "";

                    if (ribuan) {
                        separator = sisa ? "." : "";
                        rupiah += separator + ribuan.join(".");
                    }

                    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
                    return rupiah;
                },
                formatRupiahInput(event) {
                    let value = event.target.value
                    this.formattedBayar = value
                },
                bayarSemua() {
                    this.payloadBayar.bayar = parseFloat(this.payloadBayar.sisa)
                },
                sendWA() {
                    const route = '{{ route('send_wa') }}'
                    axios.post(route, {
                            url: @json(Request::fullUrl()),
                            tanggal: this.data.tanggal,
                            no_invoice: this.data.no_invoice,
                            no_spk: this.data.no_spk,
                            customer: this.data.customer.nama,
                            sisa: this.data.sisa,
                            status: this.data.status,
                            nohp: this.data.customer.no_hp,
                            url_tagihan: this.url_tagihan
                        })
                        .then(response => {
                            if (response) {
                                console.log(response)
                            }
                        })
                        .catch(error => {
                            console.log(error)
                        })
                },
                printStruk() {
                    var width = 1000;
                    var height = 600;
                    var top = 100;
                    var left = 300;

                    window.open(this.url_struk, '_blank',
                        `top=${top},left=${left},height=${height},width=${width},scrollbars=yes,status=yes`);
                }
            },
            setup() {},
            mounted() {
                this.loadPaymentMethod()
                const targetModal = document.getElementById('modal-invoice')
                this.modal = new Modal(targetModal, this.options);
                console.log(this.page)
            },
            computed: {
                formattedSubtotal() {
                    return new Intl.NumberFormat('id-ID').format(this.payloadBayar.subtotal);
                },
                formattedDiskon() {
                    return new Intl.NumberFormat('id-ID').format(this.payloadBayar.disc);
                },
                hitungKembalian() {
                    const kembali = this.payloadBayar.total_uang - this.payloadBayar.bayar
                    this.payloadBayar.kembalian = kembali
                    return kembali
                },
                formattedBayar: {
                    get() {
                        return this.formatRupiah(this.payloadBayar.bayar);
                    },
                    set(value) {
                        this.payloadBayar.bayar = value.replace(/\D/g, '');
                    }
                }
            }
        }).mount('#app')
    </script>
@endpush
