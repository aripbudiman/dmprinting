@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.menucategory')" :tr="'show'" :actions="'delete'">
        <x-slot name="header">
            <x-header-table :title="'List Menu Category'">
                <a href="{{ route('menu.list-menu-category.create') }}" class="btn btn-md btn-indigo rounded-none">Buat
                    Category Menu Baru</a>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script type="text/javascript">
        function _show(id, el) {
            window.location.href = "{{ route('menu.list-menu-category.edit', ':id') }}".replace(':id', id);
        }

        function _deleteItem(id, el) {
            $.ajax({
                type: "POST",
                url: "{{ route('menu.list-menu-category.delete', ':id') }}".replace(':id', id),
                data: {
                    _token: '{{ csrf_token() }}'
                },
                dataType: "JSON",
                success: function(response) {
                    $(el).closest('tr').remove()
                    setInterval(() => {
                        location.reload()
                    }, 3000);
                    alert(response.message, 'success')
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText, 'error')
                }
            });
        }
    </script>
@endpush
