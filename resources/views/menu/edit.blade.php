@extends('layouts.main')
@section('konten')
    @include('menu._form', ['menu' => $menu])
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-menu').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('menu.update', $menu->id) }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response) {
                            alert(response, 'success');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error(error);
                    }
                });
            })
        });
    </script>
@endpush
