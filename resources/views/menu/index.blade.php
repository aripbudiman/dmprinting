@extends('layouts.main')
@section('konten')
    <x-table :data="$menus" :columns="config('table_config.menu')" :tr="'show'" :actions="'getcontroller:delete'">
        <x-slot name="header">
            <x-header-table :title="'List Menu'">
                <a href="{{ route('menu.create') }}" class="btn btn-md btn-indigo">Buat Menu Baru</a>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script type="text/javascript">
        function _getcontrollerItem(id, el) {
            const valueSync = el.parentNode.parentNode.parentNode.previousElementSibling.textContent;
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    valueSync: valueSync
                },
                dataType: "JSON",
                success: function(response) {
                    if (response == 'Berhasil') {
                        location.reload();
                    }
                }
            });
        }

        function _deleteItem(id, el) {
            $.ajax({
                type: "POST",
                url: "{{ route('menu.delete', ':id') }}".replace(':id', id),
                data: {
                    _token: '{{ csrf_token() }}'
                },
                dataType: "JSON",
                success: function(response) {
                    $(el).closest('tr').remove()
                    setInterval(() => {
                        location.reload()
                    }, 2000);
                    alert(response.message, 'success', 2000)
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText, 'error')
                }
            });
        }

        function _show(id, el) {
            window.location.href = "{{ route('menu.edit', ':id') }}".replace(':id', id);
        }
    </script>
@endpush
