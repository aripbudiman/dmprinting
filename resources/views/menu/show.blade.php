@extends('layouts.main')
@section('konten')
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">Detail Menu</h1>
        </div>
        <div class="p-5">
            <ul class="flex flex-col">
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Id</span>
                        <span>{{ $menu->id }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Category</span>
                        <span>{{ $menu->categorymenu->nama ?? '' }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Nama</span>
                        <span>{{ $menu->nama }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Slug</span>
                        <span>{{ $menu->slug }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Controller</span>
                        <span>{{ $menu->controller }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Route Name</span>
                        <span>{{ $menu->route_name }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Namespace</span>
                        <span>{{ $menu->namespace }}</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="px-5 pb-5  flex gap-x-2 justify-end">
            <button class="btn btn-lg btn-emerald">Edit</button>
            <a href="{{ route('menu.index') }}" class="btn btn-lg btn-rose">Kembali</a>
        </div>
    </div>
@endsection
