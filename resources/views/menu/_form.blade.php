<div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
    <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
        <h1 class="text-xl text-gray-800">{{ isset($menu) ? 'Edit Menu' : 'Buat Menu Baru' }}</h1>
    </div>
    <form id="form-menu" class="p-5 flex flex-col gap-3">
        @csrf
        <div class="sm:hidden hidden sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="id" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Id</label>
            <input type="text" id="id" value="{{ $menu->id ?? '' }}" name="id" class="dmv-input"
                placeholder="id" readonly>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="category_id" class="block xl:w-1/3 max-w-lg text-sm font-medium dark:text-white">Menu
                Category</label>
            <select class="dmv-select capitalize" name="category_id">
                @foreach ($categories as $item)
                    <option class="capitalize" value="{{ $item->id }}"
                        {{ isset($menu) && $menu->category_id == $item->id ? 'selected' : '' }}>
                        {{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="nama" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Nama</label>
            <input type="text" value="{{ $menu->nama ?? '' }}" name="nama" class="dmv-input" placeholder="nama">
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="slug" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Slug</label>
            <input type="text" value="{{ $menu->nama ?? '' }}" name="slug" class="dmv-input" placeholder="slug"
                readonly>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="controller" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Controller</label>
            <input type="text" value="{{ $menu->controller ?? '' }}" name="controller" class="dmv-input"
                placeholder="controller">
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="route_name" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Route Name</label>
            <input type="text" value="{{ $menu->route_name ?? '' }}" name="route_name" class="dmv-input"
                placeholder="route name">
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="namespace" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Namespace</label>
            <input type="text" value="{{ $menu->namespace ?? '' }}" name="namespace" class="dmv-input"
                placeholder="route name">
        </div>
        <div class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full">
            <button type="submit" class="btn btn-indigo">{{ isset($menu) ? 'Update' : 'Simpan' }}</button>
            <button id="sync" type="button" class="btn btn-emerald"><x-icon-sync class="w-5 h-5" />Sync</button>
            <a href="{{ route('menu.index') }}" class="btn btn-rose">Cancel</a>
        </div>
    </form>
</div>
@push('scripts')
    <script type="text/javascript">
        $('[name="nama"]').keyup(function() {
            $('[name="slug"]').val($(this).val().replace(/\s+/g, '-').toLowerCase());
        })

        $('#sync').click(function(e) {
            e.preventDefault();
            const routeName = $('[name="route_name"]').val();
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    route_name: routeName,
                },
                dataType: "JSON",
                success: function(response) {
                    $('[name="controller"]').val(response[0]);
                    $('[name="namespace"]').val(response[1]);
                    alert('Sync berhasil', 'success');
                },
                error: function(xhr, status, error) {
                    if (xhr.status == 404) {
                        $('[name="controller"]').val('');
                        $('[name="namespace"]').val('');
                        alert('Route name tidak ditemukan', 'error');
                    }
                }
            });
        })
    </script>
@endpush
