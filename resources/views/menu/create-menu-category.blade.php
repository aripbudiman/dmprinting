@extends('layouts.main')
@section('konten')
    @include('menu._form_category')
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-menu').on('submit', function(e) {
                e.preventDefault();
                const formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('menu.list-menu-category.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response) {
                            alert(response, 'success');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error)
                    }
                });
            })
        });
    </script>
@endpush
