@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.pesanan_cetakan')" :tr="'show', 'edit'">
        <x-slot name="header">
            <x-header-table :title="'List Pesanan Cetakan'">
                <div class="space-x-2">
                    <a href="{{ route('pesanan-cetakan.create') }}" class="btn btn-md rn btn-indigo">
                        Buat Pesanan Cetakan
                    </a>
                </div>
            </x-header-table>
        </x-slot>
        <x-slot name="customfilter">
            <form action="{{ Request::url() }}" method="GET" class="grid grid-cols-3 mb-5">
                <div date-rangepicker datepicker-format="yyyy-mm-dd" class="flex items-center gap-x-2">
                    <div class="relative">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <x-icon-calender />
                        </div>
                        <input name="start_date" type="text" class="date-filter" placeholder="start"
                            value="{{ $payload['start_date'] ?? '' }}" autocomplete="off">
                    </div>
                    <span class="mx-2 text-gray-500">to</span>
                    <div class="relative">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <x-icon-calender />
                        </div>
                        <input name="end_date" type="text" class="date-filter" placeholder="end"
                            value="{{ $payload['end_date'] ?? '' }}" autocomplete="off">
                    </div>
                    <button class="btn btn-indigo">Filter</button>
                </div>
            </form>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$payload['show']" />
            <x-search :keyword="$payload['keyword']" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script type="text/javascript">
        function _show(id, el) {
            window.location.href = "{{ route('pesanan-cetakan.show', ':id') }}".replace(':id', id);
        }
    </script>
@endpush
