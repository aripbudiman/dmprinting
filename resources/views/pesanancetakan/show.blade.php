@extends('layouts.main')
@section('konten')
    <div class="contianer max-w-sm bg-white rounded-md shadow-md">
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li class="font-bold flex items-center space-x-3"> <svg xmlns="http://www.w3.org/2000/svg" width="1.2em"
                    height="1.2em" viewBox="0 0 48 48">
                    <path fill="#bbdefb" d="M7 4h34v40H7z" />
                    <path fill="#2196f3"
                        d="M13 26h4v4h-4zm0-8h4v4h-4zm0 16h4v4h-4zm0-24h4v4h-4zm8 16h14v4H21zm0-8h14v4H21zm0 16h14v4H21zm0-24h14v4H21z" />
                </svg> Detail Pesanan Cetakan</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Tanggal</li>
            <li>{{ $pesanan_cetakan->tanggal }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Customer</li>
            <li>{{ $pesanan_cetakan->customer->nama }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Nama Cetakan</li>
            <li>{{ $pesanan_cetakan->nama_cetakan }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Jumlah Cetakan</li>
            <li>{{ $pesanan_cetakan->jumlah_cetakan }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Jumlah Drag</li>
            <li>{{ $pesanan_cetakan->jumlah_drag }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Jenis Cetak</li>
            <li>{{ $pesanan_cetakan->jenis_cetak }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Jumlah Plate</li>
            <li>{{ $pesanan_cetakan->jumlah_plate }}</li>
        </ul>
        <ul class="flex justify-between items-center border-b p-3 text-gray-700">
            <li>Total Harga</li>
            <li>{{ rupiah($pesanan_cetakan->total_harga) }}</li>
        </ul>
        <ul class="flex justify-end space-x-1.5 items-center border-b p-3 text-gray-700">
            {{-- <li><button class="btn btn-md btn-indigo">Print</button></li> --}}
            <li><a class="btn btn-md btn-emerald" href="{{ route('pesanan-cetakan.edit', $pesanan_cetakan->id) }}">Edit</a>
            </li>
            <li><a class="btn btn-md btn-indigo" href="{{ route('pesanan-cetakan.index') }}">Kembali</a></li>
            {{-- <li><a class="btn btn-md btn-rose">Hapus</a></li> --}}
        </ul </div>
    @endsection
