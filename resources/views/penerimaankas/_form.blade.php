<!-- Main modal -->
<div id="modal-tambah-penerimaan-kas" tabindex="-1" aria-hidden="true"
    class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 w-full max-w-md max-h-full">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
                <h3 class="text-lg font-semibold text-gray-700 dark:text-white">
                    Tambah Penerimaan Kas
                </h3>
                <button id="close" type="button"
                    class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <form class="p-4 md:p-5" id="formData">
                @csrf
                <div class="grid grid-cols-2 gap-x-5 mb-3">
                    <div class="mb-3">
                        <label for="name"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Tanggal</label>
                        <div class="relative max-w-sm">
                            <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                                <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                                    <path
                                        d="M20 4a2 2 0 0 0-2-2h-2V1a1 1 0 0 0-2 0v1h-3V1a1 1 0 0 0-2 0v1H6V1a1 1 0 0 0-2 0v1H2a2 2 0 0 0-2 2v2h20V4ZM0 18a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V8H0v10Zm5-8h10a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2Z" />
                                </svg>
                            </div>
                            <input id="tanggal" datepicker-format="dd-mm-yyyy" datepicker datepicker-autohide
                                type="text" name="tanggal"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Pilih Tanggal" autocomplete="off">
                        </div>
                    </div>
                    <div>
                        <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">No
                            Ref</label>
                        <input type="text" name="no_ref" id="no_ref"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                            placeholder="-" value="-" required="">
                    </div>
                </div>
                <div class="w-full mb-3">
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Simpan
                        Ke</label>
                    <select class="simpan-ke" name="debet">
                        <option value="">Select akun</option>
                        @foreach ($simpanke as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="w-full mb-3">
                    <label for="category"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Terima
                        Dari</label>
                    <select class="simpan-ke" name="kredit">
                        <option value="">Select akun</option>
                        @foreach ($terimadari as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-3">
                    <label for="name"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Nominal</label>
                    <input type="text" name="nominal" id="nominal"
                        class="rupiah bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                        placeholder="0">
                </div>
                <div class="mb-3">
                    <label for="keterangan"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Keterangan</label>
                    <textarea id="keterangan" name="keterangan" rows="3"
                        class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"></textarea>
                </div>
                <div class="mb-3 text-xs text-gray-500">
                    <span>Catatan:</span><br>
                    <i>(*) Harus di isi</i>
                </div>
                <div class="grid grid-cols-2 gap-x-5">
                    <button type="submit" class="btn btn-lg btn-indigo">Simpan</button>
                    <button type="button" class="btn btn-lg btn-rose">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(document).ready(function() {
            $('.simpan-ke').select2();
        });

        $('#formData').submit(function(e) {
            e.preventDefault();
            $('[name]').removeClass('border-red-500');
            const formData = new FormData(this);
            const error = validate(formData);
            if (error.length > 0) {
                alert(`${error.join(' ')}`, 'error');
                return false
            }
            $.ajax({
                type: "POST",
                url: "{{ route('penerimaan_kas.store') }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status == 'success') {
                        alert(response.message, 'success');
                    }
                }
            });
        });

        function validate(formData) {
            let error = []
            formData.forEach((value, key) => {
                if (value == '') {
                    $('[name="' + key + '"]').addClass('border-red-500');
                    if (key == 'debet') {
                        error.push('<span class="font-bold">Simpan ke</span> Tidak boleh kosong <br>')
                    } else if (key == 'kredit') {
                        error.push('<span class="font-bold">Terima dari</span> Tidak boleh kosong <br>')
                    } else {
                        error.push(`<span class="font-bold">${key}</span> Tidak boleh kosong <br>`)
                    }
                }
            });

            return error
        }
    </script>
@endpush
