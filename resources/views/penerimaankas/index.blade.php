@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.jurnal_umum')" :tr="'show'" :button="'edit'">
        <x-slot name="header">
            <x-header-table :title="'Penerimaan Kas'">
                <div class="space-x-2">
                    <button onclick="_tambah()" class="btn btn-md rn btn-indigo">
                        Tambaha Penerimaan Kas
                    </button>
                </div>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$payload['show']" />
            <x-search :keyword="$payload['keyword']" />
        </x-slot>
    </x-table>
    @include('penerimaankas._form')
@endsection
@push('scripts')
    <script>
        function _edit(id, el) {
            const targetModal = document.getElementById('modal-tambah-penerimaan-kas')
            const options = {
                placement: 'center',
                backdrop: 'dynamic',
                backdropClasses: 'bg-gray-800/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                closable: true,
                onHide: () => {
                    console.log('modal is hidden');
                },
                onShow: () => {
                    targetModal.classList.add('open')
                    console.log('modal is shown');
                },
                onToggle: () => {
                    console.log('modal has been toggled');
                },
            };
            const modal = new Modal(targetModal, options);
            modal.show()
            $('#close').click(function() {
                modal.hide()
            })
        }

        function _tambah() {
            const targetModal = document.getElementById('modal-tambah-penerimaan-kas')
            const options = {
                placement: 'center',
                backdrop: 'dynamic',
                backdropClasses: 'bg-gray-800/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                closable: true,
                onHide: () => {
                    console.log('modal is hidden');
                },
                onShow: () => {
                    targetModal.classList.add('open')
                    console.log('modal is shown');
                },
                onToggle: () => {
                    console.log('modal has been toggled');
                },
            };
            const modal = new Modal(targetModal, options);
            modal.show()
            $('#close').click(function() {
                modal.hide()
            })
        }
    </script>
@endpush
