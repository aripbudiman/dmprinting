@props(['title'])
<div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
    <h1 class="text-xl text-gray-800">{{ $title }}</h1>
    {{ $slot }}
</div>
