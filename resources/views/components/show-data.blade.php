@props(['show'])
<div class="flex items-center gap-x-2 text-gray-800">
    <p>Show</p>
    <select id="show"
        class="py-2 px-3 pe-9 block w-[75px] border-gray-200 rounded-lg text-sm focus:border-violet-500 focus:ring-violet-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600">
        <option value="10" {{ $show == 10 ? 'selected' : '' }}>10</option>
        <option value="25" {{ $show == 25 ? 'selected' : '' }}>25</option>
        <option value="50" {{ $show == 50 ? 'selected' : '' }}>50</option>
        <option value="100" {{ $show == 100 ? 'selected' : '' }}>100</option>
    </select>
    <p>Data</p>
</div>
@push('scripts')
    <script type="text/javascript">
        $('#show').change(function(e) {
            e.preventDefault();
            const angka = $(this).val();
            document.location.href = "{{ Request::url() }}" + "?show=" + angka;
        });
    </script>
@endpush
