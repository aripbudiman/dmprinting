<button id="dropdownDefaultButton" data-dropdown-toggle="dropdown-auth"
    class="text-white hover:text-gray-200 text-sm px-5 py-2.5 text-center inline-flex items-center uppercase"
    type="button">
    @auth
        {{ Auth::user()->name }} <span
            class="lowercase bg-slate-50/15 px-1 rounded-md ml-1">{{ Auth::user()->roles->first()->name }}</span>
    @endauth
    <svg class="w-2.5 h-2.5 ms-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
        <path stroke="currentColor" class="group" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
            d="m1 1 4 4 4-4" />
    </svg>
</button>

<!-- Dropdown menu -->
<div id="dropdown-auth" class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
    <ul class="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownDefaultButton">
        <li>
            <a href="#" class="dmv-menu-items w-full rounded-none pl-4">
                Profile Perusahaan
            </a>
        </li>
        <li>
            <form action="{{ route('logout') }}" method="post" class="w-full">
                @csrf
                <button type="submit" class="dmv-menu-items w-full rounded-none pl-4">
                    Logout
                </button>
            </form>
        </li>
    </ul>
</div>
