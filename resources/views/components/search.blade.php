@props(['keyword'])
<div class="max-w-md">
    <label for="hs-trailing-button-add-on-multiple-add-ons" class="sr-only">Label</label>
    <div class="flex rounded-lg shadow-sm">
        <input type="text" id="hs-trailing-button-add-on-multiple-add-ons" value="{{ $keyword ?? '' }}"
            placeholder="&#128269;" name="hs-trailing-button-add-on-multiple-add-ons"
            class="py-1 px-4 block w-full border-gray-200 shadow-sm rounded-s-md text-sm focus:z-10 focus:border-indigo-500 focus:ring-indigo-500 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400">
        <button id="search" type="button" class="btn btn-lg btn-indigo rounded-none">
            Search
        </button>
        <button id="reset" type="button" class="btn btn-md btn-violet rounded-l-none">
            Reset
        </button>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#reset').click(function(e) {
                e.preventDefault();
                $('#hs-trailing-button-add-on-multiple-add-ons').val('');
                const search = $('#hs-trailing-button-add-on-multiple-add-ons').val();
                document.location.href = "{{ Request::url() }}" + "?search=" + search;
            });
            $('#search').click(function(e) {
                e.preventDefault();
                const search = $('#hs-trailing-button-add-on-multiple-add-ons').val();
                document.location.href = "{{ Request::url() }}" + "?search=" + search;
            });
        });
    </script>
@endpush
