@props(['data', 'columns', 'actions', 'tr' => '', 'button' => ''])
@php
    $newData = $data->items();
    $links = $data->links();
    $action = isset($actions) ? explode(':', $actions) : [];
    $newcolumns = array_filter($columns, fn($item) => $item['label'] != 'Aksi');

    $permission = session('permission', []);
@endphp
<div class="bg-white shadow-md border border-gray-300 rounded-md dark:bg-slate-900">
    {{ $header ?? '' }}
    <div class="flex flex-col p-5">
        <div class="-m-1.5 overflow-x-auto">
            <div class="p-1.5 min-w-full inline-block align-middle">
                {{ $customfilter ?? '' }}
                <div class="mb-2  flex justify-between">
                    {{ $filter ?? '' }}
                </div>
                <div class="border overflow-hidden dark:bg-gray-800 dark:border-gray-700">
                    <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                        <thead>
                            <tr>
                                @foreach ($newcolumns as $column)
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400  {{ $column['class'] }}">
                                        {{ $column['label'] }}</th>
                                @endforeach
                                @if (count($action) || $button != '')
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs uppercase font-semibold bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Aksi</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody id="tbody"
                            class="divide-y divide-gray-200 dark:divide-gray-400 dark:bg-gray-800 dark:border-gray-700">
                            @foreach ($data as $key => $item)
                                @php
                                    $eventClick = '_' . $tr . '(' . $item->id . ',this)';
                                @endphp
                                <tr class="hover:bg-gray-100 cursor-pointer" ondblclick="{{ $eventClick }}">
                                    @foreach ($newcolumns as $column)
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100 {{ $column['class'] }}">
                                            @php
                                                $keys = explode('.', $column['data']);
                                                $data = $item;
                                                foreach ($keys as $key) {
                                                    $data = $data->$key ?? $item[$column['data']];
                                                }
                                                if (isset($column['fungsi']) && $column['fungsi'] === 'rupiah') {
                                                    $data = Helpers::rupiah($data);
                                                }
                                                if (isset($column['fungsi']) && $column['fungsi'] === 'formatDate') {
                                                    $data = formatDate($data);
                                                }
                                            @endphp
                                            {{ $data }}
                                        </td>
                                    @endforeach
                                    @if (count($action) > 0)
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-gray-200">
                                            <button id="dropdownDefaultButton"
                                                data-dropdown-toggle="dropdown_{{ $item->id }}"
                                                class="btn btn-md btn-indigo" type="button">Actions
                                                <x-icon-action />
                                            </button>
                                            <div id="dropdown_{{ $item->id }}"
                                                class="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow max-w-full dark:bg-gray-700">
                                                <ul class="py-2 text-sm text-gray-700 dark:text-gray-200"
                                                    aria-labelledby="dropdownDefaultButton">
                                                    @foreach ($action as $ac)
                                                        @php
                                                            $nameFunc = $ac . 'Item';
                                                            $event = '_' . $nameFunc . '(' . $item->id . ',this)';
                                                        @endphp
                                                        <li>
                                                            <button type="button" onclick="{{ $event }}"
                                                                class="block px-4 py-2 w-full text-left hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white {{ $ac == 'delete' ? 'text-rose-500' : 'text-emerald-500' }}">
                                                                {{ $ac }}
                                                            </button>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </td>
                                    @endif
                                    @if ($button != '')
                                        <td class="py-2">
                                            @if (is_null($item->verified_at))
                                                @if (in_array('PenerimaanKasController@edit', $permission))
                                                    <button class="btn btn-indigo btn-sm"
                                                        onclick="_edit('{{ $item->id }}',this)">Edit</button>
                                                @endif
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <nav id="pagination" class="flex w-full justify-between items-center mt-4">
                    {!! $links !!}
                </nav>
            </div>
        </div>
    </div>
</div>
