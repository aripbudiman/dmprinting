@props(['items', 'title'])

<button id="dropdownNavbarLink" data-dropdown-toggle="{{ $title }}"
    class="flex items-center justify-between w-full py-2 px-3 text-white hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-indigo-200 md:p-0 md:w-auto dark:text-white md:dark:hover:text-blue-500 dark:focus:text-white dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
    {{ $title }}
    <svg class="w-2.5 h-2.5 ms-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4" />
    </svg></button>
<!-- Dropdown menu -->
<div id="{{ $title }}" class="wrapper-dropdown">
    @foreach ($items as $menuCategory)
        <ul class="text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdownLargeButton">
            <li>
                <a class="block py-2 px-3 text-xs font-medium uppercase text-indigo-100 dark:text-gray-500">{{ $menuCategory->nama }}
                </a>
                <ul>
                    @foreach ($menuCategory->menu as $menu)
                        @if (session()->has('permission') && in_array($menu->controller, session('permission')))
                            <li><a href="{{ $menu->route_name ? route($menu->route_name) : url('/') }}"
                                    class="navbar-item-dropdown">
                                    {{ $menu->nama }}
                                </a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
        </ul>
    @endforeach
</div>
