@extends('layouts.main')
@section('konten')
    <div class="contianer grid grid-cols-1 xl:grid-cols-2">
        <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-lg">
            <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
                <h1 class="text-xl text-gray-800">Konfigurasi Harga Drag</h1>
            </div>
            <form id="form-konfigurasi-harga-drag" class="p-5 flex flex-col gap-3">
                @foreach ($drag as $item)
                    <div class="grid grid-cols-2 space-x-5">
                        <div class="relative">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga
                                Drag</label>
                            <input type="text" name="id[]" class="form-input angka" placeholder="Harga"
                                value="{{ $item->id }}">
                        </div>
                        <div class="relative">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga
                                Drag</label>
                            <input type="text" name="harga[]" class="form-input angka" placeholder="Harga"
                                value="{{ $item->harga }}">
                        </div>
                    </div>
                @endforeach
                <div
                    class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full border-t pt-3 mt-3">
                    <button type="submit" class="btn btn-indigo"><x-icon-sync /> Proses</button>
                    <a href="{{ route('pesanan.index') }}" class="btn btn-rose"> Kembali</a>
                </div>
            </form>
        </div>
    </div>
    <div class="contianer grid grid-cols-1 xl:grid-cols-2 mt-5">
        <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-lg">
            <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
                <h1 class="text-xl text-gray-800">Konfigurasi Harga Per Plate</h1>
            </div>
            <form id="form-konfigurasi-harga-plate" class="p-5 flex flex-col gap-3">
                @csrf
                <div class="relative">
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga Per
                        Plate</label>
                    <input type="hidden" name="id" value="harga_plate">
                    <input type="text" name="harga" class="form-input angka" placeholder="Harga"
                        value="{{ $plate->value ?? '' }}">
                </div>
                <div
                    class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full border-t pt-3 mt-3">
                    <button type="submit" class="btn btn-indigo"><x-icon-sync /> Proses</button>
                    <a href="{{ route('pesanan.index') }}" class="btn btn-rose"> Kembali</a>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $('#form-konfigurasi-harga-drag').submit(function(e) {
            e.preventDefault();
            const formData = new FormData(this);
            const result = [];
            const ids = formData.getAll('id[]');
            const hargas = formData.getAll('harga[]');

            for (let i = 0; i < ids.length; i++) {
                result.push({
                    id: ids[i],
                    harga: hargas[i]
                });
            }

            $.ajax({
                type: "POST",
                url: "{{ route('cpesanan.drag') }}",
                data: {
                    'id': 'harga_drag',
                    'value': result,
                    '_token': '{{ csrf_token() }}'
                },
                dataType: "JSON",
                success: function(response) {
                    if (response.success) {
                        alert(response.message, 'success', 1000);
                        setTimeout(() => {
                            location.reload()
                        }, 1000);
                    }
                }
            });
        });

        $('#form-konfigurasi-harga-plate').submit(function(e) {
            e.preventDefault();
            const data = new FormData(this)
            $.ajax({
                type: "POST",
                url: "{{ route('cpesanan.plate') }}",
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.success) {
                        alert(response.message, 'success', 1000);
                        setTimeout(() => {
                            location.reload()
                        }, 1000);
                    }
                }
            });
        });
    </script>
@endpush
