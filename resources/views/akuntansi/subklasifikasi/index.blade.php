@extends('layouts.main')
@section('konten')
    <div class="relative container">
        <button id="btn-create-akun" class="btn btn-md btn-indigo mb-5">Tambah Akun</button>
        <button id="mapping-subklasifikasi" class="btn btn-md btn-rose mb-5">Update Jenis</button>
        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 border border-gray-300">
            <thead class="text-xs text-gray-100 uppercase bg-indigo-600 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-5 py-3">
                        Klasifikasi
                    </th>
                    <th scope="col" class="px-5 py-3">
                        Sub Klasifikasi
                    </th>
                    <th scope="col" class="px-5 py-3">
                        Saldo Normal
                    </th>
                    <th scope="col" class="px-5 py-3">
                        Jenis
                    </th>
                </tr>
            </thead>
            <tbody>
                <form id="form-subklasifikasi">
                    @foreach ($subKlasifikasi as $item)
                        <tr
                            class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row"
                                class="px-5 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $item->klasifikasi->nama }}
                            </th>
                            <td class="px-5 py-4">
                                {{ $item->id }} - {{ $item->nama }}
                            </td>
                            <td class="px-5 py-4">
                                {{ $item->saldo_normal }}
                            </td>
                            <td class="px-5 py-4 max-w-[100px]">
                                <input type="hidden" name="id[]" value="{{ $item->id }}">
                                <select id="jenis" name="jenis[]"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option value=""></option>
                                    <option value="asset" {{ $item->jenis == 'asset' ? 'selected' : '' }}>ASSET</option>
                                    <option value="kewajiban" {{ $item->jenis == 'kewajiban' ? 'selected' : '' }}>KEWAJIBAN
                                    </option>
                                    <option value="modal" {{ $item->jenis == 'modal' ? 'selected' : '' }}>MODAL</option>
                                    <option value="pendapatan" {{ $item->jenis == 'pendapatan' ? 'selected' : '' }}>
                                        PENDAPATAN</option>
                                    <option value="hpp" {{ $item->jenis == 'hpp' ? 'selected' : '' }}>HPP</option>
                                    <option value="beban" {{ $item->jenis == 'beban' ? 'selected' : '' }}>BEBAN</option>
                                    <option value="beban_non_usaha"
                                        {{ $item->jenis == 'beban_non_usaha' ? 'selected' : '' }}>BEBAN NON USAHA</option>
                                </select>
                            </td>
                        </tr>
                    @endforeach
                </form>
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $('#mapping-subklasifikasi').click(function(e) {
            e.preventDefault();
            const formData = $('#form-subklasifikasi').serializeArray();

            const ids = formData.filter(e => e.name === 'id[]').map(e => e.value);
            const jenis = formData.filter(e => e.name === 'jenis[]').map(e => e.value);

            const data = ids.map((id, index) => {
                return {
                    id: id,
                    jenis: jenis[index] || null
                };
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{{ route('akun.mapping_jenis') }}",
                data: $('#form-subklasifikasi').serialize(),

                success: function(response) {
                    if (response.success) {
                        alert('Mapping Berhasil', 'success', 1000);
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000);
                    }
                }
            });
        });
    </script>
@endpush
