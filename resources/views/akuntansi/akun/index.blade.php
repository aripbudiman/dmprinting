@extends('layouts.main')
@section('konten')
    <div class="relative container">
        <button id="btn-create-akun" class="btn btn-md btn-indigo mb-5">Tambah Akun</button>
        <button id="mapping-saldo-normal" class="btn btn-md btn-rose mb-5">Mapping Saldo Normal</button>
        <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 border border-gray-300">
            <thead class="text-xs text-gray-100 uppercase bg-indigo-600 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Klasifikasi
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Sub Klasifikasi
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Saldo Normal
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nama Akun
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Saldo Normal
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($akun as $item)
                    <tr
                        class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $item->subklasifikasi->klasifikasi->nama }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $item->subklasifikasi->id }} - {{ $item->subklasifikasi->nama }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $item->subklasifikasi->saldo_normal }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $item->id }} - {{ $item->nama }}
                        </td>
                        <td class="px-6 py-4">
                            {{ $item->saldo_normal }}
                        </td>
                        <td class="px-6 py-4">
                            <a href="#" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('akuntansi.akun.modal-create')
@endsection
@push('scripts')
    <script type="text/javascript">
        let modal;
        const $targetEl = document.getElementById('modal-create-akun');
        // options with default values
        const options = {
            placement: 'top-right',
            backdrop: 'static',
            backdropClasses: 'bg-gray-900/40 dark:bg-gray-900/80 fixed inset-0 z-40',
            closable: true,
            onHide: () => {
                console.log('modal is hidden');
            },
            onShow: () => {
                console.log('modal is shown');
            },
            onToggle: () => {
                console.log('modal has been toggled');
            },
        };
        $('#btn-create-akun').click(function() {
            // Buat instance modal hanya jika belum dibuat
            if (!modal) {
                modal = new Modal($targetEl, options);
            }

            // Tampilkan modal
            modal.show();
        });

        // Contoh untuk menutup modal
        $('#close-modal').click(function() {
            if (modal) {
                modal.hide();
            }
        });

        $('#mapping-saldo-normal').click(function() {
            $.ajax({
                type: "POST",
                url: "{{ route('akun.mapping_saldo_normal') }}",
                data: {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    data: 'mapping'
                },
                dataType: "JSON",
                success: function(response) {
                    if (response.success) {
                        alert('Mapping berhasil', 'success', 1000);
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000);
                    }
                }
            });
        })
    </script>
@endpush
