@extends('layouts.main')
@section('konten')
    <div class="container shadow-md border border-gray-300 rounded-md">
        <div class="px-5 py-2 text-gray-700 text-xl bg-gray-200 flex justify-between">
            <h2>Piutang</h2>
            <button class="btn btn-md btn-indigo">Buat Piutang</button>
        </div>
        <div class="bg-white p-5">
            <div class="flex justify-between items-center pb-5">
                <x-show-data :show="$payload['show']" />
                <x-search :keyword="$payload['keyword']" />
            </div>
            <div class="relative overflow-x-auto">
                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 border">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-6 py-3">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Tanggal
                            </th>
                            <th scope="col" class="px-6 py-3">
                                No Ref
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Keterangan
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Nominal
                            </th>
                            <th scope="col" class="px-6 py-3">
                                AKSI
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data->data as $key=>$item)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <th scope="row"
                                    class="px-6 py-2.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{ $key + 1 }}
                                </th>
                                <td class="px-6 py-2.5">
                                    {{ formatDate($item->tanggal) }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ $item->no_ref }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ $item->keterangan }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ rupiah($item->nominal) }}
                                </td>
                                <td class="px-6 py-2.5">
                                    <button class="btn btn-md btn-indigo">Detail</button>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="py-4 px-3 border-b">Total Piutang</td>
                            <td class="border-b px-3">{{ rupiah($data->total_debet - $data->total_kredit) }}</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="mt-5">
                    {!! $data->links !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
@endpush
