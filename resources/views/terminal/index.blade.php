@extends('layouts.main')
@section('konten')
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-6xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">Command Prompt</h1>
        </div>
        <div class="p-5 flex gap-3">
            <div class="w-80 bg-gray-200 p-2 rounded-md flex flex-col gap-2">
                <button id="config-cache" class="btn btn-rose rounded-none w-full">Config Cache</button>
                <button id="clear-config" class="btn btn-rose rounded-none w-full">Clear Config</button>
                <button id="route-cache" class="btn btn-indigo rounded-none w-full">Route Cache</button>
                <button id="route-list" class="btn btn-indigo rounded-none w-full">Route List</button>
                <button id="route-clear" class="btn btn-indigo rounded-none w-full">Route Clear</button>
                <button id="clear-cache" class="btn btn-violet rounded-none w-full">Clear Cache</button>
                <button id="clear-view" class="btn btn-violet rounded-none w-full">Clear View</button>
            </div>
            <div
                class="bg-slate-700 relative w-full text-gray-300 h-[500px] rounded-xl overflow-y-auto border border-gray-800">
                <div class="sticky top-0 flex justify-between shadow-md bg-slate-800 px-3 py-2">
                    <h1>Terminal</h1>
                    <div>
                        <button class="btn btn-sm bg-yellow-500 rounded-full text-gray-300 h-3 w-3"></button>
                        <button class="btn btn-sm btn-emerald rounded-full text-gray-300 h-3 w-3"></button>
                        <button id="clear" class="btn btn-sm btn-rose rounded-full text-gray-300 h-3 w-3"></button>
                    </div>
                </div>
                <div id="output" class="py-2 px-3"></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#config-cache').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "GET",
                    url: "{{ route('terminal.config-cache') }}",
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        console.log(error)
                    }
                });
            });
            $('#route-cache').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.route-cache') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#route-list').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.route-list') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#route-clear').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.route-clear') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#clear-cache').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.clear-cache') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#clear-view').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.clear-view') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#clear-config').click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('terminal.clear-config') }}",
                    type: 'GET',
                    success: function(response) {
                        $('#output').append('<pre>' + response + '</pre>');
                    },
                    error: function(xhr, status, error) {
                        $('#output').append('Error: ' + error);
                    }
                });
            });
            $('#clear').click(function(e) {
                e.preventDefault();
                $('#output').html('');
            })
        });
    </script>
@endpush
