@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.jurnal_umum')" :actions="'detail'">
        <x-slot name="header">
            <x-header-table :title="'Jurnal Umum'">
                <div class="space-x-2">
                    <a href="{{ route('jurnal_umum.create') }}" class="btn btn-md rn btn-indigo">
                        Buat Jurnal Umum
                    </a>
                </div>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$payload['show']" />
            <x-search :keyword="$payload['keyword']" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script>
        function _detailItem(id, el) {
            window.location.href = "{{ route('jurnal_umum.show', ':id') }}".replace(':id', id);
        }
    </script>
@endpush
