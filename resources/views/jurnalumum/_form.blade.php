<div class="border border-gray-400 rounded-lg max-w-[1000px]">
    <div class="flex justify-between items-center bg-gray-300 px-5 py-2 rounded-t-lg border-b border-gray-400">
        <h1 class="text-xl text-gray-800">Jurnal Umum</h1>
    </div>
    <div class="bg-white p-5 rounded-lg">
        <form id="form-jurnalumum" method="POST" class="space-y-2">
            @csrf
            @if ($type == 'PUT')
                @method('PUT')
            @endif
            <div class="flex space-x-5 items-center">
                <div class="w-1/2 flex space-x-5">
                    <div class="mb-3 w-1/2">
                        <label for="tanggal" class="form-label">Tanggal</label>
                        <input type="date" class="form-input" id="tanggal" name="tanggal"
                            value="{{ !empty($jurnalUmum) ? $jurnalUmum->tanggal : date('Y-m-d') }}"
                            {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>
                    </div>
                    <div class="mb-3 w-1/2">
                        <label for="tanggal" class="form-label">No Ref</label>
                        <input type="text" class="form-input" id="no_ref" name="no_ref"
                            value="{{ !empty($jurnalUmum) ? $jurnalUmum->no_ref : '-' }}"
                            {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>
                    </div>
                </div>
                <div class="mb-3 w-1/2">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <textarea class="form-input" id="keterangan" name="keterangan" rows="2"
                        {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>{{ !empty($jurnalUmum) ? $jurnalUmum->keterangan : '' }}</textarea>
                </div>
            </div>
            <div class="pt-3">
                <div class="flex flex-col">
                    <div class="-m-1.5 overflow-x-auto">
                        <div class="p-1.5 min-w-full inline-block align-middle">
                            <div
                                class="border rounded-lg shadow overflow-hidden dark:border-neutral-700 dark:shadow-gray-900">
                                <table class="min-w-full divide-y divide-gray-200 dark:divide-neutral-700">
                                    <thead class="bg-gray-50 dark:bg-neutral-700">
                                        <tr>
                                            <th scope="col"
                                                class="px-3 py-3 text-start text-xs font-medium text-gray-500 uppercase dark:text-neutral-400">
                                                Akun</th>
                                            <th scope="col"
                                                class="px-3 py-3 text-start text-xs font-medium text-gray-500 uppercase dark:text-neutral-400">
                                                Debet</th>
                                            <th scope="col"
                                                class="px-3 py-3 text-start text-xs font-medium text-gray-500 uppercase dark:text-neutral-400">
                                                Kredit</th>
                                            <th scope="col"
                                                class="px-3 py-3 text-end text-xs font-medium text-gray-500 uppercase dark:text-neutral-400">
                                                Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="divide-y divide-gray-200 dark:divide-neutral-700" id="list-row">
                                        @if (!empty($jurnalUmum))
                                            @foreach ($jurnalUmum->jurnal as $jurnal)
                                                <tr>
                                                    <td
                                                        class="px-3 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-neutral-200">
                                                        <select name="akun[]"
                                                            class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                            {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>
                                                            <option selected>Pilih Akun</option>
                                                            @foreach ($akun as $item)
                                                                <option value="{{ $item->id }}"
                                                                    @if ($item->id == $jurnal->kode_akun) selected @endif>
                                                                    {{ $item->nama }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td
                                                        class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                        <input type="text" name="debet[]" oninput="decimal(this)"
                                                            class="form-input py-2 text-right" placeholder="0"
                                                            value="{{ rupiah($jurnal->debet) ?? 0 }}"
                                                            {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>
                                                    </td>
                                                    <td
                                                        class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                        <input type="text" name="kredit[]" oninput="decimal(this)"
                                                            class="form-input py-2 text-right" placeholder="0"
                                                            value="{{ rupiah($jurnal->kredit) ?? 0 }}"
                                                            {{ !empty($jurnalUmum) && empty($url) ? 'disabled' : '' }}>
                                                    </td>
                                                    <td
                                                        class="px-1 py-4 whitespace-nowrap text-center text-sm font-medium">
                                                        <button type="button"
                                                            class="btn btn-sm btn-rose rounded-none delete" disabled
                                                            onclick="deleteRow(this)">Delete</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-neutral-200">
                                                    <select name="akun[]"
                                                        class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                        <option selected>Pilih Akun</option>
                                                        @foreach ($akun as $item)
                                                            <option value="{{ $item->id }}">{{ $item->nama }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                    <input type="text" name="debet[]" oninput="decimal(this)"
                                                        class="form-input py-2 text-right" placeholder="0">
                                                </td>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                    <input type="text" name="kredit[]" oninput="decimal(this)"
                                                        class="form-input py-2 text-right" placeholder="0">
                                                </td>
                                                <td class="px-1 py-4 whitespace-nowrap text-center text-sm font-medium">
                                                    <button type="button"
                                                        class="btn btn-sm btn-rose rounded-none delete" disabled
                                                        onclick="deleteRow(this)">Delete</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-neutral-200">
                                                    <select name="akun[]"
                                                        class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                                        <option selected>Pilih Akun</option>
                                                        @foreach ($akun as $item)
                                                            <option value="{{ $item->id }}">{{ $item->nama }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                    <input type="text" name="debet[]" oninput="decimal(this)"
                                                        class="form-input py-2 text-right" placeholder="0">
                                                </td>
                                                <td
                                                    class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                                    <input type="text" name="kredit[]" oninput="decimal(this)"
                                                        class="form-input py-2 text-right" placeholder="0">
                                                </td>
                                                <td
                                                    class="px-1 py-4 whitespace-nowrap text-center text-sm font-medium">
                                                    <button type="button"
                                                        class="btn btn-sm btn-rose rounded-none delete" disabled
                                                        onclick="deleteRow(this)">Delete</button>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if (!empty($url))
                <div>
                    <button type="button" id="add-row" class="btn btn-md btn-emerald">Tambah Baris</button>
                </div>
            @endif
            <div class="grid grid-cols-2 gap-x-5 pt-3">
                @if (!empty($url))
                    <button type="submit" class="btn btn-indigo">Simpan</button>
                @else
                    <a href="{{ route('jurnal_umum.edit', $jurnalUmum->id) }}" id="edit"
                        class="btn btn-indigo">Edit</a>
                @endif
                <a href="{{ route('jurnal_umum.index') }}" class="btn btn-rose">Kembali</a>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $('#add-row').click(function(e) {
            e.preventDefault();
            const html = `<tr>
                            <td class="px-3 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-neutral-200">
                                <select name="akun[]"
                                    class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected>Pilih Akun</option>
                                    @foreach ($akun as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}
                                        </option>
                                    @endforeach
                                </select>
                            </td>
                            <td
                                class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                <input type="text" name="debet[]" oninput="decimal(this)" class="form-input py-2 text-right" placeholder="0">
                            </td>
                            <td
                                class="px-3 py-4 whitespace-nowrap text-sm text-gray-800 dark:text-neutral-200">
                                <input type="text" name="kredit[]" oninput="decimal(this)" class="form-input py-2 text-right" placeholder="0">
                            </td>
                            <td class="px-1 py-4 whitespace-nowrap text-center text-sm font-medium">
                                <button type="button" class="btn btn-sm btn-rose rounded-none delete"
                                    disabled onclick="deleteRow(this)">Delete</button>
                            </td>
                        </tr>`
            $('#list-row').append(html);
            validasiBaris()
        })

        function deleteRow(element) {
            $(element).closest('tr').remove();
            validasiBaris()
        }

        function validasiBaris(jumlahBaris = $('#list-row').children().length) {
            if (jumlahBaris > 2) {
                $('.delete').prop('disabled', false)
            } else {
                $('.delete').prop('disabled', true)
            }
        }

        $('#form-jurnalumum').submit(function(e) {
            e.preventDefault();
            const data = new FormData(this)
            let totalDebet = 0;
            let totalKredit = 0;
            // Iterate over FormData entries
            data.forEach((value, key) => {
                if (key === 'akun[]') {
                    if (value == 'Pilih Akun') {
                        alert('Akun harus dipilih!', 'warning');
                        return false
                    }
                }
                if (key === 'debet[]') {
                    totalDebet += parseFloat(value.parseRupiah()) || 0;
                } else if (key === 'kredit[]') {
                    totalKredit += parseFloat(value.parseRupiah()) || 0;
                }
            });

            if (totalDebet !== totalKredit) {
                alert('Total Debet dan Kredit tidak seimbang!', 'warning');
                return false
            } else if (totalDebet == 0 || totalKredit == 0) {
                alert('Total Debet dan Kredit tidak boleh 0!', 'warning');
                return false
            }

            $.ajax({
                type: "POST",
                url: "{{ $url ?? '' }}",
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status === 'success') {
                        window.location.href = "{{ route('jurnal_umum.index') }}"
                    }
                },
                error: function(data) {
                    console.log(data);
                }
            });
        })

        function decimal(param) {
            return $(param).val(param.value.formatRupiah())
        }
    </script>
@endpush
