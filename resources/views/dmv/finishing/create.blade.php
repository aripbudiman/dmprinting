@extends('layouts.main')
@section('konten')
    <div id="app">
        <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
            <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
                <h1 class="text-xl text-gray-800">{{ isset($finishing) ? 'Edit Finishing' : 'Buat Finishing Baru' }}</h1>
            </div>
            <form id="form-finishing" class="p-5 flex flex-col gap-3">
                @csrf
                <div class="sm:hidden hidden sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
                    <label for="id" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Id</label>
                    <input type="text" v-model="payload.id" id="id" value="{{ $finishing->id ?? '' }}"
                        name="id" class="dmv-input" placeholder="id" readonly>
                </div>
                <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
                    <label for="deskripsi"
                        class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Deskripsi</label>
                    <input type="text" v-model="payload.deskripsi" value="{{ $finishing->deskripsi ?? '' }}"
                        name="deskripsi" class="dmv-input" placeholder="deskripsi">
                </div>
                <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
                    <label for="nama" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Harga</label>
                    <input type="text" v-model="payload.harga" value="{{ $finishing->harga ?? '' }}" name="nama"
                        class="dmv-input" placeholder="harga">
                </div>
                <div class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full">
                    <button type="button" @click="tes"
                        class="btn btn-indigo">{{ isset($finishing) ? 'Update' : 'Simpan' }}</button>
                    <a href="{{ route('dmv.finishing') }}" class="btn btn-rose">Cancel</a>
                </div>
            </form>
            <button class="btn" @click="tes">tes</button>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        import {
            createApp,
            ref,
            reactive
        } from '/js/vue3.5.11.js'

        createApp({
            setup() {
                let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                const payload = reactive({
                    _token: token,
                    id: '',
                    deskripsi: '',
                    harga: ''
                });

                const store = () => {
                    $.ajax({
                        type: "POST",
                        url: "{{ $url }}",
                        data: payload,
                        dataType: "JSON",
                        success: function(response) {
                            console.log(response)
                        }
                    });
                }

                const tes = () => store()

                return {
                    payload,
                    tes
                }
            }

        }).mount('#app')
    </script>
@endpush
