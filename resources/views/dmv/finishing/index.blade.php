@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.finishing')" :title="'List Finishing'">
        <x-slot name="header">
            <x-header-table :title="'List Finishing'">
                <a href="{{ route('dmv.finishing.create') }}" class="btn btn-md rn btn-indigo">
                    Buat Finishing Baru
                </a>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
            <x-search />
        </x-slot>
    </x-table>
@endsection
