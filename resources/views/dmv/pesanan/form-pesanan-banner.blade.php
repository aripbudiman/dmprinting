<div class="contianer grid grid-cols-1 xl:grid-cols-2">
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">{{ isset($pesanan) ? 'Ubah Pesanan' : 'Buat Pesanan Baru' }}</h1>
        </div>
        <form id="form-pesanan" class="p-5 flex flex-col gap-3">
            @csrf
            <div class="grid grid-cols-2 gap-5">
                <div>
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Pilih
                        Tanggal</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <x-icon-calender />
                        </div>
                        <input datepicker datepicker-buttons datepicker-format="yyyy-mm-dd" datepicker-autoselect-today
                            type="text" class="date-filter" name="tanggal" placeholder="Pilih Tanggal"
                            autocomplete="off" value="{{ isset($pesanan) ? $pesanan->tanggal : '' }}">
                    </div>
                </div>
                <div class="mb-1 relative">
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white  required">Customer</label>
                    <input type="text" id="customer" name="customer" class="form-input"
                        placeholder="&#128269; Pilih Customer"
                        value="{{ isset($pesanan->customer) ? $pesanan->customer->nama : '' }}">
                    <input type="hidden" id="customer_id" name="customer_id" class="form-input"
                        value="{{ isset($pesanan->customer) ? $pesanan->customer->id : '' }}                        ">
                </div>
            </div>
            <div>
                <label for="message" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Nama
                    Cetakan</label>
                <textarea id="message" name="nama_cetakan" rows="3" class="form-input" placeholder="Ketik disini...">{{ isset($pesanan) ? $pesanan->nama_cetakan : old('nama_cetakan') }}</textarea>
            </div>
            <div>
                <label for="tipe_id"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Tipe</label>
                <select id="tipe_id" name="tipe_id" class="form-input">
                    <option value="0" selected>Pilih Tipe</option>
                    @foreach ($tipe as $item)
                        <option value="{{ $item->id }}"
                            {{ isset($pesanan) && $pesanan->tipe_id == $item->id ? 'selected' : '' }}>
                            {{ $item->nama }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label for="bahan_id"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Bahan</label>
                <select id="bahan_id" class="form-input" name="bahan_id">
                    <option selected>Pilih Bahan</option>
                </select>
            </div>
            <div>
                <label for="lebar_id"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Lebar</label>
                <select id="lebar_id" class="form-input" name="lebar_id">
                    <option selected data-lebar="0">Pilih Lebar</option>
                </select>
            </div>
            <div>
                <label for="finishing_id"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Finishing</label>
                <select id="finishing_id" class="form-input" name="finishing_id">
                    <option selected data-finishing="0">Pilih Finishing</option>
                    @forelse ($finishing as $item)
                        <option value="{{ $item->id }}" data-finishing="{{ $item->harga }}"
                            {{ isset($pesanan) && $pesanan->finishing_id == $item->id ? 'selected' : '' }}>
                            {{ $item->getFinishing() }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            <div class="grid grid-cols-2 gap-3">
                <div class="grid grid-cols-2 gap-3">
                    <div>
                        <label for="panjang"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Panjang</label>
                        <input type="numeric" id="panjang" name="panjang" class="form-input total" value="1">
                    </div>
                    <div>
                        <label for="Qty"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Qty</label>
                        <input type="numeric" id="qty" min="1" max="100" name="qty"
                            class="form-input total" value="1">
                    </div>
                </div>
                <div>
                    <label for="total"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Total
                        Harga</label>
                    <input type="text" class="form-input" name="harga" value="0" readonly>
                </div>
            </div>
            <div
                class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full border-t pt-3 mt-3">
                <button type="submit" class="btn btn-indigo"><x-icon-sync /> Proses</button>
                <a href="{{ route('pesanan.index') }}" class="btn btn-rose"> Kembali</a>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            if ($('#tipe_id').val() != '0') {
                $('#tipe_id').trigger('change');
            }
        });
        $('#tipe_id').change(function() {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'tipe_id': $(this).val()
                },
                success: function(response) {
                    bahan(response.bahan_id);
                    $('#bahan_id').html(response.bahan);
                    hitungTotal()
                }
            });
        });
        $('#bahan_id').change(function() {
            bahan($(this).val());
        });
        $('#lebar_id').change(function() {
            hitungTotal()
        });
        $('#finishing_id').change(function() {
            hitungTotal()
        });

        $('#panjang').keyup(function() {
            hitungTotal()
        });
        $('#qty').keyup(function() {
            hitungTotal()
        });

        function bahan(param = '') {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'bahan_id': param
                },
                success: function(response) {
                    $('#lebar_id').html(response.lebar);
                    hitungTotal()
                }
            });
        }

        function hitungTotal() {
            let lebar = $('#lebar_id option:selected').data('lebar');
            let finishing = $('#finishing_id option:selected').data('finishing');
            let panjang = $('#panjang').val() ?? 1;
            let qty = $('#qty').val() ?? 1;
            let total = (parseFloat(lebar) + parseFloat(finishing)) * parseFloat(panjang) * parseInt(qty)
            $('[name="harga"]').val(total.toLocaleString('id-ID', {
                style: 'currency',
                currency: 'IDR',
                minimumFractionDigits: 0
            }));
        }
        hitungTotal()

        $('#customer').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ Request::url() }}",
                    dataType: "json",
                    data: {
                        'autocomplete': 1,
                        'search': request.term
                    },
                    success: function(data) {
                        response(data.data);
                    }
                });
            },
            select: function(event, ui) {
                $('#customer').val(ui.item.label);
                $('#customer_id').val(ui.item.value);
                return false;
            }
        })

        $('#form-pesanan').submit(function(e) {
            e.preventDefault();
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ $route }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.success) {
                        alert(response.message, 'success');
                        setTimeout(() => {
                            window.location.reload()
                        }, 2000);
                    }
                },
                error: function(xhr, status, error) {
                    alert('Silahkan isi yang bertanda *', 'warning');
                    console.log(error)
                }
            });
        });
    </script>
@endpush
