<div class="contianer grid grid-cols-1 xl:grid-cols-2">
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">{{ isset($pesanan) ? 'Ubah Pesanan' : 'Buat Pesanan Baru' }}</h1>
        </div>
        <form id="form-pesanan-cetakan" class="p-5 flex flex-col gap-3">
            @csrf
            @if (isset($update))
                @method('PUT')
            @endif
            <div class="grid grid-cols-2 gap-5">
                <div>
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Pilih
                        Tanggal</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <x-icon-calender />
                        </div>
                        <input datepicker datepicker-buttons datepicker-format="yyyy-mm-dd" datepicker-autoselect-today
                            type="text" class="date-filter" name="tanggal" placeholder="Pilih Tanggal"
                            autocomplete="off" value="{{ isset($pesanan) ? $pesanan->tanggal : '' }}">
                    </div>
                </div>
                <div class="mb-1 relative">
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white  required">Customer</label>
                    <input type="text" id="customer" name="customer" class="form-input"
                        placeholder="&#128269; Pilih Customer"
                        value="{{ isset($pesanan->customer) ? $pesanan->customer->nama : '' }}">
                    <input type="hidden" id="customer_id" name="customer_id" class="form-input"
                        value="{{ isset($pesanan->customer) ? $pesanan->customer->id : '' }}                        ">
                </div>
            </div>
            <div>
                <label for="message" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Nama
                    Cetakan</label>
                <textarea id="message" name="nama_cetakan" rows="3" class="form-input" placeholder="Ketik disini...">{{ $pesanan->nama_cetakan ?? '' }}</textarea>
            </div>
            <div class="grid grid-cols-2 space-x-5">
                <div class="relative" id="jumlah">
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Jumlah
                        Cetak</label>
                    <input type="text" id="jumlah_cetakan" name="jumlah_cetakan" class="form-input angka"
                        placeholder="Jumlah Cetak" value="{{ $pesanan->jumlah_cetakan ?? '' }}" maxlength="7">
                    <div class="counter hidden">
                        <span id="charCount">0</span>/7
                    </div>
                </div>
                <div>
                    <label for="default-input"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Jumlah
                        Drag</label>
                    <input type="text" id="jumlah_drag" name="jumlah_drag" class="form-input"
                        placeholder="Jumlah Drag" value="{{ $pesanan->jumlah_drag ?? '' }}">
                </div>
            </div>
            <div>
                <label for="jenis_cetak"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Jenis Cetak</label>
                <ul class="grid w-full gap-6 md:grid-cols-3">
                    <li>
                        <input type="radio" id="1-muka" name="jenis_cetak" value="1-muka" class="hidden peer"
                            required {{ !empty($pesanan) && $pesanan->jenis_cetak == '1-muka' ? 'checked' : '' }} />
                        <label for="1-muka"
                            class="dark:peer-checked:text-blue-500 peer-checked:border-blue-600 peer-checked:text-blue-600 form-input text-gray-700">
                            <div class="block">
                                <div class="w-full font-semibold">1 Muka</div>
                            </div>
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="2-BBS" name="jenis_cetak" value="2-BBS" class="hidden peer"
                            {{ !empty($pesanan) && $pesanan->jenis_cetak == '2-BBS' ? 'checked' : '' }}>
                        <label for="2-BBS"
                            class="dark:peer-checked:text-blue-500 peer-checked:border-blue-600 peer-checked:text-blue-600 form-input text-gray-700">
                            <div class="block">
                                <div class="w-full font-semibold">BBS</div>
                            </div>
                        </label>
                    </li>
                    <li>
                        <input type="radio" id="2-BBK" name="jenis_cetak" value="2-BBK" class="hidden peer"
                            {{ !empty($pesanan) && $pesanan->jenis_cetak == '2-BBK' ? 'checked' : '' }}>
                        <label for="2-BBK"
                            class="dark:peer-checked:text-blue-500 peer-checked:border-blue-600 peer-checked:text-blue-600 form-input text-gray-700">
                            <div class="block">
                                <div class="w-full font-semibold">BBK</div>
                            </div>
                        </label>
                    </li>
                </ul>
            </div>
            <div class="relative" id="plate">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Jumlah
                    Plate</label>
                <input type="text" id="jumlah_plate" name="jumlah_plate" class="form-input angka"
                    placeholder="Jumlah Plate" value="{{ $pesanan->jumlah_plate ?? '' }}" maxlength="7">
                <div class="counter hidden">
                    <span id="charCount2">0</span>/7
                </div>
            </div>
            <div class="relative hidden">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga Per
                    Plate</label>
                <input type="text" id="harga_plate" name="harga_per_plate" class="form-input angka"
                    placeholder="Harga Plate" value="{{ $pesanan->harga_per_plate ?? '' }}">
            </div>
            <div class="relative hidden">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga Per
                    Drag</label>
                <input type="text" id="harga_drag" name="harga_per_drag" class="form-input angka"
                    placeholder="Harga Drag" value="{{ $pesanan->harga_per_drag ?? '' }}">
            </div>
            <div class="relative hidden">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Total Harga
                    Perdrag</label>
                <input type="text" id="total_harga_perdrag" name="total_harga_perdrag" class="form-input angka"
                    placeholder="Harga Per Drag" value="{{ $pesanan->total_harga_perdrag ?? '' }}">
            </div>
            <div class="relative">
                <label for="default-input"
                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-white required">Harga Total</label>
                <input type="text" id="harga_total" name="total_harga" class="form-input angka"
                    placeholder="Harga Total" value="{{ isset($pesanan) ? rupiah($pesanan->total_harga) : '' }}"
                    readonly>
            </div>
            <div
                class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full border-t pt-3 mt-3">
                <button type="submit" class="btn btn-indigo"><x-icon-sync /> Proses</button>
                <a href="{{ route('pesanan-cetakan.index') }}" class="btn btn-rose"> Kembali</a>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.angka').keyup(function() {
                this.value = this.value.replace(/[^0-9]/g, '');
            });
            let data = @json($drag);
            localStorage.setItem('hargaDrag', JSON.stringify(data));
            localStorage.setItem('jumlahDrag', 0);
            localStorage.setItem('jenisCetak', 1);
        });

        $('#jumlah_plate').keyup(function() {
            const hargaDrag = JSON.parse(localStorage.getItem('hargaDrag'))
            if (this.value <= 3) {
                let data = hargaDrag[0].harga
                localStorage.setItem('hargaPerDrag', data)
            } else {
                let data = hargaDrag[1].harga
                localStorage.setItem('hargaPerDrag', data)
            }
        })

        const jumlahDrug = (angka) => {
            return ($('#jumlah_cetakan').val() * angka) - 1000
        }

        $('[name="jenis_cetak"]').click(function() {
            let angkaJenis = this.value.split('-')[0];
            let hurufJenis = this.value.split('-')[1];
            let updateDrag = $('#jumlah_cetakan').val() * angkaJenis - 1000
            localStorage.setItem('jumlahDrag', updateDrag)
            localStorage.setItem('jenisCetak', angkaJenis)
            hargaPerPlate()
            hargaPerDrag()
            grandTotal()
        })

        $('#jumlah_cetakan').keyup(function() {
            const jenisCetak = localStorage.getItem('jenisCetak')
            const jumlahDrug = this.value * jenisCetak - 1000
            if (this.value >= 1000) {
                $('#jumlah_drag').val(jumlahDrug)
                localStorage.setItem('jumlahDrag', jumlahDrug)
            } else {
                $('#jumlah_drag').val(0)
                localStorage.setItem('jumlahDrag', 0)
            }
            var counter = document.getElementById('charCount');
            counter.textContent = this.value.length;
            if (this.value.length > 0) {
                $('#jumlah').find('.counter').removeClass('hidden')
            } else {
                $('#jumlah').find('.counter').addClass('hidden')
            }
            hargaPerDrag()
            hargaPerPlate()
            grandTotal()
        })

        $('#jumlah_plate').keyup(function() {
            hargaPerPlate()
            hargaPerDrag()
            grandTotal()
            var counter = document.getElementById('charCount2');
            counter.textContent = this.value.length;
            if (this.value.length > 0) {
                $('#plate').find('.counter').removeClass('hidden')
            } else {
                $('#plate').find('.counter').addClass('hidden')
            }
        })

        function hargaPerPlate(angka = '') {
            const plate = '{{ $plate }}'
            const total = $('#jumlah_plate').val() * plate
            $('#harga_plate').val(total.rupiah(0))
        }

        function hargaPerDrag() {
            let drag = localStorage.getItem('hargaPerDrag')
            const hargaDrag = $('#jumlah_plate').val().parseRupiah() * drag
            $('#harga_drag').val(hargaDrag.rupiah(0))
            const totalDrag = localStorage.getItem('jumlahDrag')
            const total = hargaDrag * totalDrag
            $('#total_harga_perdrag').val(total.rupiah(0))
        }

        function grandTotal() {
            const hargaPlate = $('#harga_plate').val().parseRupiah()
            const hargaDrag = $('#total_harga_perdrag').val().parseRupiah()
            const total = parseFloat(hargaPlate) + parseFloat(hargaDrag)
            $('#harga_total').val(total.rupiah(0))
        }

        $('#customer').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "{{ Request::url() }}",
                    dataType: "json",
                    data: {
                        'autocomplete': 1,
                        'search': request.term
                    },
                    success: function(data) {
                        response(data.data);
                    }
                });
            },
            select: function(event, ui) {
                $('#customer').val(ui.item.label);
                $('#customer_id').val(ui.item.value);
                return false;
            }
        })

        $('#form-pesanan-cetakan').submit(function(e) {
            e.preventDefault();
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ $url }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.success) {
                        alert(response.message, 'success', 2000);
                        setTimeout(() => {
                            window.location.reload()
                            $('#form-pesanan-cetakan')[0].reset();
                        }, 2000);
                    }
                },
                error: function(xhr, status, error) {
                    console.log(error)
                }
            });
        });
    </script>
@endpush
