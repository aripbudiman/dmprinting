@extends('layouts.main')
@section('konten')
    @if (Request::is('dmv/pesanan/create') || Request::is('dmv/pesanan/*/edit'))
        @include('dmv.pesanan.form-pesanan-banner')
    @else
        @include('dmv.pesanan.form-pesanan-cetakan')
    @endif
@endsection
