<div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
    <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
        <h1 class="text-xl text-gray-800">{{ isset($tipe) ? 'Edit Tipe' : 'Buat Tipe Baru' }}</h1>
    </div>
    <form id="form-tipe" class="p-5 flex flex-col gap-3">
        @csrf
        <div class="sm:hidden hidden sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="id" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Id</label>
            <input type="text" id="id" value="{{ $tipe->id ?? '' }}" name="id" class="dmv-input"
                placeholder="id" readonly>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="nama" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Nama Tipe</label>
            <input type="text" value="{{ $tipe->nama ?? '' }}" name="nama" class="dmv-input"
                placeholder="nama tipe">
        </div>
        <div class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full">
            <button type="submit" class="btn btn-indigo">{{ isset($tipe) ? 'Update' : 'Simpan' }}</button>
            <a href="{{ route('dmv.tipe.index') }}" class="btn btn-rose">Cancel</a>
        </div>
    </form>
</div>
