@extends('layouts.main')
@section('konten')
    @include('dmv.tipe._form')
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-tipe').on('submit', function(e) {
                e.preventDefault();
                const formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ route('dmv.tipe.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.success) {
                            alert(response.message, 'success');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error)
                    }
                });
            })
        });
    </script>
@endpush
