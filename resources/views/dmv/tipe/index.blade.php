@extends('layouts.main')
@section('konten')
    <x-table :data="$tipe" :columns="config('table_config.tipe')" :actions="'delete:edit'">
        <x-slot name="header">
            <x-header-table :title="'List Tipe'">
                <a href="{{ route('dmv.tipe.create') }}" class="btn btn-md rounded-none btn-indigo">
                    Buat Tipe Baru
                </a>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script type="text/javascript">
        function _deleteItem(id, el) {
            $.ajax({
                method: "POST",
                url: "{{ route('dmv.tipe.delete', ':id') }}".replace(':id', id),
                data: {
                    _token: '{{ csrf_token() }}'
                },
                dataType: "JSON",
                success: function(response) {
                    $(el).closest('tr').remove()
                    setInterval(() => {
                        location.reload()
                    }, 3000);
                    alert(response.message, 'success')
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText, 'error')
                }
            });
        }

        function _editItem(id) {
            window.location.href = "{{ route('dmv.tipe.edit', ':id') }}".replace(':id', id);
        }
    </script>
@endpush
