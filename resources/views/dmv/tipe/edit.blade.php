@extends('layouts.main')
@section('konten')
    @include('dmv.tipe._form')
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-tipe').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    method: "POST",
                    url: "{{ route('dmv.tipe.update', $tipe->id) }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.success) {
                            alert(response.message, 'success');
                            $('.btn-rose').text('Kembali');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error(error);
                    }
                });
            })
        });
    </script>
@endpush
