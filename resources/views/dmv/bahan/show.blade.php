@extends('layouts.main')
@section('konten')
    <div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">Detail Bahan</h1>
        </div>
        <div class="p-5">
            <ul class="flex flex-col">
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Id</span>
                        <span>{{ $bahan->id }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Kode Bahan</span>
                        <span>{{ $bahan->kode_bahan ?? '' }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Tipe</span>
                        <span>{{ $bahan->tipe->nama }}</span>
                    </div>
                </li>
                <li class="dmv-li">
                    <div class="flex items-center justify-between w-full">
                        <span>Nama</span>
                        <span>{{ $bahan->nama }}</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="px-5 pb-5  flex gap-x-2 justify-end">
            <a href="{{ route('dmv.bahan.edit', $bahan->id) }}" class="btn btn-lg btn-emerald">Edit</a>
            <a href="{{ route('dmv.bahan.index') }}" class="btn btn-lg btn-rose">Kembali</a>
        </div>
    </div>
@endsection
