<div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
    <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
        <h1 class="text-xl text-gray-800">{{ isset($bahan) ? 'Edit Bahan' : 'Buat Bahan Baru' }}</h1>
    </div>
    <form id="form-bahan" class="p-5 flex flex-col gap-3">
        @csrf
        <div class="sm:hidden hidden sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="id" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Id</label>
            <input type="text" id="id" value="{{ $bahan->id ?? '' }}" name="id" class="dmv-input"
                placeholder="id" readonly>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="kode_bahan" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Kode Bahan</label>
            <input type="text" value="{{ $bahan->kode_bahan ?? '' }}" name="kode_bahan" class="dmv-input"
                placeholder="nama bahan">
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="category_id" class="block xl:w-1/3 max-w-lg text-sm font-medium dark:text-white">Kategori
                Tipe</label>
            <select class="dmv-select capitalize" name="tipe_id">
                @foreach ($tipe as $item)
                    <option class="capitalize" value="{{ $item->id }}"
                        {{ isset($bahan) && $bahan->tipe_id == $item->id ? 'selected' : '' }}>
                        {{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="nama" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Nama Bahan</label>
            <input type="text" value="{{ $bahan->nama ?? '' }}" name="nama" class="dmv-input"
                placeholder="nama bahan">
        </div>
        <div class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full">
            <button type="submit" class="btn btn-indigo">{{ isset($bahan) ? 'Update' : 'Simpan' }}</button>
            <a href="{{ route('dmv.bahan.index') }}" class="btn btn-rose">Cancel</a>
        </div>
    </form>
</div>
