@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.bahan')" :tr="'show'" :actions="'delete:edit'">
        <x-slot name="header">
            <x-header-table :title="'List Bahan'">
                <a href="{{ route('dmv.bahan.create') }}" class="btn btn-md rn btn-indigo">
                    Buat Bahan Baru
                </a>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
            <x-search :keyword="$keyword" />
        </x-slot>
    </x-table>
@endsection
@push('scripts')
    <script type="text/javascript">
        function _show(id) {
            window.location.href = "{{ route('dmv.bahan.show', ':id') }}".replace(':id', id);
        }

        function _editItem(id) {
            window.location.href = "{{ route('dmv.bahan.edit', ':id') }}".replace(':id', id);
        }

        function _deleteItem(id, el) {
            $.ajax({
                method: "POST",
                url: "{{ route('dmv.bahan.delete', ':id') }}".replace(':id', id),
                data: {
                    _token: '{{ csrf_token() }}'
                },
                dataType: "JSON",
                success: function(response) {
                    $(el).closest('tr').remove()
                    setInterval(() => {
                        location.reload()
                    }, 3000);
                    alert(response.message, 'success')
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText, 'error')
                }
            });
        }
    </script>
@endpush
