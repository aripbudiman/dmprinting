@extends('layouts.main')
@section('konten')
    <x-table :data="$lebar" :columns="config('table_config.lebar')">
        <x-slot name="header">
            <x-header-table :title="'List Lebar'">
                <button type="button" class="btn btn-md rounded-none btn-indigo">
                    Buat Lebar Baru
                </button>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$show" />
        </x-slot>
    </x-table>
@endsection
