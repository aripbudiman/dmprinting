<div id="drawer-pelunasan-invoice"
    class="fixed top-0 left-0 z-40 h-screen p-4 overflow-y-auto transition-transform -translate-x-full bg-white w-[500px] dark:bg-gray-800"
    tabindex="-1" aria-labelledby="drawer-js-label">
    <div class="">
        <h5 id="drawer-js-label"
            class="mb-4 inline-flex items-center text-base font-semibold text-gray-500 dark:text-gray-400">
            <svg class="me-2 h-5 w-5 fill-indigo-700" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                    clip-rule="evenodd"></path>
            </svg>Lunasi Invoice
        </h5>
        <button id="close-drawer" type="button" aria-controls="drawer-example"
            class="absolute right-2.5 top-2.5 inline-flex h-8 w-8 items-center justify-center rounded-lg bg-transparent text-sm text-gray-400 hover:bg-indigo-200 hover:text-gray-900 dark:hover:bg-gray-600 dark:hover:text-white">
            <svg class="h-3 w-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 14 14">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
            </svg>
            <span class="sr-only">Close menu</span>
        </button>
    </div>
    <div>
        <form class="mb-6" id="form-angsuran">
            @csrf
            <div class="space-y-2 w-2/3">
                <div class="flex items-center">
                    <label for="no_invoice"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white w-1/2">No
                        Invoice</label>
                    <input type="text" id="no_invoice"
                        class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="No Invoice" readonly />
                </div>
                <div class="flex items-center">
                    <label for="customer"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white w-1/2">Customer</label>
                    <input type="text" id="customer"
                        class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="Customer" readonly />
                </div>
                <div class="flex items-center">
                    <label for="tanggal"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white w-1/2">Tanggal</label>
                    <input type="text" id="tanggal" name="tanggal"
                        class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="Tanggal" readonly value="{{ date('Y-m-d') }}" />
                </div>
                <div class="flex items-center">
                    <label for="sisa"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white w-1/2">Sisa Piutang</label>
                    <input type="text" id="sisa" name="sisa"
                        class="block w-full p-2 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="0" readonly value="" />
                </div>
            </div>

            <div class="relative overflow-x-auto overflow-y-auto my-5 max-h-44 border border-gray-200 pb-1">
                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    <thead
                        class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400 sticky top-0">
                        <tr>
                            <th scope="col" class="px-3 py-3">
                                No Pesanan
                            </th>
                            <th scope="col" class="px-3 py-3">
                                Nama Cetakan
                            </th>
                            <th scope="col" class="px-3 py-3">
                                Harga
                            </th>
                        </tr>
                    </thead>
                    <tbody id="detail-data">
                    </tbody>
                </table>
            </div>

            <div class="max-w-xl my-5" id="section-metode-pembayaran-drawer">
                <p class="col-span-4 text-gray-800">Metode Pembayaran</p>
                <div class="grid grid-cols-3 gap-2 mt-3">
                    @foreach ($metodePembayaran as $key => $item)
                        <label
                            class="flex items-center ps-4 border border-gray-300 rounded-sm has-[:checked]:border-indigo-500 has-[:checked]:bg-indigo-200 has-[:checked]:text-indigo-900 has-[:checked]:ring-indigo-500"
                            onclick="metodePembayaranDrawer('{{ $item->value }}', '{{ $item->deskripsi }}')">
                            <input id="drawer-{{ $key }}" type="radio" value="{{ $item->value }}"
                                name="metode_pembayaran" class="w-4 h-4 checked:border-indigo-500 hidden"
                                onclick="document.getElementById('drawer-kode_akun_{{ $key }}').checked = true;">
                            <input id="drawer-kode_akun_{{ $key }}" type="radio"
                                value="{{ $item->kode_akun }}" name="kode_akun"
                                class="w-4 h-4 checked:border-indigo-500 hidden"
                                onclick="document.getElementById('drawer-{{ $key }}').checked = true;">

                            {!! $item->icon !!}
                            <label for="drawer-{{ $key }}"
                                class="w-full py-2 ms-2 text-xs font-medium has-[:checked]:text-indigo-900  dark:text-gray-300">{{ $item->nama }}</label>
                        </label>
                    @endforeach
                </div>
            </div>

            <div id="section-cash" class="hidden max-w-full space-y-2 space-x-3 flex items-end justify-end my-5">
                <div class="grid grid-cols-1 gap-x-5 items-center max-w-[300px]">
                    <input type="hidden" name="invoice_id" id="invoice_id">
                    <label for="total_uang" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Total
                        Uang</label>
                    <input id="total_uang" name="total_uang" type="text"
                        class="dmv-input py-2 px-2 border border-gray-400" placeholder="0" />
                </div>
                <div class="grid grid-cols-1 gap-x-5 items-center max-w-[300px]">
                    <label for="input-uang"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Bayar</label>
                    <input id="input-uang" type="text" class="dmv-input py-2 px-2 border border-gray-400"
                        placeholder="0" readonly />
                    <input id="bayar" name="bayar" type="text" class="hidden dmv-input py-2 px-2"
                        placeholder="0" />
                </div>
                <div class="grid grid-cols-1 gap-x-5 items-center max-w-[300px]">
                    <label for="kembalian"
                        class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kembalian</label>
                    <input id="kembalian" name="kembalian" type="text"
                        class="dmv-input py-2 px-2 border border-gray-400" placeholder="0" readonly />
                </div>
            </div>
            <div id="section-tf" class="hidden max-w-full space-y-2 flex space-x-5 my-5">
                <div class="grid grid-cols-1 gap-x-5 items-center w-1/2">
                    <label for="norek" class="text-gray-800">No Rekening</label>
                    <p id="norek"></p>
                </div>
                <div class="grid grid-cols-1 gap-x-5 items-center w-1/2">
                    <label for="bayar" class="text-gray-800">Total Transfer</label>
                    <input type="text" id="input-uang-dua" class="dmv-input py-2 px-2 border border-gray-400"
                        readonly>
                </div>
            </div>
            <button type="submit" id="bayar-sekarang" class="font-medium btn btn-indigo w-full">
                Bayar Sekarang</button>
        </form>
        {{-- <div class="my-5">
            <p class="text-gray-700 font-semibold">Riwayat Pembayaran</p>
            <div class="space-y-5 mt-3">
                @for ($i = 0; $i < 2; $i++)
                    <ul class="flex justify-between items-center border-b pb-2">
                        <li class="flex gap-x-2">
                            <div class="p-2 w-10 h-10 border">
                                <svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em"
                                    viewBox="0 0 20 20">
                                    <path fill="currentColor"
                                        d="M11.67 8.537a.3.3 0 0 0-.302.296v2.212a.3.3 0 0 0 .303.296h6.663a.3.3 0 0 0 .303-.296V8.833a.3.3 0 0 0-.303-.296zm4.086-7.036c.922.044 1.585.226 2.005.612c.415.382.628.935.67 1.667v2.097a.674.674 0 0 1-.681.666a.674.674 0 0 1-.682-.666l.001-2.059c-.022-.38-.113-.616-.243-.736c-.126-.116-.51-.22-1.103-.25H2.647c-.537.02-.886.122-1.055.267c-.13.111-.228.417-.229.946l-.003 11.77c.05.514.163.857.308 1.028c.11.13.451.26.953.324h13.116c.614.012.976-.08 1.098-.203c.135-.137.233-.497.233-1.086v-2.045c0-.367.305-.666.682-.666c.376 0 .681.299.681.666v2.045c0 .9-.184 1.573-.615 2.01c-.444.45-1.15.63-2.093.61L2.54 18.495c-.897-.104-1.54-.35-1.923-.803c-.347-.41-.54-.995-.617-1.813V4.044c.002-.876.212-1.535.694-1.947c.442-.38 1.08-.565 1.927-.597zm2.578 5.704c.92 0 1.666.729 1.666 1.628v2.212c0 .899-.746 1.628-1.666 1.628h-6.663c-.92 0-1.666-.73-1.666-1.628V8.833c0-.899.746-1.628 1.666-1.628zm-4.997 1.94c-.46 0-.833.36-.833.803c0 .444.373.803.833.803c.46 0 .833-.36.833-.803c0-.444-.373-.804-.833-.804" />
                                </svg>
                            </div>
                            <ul>
                                <li class="text-gray-800 text-sm">PAY1720352888</li>
                                <li class="text-gray-700 text-xs">07 Juli 2024</li>
                            </ul>
                        </li>
                        <li class="font-bold">BRI</li>
                        <li>Rp 1.000.000</li>
                    </ul>
                @endfor
            </div>
        </div> --}}
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        const $targetEl = document.getElementById('drawer-pelunasan-invoice')

        $($targetEl).find('#total_uang').keyup(function() {
            this.value = this.value.replace(/[^0-9]/g, '');
            let bayar = $($targetEl).find('#bayar').val()
            let kembalian = this.value - bayar
            $($targetEl).find('#kembalian').val(kembalian.rupiah(0))
            this.value = this.value.formatRupiah();
        })

        $($targetEl).find('#input-uang').keyup(function() {
            let nominal = this.value.formatRupiah();
            this.value = nominal
            $($targetEl).find('#bayar').val(nominal.parseRupiah())
            let totalUang = $($targetEl).find('#total_uang').val().parseRupiah()
            let kembalian = totalUang - nominal.parseRupiah()
            $($targetEl).find('#kembalian').val(kembalian.rupiah(0))
        })

        $($targetEl).find('#input-uang-dua').keyup(function() {
            let nominal = this.value = this.value.formatRupiah();
            $($targetEl).find('#bayar').val(nominal.parseRupiah())
        })

        function metodePembayaranDrawer(value, deskripsi) {
            $($targetEl).find('#total_uang').val('')
            // $($targetEl).find('#input-uang').val('')
            // $($targetEl).find('#bayar').val('')
            // $($targetEl).find('#input-uang-dua').val('')
            $($targetEl).find('#kembalian').val('')
            $($targetEl).find('#section-cash').addClass('hidden')
            $($targetEl).find('#section-tf').addClass('hidden')
            $($targetEl).find('[name="cash"]').val('')
            if (value == 'cash') {
                $($targetEl).find('#section-cash').removeClass('hidden')
            } else if (value != 'cash') {
                $($targetEl).find('#norek').text(deskripsi)
                $($targetEl).find('#section-tf').removeClass('hidden')
            }
        }

        $('#form-angsuran').submit(function(e) {
            e.preventDefault();
            if ($($targetEl).find('#bayar').val() == '') {
                alert('Masukan nominal bayar', 'warning')
                return false
            }
            let metodepembayaran = $('[name="metode_pembayaran"]:checked').val()
            if (metodepembayaran == 'cash') {
                if ($($targetEl).find('#total_uang').val() == '') {
                    alert('Masukan nominal uang', 'warning')
                    return false
                }
            }
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('pembayaran.angsuran') }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status == 'success') {
                        console.log(response)
                        modalInvoice(response.data)
                        $('#print-struk').click(function() {
                            openPrintWindow(response.pembayaran_id)
                        })
                        loadKeranjang()
                    }
                }
            });
        });
    </script>
@endpush
