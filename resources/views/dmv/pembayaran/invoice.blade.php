@extends('layouts.main')
@section('konten')
    <div id="app">
        <div class="container">
            <div class="bg-gray-300 border border-gray-400/60 rounded-t-md flex justify-between items-center px-5 py-2">
                <h1 class="text-xl font-medium text-gray-800">List Tagihan</h1>
                <a class="btn btn-lg btn-indigo" href="{{ route('invoice.create') }}">Buat Spk / Invoice</a>
            </div>
            <div class="bg-white p-5 border-x border-gray-400/60 border-b rounded-b-md">
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    No SPK
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Customer
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Total Pesanan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Nominal Tagihan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Status Invoice
                                </th>
                                <th scope="col" class="px-6 py-3 text-center">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                                v-for="(data, index) in listData" :key="data.id">
                                <th scope="row"
                                    class="px-6 py-2.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    @{{ index + 1 }}
                                </th>
                                <td class="px-6 py-2.5">
                                    @{{ data.no_spk }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.customer.nama }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.totalpesanan }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ new Intl.NumberFormat('id-ID').format(data.sisa) }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.status }}
                                </td>
                                <td class="px-6 py-2.5 space-x-3 text-center">
                                    <button class="btn btn-md btn-emerald"
                                        @click="show(data.no_invoice,data.status)">show</button>
                                    <button class="btn btn-md btn-indigo"
                                        @click="lanjutkanPembayaran(data.no_invoice,data.status)"><x-icon-card-credit />Lanjutkan
                                        Pembayaran</button>
                                    <button @click="detailSpk(data.no_spk)" class="btn btn-md btn-rose"><x-icon-spk />Detail
                                        SPK</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="my-3 float-end">
                        <nav aria-label="Page navigation example">
                            <ul class="inline-flex -space-x-px text-base h-10">
                                <li v-for="link in links">
                                    <a @click="pagginate(link.url)"
                                        :class="['flex items-center justify-center px-4 h-10 cursor-pointer leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white',
                                            link.active ? 'bg-indigo-500' : ''
                                        ]"
                                        v-html="link.label"></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="module">
        import {
            createApp,
            ref
        } from '/js/vue3.5.11.js'

        createApp({
            data() {
                return {
                    listData: [],
                    links: [],
                }
            },
            methods: {
                load() {
                    axios.get(`{{ route('invoice') }}`)
                        .then((response) => {
                            this.listData = response.data.data
                            this.links = response.data.links
                        }).
                    catch((error) => {
                        alert(error.response?.data?.message, 'warning')
                    });
                },
                pagginate(url) {
                    axios.get(url)
                        .then((response) => {
                            this.listData = response.data.data
                            this.links = response.data.links
                        }).
                    catch((error) => {
                        console.log(error)
                    });
                },
                lanjutkanPembayaran(id) {
                    const url = `{{ route('payment', ':id') }}`.replace(':id', id);
                    window.location.href = `${url}?status=${status}&page=pay`;
                },
                show(id, status) {
                    const url = `{{ route('payment', ':id') }}`.replace(':id', id);
                    window.location.href = `${url}?status=${status}&page=show`;
                },
                detailSpk(nospk) {
                    window.location.href = `/spk/${nospk}/show`
                }
            },
            mounted() {
                this.load()
            }
        }).mount('#app')
    </script>
@endpush
