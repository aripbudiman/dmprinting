<!-- Modal -->
<div id="modal-invoice"
    class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 max-h-full"
    data-hs-overlay-keyboard="false">
    <div
        class="hs-overlay-open:mt-7 hs-overlay-open:opacity-100 hs-overlay-open:duration-500 mt-0 opacity-0 ease-out transition-all sm:max-w-lg sm:w-full m-3 sm:mx-auto">
        <div class="relative flex flex-col bg-white shadow-lg rounded-xl pointer-events-auto dark:bg-neutral-800">
            <div class="relative overflow-hidden min-h-32 bg-blue-700 text-center rounded-t-xl dark:bg-neutral-950">
                <!-- Close Button -->
                <div class="absolute top-2 end-2">
                    <button type="button"
                        class="flex justify-center items-center size-7 text-sm font-semibold rounded-full border border-transparent text-gray-300 hover:bg-gray-100 disabled:opacity-50 disabled:pointer-events-none hover:text-blue-700 dark:text-white dark:hover:bg-neutral-700"
                        id="modal-invoice-close">
                        <span class="sr-only">Close</span>
                        <svg class="flex-shrink-0 size-4" xmlns="http://www.w3.org/2000/svg" width="24"
                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path d="M18 6 6 18"></path>
                            <path d="m6 6 12 12"></path>
                        </svg>
                    </button>
                </div>
                <!-- End Close Button -->

                <!-- SVG Background Element -->
                <figure class="absolute inset-x-0 bottom-0 -mb-px">
                    <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                        viewBox="0 0 1920 100.1">
                        <path fill="currentColor" class="fill-white dark:fill-neutral-800"
                            d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
                    </svg>
                </figure>
                <!-- End SVG Background Element -->
            </div>

            <div class="relative z-10 -mt-12">
                <!-- Icon -->
                <span
                    class="mx-auto flex justify-center items-center size-[62px] rounded-full border border-gray-200 bg-white text-gray-700 shadow-sm dark:bg-neutral-800 dark:border-neutral-700 dark:text-neutral-400">
                    <img src="{{ asset('image/LOGO BARU.png') }}" alt="LOGO" width="45">
                </span>
                <!-- End Icon -->
            </div>

            <!-- Body -->
            <div class="p-4 sm:p-7 overflow-y-auto">
                <div class="text-center">
                    <h3 class="text-lg font-semibold text-gray-800 dark:text-neutral-200">
                        DMPRINTING
                    </h3>
                    <p class="text-sm text-gray-500 dark:text-neutral-500" id="invoice-number">
                    </p>
                </div>

                <!-- Grid -->
                <div class="mt-5 sm:mt-10 grid grid-cols-2 sm:grid-cols-3 gap-5">
                    <div>
                        <span class="block text-xs uppercase text-gray-500 dark:text-neutral-500">Total Bayar:</span>
                        <span class="block text-sm font-medium text-gray-800 dark:text-neutral-200"
                            id="totalbayar"></span>
                    </div>
                    <!-- End Col -->

                    <div>
                        <span class="block text-xs uppercase text-gray-500 dark:text-neutral-500">Tanggal Bayar:</span>
                        <span class="block text-sm font-medium text-gray-800 dark:text-neutral-200"
                            id="tanggal"></span>
                    </div>
                    <!-- End Col -->

                    <div>
                        <span class="block text-xs uppercase text-gray-500 dark:text-neutral-500">Metode
                            Pembayaran:</span>
                        <div class="flex items-center gap-x-2">
                            <span class="block text-sm font-medium text-gray-800 dark:text-neutral-200"
                                id="metodepembayaran"></span>
                        </div>
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Grid -->

                <div class="mt-5 sm:mt-10">
                    <h4 class="text-xs font-semibold uppercase text-gray-800 dark:text-neutral-200">Summary</h4>

                    <ul class="mt-3 flex flex-col">
                        <li
                            class="inline-flex items-center gap-x-2 py-3 px-4 text-sm border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg dark:border-neutral-700 dark:text-neutral-200">
                            <div class="flex items-center justify-between w-full">
                                <span>Total Tagihan</span>
                                <span id="totaltagihan"></span>
                            </div>
                        </li>
                        <li
                            class="inline-flex items-center gap-x-2 py-3 px-4 text-sm border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg dark:border-neutral-700 dark:text-neutral-200">
                            <div class="flex items-center justify-between w-full">
                                <span>Diskon</span>
                                <span id="diskon"></span>
                            </div>
                        </li>
                        <li
                            class="inline-flex items-center gap-x-2 py-3 px-4 text-sm font-semibold bg-gray-50 border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg dark:bg-neutral-800 dark:border-neutral-700 dark:text-neutral-200">
                            <div class="flex items-center justify-between w-full">
                                <span>Total Bayar</span>
                                <span id="totalbayar2"></span>
                            </div>
                        </li>
                        <li
                            class="inline-flex items-center gap-x-2 py-3 px-4 text-sm font-semibold bg-gray-50 border text-gray-800 -mt-px first:rounded-t-lg first:mt-0 last:rounded-b-lg dark:bg-neutral-800 dark:border-neutral-700 dark:text-neutral-200">
                            <div class="flex items-center justify-between w-full">
                                <span>Sisa Tagihan</span>
                                <span id="sisa"></span>
                            </div>
                        </li>
                    </ul>
                </div>

                <!-- Button -->
                <div class="mt-5 flex justify-end gap-x-2">
                    {{-- <a class="py-2 px-3 inline-flex justify-center items-center gap-2 rounded-lg border font-medium bg-white text-gray-700 shadow-sm align-middle hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-white focus:ring-blue-600 transition-all text-sm dark:bg-neutral-800 dark:hover:bg-neutral-800 dark:border-neutral-700 dark:text-neutral-400 dark:hover:text-white dark:focus:ring-offset-gray-800"
                        href="#">
                        <svg class="flex-shrink-0 size-4" xmlns="http://www.w3.org/2000/svg" width="24"
                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" />
                            <polyline points="7 10 12 15 17 10" />
                            <line x1="12" x2="12" y1="15" y2="3" />
                        </svg>
                        Invoice PDF
                    </a> --}}
                    <button type="button" id="print-spk"
                        class="py-2 px-3 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none">
                        <svg class="flex-shrink-0 size-4" xmlns="http://www.w3.org/2000/svg" width="24"
                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round">
                            <polyline points="6 9 6 2 18 2 18 9" />
                            <path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2" />
                            <rect width="12" height="8" x="6" y="14" />
                        </svg>
                        Print SPK
                    </button>
                    <button type="button" id="print-struk"
                        class="py-2 px-3 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none">
                        <svg class="flex-shrink-0 size-4" xmlns="http://www.w3.org/2000/svg" width="24"
                            height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round">
                            <polyline points="6 9 6 2 18 2 18 9" />
                            <path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2" />
                            <rect width="12" height="8" x="6" y="14" />
                        </svg>
                        Print
                    </button>
                </div>
                <!-- End Buttons -->

                <div class="mt-5 sm:mt-10">
                    <p class="text-sm text-gray-500 dark:text-neutral-500">If you have any questions, please contact us
                        at </p>
                </div>
            </div>
            <!-- End Body -->
        </div>
    </div>
</div>
<!-- End Modal -->
