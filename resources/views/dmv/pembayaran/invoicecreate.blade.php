@extends('layouts.main')
@section('konten')
    <div id="app">
        <div class="container">
            <div class="bg-gray-300 border border-gray-400/60 rounded-t-md flex justify-between p-3 items-center">
                <h1 class="text-xl font-medium text-gray-800">Buat SPK</h1>
                <a href="{{ url('/invoice') }}" class="btn btn-lg btn-rose">Kembali</a>
            </div>
            <div class="bg-white p-5 border-x border-gray-400/60 border-b rounded-b-md">
                <div class="flex justify-between items-center">
                    <div class="grid grid-cols-4 gap-5 w-1/2">
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">No
                                SPK</label>
                            <input type="text"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="otomatis" readonly>
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama
                                Customer</label>
                            <input type="text" v-model="customer" id="customer" @input="checkCustomer"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <input type="hidden" v-model="customerId" id="customer_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Kasir</label>
                            <input type="text" value="{{ Auth::user()->name }}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                readonly>
                        </div>
                        <div class="mb-6">
                            <label for="default-input"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Tanggal</label>
                            <input type="text" value="{{ date('Y-m-d') }}"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                readonly>
                        </div>
                    </div>
                    <div class="space-x-2">
                        <button class="btn btn-lg btn-indigo" @click="showModal">Pesanan</button>
                    </div>
                </div>
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Nama Pesanan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Qty
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Harga
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                                v-for="(data, index) in dataSpk" :key="data.id">
                                <th scope="row"
                                    class="px-6 py-2.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    @{{ index + 1 }}
                                </th>
                                <td class="px-6 py-2.5">
                                    @{{ data.pesanan?.no_pesanan }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.pesanan?.nama_cetakan ?? 'Pesanan Manual' }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.pesanan?.qty ?? '' }}
                                </td>
                                <td class="px-6 py-2.5">
                                    @{{ data.pesanan?.total_harga?.formatRupiah() ?? data.pesanan?.harga?.formatRupiah() }}
                                </td>
                                <td class="px-6 py-2.5">
                                    <button @click="deleteSpk(data.id)" class="underline text-rose-600">delete</button>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot class="hidden" id="foot">
                            <tr>
                                <td class="py-2 text-right space-x-2" colspan="3">
                                    <button class="btn btn-lg btn-indigo" @click="store">Simpan</button>
                                    <a class="btn btn-lg btn-rose" :href="'{{ route('invoice.index') }}'">Kembali</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        @include('dmv.pembayaran.modalspk')
    </div>
@endsection
@push('scripts')
    <script type="module">
        import {
            createApp,
            ref
        } from '/js/vue3.5.11.js'

        createApp({
            data() {
                return {
                    customer: '',
                    customerId: '',
                    modal: null,
                    digitalPrinting: null,
                    ongkosCetak: null,
                    cetakan: null,
                    dataSpk: null,
                    payload: [],
                    options: {
                        placement: 'center',
                        backdrop: 'dynamic',
                        backdropClasses: 'bg-blue-400/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                        closable: true,
                        onHide: () => {
                            console.log('modal is hidden');
                        },
                        onShow: () => {
                            console.log('modal is shown');
                        },
                        onToggle: () => {
                            console.log('modal has been toggled');
                        },
                    }
                }
            },
            methods: {
                createUser() {
                    alert('User created!');
                    this.message = 'User has been created!';
                },
                checkCustomer() {
                    if (this.customer === '') {
                        this.customerId = '';
                        this.dataSpk = null;
                    }
                },
                showModal() {
                    this.loadPesanan()
                    this.loadSpk(this.customerId)
                },
                hideModal() {
                    this.modal.hide();
                },
                addToSpk(data, jenis) {
                    const url = `{{ route('add_to_spk') }}`
                    this.payload.push(data.no_pesanan)
                    axios.post(url, {
                            customer_id: data.customer_id,
                            no_pesanan: data.no_pesanan,
                            jenis: jenis
                        })
                        .then((response) => {
                            if (response.status == 200) {
                                this.loadPesanan()
                                this.loadSpk(this.customerId)
                            }
                        })
                        .catch((error) => {
                            alert(error.response?.data?.message, 'warning')
                        });
                },
                loadPesanan() {
                    const url = `{{ route('pesanan.digitalprinting') }}`;
                    axios.post(url, {
                            customer_id: this.customerId
                        })
                        .then((response) => {
                            if (response.status == 200) {
                                this.digitalPrinting = response.data.digitalPrinting
                                this.ongkosCetak = response.data.PesananCetakan
                                this.cetakan = response.data.PesananManual
                                if (this.digitalPrinting?.length > 0 || this.ongkosCetak?.length > 0 || this
                                    .cetakan?.length > 0) {
                                    $('#foot').removeClass('hidden')
                                } else {
                                    $('#foot').addClass('hidden')
                                }
                            }
                            this.modal.show();
                        })
                        .catch((error) => {
                            alert(error.response?.data?.message, 'warning')
                            this.modal.hide()
                        });
                },
                loadSpk(customer_id, status = 0) {
                    axios.get(`{{ url('api/load_spk/${customer_id}/${status}') }}`)
                        .then((response) => {
                            if (response.status == 200) {
                                this.dataSpk = response.data
                                this.payloadPesanan()
                            }
                        })
                        .catch((error) => {
                            alert(error.response?.data?.message, 'warning')
                        });
                },
                deleteSpk(id) {
                    axios.delete(`{{ url('api/delete_spk') }}/${id}`)
                        .then((response) => {
                            if (response.status == 200) {
                                this.loadSpk(this.customerId)
                            }
                        })
                        .catch((error) => {
                            alert(error.response?.data?.message, 'warning')
                        });
                },
                store() {
                    const url = `{{ route('store_spk') }}`;
                    if (this.payload.length == 0) {
                        alert('Pesanan tidak boleh kosong', 'warning')
                        return false
                    }
                    axios.post(url, {
                            data: this.payload,
                            customer_id: this.customerId
                        })
                        .then((response) => {
                            if (response.status == 200) {
                                console.log(response)
                                window.location.href =
                                    `/spk/${response.data.spk}/show`
                                this.loadSpk(this.customerId)
                            }
                        })
                        .catch((error) => {
                            alert(error.response?.data?.message, 'warning')
                        });
                },
                payloadPesanan() {
                    this.payload = this.dataSpk.map(data => data.id)
                }
            },
            mounted() {
                $('#customer').autocomplete({
                    source: (request, response) => {
                        $.ajax({
                            url: "{{ Request::url() }}",
                            dataType: "json",
                            data: {
                                'autocomplete': 1,
                                'search': request.term
                            },
                            success: (data) => {
                                response(data.data);
                            }
                        });
                    },
                    select: (event, ui) => {
                        this.customer = ui.item.label;
                        this.customerId = ui.item.value;
                        this.loadSpk(ui.item.value)
                        return false;
                    }
                });
                const targetModal = document.getElementById('modal-spk')
                this.modal = new Modal(targetModal, this.options);
            }
        }).mount('#app')
    </script>
@endpush
