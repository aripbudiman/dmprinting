@extends('layouts.main')
@section('konten')
    <div class="flex justify-between">
        <div class="space-x-2">
            <button class="btn btn-lg btn-indigo" data-modal-target="modal-cetakan"
                data-modal-toggle="modal-cetakan">Banner</button>
            <button class="btn btn-lg btn-indigo" data-modal-target="modal-pesanan-cetakan"
                data-modal-toggle="modal-pesanan-cetakan">Cetakan</button>
            <button class="btn btn-lg btn-indigo" data-modal-target="modal-pesanan-manual"
                data-modal-toggle="modal-pesanan-manual">Pesanan Manual</button>
        </div>
        <div>
            <a href="{{ route('pembayaran.index') }}" class="btn btn-lg btn-rose">Kembali</a>
        </div>
    </div>
    <form id="form-pembayaran">
        @csrf
        <div class="mt-5 space-y-5 border border-gray-400 p-5 relative">
            <div class="text-2xl font-medium text-gray-600 absolute -top-4 px-2 bg-[#e9e9e9]">Pembayaran</div>
            <div class="flex space-x-5" style="margin-top: 3px">
                <div class="bg-white/50 p-5 w-1/4 shadow rounded-lg">
                    <div class="space-y-2">
                        <div class="flex space-x-4 items-center">
                            <label for="no-invoice" class="w-44 text-gray-800">No. Invoice</label>
                            <input type="text" name="no-invoice"
                                class="h-8 border border-gray-400 text-gray-700 rounded-md" value="Otomatis" readonly>
                        </div>
                        <div class="flex space-x-4 items-center">
                            <label for="customer" class="w-44 text-gray-800">Customer</label>
                            <input type="text" name="customer"
                                class="h-8 border border-gray-400 text-gray-700 rounded-md" readonly>
                        </div>
                        <div class="space-x-4 items-center hidden">
                            <label for="customer" class="w-44 text-gray-800">Customer Id</label>
                            <input type="text" name="customer_id"
                                class="h-8 border border-gray-400 text-gray-700  rounded-md" readonly>
                        </div>
                        <div class="flex space-x-4 items-center">
                            <label for="no-invoice" class="w-44 text-gray-800">Tanggal</label>
                            <input type="text" name="tanggal" class="h-8 border border-gray-400 text-gray-700 rounded-md"
                                value="{{ date('Y-m-d') }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="bg-white/50 p-5 w-1/4 shadow rounded-lg">
                    <div class="space-y-2">
                        <lablel for="kode_voucher" class="mb-2 block text-sm font-medium text-gray-800">Kode Voucher
                        </lablel>
                        <input type="text" name="kode_voucher" id="kode_voucher" class="form-input">
                        <div class="flex justify-between space-x-5 items-center">
                            <i class="text-xs text-blue-700" id="gunakan"></i>
                            <button type="button" class="btn btn-md btn-indigo rounded-md float-end font-medium"
                                id="apply-voucher">Pakai
                                Voucher</button>
                        </div>
                    </div>
                </div>
                <div class=" bg-white/50 p-3 w-1/4 shadow rounded-lg">

                    <div class="w-full border border-gray-200 rounded-lg bg-gray-50 dark:bg-gray-700 dark:border-gray-600">
                        <div class="px-4 py-2 bg-white rounded-t-lg dark:bg-gray-800">
                            <label for="comment" class="sr-only">Pesan Tagihan</label>
                            <textarea id="comment" rows="2"
                                class="w-full px-0 text-sm text-gray-900 bg-white border-0 dark:bg-gray-800 focus:ring-0 dark:text-white dark:placeholder-gray-400"
                                placeholder="Tulis Pesan ...">Yuk, bayar tagihan RP 2.0000.000 kamu di Dmprinting</textarea>
                        </div>
                        <div class="flex items-center justify-end px-3 py-1 border-t dark:border-gray-600">
                            <button type="button" class="btn btn-md btn-indigo font-medium">
                                Kirim Pesan Tagihan
                            </button>
                        </div>
                    </div>

                </div>
                @if (in_array('PembayaranController@bayarAngsuranInvoice', session('permission')))
                    <div class="bg-white/50 p-3 w-1/4 shadow rounded-lg">
                        <div class="relative overflow-y-auto max-h-[124px]">
                            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                                <thead
                                    class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400 sticky top-0">
                                    <tr>
                                        <th scope="col" class="px-4 py-2">
                                            Invoice
                                        </th>
                                        <th scope="col" class="px-4 py-2">
                                            Bill
                                        </th>
                                        <th scope="col" class="px-4 py-2">
                                            Aksi
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="list-invoice">
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
            <div class="border border-gray-300 px-4 py-5 relative" style="margin-top: 25px;">
                <div class="absolute -top-4 px-2 text-xl font-medium text-gray-600 dark:text-white bg-[#e9e9e9]">
                    <div class="flex space-x-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1.4em" height="1.4em" viewBox="0 0 24 24">
                            <path fill="currentColor"
                                d="M10 2a1.75 1.75 0 1 0 0 3.5h4A1.75 1.75 0 1 0 14 2zM3.863 16.205c-.858-3.432-1.287-5.147-.386-6.301c.901-1.154 2.67-1.154 6.207-1.154h4.63c3.538 0 5.307 0 6.208 1.154c.9 1.153.472 2.87-.386 6.301c-.546 2.183-.819 3.274-1.633 3.91c-.813.635-1.938.635-4.188.635h-4.63c-2.25 0-3.376 0-4.19-.635c-.813-.636-1.086-1.727-1.632-3.91"
                                opacity="0.5" />
                            <path fill="currentColor"
                                d="M15.58 4.502a1.743 1.743 0 0 0 .002-1.501c.683.005 1.216.036 1.692.222a3.25 3.25 0 0 1 1.426 1.09c.367.494.54 1.127.776 1.998l.047.17l.512 2.964c-.408-.282-.935-.45-1.617-.55l-.361-2.087c-.284-1.04-.387-1.367-.561-1.601a1.75 1.75 0 0 0-.768-.587c-.22-.086-.486-.111-1.148-.118M8.418 3a1.743 1.743 0 0 0 .002 1.502c-.662.007-.928.032-1.148.118a1.75 1.75 0 0 0-.768.587c-.174.234-.277.561-.56 1.6l-.362 2.089c-.681.1-1.208.267-1.617.548l.512-2.962l.047-.17c.237-.872.41-1.506.776-2a3.25 3.25 0 0 1 1.426-1.089c.476-.186 1.008-.217 1.692-.222m.332 9.749a.75.75 0 0 0-1.5 0v4a.75.75 0 0 0 1.5 0zM16 12a.75.75 0 0 1 .75.75v4a.75.75 0 0 1-1.5 0v-4A.75.75 0 0 1 16 12m-3.25.75a.75.75 0 0 0-1.5 0v4a.75.75 0 0 0 1.5 0z" />
                        </svg>
                        <h1>
                            Keranjang
                        </h1>
                    </div>
                </div>
                <div
                    class="absolute -bottom-4 right-4 px-2 text-xl font-medium text-gray-600 dark:text-white bg-gray-100 hidden">
                    <button type="button" class="btn btn-sm btn-rose">Delete Keranjang</button>
                </div>
                <div class="relative overflow-x-auto overflow-y-auto  border max-h-[29vh]">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400" id="keranjang">
                        <thead
                            class="text-xs text-gray-800 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400 sticky top-0">
                            <tr>
                                <th scope="col" class="px-3 py-3">
                                    Tanggal
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Cetakan
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Tipe
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Bahan
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Lebar
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Finishing
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Panjang
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Qty
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Harga
                                </th>
                                <th scope="col" class="px-3 py-3">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody class="overflow-x-auto">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="flex space-x-6">
                <div class="border border-gray-300 bg-white/80 p-5 space-y-3 max-w-sm">
                    <div class="flex space-x-4 items-center">
                        <label for="subtotal" class="w-32 text-gray-800">Subtotal</label>
                        <input type="text" name="subtotal" class="h-8 border border-gray-400 text-gray-700"
                            value="Otomatis" readonly>
                        <input type="hidden" name="no_pesanan">
                    </div>
                    <div class="flex space-x-4 items-center">
                        <label for="customer" class="w-32 text-gray-800">Diskon</label>
                        <div class="flex justify-end space-x-2">
                            <input type="text" name="disc_percentage"
                                class="h-8 w-[20%] border border-gray-400 text-gray-700" autocomplete="off">
                            <input type="text" name="disc" class="h-8 w-[60%] border border-gray-400 text-gray-700"
                                value="0" readonly>
                        </div>
                    </div>
                    <div class="flex space-x-4 items-center">
                        <label for="no-invoice" class="w-32 text-gray-800">Total Harga</label>
                        <input type="text" name="grand_total" class="h-8 border border-gray-400 text-gray-700"
                            readonly>
                    </div>
                    <div class="flex items-center">
                        <span class="inline-block text-red-600">*&nbsp;</span>
                        <i class="text-xs">Pembayaran DP min 10% dari total harga</i>
                    </div>
                </div>
                <div class="max-w-xl hidden bg-white/80 p-3 border border-gray-300" id="section-metode-pembayaran">
                    <p class="col-span-4 text-gray-800">Metode Pembayaran</p>
                    <div class="grid grid-cols-4 gap-2 mt-3">
                        @foreach ($metodePembayaran as $key => $item)
                            <label
                                class="flex items-center ps-4 border border-gray-300 rounded-sm has-[:checked]:border-indigo-500 has-[:checked]:bg-indigo-200 has-[:checked]:text-indigo-900 has-[:checked]:ring-indigo-500"
                                onclick="metodePembayaran('{{ $item->value }}', '{{ $item->deskripsi }}')">
                                <input id="metode_{{ $key }}" type="radio" value="{{ $item->value }}"
                                    name="metode_pembayaran" class="w-4 h-4 checked:border-indigo-500 hidden"
                                    onclick="document.getElementById('kode_akun_{{ $key }}').checked = true;">
                                <input id="kode_akun_{{ $key }}" type="radio"
                                    value="{{ $item->kode_akun ?? '' }}" name="kode_akun"
                                    class="w-4 h-4 checked:border-indigo-500 hidden"
                                    onclick="document.getElementById('metode_{{ $key }}').checked = true;">
                                {!! $item->icon !!}
                                <label for="metode_{{ $key }}"
                                    class="w-full py-2 ms-2 text-sm font-medium has-[:checked]:text-indigo-900 dark:text-gray-300">{{ $item->nama }}</label>
                            </label>
                        @endforeach

                    </div>
                </div>
                <div class="p-5 space-y-3 hidden bg-white/80 border border-gray-300" id="section-cash">
                    <div class="flex space-x-4 items-center">
                        <label for="total_uang" class="w-32 text-gray-800">Total Uang (Rp)</label>
                        <input type="text" id="total_uang" name="total_uang"
                            class="h-8 border border-gray-400 text-gray-700">
                    </div>
                    <div class="flex space-x-4 items-center">
                        <label for="bayar" class="w-32 text-gray-800">Bayar (Rp)</label>
                        <input type="text" id="input-uang" class="h-8 border border-gray-400 text-gray-700" readonly>
                        <input type="text" id="bayar" name="bayar"
                            class="h-8 border border-gray-400 text-gray-700 hidden" readonly>
                    </div>
                    <div class="flex space-x-4 items-center">
                        <label for="kembalian" class="w-32 text-gray-800">Kembali (Rp)</label>
                        <input type="text" name="kembalian" class="h-8 border border-gray-400 text-gray-700" readonly>
                    </div>
                    <div class="flex space-x-2 items-center pl-36">
                        <button type="submit" class="btn btn-md btn-indigo w-1/2">Bayar</button>
                        <button type="button" class="btn btn-md btn-rose w-1/2">Batal</button>
                    </div>
                </div>
                <div class="p-5 space-y-3 bg-white/80 border border-gray-300 hidden" id="section-tf">
                    <div class="flex space-x-4 items-center">
                        <label for="norek" class="w-32 text-gray-800">No Rekening</label>
                        <p id="norek"></p>
                    </div>
                    <div class="flex space-x-4 items-center">
                        <label for="bayar" class="w-32 text-gray-800">Total Transfer</label>
                        <input type="text" id="input-uang-dua" class="h-8 border border-gray-400 text-gray-700">
                    </div>
                    <div class="flex space-x-2 items-center pl-36">
                        <button type="submit" class="btn btn-md btn-indigo">Proses Transfer</button>
                        <button type="button" class="btn btn-md btn-rose w-1/3">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @include('dmv.pembayaran.modal-pesanan')
    @include('dmv.pembayaran.modal-pesanan-cetakan')
    @include('dmv.pembayaran.modal-pesanan-manual')
    @include('dmv.pembayaran.modal-invoice')
    @include('dmv.pembayaran.drawer-pelunasan-invoice')
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            loadKeranjang()
            setSubtotal()
        });

        function modalInvoice(data = '') {
            $('#modal-invoice').find('#invoice-number').text(data.no_invoice)
            $('#modal-invoice').find('#totalbayar').text(data.total_bayar)
            $('#modal-invoice').find('#tanggal').text(data.tanggal)
            $('#modal-invoice').find('#metodepembayaran').text(data.metode_pembayaran)
            $('#modal-invoice').find('#totaltagihan').text(data.total_tagihan)
            $('#modal-invoice').find('#diskon').text(data.diskon)
            $('#modal-invoice').find('#totalbayar2').text(data.total_bayar)
            $('#modal-invoice').find('#sisa').text(data.sisa)
            const targetModal = document.getElementById('modal-invoice')
            const options = {
                placement: 'center',
                backdrop: 'dynamic',
                backdropClasses: 'bg-blue-400/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                closable: true,
                onHide: () => {
                    console.log('modal is hidden');
                },
                onShow: () => {
                    targetModal.classList.add('open')
                    console.log('modal is shown');
                },
                onToggle: () => {
                    console.log('modal has been toggled');
                },
            };
            const modal = new Modal(targetModal, options);
            modal.show()
            $('#modal-invoice-close').click(function() {
                modal.hide()
            })
        }

        function _showInvoice(data = '') {
            if (typeof data === 'string') {
                data = JSON.parse(data);
            }
            const $targetEl = document.getElementById('drawer-pelunasan-invoice')
            const options = {
                placement: 'left',
                backdrop: true,
                bodyScrolling: true,
                edge: false,
                edgeOffset: '',
                backdropClasses: 'bg-gray-700/80 dark:bg-gray-900/70 fixed inset-0 z-30 backdrop-selector',
                onHide: () => {
                    console.log('drawer is hidden');
                },
                onShow: () => {},
                onToggle: () => {},
            };
            $($targetEl).find('#no_invoice').val(data.no_invoice)
            $($targetEl).find('#customer').val(data.customer.nama)
            $($targetEl).find('#invoice_id').val(data.id)
            $($targetEl).find('#sisa').val(Number(data.sisa.replace(/\.00$/, "")).rupiah(0))
            $($targetEl).find('#input-uang-dua').val(Number(data.sisa.replace(/\.00$/, "")).rupiah(0))
            $($targetEl).find('#input-uang').val(Number(data.sisa.replace(/\.00$/, "")).rupiah(0))
            $($targetEl).find('#bayar').val(data.sisa.replace(/\.00$/, ""))
            var html = '';
            data.invoicedetail.forEach(item => {
                html += `<tr class="bg-white dark:bg-gray-800">
                                <th scope="row"
                                    class="px-3 py-2 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    ${item.pesanan.no_pesanan}
                                </th>
                                <td class="px-3 py-2">
                                    ${item.pesanan?.nama_cetakan?.limit(25) ?? 'Pesanan Manual'}
                                </td>
                                <td class="px-3 py-2">
                                    ${item.pesanan?.harga?.formatRupiah() ?? item.pesanan?.total_harga?.formatRupiah()}
                                </td>
                            </tr>`
            });
            $($targetEl).find('#detail-data').html(html)

            const drawer = new Drawer($targetEl, options);
            drawer.show()
            $($targetEl).find('#close-drawer').click(function() {
                drawer.hide()
            })
        }

        $('#total_uang').keyup(function() {
            this.value = this.value.formatRupiah();
            if (this.value.parseRupiah() > 0) {
                $('#input-uang').prop('readonly', false);
            } else {
                $('#input-uang').prop('readonly', true);
            }
        })

        $('#input-uang').keyup(function() {
            let totalUang = $('[name="total_uang"]').val()
            $('#bayar').val(this.value.parseRupiah());
            let bayar = this.value.formatRupiah();
            this.value = bayar
            let kembalian = totalUang.parseRupiah() - bayar.parseRupiah();
            $('[name="kembalian"]').val(kembalian.rupiah(0));
        })

        $('#input-uang-dua').keyup(function() {
            $('#bayar').val(this.value.parseRupiah());
            let bayar = this.value.formatRupiah();
            this.value = bayar
        })

        const cekKeranjang = (callback) => {
            const keranjang = $('[name="no_pesanan"]').val();
            return new Promise(function(resolve, reject) {
                if (keranjang == '') {
                    resolve(callback())
                } else {
                    reject('ada isinya')
                }
            })
        }

        $('#apply-voucher').click(function(e) {
            cekKeranjang(() => {
                alert('Silahkan pilih pesanan terlebih dahulu !', 'warning')
                $('#kode_voucher').val('');
            })
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'voucher': $('#kode_voucher').val()
                },
                success: function(response) {
                    if (response.success == true) {
                        $('[name="disc_percentage"]').val(response.data);
                        $('#kode_voucher').prop('readonly', true);
                        hitungDiskon($('[name="disc_percentage"]').val())
                        $('#gunakan').text(response.message)
                        $('#apply-voucher').prop('disabled', true);
                    }
                }
            });
        })

        const validation = () => {
            let grandTotal = $('[name="grand_total"]').val().parseRupiah();
            let minBayar = grandTotal * 10 / 100
            let metodebayar = $('[name="metode_pembayaran"]').val();
            let cash = $('#bayar').val().parseRupiah();
            if (cash < minBayar) {
                alert('Minimal bayar ' + minBayar.rupiah(0) + ' atau 10% dari total harga !', 'warning')
                return false
            }
        }

        $('#form-pembayaran').submit(function(e) {
            e.preventDefault();
            validation()
            const formData = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('pembayaran.bayar') }}",
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.status == 'success') {
                        modalInvoice(response.data)
                        alert(response.message, 'success');
                        $('#print-struk').click(function() {
                            openPrintWindow(response.pembayaran_id)
                        })
                        $('#print-spk').click(function() {
                            openPrintWindowSpk(response.spk)
                        })
                        loadKeranjang()
                        $('#total_uang').val('')
                        $('#input-uang').val('')
                        $('#bayar').val('')
                        $('#kembalian').val('')
                        $('#kode_voucher').val('')
                        $('#input-uang-dua').val('')
                        $('[name="disc_percentage"]').val(0)
                        $('[name="disc"]').val(0)
                    } else {
                        alert(response.message, 'error')
                    }
                },
                error: function(xhr, status, error) {
                    console.log(error)
                }
            });
        })

        $('#tes-cetak').click(function() {
            openPrintWindow()
        })

        function openPrintWindow(url = '/dmv/pembayaran/struk/1') {
            var width = 1000;
            var height = 600;
            var top = 100;
            var left = 300;

            window.open(url, '_blank',
                `top=${top},left=${left},height=${height},width=${width},scrollbars=yes,status=yes`);
        }

        function openPrintWindowSpk(url = '/dmv/spk/1') {
            var width = 1000;
            var height = 600;
            var top = 100;
            var left = 300;

            window.open(url, '_blank',
                `top=${top},left=${left},height=${height},width=${width},scrollbars=yes,status=yes`);
        }

        function metodePembayaran(value, deskripsi) {
            $('#total_uang').val('')
            $('#input-uang').val('')
            $('#bayar').val('')
            $('#kembalian').val('')
            $('#input-uang-dua').val('')
            $('#section-cash').addClass('hidden')
            $('#section-tf').addClass('hidden')
            $('[name="cash"]').val('')
            if (value == 'cash') {
                $('#section-cash').removeClass('hidden')
            } else if (value != 'cash') {
                $('#norek').text(deskripsi)
                $('#section-tf').removeClass('hidden')
            }
        }

        $(document).on('click', '#ceklist', function() {
            $('.peer').prop('checked', true);
        });

        function setSubtotal(value = 0) {
            $('input[name="subtotal"]').val(value);
        }

        function setCustomer(value = '') {
            $('input[name="customer"]').val(value);
        }

        function setCustomerId(value = '') {
            $('input[name="customer_id"]').val(value);
        }

        function setNoPesanan(value = '') {
            $('input[name="no_pesanan"]').val(value);
        }

        function addToCart(e, jenis = '') {
            e.preventDefault();
            let tampungPesanan = [];
            $('[type="checkbox"]:checked').each(function() {
                tampungPesanan.push($(this).val())
            });
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'itempesanan': tampungPesanan,
                    'jenis': jenis
                },
                success: function(response) {
                    loadKeranjang()
                }
            });
        }

        $('[name="disc_percentage"]').on('input', function() {
            var percentage = $(this).val();
            hitungDiskon(percentage)
        });

        function hitungDiskon(percentage = 0) {
            var subtotal = $('input[name="subtotal"]').val().parseRupiah();
            var diskon = (subtotal * percentage) / 100
            $('input[name="disc"]').val(diskon.rupiah());
            hitungTotalHarga(subtotal, diskon)
        }

        function hitungTotalHarga(subtotal = 0, diskon = 0) {
            var grandtotal = subtotal - diskon
            $('input[name="grand_total"]').val(grandtotal.rupiah());
        }

        function loadKeranjang() {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'keranjang': 'keranjang'
                },
                success: function(response) {
                    console.log(response)
                    $('#keranjang').html(response.keranjang);
                    $('#section-list-pesanan').html(response.pesanan);
                    $('#section-list-pesanan-cetakan').html(response.cetakan);
                    $('#section-list-pesanan-manual').html(response.manual);
                    setSubtotal(response.subtotal.rupiah())
                    hitungTotalHarga(response.subtotal)
                    setCustomer(response.customer)
                    setCustomerId(response.customer_id)
                    setNoPesanan(response.no_pesanan)
                    loadInvoice(response.table_invoice)
                    if (response.no_pesanan != '') {
                        $('#section-metode-pembayaran').removeClass('hidden')
                    } else {
                        $('#section-metode-pembayaran').addClass('hidden')
                    }
                }
            });
        }

        function loadInvoice(data) {
            $('#list-invoice').html(data)
        }

        const deleteKeranjang = (id) => {
            $.ajax({
                type: "DELETE",
                url: `/dmv/pembayaran/${id}`,
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    loadKeranjang()
                    $('[name="disc_percentage"]').val('')
                    $('[name="disc"]').val(0)
                    $('#kode_voucher').val('')
                    $('#kode_voucher').prop('readonly', false);
                    $('#gunakan').text('')
                    $('#apply-voucher').prop('disabled', false);
                }
            });
        }
    </script>
@endpush
