@extends('layouts.main')
@section('konten')
    <style>
        tr {
            cursor: pointer;
        }
    </style>
    <div class="p-5 bg-white rounded shadow">
        {{-- <a href="{{ route('invoice.create') }}" class="btn btn-indigo btn-lg mb-5">Buat Spk</a> --}}
        <div class="relative overflow-x-auto sm:rounded-lg dark:border dark:border-gray-600">
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-300 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-6">
                            Nama
                        </th>
                        <th scope="col" class="px-4 py-6">
                            Status Membership
                        </th>
                        <th scope="col" class="px-4 py-6">
                            Total Tagihan
                        </th>
                        <th scope="col" class="px-4 py-6" align="center">
                            Pesanan Belum Dibayar
                        </th>
                        <th scope="col" class="px-4 py-6" align="center">
                            Pesanan Dibayar Sebagian
                        </th>
                    </tr>
                </thead>
                <tbody id="list-customers">
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            loadCustomers()
        });

        function pembayaran_detail(id) {
            alert(id)
            document.location.href = "{{ route('pembayaran.detail', ':id') }}".replace(':id', id)
        }

        function loadCustomers($customer = '') {
            $.ajax({
                url: '{{ Request::url() }}',
                method: 'GET',
                data: {
                    'customer': $customer
                },
                success: function(response) {
                    $('#list-customers').html(response);
                }
            });
        }
    </script>
@endpush
