<html lang="en" class="bg-gray-100 text-gray-800">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Struk Pembayaran</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <style>
        @media print {

            @page {
                size: 75mm auto;
                margin: 0;
            }

            body {
                padding: 14px;
            }
        }
    </style>
    <script type="text/javascript">
        function print_win() {
            window.print();
            setTimeout(() => {
                window.close();
            }, 500);
        }
    </script>
</head>

<body onload="print_win()" class="w-[75mm] bg-white text-xs pl-[5mm] pr-4 relative overflow-hidden">
    <header class="flex justify-center flex-col items-center space-y-3 pt-3">
        <img src="{{ asset('image/LOGO BARU.png') }}" alt="logo" class="w-12">
        <p class="text-sm text-center">Jl. Veteran No.68, RT.005/RW.003, Marga Jaya, Kec. Bekasi Sel., Kota Bks, Jawa
            Barat 17141</p>
    </header>
    <section id="informasi" class="mt-5 space-y-1 overflow-hidden">
        <p>====================================================</p>
        <ul class="flex justify-between">
            <li>No Invoice</li>
            <li>{{ $data->invoice->no_invoice }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Tanggal</li>
            <li>{{ \Carbon\Carbon::parse($data->tanggal)->format('d-m-Y') }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Customer</li>
            <li>{{ $data->invoice->customer->nama }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Kasir</li>
            <li>{{ $data->created_by }}</li>
        </ul>
        <ul class="flex justify-between">
            <li>Metode Pembayaran</li>
            <li>{{ $data->metode_pembayaran }}</li>
        </ul>
        <p>============================================================</p>
    </section>
    <section id="detail-pesanan" class="text-xs mt-3 space-y-3 border-b-2 pb-5 border-dashed border-gray-400">
        @foreach ($data->invoice->invoicedetail as $item)
            @if ($item->pesanan_type == 'App\Models\Pesanan')
                <div class="space-y-0.5">
                    <p class="text-justify text-[11px] font-medium">{{ $item->pesanan->nama_cetakan }}</p>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">{{ $item->pesanan->bahan->nama }}
                        </li>
                        <li class="">{{ $item->pesanan->qty }} Pcs</li>
                    </ul>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">{{ $item->pesanan->panjang }} X {{ $item->pesanan->lebar->meter }}
                            Meter
                        </li>
                        <li class="">{{ Helpers::rupiah($item->pesanan->harga) }}</li>
                    </ul>
                </div>
            @elseif($item->pesanan_type == 'App\Models\PesananCetakan')
                <div class="space-y-0.5">
                    <p class="text-justify text-[11px] font-medium">{{ $item->pesanan->nama_cetakan }}</p>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">{{ $item->pesanan->jenis_cetak }}
                        </li>
                        <li class="">{{ $item->pesanan->jumlah_cetakan }} Ctk</li>
                    </ul>
                    <ul class="flex justify-between items-center space-x-4">
                        <li class="text-[11px]">{{ $item->pesanan->jumlah_drag }}
                            Drag
                        </li>
                        <li class="text-[11px]">{{ $item->pesanan->jumlah_plate }}
                            Plate
                        </li>
                        <li class="">{{ Helpers::rupiah($item->pesanan->total_harga) }}</li>
                    </ul>
                </div>
            @else
                @foreach ($item->pesanan->pesananmanualdetail as $detail)
                    <div class="space-y-0.5">
                        <p class="text-justify text-[11px] font-medium uppercase">{{ $detail->nama_cetakan }}</p>
                        <ul class="flex justify-between items-center space-x-4">
                            <li class="text-[11px]">@ {{ $detail->qty }} X {{ rupiah($detail->harga_satuan) }}
                            </li>
                            <li class="">{{ rupiah($detail->total_harga) }}</li>
                        </ul>
                    </div>
                @endforeach
            @endif
        @endforeach
    </section>
    <section id="subtotal" class="text-xs mt-2 space-y-1 border-b-2 pb-3 border-dashed border-gray-400">
        <ul class="flex justify-between items-center space-x-4">
            <li>Subtotal</li>
            <li>{{ Helpers::rupiah($data->invoice->subtotal) }}</li>
        </ul>
        <ul class="flex justify-between items-center space-x-4">
            <li>Diskon</li>
            <li>{{ Helpers::rupiah($data->invoice->disc) }}</li>
        </ul>
        <ul class="flex justify-between items-center space-x-4">
            <li>Grand Total</li>
            <li>{{ Helpers::rupiah($data->invoice->grand_total) }}</li>
        </ul>
    </section>
    <section id="bayar" class="text-xs mt-2 space-y-1 border-b-2 pb-3 border-dashed border-gray-400">
        <ul class="flex justify-between items-center space-x-4">
            <li>Bayar</li>
            <li>{{ Helpers::rupiah($data->bayar) }}</li>
        </ul>
        <ul class="flex justify-between items-center space-x-4">
            <li>Sisa Tagihan</li>
            <li>{{ Helpers::rupiah($data->invoice->sisa) }}</li>
        </ul>
    </section>
    <section>
        <p class="text-[10px] text-center pt-1">Terimakasih sudah menggunakan jasa kami!</p>
        <p class="text-xs text-center pt-1">Invoice elektronik, silahkan scan QR-Code di bawah ini:</p>
        <div class="flex justify-center mt-2">
            <img src="{{ $qrCode }}" alt="QR Code">
        </div>
    </section>
</body>

</html>
