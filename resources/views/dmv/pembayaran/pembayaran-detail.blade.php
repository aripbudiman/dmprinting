@extends('layouts.main')
@section('konten')
    <div class="flex gap-x-5 h-[88vh]">
        <div class="w-full max-w-md bg-white border border-gray-300 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div class="flex items-center justify-between  px-4 py-3">
                <div class="flex justify-between items-center w-full">
                    <h1 id="title-customer" class="text-xl font-semibold text-gray-600">List Pesanan</h1>
                    <div id="list-button-customer">
                        <button type="button" class="btn btn-rose btn-md add-to-cart">
                            Tambah ke keranjang
                        </button>
                        <a href="{{ route('pembayaran.index') }}" id="btn-back" class="btn btn-indigo btn-md">
                            Kembali
                        </a>
                    </div>
                </div>
            </div>
            <div class="flow-root py-1" id="section-customer">
                <ul role="list" id="list-pesanan"
                    class="divide-y divide-gray-200 dark:divide-gray-700 h-[80vh] overflow-y-auto">
                </ul>
            </div>
        </div>
        <div class="w-full bg-white border border-gray-300 rounded-lg">
            <div class="p-5 flex justify-between">
                <div class="flex items-center space-x-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24">
                        <path fill="currentColor"
                            d="M10 2a1.75 1.75 0 1 0 0 3.5h4A1.75 1.75 0 1 0 14 2zM3.863 16.205c-.858-3.432-1.287-5.147-.386-6.301c.901-1.154 2.67-1.154 6.207-1.154h4.63c3.538 0 5.307 0 6.208 1.154c.9 1.153.472 2.87-.386 6.301c-.546 2.183-.819 3.274-1.633 3.91c-.813.635-1.938.635-4.188.635h-4.63c-2.25 0-3.376 0-4.19-.635c-.813-.636-1.086-1.727-1.632-3.91"
                            opacity="0.5" />
                        <path fill="currentColor"
                            d="M15.58 4.502a1.743 1.743 0 0 0 .002-1.501c.683.005 1.216.036 1.692.222a3.25 3.25 0 0 1 1.426 1.09c.367.494.54 1.127.776 1.998l.047.17l.512 2.964c-.408-.282-.935-.45-1.617-.55l-.361-2.087c-.284-1.04-.387-1.367-.561-1.601a1.75 1.75 0 0 0-.768-.587c-.22-.086-.486-.111-1.148-.118M8.418 3a1.743 1.743 0 0 0 .002 1.502c-.662.007-.928.032-1.148.118a1.75 1.75 0 0 0-.768.587c-.174.234-.277.561-.56 1.6l-.362 2.089c-.681.1-1.208.267-1.617.548l.512-2.962l.047-.17c.237-.872.41-1.506.776-2a3.25 3.25 0 0 1 1.426-1.089c.476-.186 1.008-.217 1.692-.222m.332 9.749a.75.75 0 0 0-1.5 0v4a.75.75 0 0 0 1.5 0zM16 12a.75.75 0 0 1 .75.75v4a.75.75 0 0 1-1.5 0v-4A.75.75 0 0 1 16 12m-3.25.75a.75.75 0 0 0-1.5 0v4a.75.75 0 0 0 1.5 0z" />
                    </svg>
                    <h2 class="text-xl font-semibold text-gray-600 dark:text-white sm:text-2xl">Keranjang</h2>
                </div>
                <div>
                    <button class="btn btn-rose btn-md" id="clear-keranjang">Hapus Keranjang</button>
                </div>
            </div>
            <div class="space-y-4 px-5 py-5 h-[65vh] overflow-y-auto" id="list-keranjang">
            </div>
            <div class="p-5 border-t bg-gray-50">
                <div class="space-y-2">
                    <dl class="flex items-center justify-between gap-4">
                        <dt class="text-base font-normal text-gray-500 dark:text-gray-400">Sub Total</dt>
                        <dd class="text-base font-medium text-gray-900 dark:text-white" id="subtotal">0
                        </dd>
                    </dl>
                    <dl class="flex items-center justify-between gap-4">
                        <dt class="text-base font-normal text-gray-500 dark:text-gray-400">Diskon
                        </dt>
                        <dd class="text-base font-medium text-rose-600 dark:text-white" id="nominal-disc">0</dd>
                    </dl>
                    <dl class="flex items-center justify-between gap-4 border-t border-gray-200 pt-2 dark:border-gray-700">
                        <dt class="text-base font-bold text-gray-900 dark:text-white">Total Bill</dt>
                        <dd class="text-base font-bold text-gray-900 dark:text-white" id="total-bill">0</dd>
                    </dl>
                </div>
            </div>

        </div>
        <div class="w-full max-w-lg space-y-4 px-2">
            <div class="space-y-4 rounded-lg border border-gray-300 bg-white p-4 shadow-sm dark:border-gray-700 dark:bg-gray-800 sm:p-6"
                id="section-kode-voucher">
                <div class="space-x-4 flex">
                    <input type="text" id="voucher"
                        class="block max-w-lg rounded-lg border border-gray-300 bg-gray-50 text-sm text-gray-900 focus:border-primary-500 focus:ring-primary-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-primary-500 dark:focus:ring-primary-500"
                        placeholder="Kode Voucher" required />
                    <button type="submit" id="apply-voucher"
                        class="items-center justify-center btn text-sm font-medium text-white btn-indigo" disabled>Pakai
                        Kode Voucher</button>
                </div>
            </div>

            <section class="bg-white antialiased dark:bg-gray-800 border border-gray-300 rounded-lg dark:border-gray-600">
                <form id="form-pembayaran" class="p-6 space-y-5">
                    @csrf
                    <div class="flex space-x-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1.8em" height="1.8em" viewBox="0 0 24 24"
                            class="fill-current text-gray-700 dark:text-gray-300">
                            <path fill="currentColor" d="M4 6h16v2H4zm0 6h16v6H4z" opacity="0.3" />
                            <path fill="currentColor"
                                d="M20 4H4c-1.11 0-1.99.89-1.99 2L2 18c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2V6c0-1.11-.89-2-2-2m0 14H4v-6h16zm0-10H4V6h16z" />
                        </svg>
                        <h1 class="text-xl font-semibold text-gray-600 dark:text-white">Payment</h1>
                    </div>
                    <div class="flex justify-between">
                        <label for="" class="text-lg font-medium text-gray-800 dark:text-white">Total Bill</label>
                        <p id="payment-total-bill" class="text-lg font-semibold text-gray-800 dark:text-white"></p>
                    </div>
                    <div class="grid grid-cols-2 gap-3 hidden">
                        <input type="text" name="tanggal" placeholder="customer_id" value="{{ date('Y-m-d') }}">
                        <input type="text" name="customer_id" placeholder="customer_id">
                        <input type="text" name="no_pesanan" placeholder="no_pesanan">
                        <input type="text" name="subtotal" placeholder="subtotal">
                        <input type="text" name="disc_percentage" placeholder="disc percentage">
                        <input type="text" name="disc" placeholder="disc">
                        <input type="text" name="kode_voucher" placeholder="kode voucher">
                        <input type="text" name="payment[metode_pembayaran]" placeholder="metode pembayaran">
                    </div>
                    <div class="mt-5 space-y-2">
                        <h4 class="text-lg font-medium text-gray-800 dark:text-white">Metode Pembayaran</h4>
                        <div class="grid grid-cols-3 gap-5">
                            @foreach ($metodePembayaran as $key => $item)
                                <label
                                    class="flex items-center ps-4 border rounded-sm has-[:checked]:border-indigo-200 has-[:checked]:bg-indigo-50 has-[:checked]:text-indigo-900 has-[:checked]:ring-indigo-500">
                                    <input id="{{ $key }}" type="radio" value="{{ $item->value }}"
                                        name="metode_pembayaran" class="w-4 h-4 checked:border-indigo-500 hidden">
                                    {!! $item->icon !!}
                                    <label for="{{ $key }}"
                                        class="w-full py-4 ms-2 text-sm font-medium has-[:checked]:text-indigo-900  dark:text-gray-300">{{ $item->nama }}</label>
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="space-y-4 hidden pay" id="section-cash">
                        <div>
                            <label for="cash" class="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
                                Cash
                            </label>
                            <input type="text" id="payment[cash]" name="payment[cash]" class="form-input">
                        </div>
                        <div>
                            <label for="kembalian" class="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
                                Kembalian
                            </label>
                            <input type="text" id="payment[kembalian]" name="payment[kembalian]" class="form-input"
                                readonly>
                        </div>
                    </div>
                    <div class="text-center space-y-2 hidden pay" id="section-transfer">
                        <div class="grid grid-cols-2 items-center">
                            <button class="btn border-2 border-blue-700 text-blue-700 hover:bg-blue-200"
                                onclick="metodePembayaran('BCA',this)">BCA</button>
                            <label for="bca">0-1823490174</label>
                        </div>
                        <div class="grid grid-cols-2 items-center">
                            <button class="btn border-2 border-blue-700 text-blue-700 hover:bg-blue-200"
                                onclick="metodePembayaran('BRI',this)">BRI</button>
                            <label for="bri">0-1823490174</label>
                        </div>
                        <div class="grid grid-cols-2 items-center">
                            <button class="btn border-2 border-blue-700 text-blue-700 hover:bg-blue-200"
                                onclick="metodePembayaran('MANDIRI',this)">Mandiri</button>
                            <label for="mandiri">0-1823490174</label>
                        </div>
                        <div class="grid grid-cols-2 items-center">
                            <button class="btn border-2 border-blue-700 text-blue-700 hover:bg-blue-200"
                                onclick="metodePembayaran('BNI',this)">BNI</button>
                            <label for="bni">0-1823490174</label>
                        </div>
                    </div>
                    <div id="section-dp" class="hidden pay">
                        <p class="text-gray-900">Pembayaran DP min 10% dari total harga</p>
                    </div>
                    <button class="btn btn-indigo w-full">Bayar</button>
                </form>
            </section>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            loadKeranjang()
            getPesanan()
        });

        $('[name="payment[cash]"]').keyup(function() {
            let subtotal = $('[name="subtotal"]').val()
            let nominal = $('[name="payment[cash]"]').val()
            let kembalian = nominal - subtotal
            $('[name="payment[kembalian]"]').val(kembalian.rupiah(0))
        })

        function metodePembayaran(type, el) {
            $('#section-transfer').find('button.btn').removeClass('bg-blue-700 text-white')
            $(el).addClass('bg-blue-700 text-white')
            $('[name="payment[metode_pembayaran]"]').val(type)
        }

        function getPesanan() {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                success: function(response) {
                    $('#list-pesanan').html(response.pesanan)
                }
            });
        }

        $('[name="metode_pembayaran"]').click(function(e) {
            $('.pay').addClass('hidden')
            if (this.value == 'cash') {
                metodePembayaran('cash')
                $('#section-cash').removeClass('hidden')
            } else if (this.value == 'transfer') {
                $('#section-transfer').removeClass('hidden')
            } else if (this.value == 'dp') {
                $('#section-dp').removeClass('hidden')
            }
        })

        const deleteKeranjang = (id) => {
            $.ajax({
                type: "DELETE",
                url: `/dmv/pembayaran/${id}`,
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    loadKeranjang()
                    getPesanan()
                }
            });
        }

        $('#apply-voucher').click(function(e) {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'voucher': $('#voucher').val()
                },
                success: function(response) {
                    if (response.success == true) {
                        let totaldisc = ($('[name="subtotal"]').val() * response.data) / 100
                        $('[name="disc_percentage"]').val(response.data);
                        $('[name="disc"]').val(totaldisc);
                        $('[name="kode_voucher"]').val($('#voucher').val());
                        $('#nominal-disc').html('-' + totaldisc.rupiah());
                        hitungBill($('[name="subtotal"]').val(), totaldisc)
                        alert(response.message, 'info')
                        $('#voucher').prop('readonly', true)
                        $('#apply-voucher').prop('disabled', true)
                    } else {
                        alert(response.message, 'error')
                        $('#nominal-disc').html(0);
                    }
                }
            });
        })

        $('#form-pembayaran').submit(function(e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                type: "POST",
                url: "{{ route('pembayaran.bayar') }}",
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    console.log(response)
                }
            });
        })

        function hitungBill(subtotal, diskon = 0) {
            let total = parseFloat(subtotal) - parseFloat(diskon);
            $('#total-bill').html(total.rupiah());
            $('#payment-total-bill').html(total.rupiah());
        }

        function loadKeranjang() {
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'keranjang': 'keranjang'
                },
                success: function(response) {
                    if (response.keranjang == '') {
                        $('#clear-keranjang').prop('disabled', true)
                        $('#lanjut-pembayaran').prop('disabled', true)
                        $('#apply-voucher').prop('disabled', true)
                    } else {
                        $('#clear-keranjang').prop('disabled', false)
                        $('#lanjut-pembayaran').prop('disabled', false)
                        $('#apply-voucher').prop('disabled', false)
                    }
                    let subtotal = response.subtotal
                    hitungBill(subtotal)
                    $('#subtotal').html(subtotal.rupiah());
                    $('[name="customer_id"]').val(response.customer_id);
                    $('[name="no_pesanan"]').val(response.no_pesanan);
                    $('[name="subtotal"]').val(response.subtotal);
                    $('#list-keranjang').html(response.keranjang);
                }
            });
        }

        $('.add-to-cart').click(function(e) {
            e.preventDefault();
            let tampungPesanan = [];
            $('[type="checkbox"]:checked').each(function() {
                tampungPesanan.push($(this).val())
            });
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}",
                data: {
                    'itempesanan': tampungPesanan
                },
                success: function(response) {
                    loadKeranjang()
                    getPesanan()
                }
            });
        });

        $('#clear-keranjang').click(function(e) {
            $.ajax({
                type: "POST",
                url: "{{ route('pembayaran.clear-keranjang') }}",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    loadKeranjang()
                    getPesanan()
                    alert(response.message, 'success')
                }
            });
        })
    </script>
@endpush
