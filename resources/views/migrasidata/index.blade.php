@extends('layouts.main')
@section('konten')
    <div>
        <button class="btn btn-indigo mb-2 float-end" onclick="getSelectedItems()"> <x-icon-sync /> Migrasi Data</button>
        <table>
            <tr>
                <div class="relative overflow-x-auto">
                    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    No Pesanan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Nama Cetakan
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    <input type="checkbox" name="all" id="all" onchange="checkAll(this)">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($pesanan as $item)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row"
                                        class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        <ul>
                                            <li>{{ $item->no_pesanan }}</li>

                                        </ul>
                                    </th>
                                    <td class="px-6 py-4">
                                        <ul>
                                            <li>{{ $item->nama_cetakan }}</li>
                                        </ul>
                                    </td>
                                    <td scope="col" class="px-6 py-3"><input type="checkbox"
                                            value="{{ $item->no_pesanan }}" class="checkbox-item"
                                            data-no-pesanan="{{ $item->no_pesanan }}"
                                            name="pesanan[{{ $item->no_pesanan }}]"></td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                        <tfoot>
                            {!! $pesanan->links() !!}
                        </tfoot>
                    </table>
                </div>
            </tr>
        </table>
    </div>
@endsection
@push('scripts')
    <script>
        function getSelectedItems() {
            var checkboxes = document.getElementsByClassName('checkbox-item');
            var selectedItems = [];
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    selectedItems.push(checkboxes[i].value);
                }
            }
            // var selectedItemsString = selectedItems.join(',');
            $.ajax({
                type: "GET",
                url: "{{ Request::url() }}?no_pesanan=" + selectedItems,
                success: function(response) {
                    if (response) {
                        alert(response, 'info');
                        setTimeout(() => {
                            location.reload();
                        }, 3000);
                    }
                }
            });
        }

        function checkAll(source) {
            var checkboxes = document.getElementsByClassName('checkbox-item');
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = source.checked;
            }
        }
    </script>
@endpush
