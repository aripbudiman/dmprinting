@extends('layouts.main')
@section('konten')
    <div class="bg-white p-10 container mx-auto shadow rounded-md">
        <div class="mb-5 text-center">
            <h1 class="text-3xl font-bold  text-gray-600">DMPRINTING</h1>
            <h2 class="text-2xl font-semibold  text-gray-600">Laporan Neraca Saldo</h2>
            <p class="text-lg font-medium  text-gray-600">{{ periode('d-F-Y') }}</p>
        </div>
        <div class="w-full overflow-x-auto">
            <div class="max-w-2xl flex space-x-4">
                <div class="relative my-6">
                    <input id="start_date" type="date" name="start_date" value="{{ startDate() }}"
                        class="relative w-full h-12 px-4 placeholder-transparent transition-all border rounded outline-none peer border-slate-200 text-slate-500 autofill:bg-white invalid:border-pink-500 invalid:text-pink-500 focus:border-indigo-500 focus:outline-none invalid:focus:border-pink-500 focus-visible:outline-none disabled:cursor-not-allowed disabled:bg-slate-50 disabled:text-slate-400" />
                    <label for="start_date"
                        class="absolute -top-2 left-2 z-[1] cursor-text px-2 text-xs text-slate-400 transition-all before:absolute before:left-0 before:top-0 before:z-[-1] before:block before:h-full before:w-full before:bg-white before:transition-all peer-placeholder-shown:top-3 peer-placeholder-shown:text-base peer-autofill:-top-2 peer-required:after:text-pink-500 peer-required:after:content-['\00a0*'] peer-invalid:text-pink-500 peer-focus:-top-2 peer-focus:cursor-default peer-focus:text-xs peer-focus:text-indigo-500 peer-invalid:peer-focus:text-pink-500 peer-disabled:cursor-not-allowed peer-disabled:text-slate-400 peer-disabled:before:bg-transparent">
                        Dari </label>
                </div>
                <div class="relative my-6">
                    <input id="start_date" type="date" name="start_date" value="{{ endDate() }}"
                        class="relative w-full h-12 px-4 placeholder-transparent transition-all border rounded outline-none peer border-slate-200 text-slate-500 autofill:bg-white invalid:border-pink-500 invalid:text-pink-500 focus:border-indigo-500 focus:outline-none invalid:focus:border-pink-500 focus-visible:outline-none disabled:cursor-not-allowed disabled:bg-slate-50 disabled:text-slate-400" />
                    <label for="start_date"
                        class="absolute -top-2 left-2 z-[1] cursor-text px-2 text-xs text-slate-400 transition-all before:absolute before:left-0 before:top-0 before:z-[-1] before:block before:h-full before:w-full before:bg-white before:transition-all peer-placeholder-shown:top-3 peer-placeholder-shown:text-base peer-autofill:-top-2 peer-required:after:text-pink-500 peer-required:after:content-['\00a0*'] peer-invalid:text-pink-500 peer-focus:-top-2 peer-focus:cursor-default peer-focus:text-xs peer-focus:text-indigo-500 peer-invalid:peer-focus:text-pink-500 peer-disabled:cursor-not-allowed peer-disabled:text-slate-400 peer-disabled:before:bg-transparent">
                        Sampai </label>
                </div>
                <div class="relative my-6">
                    <button class="btn btn-indigo">Filter</button>
                </div>
                <div class="relative my-6">
                    <a href="{{ url('/laporan/jurnalumum/export?jenis=neracasaldo') }}" class="btn btn-emerald">Export
                        Excel</a>
                </div>
            </div>
            <table class="w-full text-left border border-collapse rounded sm:border-separate border-slate-200 mb-3"
                cellspacing="0">
                <tbody>
                    <tr>
                        <th scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 stroke-slate-700 text-slate-100 bg-slate-500">
                            Akun</th>
                        <th scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 stroke-slate-700 text-slate-100 bg-slate-500">
                            Debet</th>
                        <th scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 stroke-slate-700 text-slate-100 bg-slate-500">
                            Kredit</th>
                    </tr>
                    @php
                        $totalDebet = 0;
                        $totalKredit = 0;
                    @endphp
                    @forelse ($data as $item)
                        <tr>
                            <td scope="col"
                                class="h-12 px-6 text-sm first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                                {{ $item->nama }}</td>
                            <td scope="col"
                                class="h-12 px-6 text-sm first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                                {{ $item->saldo_normal == 'debet' ? rupiah($item->debet - $item->kredit) : '0' }}</td>
                            <td scope="col"
                                class="h-12 px-6 text-sm first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                                {{ $item->saldo_normal == 'kredit' ? rupiah($item->kredit - $item->debet) : '0' }}</td>
                        </tr>
                        @php
                            $totalDebet += $item->debet;
                            $totalKredit += $item->kredit;
                        @endphp
                    @empty
                    @endforelse
                    <tr>
                        <td scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                            TOTAL</td>
                        <td scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                            {{ rupiah($totalDebet) }}</td>
                        <td scope="col"
                            class="h-12 px-6 text-sm font-bold first:border-l-0 border-b stroke-slate-700 text-slate-600 ">
                            {{ rupiah($totalKredit) }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection
