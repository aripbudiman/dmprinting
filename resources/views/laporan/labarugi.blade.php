@extends('layouts.main')
@section('konten')
    <div class="bg-white p-5 rounded-md shadow container mx-auto">
        <div class="mb-5 text-center">
            <h1 class="text-3xl font-bold  text-gray-600">DMPRINTING</h1>
            <h2 class="text-2xl font-semibold  text-gray-600">Laporan Jurnal Umum</h2>
            <p class="text-lg font-medium  text-gray-600">{{ periode('d-F-Y') }}</p>
        </div>
        <div class="mx-auto max-w-6xl border p-3">
            <h2 class="text-lg font-bold  text-slate-700">PENDAPATAN</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $pendapatanDebet = 0;
                    $pendapatanKredit = 0;
                @endphp
                @foreach ($pendapatan as $item)
                    <tr class="text-left">
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%]">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_debet, 2) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_kredit, 2) }}</td>
                                    </tr>
                                    @php
                                        $pendapatanDebet += $akun->total_debet;
                                        $pendapatanKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr class="text-left">
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[75%]">Total Pendapatan</td>
                                @php
                                    $totalPendapatan = $pendapatanKredit - $pendapatanDebet;
                                @endphp
                                <td class="w-[25%]">{{ rupiah($totalPendapatan) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
            <h2 class="text-lg font-bold  text-slate-700">HARGA POKOK PENJUALAN</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $hppDebet = 0;
                    $hppKredit = 0;
                @endphp
                @foreach ($hpp as $item)
                    <tr class="text-left">
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%]">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_debet, 2) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_kredit, 2) }}</td>
                                    </tr>
                                    @php
                                        $hppDebet += $akun->total_debet;
                                        $hppKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr class="text-left">
                    <th>
                        <table class="w-full">
                            <tr>
                                @php
                                    $totalHPP = $hppDebet - $hppKredit;
                                @endphp
                                <td class="w-[75%]">Total Harga Pokok Penjualan</td>
                                <td class="w-[25%]">{{ rupiah($totalHPP) }}</td>
                            </tr>
                            <tr>
                                <td class="w-[75%]">LABA KOTOR</td>
                                <td class="w-[25%]">{{ rupiah($totalPendapatan - $totalHPP) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
            <h2 class="text-lg font-bold  text-slate-700">BEBAN USAHA</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $bebanUsahaDebet = 0;
                    $bebanUsahaKredit = 0;
                @endphp
                @foreach ($bebanUsaha as $item)
                    <tr class="text-left">
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%]">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_debet, 2) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_kredit, 2) }}</td>
                                    </tr>
                                    @php
                                        $bebanUsahaDebet += $akun->total_debet;
                                        $bebanUsahaKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr class="text-left">
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[75%]">Total Beban Usaha</td>
                                @php
                                    $totalBebanUsaha = $bebanUsahaDebet - $bebanUsahaKredit;
                                @endphp
                                <td class="w-[25%]">{{ rupiah($totalBebanUsaha) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
            <h2 class="text-lg font-bold  text-slate-700">BEBAN NON USAHA</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $bebanNonUsahaDebet = 0;
                    $bebanNonUsahaKredit = 0;
                @endphp
                @foreach ($bebanNonUsaha as $item)
                    <tr class="text-left">
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%]">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_debet, 2) }}</td>
                                        <td class="w-[25%]">{{ rupiah($akun->total_kredit, 2) }}</td>
                                    </tr>
                                    @php
                                        $bebanNonUsahaDebet += $akun->total_debet;
                                        $bebanNonUsahaKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr class="text-left">
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[75%]">Total Beban Non Usaha</td>
                                @php
                                    $totalBebanNonUsaha = $bebanNonUsahaDebet - $bebanNonUsahaKredit;
                                    $totalBeban = $totalBebanUsaha + $totalBebanNonUsaha;
                                    $labaRugi = $totalPendapatan - $totalHPP - $totalBeban;
                                @endphp
                                <td class="w-[25%]">{{ rupiah($totalBebanNonUsaha) }}</td>
                            </tr>
                            <tr>
                                <td class="w-[75%]">TOTAL BEBAN</td>
                                <td class="w-[25%]">{{ rupiah($totalBeban) }}</td>
                            </tr>
                            <tr>
                                <td class="w-[75%]">LABA/RUGI</td>
                                <td class="w-[25%]">{{ rupiah($labaRugi) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
        </div>
    </div>
@endsection
