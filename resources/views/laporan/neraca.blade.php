@extends('layouts.main')
@section('konten')
    <div class="bg-white p-5 rounded-md shadow min-w-[400px] mx-auto">
        <div class="mb-5 text-center">
            <h1 class="text-3xl font-bold  text-gray-600">DMPRINTING</h1>
            <h2 class="text-2xl font-semibold  text-gray-600">Laporan Neraca</h2>
            <p class="text-lg font-medium  text-gray-600">{{ periode('d-F-Y') }}</p>
        </div>
        <div class="mx-auto max-w-6xl border p-3">
            <h2 class="text-lg font-bold  text-slate-700">ASSET</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $assetDebet = 0;
                    $assetKredit = 0;
                @endphp
                @foreach ($asset as $item)
                    <tr>
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%] text-left">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[50%] text-right">
                                            {{ rupiah($akun->total_debet - $akun->total_kredit, 2) }}</td>
                                    </tr>
                                    @php
                                        $assetDebet += $akun->total_debet;
                                        $assetKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[50%] text-left">TOTAL ASSET</td>
                                @php
                                    $totalAsset = $assetDebet - $assetKredit;
                                @endphp
                                <td class="w-[50%] text-right">{{ rupiah($totalAsset, 2) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
            <h2 class="text-lg font-bold  text-slate-700">KEWAJIBAN</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $kewajibanDebet = 0;
                    $kewajibanKredit = 0;
                @endphp
                @foreach ($kewajiban as $item)
                    <tr>
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%] text-left">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[50%] text-right">
                                            {{ rupiah($akun->total_kredit - $akun->total_debet, 2) }}</td>
                                    </tr>
                                    @php
                                        $kewajibanDebet += $akun->total_debet;
                                        $kewajibanKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[50%] text-left">TOTAL KEWAJIBAN</td>
                                @php
                                    $totalKewajiban = $kewajibanKredit - $kewajibanDebet;
                                @endphp
                                <td class="w-[50%] text-right">{{ rupiah($totalKewajiban, 2) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
            <h2 class="text-lg font-bold  text-slate-700">MODAL</h2>
            <table class="w-full text-left rtl:text-right text-gray-800 dark:text-gray-400">
                @php
                    $modalDebet = 0;
                    $modalKredit = 0;
                @endphp
                @foreach ($modal as $item)
                    <tr>
                        <th class="capitalize font-semibold">{{ strtolower($item->nama) }}</th>
                    </tr>
                    <tr>
                        <td class="pl-5">
                            <table class="w-full">
                                @foreach ($item->akun as $akun)
                                    <tr>
                                        <td class="w-[50%] text-left">{{ strtolower($akun->nama) }}</td>
                                        <td class="w-[50%] text-right">
                                            {{ rupiah($akun->total_kredit - $akun->total_debet, 2) }}</td>
                                    </tr>
                                    @php
                                        $modalDebet += $akun->total_debet;
                                        $modalKredit += $akun->total_kredit;
                                    @endphp
                                @endforeach
                            </table>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th>
                        <table class="w-full">
                            <tr>
                                <td class="w-[50%] text-left">TOTAL KEWAJIBAN</td>
                                @php
                                    $totalModal = $modalKredit - $modalDebet;
                                @endphp
                                <td class="w-[50%] text-right">{{ rupiah($totalModal, 2) }}</td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
        </div>
    </div>
@endsection
