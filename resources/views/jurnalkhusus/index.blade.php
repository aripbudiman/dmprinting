@extends('layouts.main')
@section('konten')
    <div class="bg-white shadow-md border border-gray-300 rounded-md dark:bg-slate-900">
        <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
            <h1 class="text-xl text-gray-800">Penerimaan Kas</h1>
            <button class="btn btn-md btn-indigo" onclick="_tambah()">Buat Penerimaan Kas</button>
        </div>
        <div class="flex flex-col p-5">
            <div class="-m-1.5 overflow-x-auto">
                <div class="p-1.5 min-w-full inline-block align-middle">
                    <div class="mb-2  flex justify-between">
                        <x-show-data :show="$payload['show']" />
                        <x-search :keyword="$payload['keyword']" />
                    </div>
                    <div class="border overflow-hidden dark:bg-gray-800 dark:border-gray-700">
                        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                            <thead>
                                <tr>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        No
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Tanggal
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Voucher
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        No Ref
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Keterangan
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Nominal
                                    </th>
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-semibold uppercase bg-gray-300 text-slate-900 dark:bg-gray-700 dark:text-gray-400">
                                        Aksi
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbody"
                                class="divide-y divide-gray-200 dark:divide-gray-400 dark:bg-gray-800 dark:border-gray-700">
                                @php
                                    $iterationStart = ($data->current_page - 1) * $data->per_page + 1;
                                @endphp
                                @forelse ($data->data as $key=>$item)
                                    <tr class="hover:bg-gray-100 cursor-pointer">
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ $iterationStart++ }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ formatDate($item->tanggal) }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ $item->voucher }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ $item->no_ref }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ $item->keterangan }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            {{ rupiah($item->nominal) }}
                                        </td>
                                        <td
                                            class="px-4 py-1.5 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-slate-100">
                                            <button class="btn btn-md btn-indigo">Aksi</button>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                            <tfoot class="text-gray-800">
                                <tr class="bg-gray-100 w-1/3">
                                    <td class="py-1 px-3 border-b">Total</td>
                                    <td class="border-b">{{ rupiah($data->total_debet) }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <nav id="pagination" class="flex w-full justify-between items-center mt-4">
                        {!! $data->links !!}
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @include('jurnalkhusus._form', ['label_debet' => 'Simpan Ke', 'label_kredit' => 'Terima dari'])
@endsection
@push('scripts')
    <script>
        let modal;
        $(document).ready(function() {
            sessionStorage.setItem('route', "{{ $route }}")
            sessionStorage.setItem('method', "{{ $method }}")
            const targetModal = document.getElementById('modal-tambah-penerimaan-kas');
            const options = {
                placement: 'center',
                backdrop: 'dynamic',
                backdropClasses: 'bg-gray-800/70 dark:bg-gray-900/80 fixed inset-0 z-40',
                closable: true,
                onHide: () => {
                    defaultValue()
                },
                onShow: () => {
                    defaultValue()
                    targetModal.classList.add('open');
                    if (editData) {
                        const debetAkun = editData.jurnal[0].kode_akun;
                        const kreditAkun = editData.jurnal[1].kode_akun;
                        $('.simpan-ke[name="debet"]').val(debetAkun).trigger('change');
                        $('.simpan-ke[name="kredit"]').val(kreditAkun).trigger('change');
                        $('[name="nominal"]').val(parseInt(editData.nominal).rupiah(0));
                        $('[name="keterangan"]').val(editData.keterangan);
                        $('[name="tanggal"]').val(moment(editData.tanggal).format('DD-MM-YYYY'));
                        $('[name="no_ref"]').val(editData.no_ref);
                    } else {
                        sessionStorage.setItem('route', "{{ $route }}")
                        sessionStorage.setItem('method', "{{ $method }}")
                        defaultValue()
                    }
                },
                onToggle: () => {
                    console.log('modal has been toggled');
                },
            };

            modal = new Modal(targetModal, options);
        });

        function _edit(id, el) {
            fetch(`{{ route('penerimaan_kas.edit', ':id') }}`.replace(':id', id))
                .then(response => response.json())
                .then(data => {
                    editData = data.data;
                    sessionStorage.setItem('route', data.route);
                    sessionStorage.setItem('method', data.method);
                    modal.show();
                })
                .catch(error => {
                    console.error('Error fetching data:', error);
                });
        }


        function _tambah() {
            editData = null
            modal.show()
        }

        function _close() {
            modal.hide()
        }

        function defaultValue() {
            $('.simpan-ke[name="debet"]').val('').trigger('change');
            $('.simpan-ke[name="kredit"]').val('').trigger('change');
            $('[name="nominal"]').val('');
            $('[name="keterangan"]').val('');
            $('[name="tanggal"]').val('');
            $('[name="no_ref"]').val('-');
        }
    </script>
@endpush
