@extends('layouts.main')
@section('konten')
    <div class="bg-white rounded-md border p-5">
        <div class="pb-3 flex justify-between">
            <h1 class="text-2xl font-semibold">Peran & Akses</h1>
            <div class="flex gap-x-4">
                <div>
                    <label for="hs-trailing-button-add-on-with-icon-and-button" class="sr-only">Label</label>
                    <div class="relative flex rounded-lg shadow-sm">
                        <input type="text" id="hs-trailing-button-add-on-with-icon-and-button"
                            name="hs-trailing-button-add-on-with-icon-and-button"
                            class="py-3 px-4 ps-11 block w-full border-gray-200 shadow-sm rounded-s-lg text-sm focus:z-10 focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400 dark:focus:ring-gray-600">
                        <div class="absolute inset-y-0 start-0 flex items-center pointer-events-none ps-4">
                            <svg class="flex-shrink-0 size-4 text-gray-400" xmlns="http://www.w3.org/2000/svg"
                                width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <circle cx="11" cy="11" r="8" />
                                <path d="m21 21-4.3-4.3" />
                            </svg>
                        </div>
                        <button type="button"
                            class="py-3 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-semibold rounded-e-md border border-transparent bg-indigo-600 text-white hover:bg-indigo-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">Search</button>
                    </div>
                </div>
                <a href="{{ route('rolepermission.create') }}" type="button"
                    class="py-3 px-4 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-indigo-600 text-white hover:bg-indigo-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">
                    Buat Peran
                </a>
            </div>
        </div>
        <div class="flex flex-col">
            <div class="-m-1.5 overflow-x-auto">
                <div class="p-1.5 min-w-full inline-block align-middle">
                    <div class="border overflow-hidden dark:border-gray-700">
                        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
                            <thead>
                                <tr class="bg-violet-500 text-white">
                                    <th scope="col"
                                        class="px-6 py-3 text-start text-xs font-medium text-white uppercase">
                                        Name</th>
                                    <th scope="col" class="px-6 py-3 text-end text-xs font-medium text-white uppercase">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="divide-y divide-gray-200 dark:divide-gray-700">
                                @foreach ($roles as $item)
                                    <tr onclick="alert('Silahkan klik/tap 2x pada baris data untuk melihat rincian data.')"
                                        ondblclick="edit('{{ $item->id }}')" class="hover:bg-gray-100">
                                        <td
                                            class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-800 dark:text-gray-200">
                                            {{ $item->name }}</td>
                                        <td class="px-6 py-4 whitespace-nowrap text-end text-sm font-medium">
                                            <button type="button" id="btn-delete"
                                                onclick="deleteItem('{{ $item->id }}')"
                                                class="inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent text-blue-600 hover:text-blue-800 disabled:opacity-50 disabled:pointer-events-none dark:text-blue-500 dark:hover:text-blue-400 dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        function edit(id) {
            window.location.href = "{{ route('rolepermission.settingrolepermission', ':id') }}".replace(':id', id);
        }

        function deleteItem(id) {
            $.ajax({
                method: "POST",
                url: "/roles/" + id,
                data: {
                    _token: "{{ csrf_token() }}",
                },
                dataType: "JSON",
                success: function(response) {
                    if (response.success) {
                        alert(response.message, 'success');
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    console.log(error)
                }
            });
        }
    </script>
@endpush
