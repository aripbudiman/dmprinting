@extends('layouts.main', ['title' => 'List Pengguna'])
@section('konten')
    <x-table 
    :data="$users" 
    :columns="config('table_config.pengguna')" 
    />
@endsection
