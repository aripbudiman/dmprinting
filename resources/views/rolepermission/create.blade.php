@extends('layouts.main')
@section('konten')
    @include('rolepermission._form')
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-roles').on('submit', function(e) {
                e.preventDefault();
                const formData = new FormData(this);
                $.ajax({
                    method: "POST",
                    url: "{{ route('roles.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.success) {
                            alert(response.message, 'success');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error)
                    }
                });
            })
        });
    </script>
@endpush
