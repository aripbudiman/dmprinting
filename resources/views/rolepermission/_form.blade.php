<div class="bg-white shadow-md border border-gray-300 rounded-md max-w-xl">
    <div class="flex justify-between items-center bg-gray-200 px-5 py-2 border-b border-gray-300">
        <h1 class="text-xl text-gray-800">Form Roles</h1>
    </div>
    <form id="form-roles" class="p-5 flex flex-col gap-3">
        @csrf
        <div class="sm:hidden  hidden sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="id" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Nama</label>
            <input type="text" value="{{ $roles->id ?? '' }}" name="id" class="dmv-input" placeholder="nama">
        </div>
        <div class="sm:inline-flex sm:items-center space-y-2 sm:space-y-0 sm:space-x-3 w-full">
            <label for="nama" class="block w-1/3 max-w-lg text-sm font-medium dark:text-white">Nama</label>
            <input type="text" value="{{ $roles->name ?? '' }}" name="name" class="dmv-input" placeholder="name">
        </div>
        <div class="sm:inline-flex sm:items-center justify-end space-y-1 sm:space-y-0 sm:space-x-2 w-full">
            <button type="submit" class="btn btn-indigo">{{ isset($roles) ? 'Update' : 'Simpan' }}</button>
            <a href="{{ route('rolepermission.index') }}" class="btn btn-rose">Cancel</a>
        </div>
    </form>
</div>
