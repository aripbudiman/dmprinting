@extends('layouts.main')
@section('konten')
<div class="bg-white rounded-md border">
    <div class="p-5 bg-gray-100 flex justify-between items-center">
        <h1 class="text-2xl font-semibold text-gray-800">Setting Role Permission</h1>
        <a href="{{ route('rolepermission.index') }}" type="button"
            class="py-3 px-4 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-teal-500 text-white hover:bg-teal-600 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">
            Lihat Semua
        </a>
    </div>
    <form id="myform">
        @csrf
        <div class="p-5 grid grid-cols-3">
            <div>
                <table class="w-20 h-20 uppercase">
                    <tr>
                        <td>Id</td>
                        <td>{{ $roles->id }}</td>
                    </tr>
                    <tr>
                        <td>Roles</td>
                        <td>{{ $roles->name }}</td>
                    </tr>
                </table>
                <div>
                    <button id="simpan" type="submit"
                        class="py-3 px-4 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-blue-600 text-white hover:bg-blue-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">
                        Simpan
                    </button>
                    <button type="button" onclick="checkAll()"
                        class="py-3 px-4 inline-flex items-center gap-x-2 text-sm font-semibold rounded-lg border border-transparent bg-violet-600 text-white hover:bg-violet-700 disabled:opacity-50 disabled:pointer-events-none dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600">
                        Pilih Semua Permission
                    </button>
                </div>
            </div>
            <div class="grid grid-cols-1 gap-x-5 space-y-2">
                @foreach ($permissions as $item)
                <label for="{{ $item->id }}"
                    class="flex p-3 w-full bg-white border border-gray-200 rounded-lg text-sm focus:border-blue-500 focus:ring-blue-500 dark:bg-slate-900 dark:border-gray-700 dark:text-gray-400">
                    <input type="checkbox" name="permission_id[]" value="{{ $item->id }}"
                        class="shrink-0 mt-0.5 border-gray-200 rounded text-blue-600 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-gray-800 dark:border-gray-700 dark:checked:bg-blue-500 dark:checked:border-blue-500 dark:focus:ring-offset-gray-800"
                        id="{{ $item->id }}" {{ $roles->permissions->contains($item->id) ? 'checked' : '' }}>
                    <span class="text-sm text-gray-500 ms-3 dark:text-gray-400">{{ $item->name }}</span>
                </label>
                @endforeach
            </div>
        </div>
    </form>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    function checkAll() {
        $('input[type="checkbox"]').prop('checked', true);
    }

    $('#myform').submit(function (e) {
        var formData = new FormData(this);
        $.ajax({
            method: "POST",
            url: "{{ route('rolepermission.store',$roles->id) }}",
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                if (response == 'ok') {
                    alert('Permission Telah Ditambahkan', 'success')
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
        e.preventDefault();
    });

</script>
@endpush
