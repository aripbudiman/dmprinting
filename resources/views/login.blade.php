<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="bg-gray-50">
    <x-auth-session-status class="mb-4" :status="session('status')" />
    <div class="h-screen w-full grid grid-cols-2">
        <div id="left-container" class="bg-indigo-800 flex justify-center items-center dark:bg-slate-900">
            <div class="flex justify-center items-center flex-col">
                <img src="{{ asset('image/printing-machine.png') }}" alt="printer" width="200">
                <h1 class="text-7xl font-semibold text-white">
                    AdPrint Solutions
                </h1>
            </div>
        </div>
        <div id="right-container" class="flex justify-center items-center dark:bg-slate-900"
            style="background-image: url({{ asset('image/dot-grid.jpg') }})">
            <form class="max-w-full" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="relative mb-2">
                    <h1 class="text-xl typewriter text-indigo-700 dark:text-gray-300 font-semibold">Login</h1>
                </div>
                <div class="relative">
                    <input type="text"
                        class="peer py-3 pe-0 ps-8 block w-full bg-transparent border-t-transparent border-b-2 border-x-transparent border-b-gray-200 text-sm focus:border-t-transparent focus:border-x-transparent focus:border-b-blue-500 focus:ring-0 disabled:opacity-50 disabled:pointer-events-none dark:border-b-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 dark:focus:border-b-gray-600"
                        name="name" placeholder="Enter name" value="{{ old('name') }}">
                    <div
                        class="absolute inset-y-0 start-0 flex items-center pointer-events-none ps-2 peer-disabled:opacity-50 peer-disabled:pointer-events-none">
                        <svg class="flex-shrink-0 size-4 text-gray-500" xmlns="http://www.w3.org/2000/svg"
                            width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <path d="M19 21v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2"></path>
                            <circle cx="12" cy="7" r="4"></circle>
                        </svg>
                    </div>
                    <x-input-error :messages="$errors->get('name')" class="mt-2" />
                </div>
                <div class="relative">
                    <input type="password" name="password"
                        class="peer py-3 pe-0 ps-8 block w-full bg-transparent border-t-transparent border-b-2 border-x-transparent border-b-gray-200 text-sm focus:border-t-transparent focus:border-x-transparent focus:border-b-blue-500 focus:ring-0 disabled:opacity-50 disabled:pointer-events-none dark:border-b-gray-700 dark:text-gray-400 dark:focus:ring-gray-600 dark:focus:border-b-gray-600"
                        placeholder="Enter password">
                    <div
                        class="absolute inset-y-0 start-0 flex items-center pointer-events-none ps-2 peer-disabled:opacity-50 peer-disabled:pointer-events-none">
                        <svg class="flex-shrink-0 size-4 text-gray-500" xmlns="http://www.w3.org/2000/svg"
                            width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
                            stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <path d="M2 18v3c0 .6.4 1 1 1h4v-3h3v-3h2l1.4-1.4a6.5 6.5 0 1 0-4-4Z"></path>
                            <circle cx="16.5" cy="7.5" r=".5"></circle>
                        </svg>
                    </div>
                    <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>
                <div class="relative mt-5">
                    <button type="submit" class="w-full block btn btn-indigo text-center">Login</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
