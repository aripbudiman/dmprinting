<div id="modal-create-customer" tabindex="-1" aria-hidden="true"
    class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 w-full max-w-lg max-h-full">
        <!-- Modal content -->
        <form id="form-customer" method="POST" class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            @csrf
            <div id="methodField"></div>
            <!-- Modal header -->
            <div
                class="flex items-center justify-between bg-indigo-500 p-4 md:py-3 md:px-5 border-b rounded-t dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-100 dark:text-white flex items-center space-x-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1.7em" height="1.7em" viewBox="0 0 32 32">
                        <path fill="currentColor"
                            d="M16 3C8.832 3 3 8.832 3 16s5.832 13 13 13s13-5.832 13-13S23.168 3 16 3m0 2c6.087 0 11 4.913 11 11s-4.913 11-11 11S5 22.087 5 16S9.913 5 16 5m-1 5v5h-5v2h5v5h2v-5h5v-2h-5v-5z" />
                    </svg> TAMBAH CUSTOMER
                </h3>
                <button id="close-modal" onclick="_close()" type="button"
                    class="text-gray-100 bg-transparent hover:bg-gray-200 hover:text-indigo-700 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                        viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
                    </svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-4 md:p-5 space-y-8">
                <div class="mb-6">
                    <label for="nama" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama
                        Customer</label>
                    <input type="text" id="nama" name="nama"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="nama" required />
                </div>
                <div class="mb-6">
                    <label for="no_hp" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">No
                        Hp</label>
                    <input type="text" id="no_hp" name="no_hp"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                        placeholder="08xxxxxxx" required />
                </div>
                <div class="mb-6">
                    <label for="member" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Status
                        Membership</label>
                    <select id="member" name="member"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option value="member">Member</option>
                        <option value="non member">Non Member</option>
                    </select>
                </div>
            </div>
            <!-- Modal footer -->
            <div
                class="flex items-center space-x-3 p-4 md:py-3 md:px-5 border-t border-gray-200 rounded-b dark:border-gray-600">
                <button type="submit" class="btn btn-indigo w-full max-w-lg">Simpan</button>
                <button type="button" class="btn btn-rose w-full max-w-lg" onclick="_close()">Batal</button>
            </div>
        </form>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">
        $('#form-customer').submit(function(e) {
            e.preventDefault();
            const data = new FormData(this);
            var actionUrl = $(this).attr('action');
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    $('input').val('');
                    modal.hide();
                    if (response.status == 'success') {
                        alert(response.message, 'success', 1000)
                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);
                    }
                },
                error: function(xhr, status, error) {
                    console.log(error)
                }
            });
        });
    </script>
@endpush
