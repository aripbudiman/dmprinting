@extends('layouts.main')
@section('konten')
    <x-table :data="$data" :columns="config('table_config.customer')" :tr="'show'" :button="'edit'">
        <x-slot name="header">
            <x-header-table :title="$title">
                <div class="space-x-2">
                    <button onclick="_tambah()" class="btn btn-md rn btn-indigo">
                        Tambah Customer
                    </button>
                </div>
            </x-header-table>
        </x-slot>
        <x-slot name="filter">
            <x-show-data :show="$payload['show']" />
            <x-search :keyword="$payload['keyword']" />
        </x-slot>
    </x-table>
    @include('customer.modal-create')
@endsection
@push('scripts')
    <script>
        let modal;
        const $targetEl = document.getElementById('modal-create-customer');
        const options = {
            placement: 'top-right',
            backdrop: 'static',
            backdropClasses: 'bg-gray-900/40 dark:bg-gray-900/80 fixed inset-0 z-40',
            closable: true,
            onHide: () => {
                console.log('modal is hidden');
            },
            onShow: () => {
                console.log('modal is shown');
            },
            onToggle: () => {
                console.log('modal has been toggled');
            },
        };

        function _tambah() {
            if (!modal) {
                modal = new Modal($targetEl, options);
            }
            $('#form-customer').attr('action', '{{ route('customer.store') }}');
            modal.show();
        }

        function _close() {
            $('input').val('');
            $('#methodField').html('');
            modal.hide();
        }

        function _edit(id, el) {
            $.ajax({
                type: "GET",
                url: `{{ route('customer.edit', ':id') }}`.replace(':id', id),
                processData: false,
                contentType: false,
                success: function(response) {
                    $('#form-customer #nama').val(response.nama);
                    $('#form-customer #no_hp').val(response.no_hp);
                    $('#form-customer #member').val(response.member);
                }
            });
            $('#methodField').html('@method('PUT')');
            $('#form-customer').attr('action', '{{ route('customer.update', ':id') }}'.replace(':id', id));
            if (!modal) {
                modal = new Modal($targetEl, options);
            }
            modal.show();
        }
    </script>
@endpush
