@extends('layouts.main')
@section('konten')
    <div class="container shadow-md border border-gray-300 rounded-md">
        <div class="px-5 py-2 text-gray-700 text-xl bg-gray-200 flex justify-between">
            <h2>Pesanan Manual</h2>
            <a href="{{ route('pesanan_manual.create') }}" class="btn btn-md btn-indigo">Buat Pesanan Manual</a>
        </div>
        <div class="bg-white p-5">
            <div class="flex justify-between items-center pb-5">
                <x-show-data :show="$payload['show']" />
                <x-search :keyword="$payload['keyword']" />
            </div>
            <div class="relative overflow-x-auto">
                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 border">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-6 py-3">
                                No
                            </th>
                            <th scope="col" class="px-6 py-3">
                                No Pesanan
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Customer
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Tanggal
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Total Harga
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Status Pembayaran
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $key=> $item)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 cursor-pointer hover:bg-gray-100"
                                ondblclick="edit({{ $item->id }})">
                                <th scope="row"
                                    class="px-6 py-2.5 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{ $data->firstItem() + $key }}
                                </th>
                                <td class="px-6 py-2.5">
                                    {{ $item->no_pesanan }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ $item->customer->nama }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ formatDate($item->tanggal) }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ rupiah($item->total_harga) }}
                                </td>
                                <td class="px-6 py-2.5">
                                    {{ $item->status_pembayaran ?? 'Belum Bayar' }}
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        function edit(id) {
            window.location.href = "{{ route('pesanan_manual.edit', ':id') }}".replace(':id', id);
        }
    </script>
@endpush
