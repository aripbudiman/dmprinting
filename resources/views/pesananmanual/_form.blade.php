    <div class="max-w-7xl shadow border border-gray-300 rounded-md h-full">
        <div class="px-5 py-3 text-gray-700 text-xl bg-gray-200 flex justify-between">
            <h2>Pesanan Manual Detail</h2>
        </div>
        <hr class="border-t border-gray-300">
        <form id="form-pesanan-manual" class="relative overflow-x-auto sm:rounded-b-lg px-5 pt-5 bg-white">
            @csrf
            @if (isset($pesanan))
                @method('PUT')
            @endif
            <div class="max-w-md flex space-x-3">
                <div class="w-1/2">
                    <label for="customer" class="text-gray-700">Customer</label>
                    <input type="text" id="customer" class="form-input" value="{{ $pesanan->customer->nama ?? '' }}"
                        placeholder="Search..." />
                    <input type="text" name="customer_id" id="customer_id" value="{{ $pesanan->customer_id ?? '' }}"
                        class="form-input hidden" placeholder="Search..." />
                </div>
                <div class="w-1/2">
                    <label for="tanggal" class="text-gray-700">Tanggal</label>
                    <input type="date" name="tanggal" id="tanggal" class="form-input"
                        value="{{ $pesanan->tanggal ?? '' }}" placeholder="Search..." />
                </div>
            </div>
            <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 mt-3 border">
                <thead class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-2 py-3" align="center">
                            No
                        </th>
                        <th scope="col" class="px-2 py-3">
                            Nama Pesanan
                        </th>
                        <th scope="col" class="px-2 py-3" width="130">
                            Qty
                        </th>
                        <th scope="col" class="px-2 py-3" width="250">
                            Harga Satuan
                        </th>
                        <th scope="col" class="px-2 py-3" width="250">
                            Total Harga
                        </th>
                        <th scope="col" class="px-2 py-3" width="80">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody id="list-row">
                    @if (isset($pesanan))
                        @foreach ($pesanan->pesananmanualdetail as $key => $item)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <th align="center" scope="row" width="70"
                                    class="px-2 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{ $key + 1 }}
                                </th>
                                <td class="px-2 py-4">
                                    <input type="text" class="form-input" name="nama_cetakan[]"
                                        value="{{ $item->nama_cetakan }}">
                                </td>
                                <td class="px-2 py-4">
                                    <input type="text" class="form-input qty" name="qty[]"
                                        value="{{ $item->qty }}">
                                </td>
                                <td class="px-2 py-4">
                                    <input type="text" class="form-input harga_satuan" name="harga_satuan[]"
                                        value="{{ rupiah($item->harga_satuan) }}">
                                </td>
                                <td class="px-2 py-4">
                                    <input type="text" class="form-input total_harga" name="total_harga[]"
                                        value="{{ rupiah($item->total_harga) }}">
                                </td>
                                <td class="px-2 py-4">
                                    <button
                                        class="font-medium text-red-600 dark:text-red-500 hover:underline delete-row">Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <th align="center" scope="row" width="70"
                                class="px-2 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                1
                            </th>
                            <td class="px-2 py-4">
                                <input type="text" class="form-input" name="nama_cetakan[]">
                            </td>
                            <td class="px-2 py-4">
                                <input type="text" class="form-input qty" name="qty[]">
                            </td>
                            <td class="px-2 py-4">
                                <input type="text" class="form-input harga_satuan" name="harga_satuan[]">
                            </td>
                            <td class="px-2 py-4">
                                <input type="text" class="form-input total_harga" name="total_harga[]">
                            </td>
                            <td class="px-2 py-4">
                                <button
                                    class="font-medium text-red-600 dark:text-red-500 hover:underline delete-row">Delete</button>
                            </td>
                        </tr>
                    @endif
                </tbody>
                <tfoot class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <td class="px-2 py-2" colspan="3">
                            <button type="button" id="add-row" class="font-semibold btn btn-md btn-indigo">+
                                Tambah
                                Baris</button>
                        </td>
                        <td class="px-2 py-2">
                            <label for="grand_total"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Grand Total
                            </label>
                        </td>
                        <td class="px-2 py-2">
                            <input type="text" class="form-input grand_total" name="grand_total"
                                value="{{ isset($pesanan) ? rupiah($pesanan->total_harga) : '' }}" readonly>
                        </td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
            <hr class="border-t border-gray-300 mt-3 -ml-5 -mr-5">
            <div class="flex justify-center py-3 space-x-2">
                <button type="submit" class="btn btn-indigo">Simpan</button>
                <a href="{{ route('pesanan_manual.index') }}" class="btn btn-rose">Kembali</a>
            </div>
        </form>
    </div>
    @push('scripts')
        <script type="text/javascript">
            $('#customer').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "{{ Request::url() }}",
                        dataType: "json",
                        data: {
                            'autocomplete': 1,
                            'search': request.term
                        },
                        success: function(data) {
                            response(data.data);
                        }
                    });
                },
                select: function(event, ui) {
                    $('#customer').val(ui.item.label);
                    $('#customer_id').val(ui.item.value);
                    return false;
                }
            })
            $(document).ready(function() {
                function updateTotalHarga(row) {
                    const qty = parseFloat(row.find('.qty').val()) || 0;
                    const hargaSatuan = parseFloat(row.find('.harga_satuan').val().parseRupiah()) || 0;
                    const totalHarga = qty * hargaSatuan;
                    row.find('.total_harga').val(totalHarga.rupiah(0));
                    updateGrandTotal();
                }

                let rowCount = 1
                $('#add-row').on('click', function() {
                    rowCount++
                    let html = `<tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th align="center" scope="row" width="70"
                            class="px-2 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            ${rowCount}
                        </th>
                        <td class="px-2 py-4">
                            <input type="text" class="form-input" name="nama_cetakan[]">
                        </td>
                        <td class="px-2 py-4">
                            <input type="text" class="form-input qty" name="qty[]">
                        </td>
                        <td class="px-2 py-4">
                            <input type="text" class="form-input harga_satuan" name="harga_satuan[]">
                        </td>
                        <td class="px-2 py-4">
                            <input type="text" class="form-input total_harga" name="total_harga[]">
                        </td>
                        <td class="px-2 py-4">
                            <button class="font-medium text-red-600 dark:text-red-500 hover:underline delete-row">Delete</button>
                        </td>
                    </tr>`
                    $('#list-row').append(html);
                    updateRowNumbers();
                })

                $(document).on('input', '.harga_satuan', function() {
                    let angka = this.value.formatRupiah();
                    $(this).val(angka);
                });

                $(document).on('input', '.qty, .harga_satuan', function() {
                    const row = $(this).closest('tr');
                    updateTotalHarga(row);
                    updateGrandTotal();
                });

                $(document).on('click', '.delete-row', function() {
                    $(this).closest('tr').remove();
                    updateRowNumbers();
                    updateGrandTotal();
                });


                function updateRowNumbers() {
                    $('#list-row tr').each(function(index) {
                        $(this).find('th').text(index + 1);
                    });
                    rowCount = $('#list-row tr').length;
                }

                function updateGrandTotal() {
                    let grandTotal = 0;
                    $('.total_harga').each(function() {
                        let angka = $(this).val().parseRupiah() || 0;
                        grandTotal += parseFloat(angka);
                    });

                    $('.grand_total').val(grandTotal.rupiah(0));
                }

            });

            $('#form-pesanan-manual').submit(function(e) {
                e.preventDefault();
                const form = new FormData(this);
                $.ajax({
                    type: "POST",
                    url: "{{ $url }}",
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.status == 'success') {
                            alert(response.message, 'success', 1500);
                            setTimeout(() => {
                                if (response.redirect == 'edit') {
                                    window.location.reload();
                                } else {
                                    window.location.href = response.redirect
                                }
                            }, 1500);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.log(error)
                    }
                });
            });
        </script>
    @endpush
