import './bootstrap';
// import 'flowbite';
// import 'preline'
import axios from 'axios';
import Alpine from 'alpinejs';
import { Modal, Drawer } from 'flowbite';
import moment from "moment";

window.Alpine = Alpine;
window.Modal = Modal;
window.axios = axios;
window.Drawer = Drawer;
window.moment = moment;
Alpine.start();

